<?php
//BANCO DE DADOS
define('DB_HOST','localhost:3306');					//HOST DO BANCO
define('DB_DATABASE','meu_comercio');				//HOST DO BANCO
define('DB_USER','root');							//HOST DO BANCO
define('DB_PASSWORD','1');							//HOST DO BANCO

//CAMINHOS
define('INITIAL_PARAM', 0);							//O Parametro inicial do qual comeca a pegar depois do HOST. EX: http://localhost/site_produtos/. HOST=http://localhost/. PARAM[0] = site_produtos/

define('HOST','http://localhost/Meu_Comercio2.0/v2/');	//URL PRINCIPAL DO SISTEMA
define('HOME','');									//URL DA PAGINA INICIAL

$path = dirname(__FILE__);//realpath("");
//$path = str_replace( "\\", "/", $path);
define('ROOT',$path);								//CAMINHO DA PASTA FISICA INICIAL

define("TERCEIROS", 'library/');					//CAMINHO DE BIBLIOTECAS DE TERCEIROS
define("FRAMEWORK", '../../framework_k13_v1.0/');	//CAMINHO DO FRAMEWORK

//CONFIGURAÇÃO DO SMARTY
define("SMARTY_DIR", TERCEIROS."Smarty/");			//CAMINHO DO SMARTY
define("TEMPLATES", "view/");						//CAMINHO DOS TEMPLATES DO SMARTY
define("SYSTEM_TEMPLATE", "template1");		//NOME DO TEMPLATE VISUAL DO SITE/SISTEMA - ENCONTRADO NA PASTA view/templates

define("TMP", TERCEIROS."tmp/");					//CAMINHO DOS TEMPLATES GERADOS
define("TMP_CACHE", TERCEIROS."cache/");			//CAMINHO DO CACHE DOS TEMPLATES GERADOS

//DEBUG
$sysDebug = (isset($_REQUEST['sysDebug'])?$_REQUEST['sysDebug']:0);
define('DEBUG', $sysDebug);									//DEBUG do sistema

