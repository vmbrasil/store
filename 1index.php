<?php
header('Content-Type: text/html; charset=iso-8859-1');//utf-8

session_start();

//Configura��es
require_once ('config.php');

//Auto load-----------------------------------------------------------------------
require_once (FRAMEWORK.'class/controller/K13FriendlyUrl.class.php');


/** Autoload
	* Carrega as classes automaticam
 * */
function __autoload($className){
	FrontControllerFactory::autoload($className);
}//Fim autoload


/**
 * Classe responsavel por controlar a ponta do sistema
 * Fabrica as classes controladoras necessarias
 * Age acordo com os parametros da pagina
 * */
class FrontControllerFactory{
	public static $param = array('','','');	//array de parametros da pagina
	public static $include_path = array();	//array dos caminhos para includes das classes autoload
	
	/** construtor */
	public function __construct(){
		$this->init_autoload();
		$this->get_params();
		$this->configure();
		$this->fabricar();
	}
	
	/** 
	 * Carrega as configura��es do sistema
	 * */
	protected function configure(){
		//Escolhe Mostrar Erros do Sistema
		if(DEBUG){
			error_reporting(E_ALL);ini_set('display_errors',true);
		}
		else{
			error_reporting(NULL);ini_set('display_errors',false);
		}
		
		//Carrega as configura��es do sistema e conexao com o banco
		Config::get_instance()->configure();
//			K13Path::setSubDir('../');
			K13Path::setDirFramework(FRAMEWORK);
			
		Config::set_debug(DEBUG);
	}
	
	/** 
	 * Carrega os parametros da url amigavel da pagina
	 * */
	protected function get_params(){
		/*
		$gets = explode("/", str_replace(strrchr($_SERVER["REQUEST_URI"], "?"), "", $_SERVER["REQUEST_URI"]));
		array_shift($gets);#:: Elimita o 1� tem da array que � sempre vazio
		self::$param = $gets;
		*/
		K13FriendlyUrl::process_Url(2);//1-local,0-servidor
	}
	
	/**
	* Fabrica as instancias controladoras
	* D� include nos arquivos.
	* 
	* Regra: 1-Pagina inicial: 'modules/system/controller/inicio.php'
	* 2-Pagina menu subpastas. Verifica se a pasta existe,exibe lista.
	* 3-Arquivo controlador. Verifica se o arquivo existe.
	*/
	protected function fabricar(){
		/* ESTRUTURA DOS PARAMETROS
		 * 0 - O modulo desejado
		 * 1 - A pagina do modulo
		 * 1.1 - Caso a pagina nao seja selecionada:
		 * 1.1.1 - se existir pagina padrao
		 * 1.1.2 - senao o menu padrao sera aberto
		 * */
		$module_controller = K13FriendlyUrl::get_url_param(0);
		switch($module_controller){
//			case 'modulo_especifico':
//				new ModuloFactory();
//			break;
			//N�o foi definido o modulo ou Chamou o inicial
			default:
			{
				//Pagina Inicial do Sistema------------
				if( empty($module_controller) || $module_controller == 'index.php'){
					include 'modules/publico/controller/inicio.php';
				}
				//Pagina Especificada pela URL------------
				else {
					$url_controller = K13FriendlyUrl::get_url_param(1);
					//Menu Controlador - pagina n�o informada
					if( (empty($url_controller) && is_dir('modules/' .$module_controller) ) || ($url_controller == 'index.php' && is_dir('modules/' .$module_controller))){
						echo 'Menu Diretorio. implementar. index 241';
					}
					//Pagina Informada
					else{
						$arquivo = 'modules/' .$module_controller .'/controller/' .$url_controller .'.php';
						if(file_exists($arquivo)){
							include ($arquivo);
						}
						//Aceita primeiro parametro como uma pagina de system
						elseif(file_exists('modules/system/controller/' .$module_controller .'.php')){
							include ('modules/system/controller/' .$module_controller .'.php');
						}
						//Aceita primeiro parametro como uma pagina de public
						elseif(file_exists('modules/publico/controller/' .$module_controller .'.php')){
							include ('modules/publico/controller/' .$module_controller .'.php');
						}
						//Pagina N�o encontrada
						else {
							include ('/modules/system/controller/404.php');	
						}
					}
				}//fim pag especifica

			}//fim default
		}//fim switch modules
		
	}//fim func fabricar

	/** 
	 * Inicializa o autoload
	 * */
	protected function init_autoload(){
	//-------Carrega o include Patch---------
	self::$include_path = array();
		//2-Home. Depois as 3 pastas: controller,model,view
		self::$include_path[] = 'controller';
		self::$include_path[] = 'model';
		self::$include_path[] = 'class';
		self::$include_path[] = 'config';
		self::$include_path[] = 'view';
		
		//3-Modulo Atual. Depois as 3 pastas: controller,model,view
		/*
		$gets = explode("/", str_replace(strrchr($_SERVER["REQUEST_URI"], "?"), "", $_SERVER["REQUEST_URI"]));
		array_shift($gets);#:: Elimita o 1� tem da array que � sempre vazio
		self::$param = $gets;
		*/
		$page_param = K13FriendlyUrl::get_url_param(0);
		if(!empty($page_param)){
			self::$include_path[] = 'modules/'.$page_param.'/model';
			self::$include_path[] = 'modules/'.$page_param.'/controller';
			self::$include_path[] = 'modules/'.$page_param.'/view';
		}
		
		//4-Modulo System. Depois as 3 pastas: controller,model,view
		self::$include_path[] = 'modules/system/model';
		self::$include_path[] = 'modules/system/controller';
		self::$include_path[] = 'modules/system/view';
		
		//5-Modulo publico. Depois as 3 pastas: controller,model,view
		self::$include_path[] = 'modules/publico/model';
		self::$include_path[] = 'modules/publico/controller';
		self::$include_path[] = 'modules/publico/view';
		
		//6-Todos modulos. Propria pasta. Depois as 3 pastas: controller,model,view
		foreach(new DirectoryIterator(ROOT.'/modules') as $file){
			if ($file->isDir() && $file->getFilename() != "." && $file->getFilename() != "..") {//retira-se os dir . e ..
				self::$include_path[] = 'modules/'.$file->getFilename().'/model';
				self::$include_path[] = 'modules/'.$file->getFilename().'/controller';
				self::$include_path[] = 'modules/'.$file->getFilename();//Propria pasta
				self::$include_path[] = 'modules/'.$file->getFilename().'/view';
			}
		}
		
		
		//7-Framework
		self::$include_path[] = FRAMEWORK.'class';
		self::$include_path[] = FRAMEWORK.'libs';
		self::$include_path[] = FRAMEWORK.'configs';
		self::$include_path[] = FRAMEWORK.'entidade';
		
		//8-Todos modulos do framework. Propria pasta. Depois as 3 pastas: controller,model,view
		foreach(new DirectoryIterator(FRAMEWORK.'/modules') as $file){
			if ($file->isDir() && $file->getFilename() != "." && $file->getFilename() != "..") {//retira-se os dir . e ..
				self::$include_path[] = FRAMEWORK.'modules/'.$file->getFilename().'/controller';
				self::$include_path[] = FRAMEWORK.'modules/'.$file->getFilename().'/model';
				self::$include_path[] = FRAMEWORK.'modules/'.$file->getFilename();//Propria pasta
				self::$include_path[] = FRAMEWORK.'modules/'.$file->getFilename().'/view';
			}
		}
		
		//9-Libs & Smarty
		self::$include_path[] = TERCEIROS;

		self::$include_path[] = SMARTY_DIR;
		
		if(DEBUG) var_dump(self::$include_path);
	}
	
	/** Autoload
		 *  Prioridade:
		 * 1-Mesma Pasta. Depois as 3 pastas: controller,model,view
		 * 2-Home. Depois as 3 pastas: controller,model,view
		 * 3-Modulo Atual. Depois as 3 pastas: controller,model,view
		 * 4-Modulo System. Depois as 3 pastas: controller,model,view
		 * 5-Modulo publico. Depois as 3 pastas: controller,model,view
		 * 6-Todos modulos. Depois as 3 pastas: controller,model,view
		 * 7-Framework
		 * 8-Libs & Smarty
	 * */
	public static function autoload($className){
		
		/* ALGORITMO:
		 * Busca o arquivo php na mesma pasta
		 * Busca o arquivo nas pastas configuradas(include paths)
		 * Busca 3 tipos de pastas mvc: controller,model,view
		 * Busca 3 tipos de extensoes: .class.php,.php,.cls.php; nessa ordem de prioridade
		 * Busca at� 5 diretorios acima. '../'
		 * Por fim busca classes do Smarty se satisfazer as condi�oes
		 * */
		//1-Mesma Pasta. Depois as 3 pastas: controller,model,view
		if(file_exists($className . '.php')) include_once($className . '.php');
	
		self::$include_path[] = TERCEIROS."{$className}";
		self::$include_path[] = SMARTY_DIR."{$className}";
		
		//Processa----------------------
		foreach(self::$include_path as $prefix){//include paths
			$path[0] = $prefix . '/' . $className . '.class.php';
			$path[1] = $prefix . '/' . $className . '.php';
			$path[2] = $prefix . '/' . $className . '.cls.php';
	
			foreach($path as $thisPath){//cada sufix
				$subdirs = '';
	//			echo $thisPath .'<br>';//Debug
	
				if(file_exists($thisPath)){
					require_once($thisPath);
					if(DEBUG) var_dump($thisPath);
					return $thisPath;
				}
	
				for($i=0; $i<5; $i++){//5 subdirs
					$thisPath = $subdirs .$thisPath;
					$subdirs = '../';
					if(file_exists($thisPath)){
						require_once($thisPath);
						if(DEBUG) var_dump($thisPath);
						return $thisPath;
					}
				}//subdirs
			}//sufix
		}//paths
	
		//Smarty Template-------------------
		$_class = strtolower($className);
		if (substr($_class, 0, 16) === 'smarty_internal_' || $_class == 'smarty_security') {
			$thisPath = SMARTY_SYSPLUGINS_DIR .$_class .'.php';
			$thisPath = str_replace('\\', '/', $thisPath);
			if(file_exists($thisPath)){
				include ($thisPath);
				return $thisPath;
			}
		}
	
		//N�o carregou
		echo 'N�o foi possivel carregar a classe ' .$className ."<br /><pre> \n";
		debug_print_backtrace();
		echo '</pre>';
	}//Fim autoload

}//fim class

new FrontControllerFactory();
?>