<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 17:03:59
         compiled from "../framework/modules/view/K13ViewCadastroDetalhe.tpl" */ ?>
<?php /*%%SmartyHeaderCode:98165568c62f796691-60536895%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7dbe1d811df4c9875c4578a62768f849141af8bb' => 
    array (
      0 => '../framework/modules/view/K13ViewCadastroDetalhe.tpl',
      1 => 1362757642,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '98165568c62f796691-60536895',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<div class="page_box page-cadastro-detalhe">
			<div class="form_wrapper">
			
				<div class="form_box_detalhe" <?php if ($_smarty_tpl->getVariable('show_form')->value==false){?>style="display: none;"<?php }?>>
					<form action="<?php echo $_smarty_tpl->getVariable('form_file_action')->value;?>
" method="post" class="form_detalhe" id="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" name="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
">
					<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/msg_system.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
					<?php if (count($_smarty_tpl->getVariable('tabs')->value)>0){?>
						<div id="tabs">
							<ul>
								<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('tabs')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value){
?>
								<li><a href="#tab_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['tab']->value['label'];?>
</a></li>
								<?php }} ?>
							</ul>
							
							<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('tabs')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value){
?>
							<div id="tab_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
">
								<?php if ($_smarty_tpl->tpl_vars['tab']->value['url']==''){?>
									<?php echo $_smarty_tpl->tpl_vars['tab']->value['form'];?>

								<?php }else{ ?>
									<iframe name="frame_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" id="frame_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" src="<?php echo $_smarty_tpl->tpl_vars['tab']->value['url'];?>
" border="0" width="100%" onload="set_config_tab_iframe('#frame_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
');"></iframe>
								<?php }?>
							</div>
							<?php }} ?>
						</div>
					<?php }else{ ?>
						<?php echo $_smarty_tpl->getVariable('cad_form')->value;?>

					<?php }?>
						<input type="hidden" name="action" value="<?php echo $_smarty_tpl->getVariable('cad_action')->value;?>
" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" />
						<input type="hidden" name="form" value="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" />
						<input type="hidden" name="campo_mestre" value='<?php echo $_smarty_tpl->getVariable('campo_mestre')->value;?>
' form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" />
						
						<?php if ($_smarty_tpl->getVariable('show_action_box')->value==true&&$_smarty_tpl->getVariable('erro_permissao')->value!=true){?>
						<div class="tool_box">
							<button type="button" value="Inserir" title="Inserir Registro" class="bt_submit bt_submit_detalhe" role="button" aria-disabled="false" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
"><span class="cad-button-icon-inserir"></span><span class="cad-button-text">Inserir</span></button>
							<div class="clear"></div>
						</div>
						<?php }?>
					</form>
				</div>
			</div><!-- Fim form_wrapper -->
			
			<?php if ($_smarty_tpl->getVariable('erro_permissao')->value!=true){?>
			<!-- Grid -->
			<div class="grid_detalhe">
				<fieldset rel="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
">
					<legend class="ui-widget-header sub_titulo"><?php echo $_smarty_tpl->getVariable('detalhe_grid_titulo')->value;?>
</legend>
					<button id="up_grid_<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" data-grid-form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" class="bt_update_grid_detalhe" title="Atualizar Lista">Atualizar Lista</button>
					<?php echo $_smarty_tpl->getVariable('detalhe_grid')->value;?>

				</fieldset>
			</div>
			<?php }?>
	</div><!-- Fim page_box -->

<?php echo $_smarty_tpl->getVariable('htmlBody')->value;?>
