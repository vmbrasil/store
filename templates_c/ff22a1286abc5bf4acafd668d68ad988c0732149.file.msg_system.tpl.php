<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 17:03:59
         compiled from "../framework/modules/view/msg_system.tpl" */ ?>
<?php /*%%SmartyHeaderCode:214255568c62f9eea57-20019160%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ff22a1286abc5bf4acafd668d68ad988c0732149' => 
    array (
      0 => '../framework/modules/view/msg_system.tpl',
      1 => 1353011993,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '214255568c62f9eea57-20019160',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
				<div class="message_box" rel="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
">
					<div class="msg_erro" <?php if (count($_smarty_tpl->getVariable('form_errorMessage')->value)<=0){?>style="display: none"<?php }?>>
						<h2>
							ATENÇÃO!
							<button type="button" title="Fechar" class="bt-fechar">X</button>
						</h2>
						<ul>
						<?php  $_smarty_tpl->tpl_vars['errmsg'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('form_errorMessage')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['errmsg']->key => $_smarty_tpl->tpl_vars['errmsg']->value){
?>
							<li><?php echo $_smarty_tpl->tpl_vars['errmsg']->value;?>
</li>
						<?php }} ?>
						</ul>
					</div>
					<div class="msg_sucesso" <?php if ($_smarty_tpl->getVariable('form_sucessMessage')->value==''){?>style="display: none"<?php }?>>
						<h2>
							SUCESSO!
							<button type="button" title="Fechar" class="bt-fechar">X</button>
						</h2>
						<div class="texto">
							<p><?php echo $_smarty_tpl->getVariable('form_sucessMessage')->value;?>
</p>
						</div>
					</div>
				</div>
