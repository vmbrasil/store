<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 17:03:54
         compiled from "../framework/modules/view/K13HtmlFormInput.tpl" */ ?>
<?php /*%%SmartyHeaderCode:161965568c62ad9a0c0-48870306%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f83d083f571b45574ae7fd80e11a14370753dd5' => 
    array (
      0 => '../framework/modules/view/K13HtmlFormInput.tpl',
      1 => 1423245269,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '161965568c62ad9a0c0-48870306',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include '../framework/terceiros/smarty/plugins\function.html_options.php';
?>
<?php if ($_smarty_tpl->getVariable('input')->value['type']=='ttext'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
		<textarea name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?><?php if ($_smarty_tpl->getVariable('input')->value['rows']!=''){?> rows="<?php echo $_smarty_tpl->getVariable('input')->value['rows'];?>
"<?php }?><?php if ($_smarty_tpl->getVariable('input')->value['cols']!=''){?> cols="<?php echo $_smarty_tpl->getVariable('input')->value['cols'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?>><?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
</textarea>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='ttexteditor'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
		<textarea name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?><?php if ($_smarty_tpl->getVariable('input')->value['rows']!=''){?> rows="<?php echo $_smarty_tpl->getVariable('input')->value['rows'];?>
"<?php }?><?php if ($_smarty_tpl->getVariable('input')->value['cols']!=''){?> cols="<?php echo $_smarty_tpl->getVariable('input')->value['cols'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?>><?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
</textarea>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='thidden'){?>
	<input type="hidden" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?> form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tselect'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
	<?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>
		<!-- Select Representativo -->
		<select name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
_disabled_readonly" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
_disabled_readonly" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
_disabled_readonly" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly" disabled="disabled"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?>>
		 	<?php if ($_smarty_tpl->getVariable('input')->value['notnull']!=true){?><option value="">--Selecione--</option><?php }?>
			<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->getVariable('input')->value['options'],'selected'=>$_smarty_tpl->getVariable('input')->value['value']),$_smarty_tpl);?>

		</select>
		<!-- Select Real -->
		<select name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
;display: none;" <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?>>
		 	<?php if ($_smarty_tpl->getVariable('input')->value['notnull']!=true){?><option value="">--Selecione--</option><?php }?>
			<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->getVariable('input')->value['options'],'selected'=>$_smarty_tpl->getVariable('input')->value['value']),$_smarty_tpl);?>

		</select>
	<?php }else{ ?>
		<select name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?>>
		 	<?php if ($_smarty_tpl->getVariable('input')->value['notnull']!=true){?><option value="">--Selecione--</option><?php }?>
			<?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->getVariable('input')->value['options'],'selected'=>$_smarty_tpl->getVariable('input')->value['value']),$_smarty_tpl);?>

		</select>
	<?php }?>

		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tcheckbox'||($_smarty_tpl->getVariable('input')->value['type']=='tboolean')){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<input type="checkbox" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="true" <?php if ($_smarty_tpl->getVariable('input')->value['value']==true||$_smarty_tpl->getVariable('input')->value['value']==1){?>checked="checked"<?php }?> class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
<?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>display:none;<?php }?>" <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>
			<?php echo $_smarty_tpl->getVariable('input')->value['html'];?>

		<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tinteger'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<input type="number" step="1" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tdecimal'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<input type="text" step="0.01" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo number_format($_smarty_tpl->getVariable('input')->value['value'],((($tmp = @$_smarty_tpl->getVariable('input')->value['decimals'])===null||$tmp==='' ? "2" : $tmp)),",",".");?>
" data-decimals="<?php echo (($tmp = @$_smarty_tpl->getVariable('input')->value['decimals'])===null||$tmp==='' ? "2" : $tmp);?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength']+3;?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tpassword'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<input type="password" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_pass_force']){?>
		<ul class="pass_force_box" id="pass_force_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" title="Medidor de Seguranca da senha">
			<li class="percent"></li>
			<li class="nivel"></li>
			<li class="description"></li>
		</ul>
		<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_redigit_pass']){?>
		<!-- Redigitar password -->
		<span class="re_password_box">
			<label for="re_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="Redigite <?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
				Redigite <?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
			</label>
			<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
			<input type="password" id="re_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" name="redigit_password_<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" value="" class="re_password_input" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
			<span class="re_status" id="re_status_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
"></span>
			<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		</span>
		<?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='datepicker'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false){?><span class="ico_calendar" id="ico<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
"><?php }?>
			<input type="text" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false){?></span><?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='datetimepicker'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false){?><span class="ico_calendartime" id="ico<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
"><?php }?>
			<input type="text" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false){?></span><?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='autocomplete'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="AutoComplete_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<input type="text" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" class="AutoCompleteId" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> size="10" <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> readonly="readonly" <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> tabindex="-1" />
		<span id="AutoCompleteIcone_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="AutoCompleteIcone" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
"><?php echo $_smarty_tpl->getVariable('input')->value['autocomplete_icone'];?>
</span>
		<span class="autocomple_input_box" style="display:inline-block;" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" >
			<input type="text" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="AutoComplete_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
" name="AutoComplete_<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['autocomplete_label'];?>
" class="autocomplete ui-autocomplete-input ui-widget-content ui-corner-left<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" data-ac="<?php echo $_smarty_tpl->getVariable('input')->value['ac'];?>
" data-param="<?php echo $_smarty_tpl->getVariable('input')->value['param'];?>
" data-vinculos="<?php echo $_smarty_tpl->getVariable('input')->value['json_vinculos'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?><?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?>  <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
			<button type="button" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="AutoCompleteButton_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" title="Mostrar Itens" class="autoCompleteButton ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-right ui-button-icon" role="button" aria-disabled="false" tabindex="-1"><span class="ui-button-icon-primary ui-icon ui-icon-triangle-1-s"></span><span class="ui-button-text">&nbsp;</span></button>
		</span>
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tcolor'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 ui-widget <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="AutoComplete_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<input type="hidden" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="colorData_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> readonly="readonly" <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> tabindex="-1" />
		<div id="colorSelector_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> ><div style="background-color: <?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
"></div></div>
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tlist'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="input_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		
		<input type="hidden" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?> form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<div class="tlist_box ui-tabs ui-widget ui-widget-content ui-corner-all">
			<header>
				<input type="text" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="input_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['readonly']==true){?>display:none;<?php }?>" class="tlist_input <?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
  ui-widget-content ui-corner-left<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" title="Digite o texto a ser inserido na lista" <?php if ($_smarty_tpl->getVariable('input')->value['mask']!=''){?>data-mask="<?php echo $_smarty_tpl->getVariable('input')->value['mask'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?><?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
				<button type="button" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="listButtonAdd_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" title="Adicionar Item" class="listButtonAdd ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-all ui-button-icon" role="button" aria-disabled="false" <?php if ($_smarty_tpl->getVariable('input')->value['readonly']==true){?>style="display:none;"<?php }?>><span class="ui-button-icon-primary ui-icon ui-icon-plusthick"></span><span class="ui-button-text">&nbsp;</span></button>
			</header>
			<ol class="tlist_list" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
			</ol>
		</div>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tslug'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<input type="text" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['slug']!=''){?>data-slug="#<?php echo $_smarty_tpl->getVariable('input')->value['slug'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='timage'){?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 campo-image ui-widget <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="ImageExplorerButton_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<input type="text" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" style="display: none; <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
<?php }?>" <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> tabindex="-1" />
		<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false&&$_smarty_tpl->getVariable('input')->value['show_add_button']){?><button type="button" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="ImageExplorerButton_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" title="Selecionar imagem" class="image-explorer ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-left ui-corner-right ui-button-icon" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-image"></span><span class="ui-button-text">Selecionar imagem</span></button><?php }?>
		a implementar
		<ul class="file-container" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="listfiles_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<?php if (count($_smarty_tpl->getVariable('input')->value['itens_data']['itens'])>0&&$_smarty_tpl->getVariable('input')->value['itens_data']!=false){?>
			<?php  $_smarty_tpl->tpl_vars['file_item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('input')->value['itens_data']['itens']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['file_item']->key => $_smarty_tpl->tpl_vars['file_item']->value){
?>
			<li data-item-id="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['id'];?>
">
				<div class="box-file-viewer">
					<button type="button" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-item-id="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['id'];?>
" class="bt-file-item-remove" title="Remover Imagem">Remover</button>
					<a href="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['large'];?>
" class="link-file-ico">
						<span class="hover-effect"></span>
						<img src="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['thumb'];?>
" alt="Arquivo" width="89" heigth="89">
					</a>
				</div>
			</li>
			<?php }} ?>
		<?php }else{ ?>
			<li class="empty">
				<p>
				<strong>Vazio.</strong>
				<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false&&$_smarty_tpl->getVariable('input')->value['show_add_button']){?>
					<br>Para inserir imagem clique no botão ao lado "Selecionar imagem".
				<?php }?>
				</p>
			</li>
		<?php }?>
		</ul>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tlistimage'){?>
	<div class="linha_campo campo-list-images ui-widget <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="ImageListExplorerButton_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<textarea type="text" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="data" style="display: none; <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
<?php }?>" <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> tabindex="-1" /><?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
</textarea>
		<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false&&$_smarty_tpl->getVariable('input')->value['show_add_button']){?><button type="button" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="ImageListExplorerButton_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" title="Selecionar imagens" class="image-explorer-mult ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-left ui-corner-right ui-button-icon" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-image"></span><span class="ui-button-text">Selecionar imagens</span></button><?php }?>
		
		<ul class="list-files-slider" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="listfiles_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<?php if (count($_smarty_tpl->getVariable('input')->value['itens_data']['itens'])>0&&$_smarty_tpl->getVariable('input')->value['itens_data']!=false){?>
			<?php  $_smarty_tpl->tpl_vars['file_item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('input')->value['itens_data']['itens']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['file_item']->key => $_smarty_tpl->tpl_vars['file_item']->value){
?>
			<li data-item-id="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['id'];?>
">
				<div class="box-file-viewer">
					<input type="radio" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['id'];?>
" class="file-item-select" name="file_item_select_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" title="Selecionar como imagem principal">
					<button type="button" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-item-id="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['id'];?>
" class="bt-file-item-remove" title="Remover Imagem">Remover</button>
					<a href="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['large'];?>
" class="link-file-ico">
						<span class="hover-effect"></span>
						<img src="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['thumb'];?>
" alt="Arquivo" width="89" heigth="89">
					</a>
				</div>
			</li>
			<?php }} ?>
		<?php }else{ ?>
			<li class="empty">
				<p>
				<strong>Vazio.</strong>
				<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false&&$_smarty_tpl->getVariable('input')->value['show_add_button']){?>
					<br>Para inserir imagens clique no botão ao lado "Selecionar imagens".
				<?php }?>
				</p>
			</li>
		<?php }?>
		</ul>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

	
<?php }elseif($_smarty_tpl->getVariable('input')->value['type']=='tfile'){?>
	<div class="linha_campo campo-file <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 ui-widget <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="ImageExplorerButton_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<input type="text" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" style="display: none; <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
<?php }?>" <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="true"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> tabindex="-1" />
		<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false&&$_smarty_tpl->getVariable('input')->value['show_add_button']){?><button type="button" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="ImageExplorerButton_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" title="Selecionar Arquivo" class="file-explorer ui-button ui-widget ui-state-default ui-button-icon-only ui-corner-left ui-corner-right ui-button-icon" role="button" aria-disabled="false"><span class="ui-button-icon-primary ui-icon ui-icon-image"></span><span class="ui-button-text">Selecionar imagem</span></button><?php }?>
		a implementar
		<ul class="file-container" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" id="listfiles_<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<?php if (count($_smarty_tpl->getVariable('input')->value['itens_data']['itens'])>0&&$_smarty_tpl->getVariable('input')->value['itens_data']!=false){?>
			<?php  $_smarty_tpl->tpl_vars['file_item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('input')->value['itens_data']['itens']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['file_item']->key => $_smarty_tpl->tpl_vars['file_item']->value){
?>
			<li data-item-id="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['id'];?>
">
				<div class="box-file-viewer">
					<button type="button" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-item-id="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['id'];?>
" class="bt-file-item-remove" title="Remover Arquivo">Remover</button>
					<a href="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['large'];?>
" class="link-file-ico">
						<span class="hover-effect"></span>
						<img src="<?php echo $_smarty_tpl->tpl_vars['file_item']->value['thumb'];?>
" alt="Arquivo" width="89" heigth="89">
					</a>
				</div>
			</li>
			<?php }} ?>
		<?php }else{ ?>
			<li class="empty">
				<p>
				<strong>Vazio.</strong>
				<?php if ($_smarty_tpl->getVariable('input')->value['readonly']==false&&$_smarty_tpl->getVariable('input')->value['show_add_button']){?>
					<br>Para inserir um arquivo clique no botão ao lado "Selecionar arquivo".
				<?php }?>
				</p>
			</li>
		<?php }?>
		</ul>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }else{ ?>
	<div class="linha_campo <?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
 <?php if ($_smarty_tpl->getVariable('input')->value['group']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['group'];?>
<?php }?>" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
		<label for="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['label']['class'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" data-text="<?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
">
			<?php if ($_smarty_tpl->getVariable('input')->value['label']['label']!=''){?><?php echo $_smarty_tpl->getVariable('input')->value['label']['label'];?>
:<?php }?>
		</label>
		<?php if ($_smarty_tpl->getVariable('input')->value['show_tooltip']){?><?php echo $_smarty_tpl->getVariable('input')->value['tooltip'];?>
<?php }?>
		<?php if ($_smarty_tpl->getVariable('input')->value['prefix']!=''){?><span class="prefix"><?php echo $_smarty_tpl->getVariable('input')->value['prefix'];?>
</span><?php }?>
		<input type="text" name="<?php echo $_smarty_tpl->getVariable('input')->value['name'];?>
" id="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
" value="<?php echo $_smarty_tpl->getVariable('input')->value['value'];?>
" class="<?php echo $_smarty_tpl->getVariable('input')->value['type'];?>
<?php echo $_smarty_tpl->getVariable('input')->value['class'];?>
" title="<?php echo $_smarty_tpl->getVariable('input')->value['title'];?>
" <?php if ($_smarty_tpl->getVariable('input')->value['mask']!=''){?>data-mask="<?php echo $_smarty_tpl->getVariable('input')->value['mask'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['style']!=''){?>style="<?php echo $_smarty_tpl->getVariable('input')->value['style'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['size']!=''){?>size="<?php echo $_smarty_tpl->getVariable('input')->value['size'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['maxlength']!=''){?>maxlength="<?php echo $_smarty_tpl->getVariable('input')->value['maxlength'];?>
"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['readonly']!=''){?>readonly="readonly"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['notnull']==true){?>required="required"<?php }?> <?php if ($_smarty_tpl->getVariable('input')->value['form']!=''){?>form="<?php echo $_smarty_tpl->getVariable('input')->value['form'];?>
"<?php }?> />
		<?php if ($_smarty_tpl->getVariable('input')->value['sufix']!=''){?><span class="sufix"><?php echo $_smarty_tpl->getVariable('input')->value['sufix'];?>
</span><?php }?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13Html_buttons_input.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div>
	<?php echo $_smarty_tpl->getVariable('input')->value['letras_miudas'];?>

<?php }?>