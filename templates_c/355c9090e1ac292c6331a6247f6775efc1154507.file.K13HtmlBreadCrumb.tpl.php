<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 17:03:54
         compiled from "../framework/modules/view/K13HtmlBreadCrumb.tpl" */ ?>
<?php /*%%SmartyHeaderCode:199265568c62a761286-23578171%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '355c9090e1ac292c6331a6247f6775efc1154507' => 
    array (
      0 => '../framework/modules/view/K13HtmlBreadCrumb.tpl',
      1 => 1359032110,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '199265568c62a761286-23578171',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (count($_smarty_tpl->getVariable('breadcrumb')->value)>0&&$_smarty_tpl->getVariable('breadcrumb')->value!=false){?>
<div class="k13-view-component-breadcrumb" rel="<?php echo $_smarty_tpl->getVariable('breadcrumb')->value['id'];?>
">
	<a href="#" onclick="return false;" class="bt-label ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
		<span class="ui-icon"></span>
		<span class="ui-button-text">Você está AQUI:</span>
	</a>
	<div class="wrapper-hover">
		<ul>
			<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('breadcrumb')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
			<li id="bread_id_<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" class="item <?php echo $_smarty_tpl->tpl_vars['item']->value['class'];?>
">
				<a href="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['url'])===null||$tmp==='' ? '#' : $tmp);?>
" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['description'];?>
" role="button" aria-disabled="false">
					<span class="text"><span class="ui-icon"></span><?php echo $_smarty_tpl->tpl_vars['item']->value['label'];?>
</span>
				</a>
			</li>
			<?php }} ?>
		</ul>
	</div>
</div>
<?php }?>