<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 17:03:58
         compiled from "../framework/modules/view/K13Html_buttons_input.tpl" */ ?>
<?php /*%%SmartyHeaderCode:290265568c62e9180e8-14999469%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '03972cdf7c67a9c46f30050a5ed1b12d46efce9a' => 
    array (
      0 => '../framework/modules/view/K13Html_buttons_input.tpl',
      1 => 1379863959,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '290265568c62e9180e8-14999469',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<ul class="list_input_buttons" rel="<?php echo $_smarty_tpl->getVariable('input')->value['id'];?>
">
<?php if (count($_smarty_tpl->getVariable('input')->value['buttons'])>0&&$_smarty_tpl->getVariable('input')->value['buttons']!=false){?>
	<?php  $_smarty_tpl->tpl_vars['button'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('input')->value['buttons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['button']->key => $_smarty_tpl->tpl_vars['button']->value){
?>
	<li id="<?php echo $_smarty_tpl->tpl_vars['button']->value['id'];?>
" class="<?php echo $_smarty_tpl->tpl_vars['button']->value['class'];?>
" <?php if ($_smarty_tpl->tpl_vars['button']->value['rel']!=''){?>rel="<?php echo $_smarty_tpl->tpl_vars['button']->value['rel'];?>
"<?php }?>>
		<a href="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['button']->value['url'])===null||$tmp==='' ? '#' : $tmp);?>
" title="<?php echo $_smarty_tpl->tpl_vars['button']->value['title'];?>
" class="bt-campo ui-button ui-widget ui-state-default ui-corner-left ui-corner-right ui-button-icon" role="button" aria-disabled="false">
			<span class="ui-icon"><?php if ($_smarty_tpl->tpl_vars['button']->value['ico']!=''){?><img src="<?php echo $_smarty_tpl->tpl_vars['button']->value['ico'];?>
" width="32" height="32"><?php }?></span>
			<span class="ui-button-text"><?php echo $_smarty_tpl->tpl_vars['button']->value['label'];?>
</span>
		</a>
	</li>
	<?php }} ?>
<?php }?>
</ul>
