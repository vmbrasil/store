<?php
/**
 * Pagina view/controller do perfil de usuario
 * Esta � uma pagina publica pertencente ao sistema Meu Comercio
 * Quando o usuario comum precisar entrar em alguma pagina de seu perfil, � pedido para se logar aqui.
 * usuario/perfil/
 * */

//error_reporting(E_ALL);ini_set('display_errors',true);

class PageUserPerfil extends PageLoja{
	public function __construct(){//construtor
		parent::__construct();
	}
	
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->add_titulo(' - Identifica��o de Usu�rio');
		
		//Css e JS Padrao
		$this->addCssFileToHead(K13Path::getHost().'view/style/perfil.css');
		$this->addJsFileToHead(K13Path::getHost() .'view/script/perfil.js');
		
		//Template Especifico
		$this->set_propertie('templatePage','modules/usuario/view/perfil.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
		
		$this->event_action($this->get_param('action'));
	}
	
	/**
	 * Metodo para controle do usuario interceptar os dados pouco antes de eventAction
	 * @type: view/controller
	**/
	public function event_action($action = ''){
		if($action == 'logar'){
			if( K13UserENT::get_instance()->logar($this->get_post('login'),$this->get_post('senha')) ){
				
/* Foi comentado pois a relocacao � feita depois de escolher um grupo, se tornando obtigatorio selecionar um grupo antes de entrar no sistema.
				$relocation_url = K13SystemVar::get_location_url();
				K13SystemVar::set_location_url('');//para limpar a variavel
				if(empty($relocation_url)) $relocation_url = K13SystemVar::get_default_url_entrance();
*/
				$relocation_url = K13SystemVar::get_default_url_group_selection();
				
				
				echo K13DataFormat::custom_json_encode(array('sucesso'=> true,'mensagem'=>'seja bem vindo!','url'=> $relocation_url));
				exit;
			}
			else{
				echo K13DataFormat::custom_json_encode(array('sucesso'=> false,'mensagem'=>'well...hehehe, nao foi dessa vez!'));
				exit;
			}
		}
	}
	
	/**
	 * Carrega os dados padroes da pagina(header,topo, etc)
	**/
	protected function carregarDadosPadrao(){
		parent::carregarDadosPadrao();
		$this->carregar_BarraDiretorios();
	}
	
	/**
	 * Carrega a Barra de Diretorios
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
		
		$diretorios = array(
			array('label'	=>	'In�cio',	'url' => HOST)
			,array('label'	=>	'Usu�rio',	'url' => HOST.'usuario/')
			,array('label'	=>	'Perfil',	'url' => HOST.'usuario/perfil/')
		);
		
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}
	
	
	
}//fim class
?>