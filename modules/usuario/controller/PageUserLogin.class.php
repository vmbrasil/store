<?php
/**
 * Pagina view/controller do login de usuario
 * Esta � uma pagina publica pertencente ao sistema Meu Comercio
 * Quando o usuario comum precisar entrar em alguma pagina de seu perfil, � pedido para se logar aqui.
 * usuario/login/
 * */

//error_reporting(E_ALL);ini_set('display_errors',true);

class PageUserLogin extends PageLoja{
	public function __construct(){//construtor
		parent::__construct();
	}
	
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->add_titulo(' - Identifica��o de Usu�rio');
		
		//Css e JS Padrao
		$this->addCssFileToHead(K13Path::getHost().'view/style/login.css');
		$this->addJsFileToHead(K13Path::getHost() .'view/script/login.js');
		
		//Template Especifico
		$this->set_propertie('templatePage','modules/usuario/view/login.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
		
		$this->event_action($this->get_param('action'));
	}
	
	/**
	 * Metodo para controle do usuario interceptar os dados pouco antes de eventAction
	 * @type: view/controller
	**/
	public function event_action($action = ''){
		if($action == 'logar'){
			if( K13UserENT::get_instance()->logar($this->get_post('login'),$this->get_post('senha')) ){
				$relocation_url = K13SystemVar::get_location_url();
//				var_dump(get_class($this) .'event_action: ' .$relocation_url);exit();
				K13SystemVar::set_location_url('');//para limpar a variavel
				
				if(empty($relocation_url) || strchr($relocation_url, '/usuario/login') ) $relocation_url = K13Path::getHost() .'usuario/perfil/';//entra no perfil do usuario por padrao
				
				echo K13Json::json_encode(array('sucesso'=> true,'mensagem'=>'seja bem vindo!','url'=> $relocation_url));
				exit;
			}
			else{
				echo K13Json::json_encode(array('sucesso'=> false,'mensagem'=>'well...hehehe, nao foi dessa vez!'));
				exit;
			}
		}
	}
	
	/**
	 * Carrega a Barra de Diretorios
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
		
		$diretorios = array(
			array('label'	=>	'In�cio',	'url' => HOST)
			,array('label'	=>	'Usu�rio',	'url' => HOST.'usuario/')
			,array('label'	=>	'Identificar / Entrar',	'url' => HOST.'usuario/login/')
		);
		
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}
	
	
	
}//fim class
?>