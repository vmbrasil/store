<?php
/**
 * Pagina view/controller do cadastro de usuario
 * Esta � uma pagina publica pertencente ao sistema Meu Comercio
 * Quando o usuario comum precisar se cadastrar no site
 * usuario/cadastrar/
 * */

//error_reporting(E_ALL);ini_set('display_errors',true);

class PageNewUserCadastro extends PageLoja{
	public function __construct(){//construtor
		parent::__construct();
	}
	
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->add_titulo(' - Cadastro de Usu�rio');
		
		//Css e JS Padrao
		$this->addCssFileToHead(K13Path::getHost() .Config::get_instance()->get_propertie('system_template_url') .'css/cadastrar.css');
		
		$this->addJsFileToHead(K13Path::getUriFramework() .'modules/view/js/fw_system_lib.js');
		$this->addJsFileToHead(K13Path::getHost() .Config::get_instance()->get_propertie('system_template_url') .'js/cadastrar.js');
		
		//Template Especifico
		$this->set_propertie('templatePage','modules/usuario/view/cadastrar.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
		
//		var_dump($this->get_param('action'));exit();
		$this->event_action($this->get_param('action'));
	}
	
	/**
	 * Metodo para controle do usuario interceptar os dados pouco antes de eventAction
	 * @type: view/controller
	**/
	public function event_action($action = ''){
		if($action == 'cadastrar'){
			//Carrega dados-----------------------------------------
			$paramSystem = K13ParamSystemENT::get_instance();
			
			$id_grupo_cliente = $paramSystem->get_param_value('id_grupo_cliente');
			$dados_usuario = array(
				'apelido' => $this->get_post('nome')
				,'login' => $this->get_post('email')
				,'password' => $this->get_post('senha')
				,'email' => $this->get_post('email')
				,'id_grupo_principal' => $id_grupo_cliente
				,'ativo' => 1
			);
			
			$id_categoria_pessoa = $paramSystem->get_param_value('id_categoria_pessoa_fisica');
			$id_status_pessoa = $paramSystem->get_param_value('id_status_pessoa_fisica');
			$tlist = new K13HtmlInputList();
			
			//tel
			$tlist->clear_value();
			$tlist->add_item($this->get_post('telefone'));
			$telefone = $tlist->get_value();
			//cel
			$tlist->clear_value();
			$tlist->add_item($this->get_post('celular'));
			$celular = $tlist->get_value();
			//email
			$tlist->clear_value();
			$tlist->add_item($this->get_post('email'));
			$email = $tlist->get_value();
			//site
			$tlist->clear_value();
			$tlist->add_item($this->get_post('site'));
			$site = $tlist->get_value();
			
			$dados_pessoa = array(
				'nome' => $this->get_post('nome')
				,'telefone' => $telefone
				,'celular' => $celular
				,'email' => $email
				,'site' => $site
				,'ativo' => 1
				,'id_categoria' => $id_categoria_pessoa
				,'id_status' => $id_status_pessoa
			);
			
			//Valida dados-----------------------------------------
			
			//Verifica se o e-mail inserido � valido
			if( ! K13DataFormat::validarEmail($this->get_post('email')) ) {
				$this->add_erro('Erro! E-mail incorreto. Insira um e-mail v�lido.');
				echo K13Json::json_encode(array(
					'sucesso'=> false
					,'mensagem'=>'Erro! E-mail incorreto. Insira um e-mail v�lido.'
					,'errors'=> $this->get_errors())
				);
				exit();
			}
			//Verifica se ja existe o usuario
			if(K13UserENT::get_instance()->get_dao()->exists(" AND login = '".$this->get_post('email') ."'")){
				$this->add_erro('Erro! Esta conta de usu�rio j� existe. Escolha outro login.');
				echo K13Json::json_encode(array(
					'sucesso'=> false
					,'mensagem'=>'Erro! Esta conta de usu�rio j� existe. Escolha outro login.'
					,'errors'=> $this->get_errors())
				);
				exit();
			}
			//Verifica se existe a categoria correta
			if(empty($id_categoria_pessoa)){
				$this->add_erro('Erro interno! N�o foi possivel carregar a categoria da pessoa f�sica. Pode ser que n�o esteja configurado corretamente.');
				echo K13Json::json_encode(array(
					'sucesso'=> false
					,'mensagem'=>'Erro interno! N�o foi possivel carregar a categoria da pessoa f�sica. Pode ser que n�o esteja configurado corretamente.'
					,'errors'=> $this->get_errors())
				);
				exit();
			}
			//Verifica se existe o grupo correto
			if(empty($id_grupo_cliente)){
				$this->add_erro('Erro interno! N�o foi possivel carregar o grupo de usu�rio do cliente. Pode ser que n�o esteja configurado corretamente.');
				echo K13Json::json_encode(array(
					'sucesso'=> false
					,'mensagem'=> 'Erro interno! N�o foi possivel carregar o grupo de usu�rio do cliente. Pode ser que n�o esteja configurado corretamente.'
					,'errors'=> $this->get_errors())
				);
				exit();
			}
			//Verifica se existe o status obrigatorio
			if(empty($id_status_pessoa)){
				$this->add_erro('Erro interno! N�o foi possivel carregar o status da pessoa. Pode ser que n�o esteja configurado corretamente.');
				echo K13Json::json_encode(array(
					'sucesso'=> false
					,'mensagem'=> 'Erro interno! N�o foi possivel carregar o status da pessoa. Pode ser que n�o esteja configurado corretamente.'
					,'errors'=> $this->get_errors())
				);
				exit();
			}
			
			//Cadastra o usuario-----------------------------------------
			/* ALGORITMO:
			 * 1-cria o usuario
			 * 2-cria a pessoa
			 * 3-atualiza o usuario referenciando a pessoa
			 * */
			
			//1-cria o usuario
			$usuario = new K13UserENT();
			$usuario->carregar_by_array($dados_usuario);
//			$usuario->set_debug(1);
			if(! $usuario->get_dao()->insert()){
				$this->merge_errors($usuario->get_dao()->get_errors());
				echo K13Json::json_encode(array(
					'sucesso'=> false
					,'mensagem'=>'Erro no cadastro de usu�rio!'
					,'errors'=> $this->get_errors())
				);
				exit();
			}
		
			//2-cria a pessoa
			$objPessoa = new PessoaENT();
			$objPessoa->carregar_by_array($dados_pessoa);
			if(! $objPessoa->get_dao()->insert()){
				$this->merge_errors($objPessoa->get_errors());
				echo K13Json::json_encode(array(
						'sucesso'=> false
						,'mensagem'=>'Erro no cadastro de usu�rio!'
						,'errors'=> $this->get_errors())
					);
				exit();
			}
			else{
				//3-atualiza o usuario referenciando a pessoa
				
				//Carrega id da pessoa salva
				$dados_usuario['id_pessoa'] = $objPessoa->get_primaryKeyValueByPosition(0);
				if(empty($dados_usuario['id_pessoa'])) {$this->add_erro('Erro interno. Id da pessoa inv�lido. N�o foi possivel recuperar o id da pessoa.');}
				
				$usuario->set('id_pessoa', $dados_usuario['id_pessoa']);
//				$usuario->set_debug(1);
				if(empty($dados_usuario['id_pessoa']) || ! $usuario->get_dao()->salvar()){
					$this->merge_errors($usuario->get_errors());
					$objPessoa->get_dao()->deletar();//deleta a pessoa criada
					echo K13Json::json_encode(array(
						'sucesso'=> false
						,'mensagem'=>'Erro no cadastro de usu�rio!'
						,'errors'=> $this->get_errors())
					);
					exit();
				}
			}
			
			
			//Envia um e-mail para o usuario-----------------------------------------
			//TODO - enviar email para usuario cadastrado
			
			//Loga o usuario-----------------------------------------
			if( K13UserENT::get_instance()->logar($this->get_post('email'),$this->get_post('senha')) ){
				$relocation_url = K13SystemVar::get_location_url();
				K13SystemVar::set_location_url('');//para limpar a variavel
				if(empty($relocation_url)) $relocation_url = K13Path::getHost() .'usuario/perfil/';//entra no perfil do usuario por padrao
				
				echo K13Json::json_encode(array('sucesso'=> true,'mensagem'=>'Seja bem vindo '.$this->get_post('nome').'!','url'=> $relocation_url));
				exit;
			}
			else{
				$relocation_url = K13Path::getHost() .'usuario/login/';//entra no login do usuario
				echo K13Json::json_encode(array('sucesso'=> true,'mensagem'=>'Usu�rio cadastrado com sucesso, mas n�o foi possivel se logar, insira seus dados para se logar!','url'=> $relocation_url));
				exit;
			}
		}
	}
	
	/**
	 * Carrega a Barra de Diretorios
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
		
		$diretorios = array(
			array('label'	=>	'In�cio',	'url' => HOST)
			,array('label'	=>	'Usu�rio',	'url' => HOST.'usuario/')
			,array('label'	=>	'Novo Cadastro',	'url' => HOST.'usuario/cadastrar/')
		);
		
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}
	
	
	
}//fim class
?>