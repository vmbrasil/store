	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<!-- DirBar -->
		{include file="modules/publico/view/sub_templates/bread_crumb.tpl"}
		
			<h2>Cadastro de Usu�rio</h2>
			

			<div class="janela" id="wcadastro">

				<form action="{$HOST}usuario/cadastrar/" method="post" id="form_cadastro">
					{include file=$k13fwFullDir|cat:"modules/view/msg_system.tpl"}
					
					<!-- Dados Pessoais -->
					<fieldset>
						<legend>Dados Pessoais</legend>
						
						<div rel="cad_nome" class="linha_campo">
							<label rel="cad_nome" class="notnull" for="cad_nome">Nome Completo:</label>
							<input type="text" rel="cad_nome" id="cad_nome" name="nome" class="tstring notnull" value="" required="required" autofocus="autofocus">
						</div>
						
						<div rel="cad_telefone" class="linha_campo">
							<label rel="cad_telefone" for="cad_telefone">Telefone:</label>
							<input type="text" rel="cad_telefone" id="cad_telefone" name="telefone" class="tstring" value="">
						</div>
						
						<div rel="cad_cel" class="linha_campo">
							<label rel="cad_cel" for="cad_cel">Celular:</label>
							<input type="text" rel="cad_cel" id="cad_cel" name="celular" class="tstring" value="">
						</div>
						
						<div rel="cad_site" class="linha_campo">
							<label rel="cad_site" for="cad_site">Site:</label>
							<input type="text" rel="cad_site" id="cad_site" name="site" class="tstring" value="">
						</div>
						
					</fieldset>
					
					<!-- Endere�o -->
					<fieldset>
						<legend>Endere�o</legend>
						
						<div rel="cad_end" class="linha_campo">
							<label rel="cad_end" class="notnull" for="cad_end">Endere�o:</label>
							<input type="text" rel="cad_end" id="cad_end" name="endereco" class="tstring notnull" value="" required="required">
						</div>
						
						<div rel="cad_pais" class="linha_campo">
							<label rel="cad_pais" class="notnull" for="cad_pais">Pa�s:</label>
							<input type="text" rel="cad_pais" id="cad_pais" name="pais" class="tstring notnull" value="" required="required">
						</div>
						
						<div rel="cad_uf" class="linha_campo">
							<label rel="cad_uf" class="notnull" for="cad_uf">Regi�o / Estado:</label>
							<input type="text" rel="cad_uf" id="cad_uf" name="uf" class="tstring notnull" value="" required="required">
						</div>
						
						<div rel="cad_cid" class="linha_campo">
							<label rel="cad_cid" class="notnull" for="cad_cid">Cidade:</label>
							<input type="text" rel="cad_cid" id="cad_cid" name="cidade" class="tstring notnull" value="" required="required">
						</div>
						
						<div rel="cad_cep" class="linha_campo">
							<label rel="cad_cep" class="notnull" for="cad_cep">CEP:</label>
							<input type="text" rel="cad_cep" id="cad_cep" name="cep" class="tstring notnull" value="" required="required">
						</div>
						
					</fieldset>
					
					<!-- Login -->
					<fieldset>
						<legend>Conta do Usu�rio</legend>
						
						<div rel="cad_email" class="linha_campo">
							<label rel="cad_email" class="notnull" for="cad_email">E-mail:</label>
							<input type="text" rel="cad_email" id="cad_email" name="email" class="tstring notnull" value="" required="required">
						</div>
						
						<div rel="cad_senha" class="linha_campo">
							<label rel="cad_senha" class="notnull" for="cad_senha">Senha:</label>
							<input type="password" rel="cad_senha" id="cad_senha" name="senha" class="tstring notnull" value="" required="required">
						</div>
						
						<div rel="cad_resenha" class="linha_campo">
							<label rel="cad_resenha" class="notnull" for="cad_resenha">Redigite a Senha:</label>
							<input type="password" rel="cad_resenha" id="cad_resenha" class="tstring notnull" value="" required="required">
						</div>
						
					</fieldset>
					
					<!-- Opcoes -->
					<fieldset>
						<legend>Termos da Loja</legend>
						
						<div rel="cad_news" class="linha_campo">
							<input type="checkbox" rel="cad_news" id="cad_news" name="newsletter" class="checkbox" value="1">
							<label rel="cad_news" class="checkbox" for="cad_news">Marque para receber Novidades em seu e-mail:</label>
						</div>
						
						<div rel="cad_politica" class="linha_campo">
							<input type="checkbox" rel="cad_politica" id="cad_politica" class="checkbox notnull" value="1" required="required">
							<label rel="cad_politica" class="checkbox notnull" for="cad_politica">Li e aceito as pol�ticas de privacidade do site:</label>
						</div>
						
					</fieldset>
					
					<div class="tool_box">
						<input type="hidden" value="cadastrar" name="action">
						<button aria-disabled="false" role="button" class="bt_submit" type="submit">Cadastrar</button>
					</div>
				</form>

		
			</div><!-- Fim Janela -->
	</article>