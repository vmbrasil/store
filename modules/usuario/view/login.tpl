	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<!-- DirBar -->
		{include file="modules/publico/view/sub_templates/bread_crumb.tpl"}
		
			<h2>Identifica��o de Usu�rio</h2>
			

			<div class="janela" id="wlogin">

				<form action="{$HOST}usuario/login/" method="post" id="form_login">
					{include file=$k13fwFullDir|cat:"modules/view/msg_system.tpl"}
					<label class="linha_campo" id="msg_invalid_login">Login ou Senha invalidos!</label>
				
					<div rel="qtd" class="linha_campo">
						<label rel="login" class="notnull" for="login">Usu�rio:</label>
						<input type="text" rel="login" id="login" name="login" form="form_login" title="Login de usu�rio para entrar no sistema" class="tstring notnull" value="" required="true" autocomplete="off" autofocus="autofocus">
					</div>
					
					<div rel="qtd" class="linha_campo">
						<label rel="senha" class="notnull" for="senha">Senha:</label>
						<input type="password" rel="senha" id="senha" name="senha" form="form_login" required="true" title="Senha de usu�rio para entrar no sistema" class="tpassword notnull" value="" autocomplete="off">
					</div>
					
					<div class="tool_box">
						<input type="hidden" value="logar" name="action">
						<button form="form_login" aria-disabled="false" role="button" class="bt_submit bt_submit_detalhe tool" title="Entrar no Sistema" value="Entrar" type="submit"><span class="cad-button-icon-login"></span><span class="cad-button-text">Entrar</span></button>
						<a hre="#" class="mini_link" id="recuperar_senha" title="Clique aqui caso voc� tenha esquecido ou perdido a senha">Esqueci a senha</a>
						<div class="clear"></div>
					</div>
				</form>

		
			</div><!-- Fim Janela -->
	</article>