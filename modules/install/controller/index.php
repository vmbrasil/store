<?php
//error_reporting(NULL);ini_set('display_errors',false);
//error_reporting(E_ALL);ini_set('display_errors',1);

/**
 * Instala o sistema
 * */
class Install extends K13InstallPage{
	
	public function __construct(){
		
		$classes = array(
			//Sistema
			'K13SetorTipoENT'
			,'K13SetorENT'
			,'K13ParamSystemENT'
			,'K13FolderENT'
			,'K13FileENT'
			,'K13ImagemENT'
			,'K13ModuloENT'
			,'K13LocalENT'
			,'K13PermissaoENT'
			,'K13MenuSistemaENT'
			
			//GeoLocation
			,'PaisENT'
			,'UfENT'
			,'CidadeENT'
			,'BairroENT'
			,'LogradouroENT'
			,'RuaENT'
			,'EnderecoENT'
			
			//Pessoas
			,'PessoaCategoriaENT'
			,'PessoaStatusENT'
			,'EtiniaENT'
			,'PessoaENT'
			,'PessoaFisicaENT'
//			,'FamiliaENT'
//			,'MembroFamiliaENT'
//			,'PessoaJuridicaENT'
//			,'PessoaJuridicaRamoAtividadeENT'

			
			//USUARIO DO SISTEMA
			,'K13UserGroupENT'
			,'K13UserGroupSetorENT'
			,'K13UserENT'
			,'K13UserGroupUserENT'
			
			//Loja
			,'CategoriaLojaENT'
			,'CategoriaFilhaENT'
//			,'MenuItemENT'
			,'MarcaENT'
			,'StatusProdutoENT'
			,'ProdutoENT'
			,'ProdutoCategoriaENT'
			,'ProdutoVideoENT'
			,'ProdutoRelacionadoENT'
			,'ProdutoCompreJuntoENT'
			,'ComentarioENT'
			
			/* N�o vai existir mais
			 * ,'MenuSistemaPrincipalENT'
			//,'NoticiaENT'
			,'DepartamentoENT'
			,'VendaENT'
			,'ItemVendaENT'*/
		);
		
		//Arquivos a instalar
		$config = array(
			'url' => 'modules/install/controller/install_meu_comercio.sql'
		);
		
		if(DEBUG) var_dump($classes);
		
		//Instala
		$install = new K13Install($classes,true,DEBUG,$config);
		$this->result = $install->get_result();
		$this->count_errors = $install->get_count_errors();
		$this->count_succes = $install->get_count_succes();
		
		//Gera a interface
		parent::__construct();
	}
	
}//Fim classe

new Install();
?>