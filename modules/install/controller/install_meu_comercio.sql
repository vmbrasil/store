#Grupo de Usuario
INSERT INTO `meu_comercio`.`sys_user_group` (`datacad`, `id_usuario_cad`, `ativo`, `id_user_group`, `title`) VALUES (NOW(), 1, 1, 1, 'Cliente');
INSERT INTO `meu_comercio`.`sys_user_group` (`datacad`, `id_usuario_cad`, `ativo`, `id_user_group`, `title`) VALUES (NOW(), 1, 1, 2, 'Administrador');

#Usuario do Sistema Padrao
INSERT INTO `meu_comercio`.`sys_user` (`datacad`, `ativo`, `id_user`, `apelido`, `login`, `email`, `observacao`) VALUES (NOW(), 1, 1, 'System User', 'system', '', 'auto created user: system');

#Categoria de Pessoa
INSERT INTO `meu_comercio`.`pessoa_categoria` (`id_categoria_pessoa`, `nome`, `pessoa_tipo`) VALUES (1, 'Cliente', 'F');
#Status da Pessoa
INSERT INTO `meu_comercio`.`pessoa_status` (`id_status`, `title`, `id_categoria`, `cor`) VALUES (1, 'Ativo', 1, '00FF00');

#Parametros do Sistema
INSERT INTO `meu_comercio`.`sys_param_system` (`datacad`, `id_usuario_cad`, `ativo`, `id_param`, `nome`, `valor`, `categoria`, `descricao`) VALUES (NOW(), 1, 1, 1, 'id_categoria_pessoa_fisica', '1', 'pessoa', 'Categoria da Pessoa fisica que indica o Cliente');
INSERT INTO `meu_comercio`.`sys_param_system` (`datacad`, `id_usuario_cad`, `ativo`, `id_param`, `nome`, `valor`, `categoria`, `descricao`) VALUES (NOW(), 1, 1, 2, 'id_status_pessoa_fisica', '1', 'pessoa', 'Status da Pessoa Fisica');
INSERT INTO `meu_comercio`.`sys_param_system` (`datacad`, `id_usuario_cad`, `ativo`, `id_param`, `nome`, `valor`, `categoria`, `descricao`) VALUES (NOW(), 1, 1, 3, 'id_grupo_cliente', '1', 'pessoa', 'Grupo de usuario do sistema para Pessoas Fisica - Cliente');
