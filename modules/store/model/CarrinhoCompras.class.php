<?php
/** ///////////////////////////////////////////////////////////
*	Classe Singleton (unica) para manipular os dados da sessao do carrinho
*	Logo abaixo criaremos as fun��es pr�ticas de funcionamento
*	do carrinho, que s�o as fun��es de inserir, excluir, e apagar
*	o carrinho de compras.
*/ ///////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
// Inicia a classe "Carrinho_Compras"
//////////////////////////////////////////////////////////////
class CarrinhoCompras{
	public static $instance; //Instancia unica (Singleton)

	public static $itens; // @array(codigo,qtd,subtotal) - Array de todos itens no carrinho com id do produto, quantidade e o valor do subtotal armazenado
	public static $count; //Contagem do numero de itens
	
	public static $cep_frete; //CEP para calculo do frete
	public static $id_desconto; //id do desconto
	public static $cod_desconto; //codigo do desconto digitado

	public static $vlr_frete; //Valor calculado do frete
	public static $vlr_desconto; //Valor carregado do desconto

	public static $id_end_entrega; //id do endereco de entrega
	public static $id_end_cobranca; //id do endereco de cobranca

	public static $total; //@decimal - Total do valor calculado que tem no carrinho
	
	public static $id_usuario; //Id do usuario que esta fazendo a compra
	
	//0-(nao inseriu); 1-(Cria o carrinho); 2-(Novo registro); 3-(Atualiza); 4-Valor errado corrigido
	const EXEC_ERROR = 0;
	const EXEC_CREATE = 1;
	const EXEC_NEW = 2;
	const EXEC_UPDATE = 3;
	const EXEC_CORRIGIDO = 4;

//___METODOS______________________________________________________________________________
	function __destruct(){//destrutor
	}

	/**
    	Colocando o constructor como private impede que a classe seja instanciada.
    */
	private function __construct(){//construtor
		self::carregar();
	}
	
	/**
		* Pega a instancia unica desta classe
		* Singleton Pattern
	*/
	public static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new CarrinhoCompras();
		}
		return self::$instance;
	}

	/**
		Pega os itens
	*/
	static function get_itens(){
		return self::$itens;
	}
	
	
	/**
		Pega o item pelo codigo
	*/
	static function get_item($codigo){
		return self::$itens[$codigo];
	}
	
	
	/**
	* Carrega os dados do carrinho
	* @return void
	*/
	static function carregar(){
		self::$itens = $_SESSION['carrinho_compras']['item'];

		self::$cep_frete = $_SESSION['carrinho_compras']['cep_frete']; //CEP para calculo do frete

		self::$id_desconto = $_SESSION['carrinho_compras']['id_desconto']; //id do desconto

		return true;
	}

	/**
	* Pega o numero total de itens
	* @return integer
	*/
	static function get_count_itens(){
//		$this->itens = $_SESSION['carrinho_compras']['item'];

		if(! empty(self::$itens)){//Existe itens
			$chaves = array_keys(self::$itens);
			self::$count  = sizeof($chaves);
			return self::$count;
		}
		else return 0;
	}

	/**
	* calcula o valor total dos produtos no carrinho
	* @return float
	*/
	static function get_valor_produtos(){
		$total = 0.0;

		if(! empty(self::$itens)){//Existe itens
			$objProduto = new ProdutoENT();
			foreach(self::$itens as $produto){
				$preco = $objProduto->load_preco($produto['codigo']);
				$total += $preco * $produto['qtde'];
			}
			return $total;
		}
		else return 0;
	}
	
	/**
	* calcula o valor total de 1 item no carrinho relativo a quantidade
	* @return float
	*/
	static function get_subtotal_item($item_codigo){
		$total = 0.0;
		$objProduto = new ProdutoENT();
		$item = self::get_item($item_codigo);
		$valor = $objProduto->load_preco($item['codigo']);
		$total = $valor * $item['qtde'];
		
		return $total;
	}
	
	/**
	* calcula o valor do frete
	* @return float
	*/
	static function get_valor_frete(){
		$total = 0.0;
/*		$total = self::get_valor_produtos();*/

		return $total;
	}

	/**
	* pega o cep do frete digitado
	* @return string
	*/
	static function get_cep_frete(){
		return self::$cep_frete;
	}

	/**
	* calcula o valor do desconto
	* @return float
	*/
	static function get_valor_desconto(){
		$total = 0.0;
/*		$total = self::get_valor_produtos();*/

		return $total;
	}

	/**
	* pega o codigo de desconto digitado
	* @return string
	*/
	static function get_cod_desconto(){
		return self::$cod_desconto;
	}


	/**
	* calcula o valor total do carrinho
	* @return float
	*/
	static function get_valor_total(){
		$total = 0.0;
		$total += self::get_valor_produtos();
		$total += self::get_valor_frete();
		$total -= self::get_valor_desconto();

		return $total;
	}

	/**
	* calcula o valor total do subtotal no carrinho
	* @return float
	*/
	static function get_valor_subtotal(){
		return self::get_valor_produtos();
	}
	
	/**
	* calcula o peso total do carrinho (todos produtos)
	* @return float
	*/
	static function get_peso_total(){
		$total = 0.0;
/*		$total = self::get_valor_produtos();*/

		return $total;
	}

	/**
	* Pega a quantidade de um item
	* @param id_produto - id do produto no carrinho
	* @return integer
	*/
	static function get_quantidade_item($id_produto){
		$codigo = $id_produto;
//		self::$itens = $_SESSION['carrinho_compras']['item'];

		if(! empty(self::$itens)){//Existe itens
			return self::$itens[$id_produto]['qtde'];
		}
		else{//Nao existe itens
			return 0;
		}

	}

	/**
	* Salva os dados atuais na sessao
	* @return boolean
	*/
	static function salvar(){
		$_SESSION['carrinho_compras']['cep_frete'] = self::$cep_frete; //CEP para calculo do frete
		$_SESSION['carrinho_compras']['id_desconto'] = self::$id_desconto; //id do desconto

		$_SESSION['carrinho_compras']['item'] = NULL;
		$_SESSION['carrinho_compras']['item'] = self::$itens;

		return true;
	}

	/**
	* Insere itens no array da sessao do carrinho e no atributo da classe
	* soma a quantidade
	* @param id_produto - id do produto a inserir/atualizar
	* @param quantidade - quantidade a inserir
	* @return integer - 0-(nao inseriu); 1-(Inicia inser��o); 2-(Novo registro); 3-(Atualiza)
	*/
	static function inserir_item($id_produto,$quantidade = 1){
		if(empty($quantidade) || $quantidade <= 0){
			$quantidade = 1;
		}

		return self::set_item($id_produto, self::get_quantidade_item($id_produto)+$quantidade);
	}

	/**
	* Insere itens no array da sessao do carrinho e no atributo da classe
	* seta a quantidade
	* @param id_produto - id do produto a inserir/atualizar
	* @param quantidade - quantidade a inserir
	* @return integer - 0-(nao inseriu); 1-(Cria carrinho); 2-(Novo registro); 3-(Atualiza); 4-Valor errado corrigido
	* 	const EXEC_ERROR = 0;
	*	const EXEC_CREATE = 1;
	*	const EXEC_NEW = 2;
	*	const EXEC_UPDATE = 3;
	*	const EXEC_CORRIGIDO = 4;

	*/
	static function set_item($id_produto,$quantidade = 1){
		$retorno = self::EXEC_ERROR;
	    
		if(empty($id_produto)) return $retorno;
		if(empty($quantidade) || $quantidade <= 0){
			$quantidade = 1;
			$retorno = self::EXEC_CORRIGIDO;//erro corrigido
		}

		$codigo = $id_produto;
		$qtde = $quantidade;
//		self::$itens = $_SESSION['carrinho_compras']['item'];

		//Status do retorno
		if(empty(self::$itens)){//Nao existe itens (Cria o carrinho)
			$retorno = self::EXEC_CREATE;
		}
		else{
			if($retorno != self::EXEC_CORRIGIDO){
				//Nao existe o item (Insere)
				if(empty(self::$itens[$codigo])){
					$retorno = self::EXEC_NEW;
				}
				else {//Existe itens (Atualiza)
					$retorno = self::EXEC_UPDATE;
				}
			}
		}

		//Salva os dados
		self::$itens[$codigo]['codigo'] = $codigo;
		self::$itens[$codigo]['qtde'] = $qtde;
		self::$itens[$codigo]['subtotal'] = self::get_subtotal_item($codigo);
		self::salvar();
		
		return $retorno;
	}

	/**
	* Excluir um item informado
	* @param id_produto - id do produto a inserir/atualizar
	* @return boolean
	*/
	static function excluir_item($id_produto){
		//var_dump(self::$itens);
		if(! empty(self::$itens)){
			if(is_array($id_produto)){
				foreach ($id_produto as $value){
					unset(self::$itens[$value]);
				}
			}
			else{
				if(! isset(self::$itens[$id_produto]))return false;//Nao existe
				unset(self::$itens[$id_produto]);
			}
			self::salvar();
			return true;
		}
		return false;
	}

	/**
	* Excluir todos itens do carrinho
	* @return boolean
	*/
	static function limpar_carrinho(){
//		self::$itens  = $_SESSION['carrinho_compras'];

		if(! empty(self::$itens)){
			//unset(self::$itens);
			self::$itens = NULL;
			$_SESSION['carrinho_compras']['item'] = NULL;
			unset($_SESSION['carrinho_compras']['item']);
			return true;
		}
		return false;
	}


	/**
	* @deprecated
	* Calcula o valor das parcelas de acordo com o numero das parcelas
	* @param $num - Integer - numero  de parcelas
	* @param $valor - float - valor total
	* @return Float - valor da parcela
	*/
	static function calcular_valor_parcelas($num = 1,$total = 0.0){
		//Fator de calculo de cada parcela
		$fator[1] = 1.00000;
		$fator[2] = 0.51495;
		$fator[3] = 0.34670;
		$fator[4] = 0.26255;
		$fator[5] = 0.21210;
		$fator[6] = 0.17847;
		$fator[7] = 0.15446;
		$fator[8] = 0.13645;
		$fator[9] = 0.12246;
		$fator[10] = 0.11127;
		$fator[11] = 0.10212;
		$fator[12] = 0.09450;
		$fator[13] = 0.08806;
		$fator[14] = 0.08254;
		$fator[15] = 0.07777;
		$fator[16] = 0.07359;
		$fator[17] = 0.06991;
		$fator[18] = 0.06664;

		$retorno = $total*$fator[$num];
		return $retorno;
	}

	/**
	* Processa a entrada de dados no carrinho
	* @param exec string - o comando da execu�ao
	* @param array_dados array[$id_produto][$qtd] - entrada de dados
	*
	* @return array(sucesso=> bool, mensagem => string) - Mensagem de resultado
	* @example: processar_entrada('ADD', array())
	*/
	static function processar_entrada($exec, $array_dados){
		$retorno = array('success'=>false,'message'=>'');
		
		if(strtoupper($exec) == 'CLEAR'){
			$ret = self::limpar_carrinho();
			if($ret == true){
 				$retorno['success'] = true;
				$retorno['message'] .= 'Todos os itens de seu carrinho foram removidos.\n';
			}
			else {
 				$retorno['success'] = false;
				$retorno['message'] .= 'Ocorreu um erro ao remover os itens!\n';
			}
		}
		foreach ($array_dados as $item){
			switch(strtoupper($exec)){
			case 'ADD'://Adicionar 1 ou+ produtos
				$ret = self::inserir_item($item[0],$item[1]);
				if($ret == self::EXEC_ERROR){
 					$retorno['success'] = false;
					$retorno['message'] .= 'N�o foi possivel adicionar o produto informado.\n Ocorreu um erro interno, contacte o administrador.\n';
				}
				if($ret == self::EXEC_UPDATE){
 					$retorno['success'] = true;
					$retorno['message'] .= 'O produto escolhido j� existe na lista. Um novo item foi adicionado a quantidade!\n';
				}
			break;
			case 'UPD'://Atualizar carrinho
				//Valida
				$valido = true;
				if(! is_numeric($item[0])){
					$retorno['success'] = false;
					$retorno['message'] .= 'Produto inv�lido ou n�o encontrado.\nPode ser que o produto informado tenha sido removido ou o estoque est� vazio.\nCaso o problema persista, contacte o administrador.\n';
					$valido = false;
				}
				if(! is_numeric($item[1])){
					$retorno['success'] = false;
					$retorno['message'] .= 'Valor inv�lido inserido, insira um numero inteiro e positivo.\n';
					$valido = false;
				}
				if($valido){
					//Executa processo
					$ret = self::set_item($item[0],$item[1]);
					$retorno['data'] = self::get_cart_data();//dados do carrinho
					//Status
					if($ret == self::EXEC_ERROR){
						$retorno['success'] = false;
						$retorno['message'] .= 'N�o foi possivel atualizar o produto informado.\n Ocorreu um erro interno, contacte o administrador.\n';
					}
					elseif($ret == self::EXEC_CORRIGIDO){
	 					$retorno['success'] = false;
						$retorno['message'] .= 'Valor inv�lido inserido, insira um numero inteiro e positivo. A quantidade foi reiniciada!\n';
					}
					else {
	 					$retorno['success'] = true;
						$retorno['message'] .= 'Alterado com sucesso!\n';
					}
				}
			break;
			case 'REM'://Remover item
				$ret = self::excluir_item($item[0]);
				if($ret == false){
 					$retorno['success'] = false;
					$retorno['mesagem'] .= 'N�o foi possivel remover o produto informado. Talvez o item ja tenha sido removido.\n';
				}
				else {
 					$retorno['success'] = true;
					$retorno['message'] .= 'Removido com sucesso!\n';
				}
							break;
			case 'CLEAR'://Remover item
				$ret = self::limpar_carrinho();
				if($ret == true){
 					$retorno['success'] = true;
					$retorno['message'] .= 'Todos os itens de seu carrinho foram removidos.\n';
				}
				else {
 					$retorno['success'] = false;
					$retorno['message'] .= 'Ocorreu um erro ao remover os itens!\n';
				}
			break;
			default:
 				$retorno['success'] = false;
				$retorno['message'] .= 'Parametros incorretos!\n';
			}
		}
		
		return $retorno;
	}
	
	/**
	* Monta os dados do carrinho em array
	* Esta 'e a func principal chamada no controller
	* @return array
	*/
	public static function get_cart_data(){
		self::carregar();

		//Itens
		$objProduto	= new ProdutoENT();
		
		$itens = array_reverse(self::get_itens());
		foreach($itens as $key => $item){
			$objProduto->get_dao()->carregar(array($objProduto->get_primaryKeyNameByPosition(0) => $item['codigo']));
			$tupla = $objProduto->get_vitrine_data();
			$itens[$key]['produto'] = $tupla;
			
			$itens[$key]['subtotal'] = self::get_subtotal_item($item['codigo']);
		}
		
		//Retorno
		$carrinho = array();
		
		$carrinho['itens'] = $itens;
		$carrinho['subtotal'] = self::get_valor_subtotal();
		$carrinho['total'] = self::get_valor_total();
		$carrinho['valor_desconto'] = self::get_valor_desconto();
		$carrinho['valor_frete'] = self::get_valor_frete();
		$carrinho['cod_desconto'] = self::get_cod_desconto();
		$carrinho['cep'] = self::get_cep_frete();
		$carrinho['parcelas'] = '12';//pega do parcelador
		$carrinho['parcelas_valor'] = K13DataFormat::currencyFormatted(self::get_valor_total()/12);//pega do parcelador
		$carrinho['parcelas_juros'] = 'sem juros';//pega do parcelador
		
		return $carrinho;
	}
	
	/**
	* @deprecated
	* Monta o html da janela do carrinho
	* @return string
	*/
	static function get_html_cart(){
		global $objBD;
		$retorno = '';/*String - Retorno do html*/
	    
//		self::$itens = $_SESSION['carrinho_compras'];
		
		if (! empty(self::$itens)){//Contem itens
		
			//Cada produto
			include_once("{$root_pasta}_classes/Produto.cls.php");//Classe do Produto
			$objProduto = new Produto($objBD);
			//Marca do produto
			include_once("{$root_pasta}_classes/Marca.cls.php");//Classe da Marca
			$objMarca = new Marca($objBD);

			//Imagens das fotos do produto
/*			include_once("{$root_pasta}_classes/Imagem.cls.php");//Classe de Imagens upadas
			$objImagem = new Imagem($objBD);
*/

			/*Inicia a gera��o do HTML*/
			$retorno .= self::get_html_header_cart();
			
			$retorno .=	self::get_html_botoes_acao();

			$retorno .='
			<table width="100%" border="0" cellspacing="0" cellpadding="5" class="car_tab_produtos">
				<thead>
				<tr>
					<th width="50%">Produto</th>
					<th>Quantidade</th>

					<th>Valor Unit&aacute;rio</th>
					<th colspan="2">Valor Total</th>
				</tr>
				</thead>
		        
			';

			//Cada produto============================================================================
			self::$itens = array_reverse(self::$itens);
			$chaves = array_keys(self::$itens);
			self::$count  = sizeof($chaves);
			for ($i = 0; $i < self::$count; $i++){
			
				$codigo = self::$itens[$i]['codigo'];
				$quantidade = self::$itens[$i]['qtde'];
				//print $codigo." - ".$quant."<br />";
				$objProduto->carregar($codigo);
				
				//Foto do produto
				$foto_url = $objProduto->get_objImagem_n(0)->get_thumb_url();
				
				//Marca
				if($objMarca->carregar($objProduto->get_marca_id())) $marca_html = '<p>' .$objMarca->get_descricao() .'</p>';
				else $marca_html = '';
				
				//Pre�o anterior do produto
				$prod_preco_a = $objProduto->get_precoA();
				if(! empty($prod_preco_a) && $prod_preco_a > 0) $prod_preco_a = '<spam>De: R$ ' .currencyFormatted($prod_preco_a).'</spam><br />Por: ';
				else $prod_preco_a = '';
				
				//Total do Item
				$item_total = (float)($objProduto->get_precoV() * $quantidade);
				
				//Total do carrinho
				self::$total += $item_total;
				
				//Gera a linha do produto
				$linha_produtos .= '
				<tr>
					<td height="80">
						<img src="'.$foto_url.'" width="64" height="64" style="margin-right: 10px;float: left">
						<a href="produto.php?id='.$objProduto->get_id().'">'.$objProduto->get_titulo().'</a> '.$marca_html.'
					</td>

					<td>
						<div align="center">
							<input name="txtqtd" id="txtqtd'.$objProduto->get_id().'" type="text" value="'.$quantidade.'" size="8" maxlength="6" class="car_input_qtd">
						</div>
					</td>
					<td> <div class="car_preco_unit">
						'.$prod_preco_a.'
						R$ '.currencyFormatted($objProduto->get_precoV()).'</div>
					</td>

					<td><div class="car_total_unit">R$ '.currencyFormatted($item_total).'</div></td>
					<td>
						<form action="carrinho.php" method="post">
							<input type="hidden" name="exec" value="rem">
							<input type="hidden" name="pid" value="'.$objProduto->get_id().'">
							<input name="btremover" type="submit" value="Retirar" class="car_remove_item" title="Retirar Item do carrinho" onclick="return confirm(\'Deseja realmente remover este item?\')">
						</form>
					</td>
				</tr>
				';
				
				//Pega cada campo para o javascript
				$javascript_qtd .= 'array_pid['.$i.'] = '.$objProduto->get_id().";\n";
				$javascript_qtd .= 'array_qtd['.$i.'] = document.getElementById("txtqtd'.$objProduto->get_id()."\");\n";
			}//Fim cada produtos
			
			//--Linha de Produtos------------------------------------------------------------------
			$retorno .= $linha_produtos;
			
			//---Formulario de Edi��o-----------------------------------------------------------------
			$retorno .= '
				<script language="javascript">
				    function atualizar_carrinho(){
				        var pid_retorno = "";
				        var qtd_retorno = "";
						var array_pid = new Array('.self::$count.');
						var array_qtd = new Array('.self::$count.');
						'.$javascript_qtd.'
						
						for(i=0; i<array_pid.length; i++){
							pid_retorno += "" + array_pid[i];
							if (i<array_pid.length-1)pid_retorno += "||";

							qtd_retorno += "" + array_qtd[i].value;
							if (i<array_qtd.length-1)qtd_retorno += "||";
						}
						
						document.getElementById("pid").value += pid_retorno;
						document.getElementById("qtd").value += qtd_retorno;
						document.getElementById("formEdit").submit();
					}
				</script>
				<form name="formEdit" id="formEdit" method="post" action="carrinho.php">
					<input type="hidden" name="exec" value="upd">
					<input type="hidden" name="qtd" id="qtd" value="">
					<input type="hidden" name="pid" id="pid" value="">
				</form>
			';
			
			//--Calcula outros valores do carrinho------------------------------------------------------------------

			//Calcula Frete
			if(! empty(self::$cep_frete)){
//			    self::$vlr_frete = calcularFrete(self::$cep_frete, outros dados);
				self::$total += self::$vlr_frete;
			}
			
			//Calcula Desconto
			if(! empty(self::$id_desconto)){
/*				if($objDesconto->carregar(self::$id_desconto)){
					self::$total -= $vlr_desconto;
				}
*/			}

			//$parcela_12x_valor = (float)(self::$total / 12);
			$parcela_12x_valor = (float)(self::calcular_valor_parcelas(12,self::$total));
			//Diz se tem ou nao juros nas parcelas
			if(false) $sem_juros_descricao = 'sem juros';

			$retorno .= '
			</table>

			<div class="car_subtotal">
				<form action="carrinho.php" method="post">
					<input type="hidden" name="exec" value="clear">
					<input name="btLimpar" type="submit" value="Limpar" class="car_remove_item" title="Limpar todos itens do carrinho" onclick="return confirm(\'ATEN��O!\nVoc� est� prestes a remover todos os itens do carrinho.\nAo confirmar n�o ser� possivel desfazer a a��o.\n\nDeseja realmente remover todos produtos do carrinho?\')">
				</form>
				<span>(Subtotal) <strong>R$ '.currencyFormatted(self::$total).'</strong></span>
				<div class="clear"></div>
			</div>

			<div class="car_valedesconto">

				<fieldset>
					<legend>Vale-Desconto, Vale-Compra ou Cupom: </legend>

					<label>
						Digite o n�mero do seu Vale-Presente, Vale<br />
						compra ou Cupom e clique no bot�o OK<br />
					</label>

					<form action="dgdsfg" method="post">
						<input name="desc" type="text" maxlength="20" class="car_input">
						<input name="" type="button" value="OK">
					</form>
				</fieldset>

				<div class="car_valedesconto_subtotal">
					<p>Vale-Presente,
					Vale-Compra ou Cupom</p>
					<strong>(R$ 0,00)</strong>
				</div>
			</div>


			<div class="car_cep">
				<fieldset>
					<legend>Calcular Valor do Frete:</legend>

					<label>Digite o CEP e clique no bot�o OK<br />
						<a href="#">N�o sei meu CEP</a>
					</label>

					<form action="dgdsfg" method="post">
						<input name="frete" type="text" maxlength="8" class="car_input">
						<input name="" type="button" value="OK">
					</form>
				</fieldset>

				<div class="car_cep_subtotal">
					<p>Frete</p>
					<strong>(R$ 0,00) <br />� Calcular</strong>
				</div>
			</div>

			<div class="car_total">Valor Total	 <strong>R$ '.currencyFormatted(self::$total).'</strong> </div>

			<div class="car_parcelamento">Ou '.$sem_juros_descricao.' em at� 12x de R$ '.currencyFormatted($parcela_12x_valor).' </div>
			
			<p class="msg_mensagem">
				Para continuar seu pedido, confira na lista acima o(s) produto(s) escolhido(s) por voc�. Se desejar alterar a quantidade de algum produto, basta digitar o novo valor no campo "Quantidade" e depois clicar no bot�o "Atualizar". Para remover/excluir algum produto do pedido clique no link "Excluir" do produto correspondente.
			</p>

			
			'.self::get_html_botoes_acao().'
			</div><!-- Janela -->
			';
		}
		else{//NAO Contem itens (Vazio)
			$retorno .= self::get_html_header_cart();
			$retorno .= '

					<div class="carrinho_vazio">
						<h2>Seu carrinho de compras est� vazio!</h2>
						<p>Para inserir produtos no seu carrinho navegue pelos departamentos ou utilize a busca do site. Ao encontrar os produtos desejados, clique no bot�o "Comprar".</p>
						<div class="car_botoes">
							<a href="vitrine.php" title="Comprar mais produtos" class="car-bt-continuar-compra">Continuar comprando</a>
						</div>
					</div>

				</div><!-- Janela -->
			';
		}
		
		return $retorno;
	}
	
	/**
	* @deprecated
	* Monta o html do cabe�alho da janela do carrinho
	* @return string
	*/
	static function get_html_header_cart(){
		$retorno = '
		<div class="janela">

			<div style="float: left"><h2 class="subtitulo">Meu Carrinho</h2></div>

			<div class="car_etapa">
				<ul>
					<li class="atual">Carrinho</li>
					<li>Identifica��o</li>
					<li>Transporte</li>
					<li>Pagamento</li>
					<li>Finaliza��o</li>
				</ul>
			</div>

			<hr>
		';
		return $retorno;
	}
	
	/**
	 * @deprecated
	* Monta o html dos bot�es do carrinho
	* @return String
	*/
	static function get_html_botoes_acao(){
		$retorno = '
			<div class="car_botoes">
				<a href="vitrine.php" title="Comprar mais produtos" class="car-bt-continuar-compra">Continuar comprando</a>
				<input name="btAtualizar" type="button" value="Atualizar Carrinho" class="car-bt-atualizar" title="Atualizar Modifica��es" onclick="atualizar_carrinho()">
				<a href="finalizar_compra.php" title="Finalizar Compra" class="car-bt-finalizar-compra">Finalizar Compra</a>
			</div>
		';
		return $retorno;
	}
}//fim class
?>