<?php
class PaginaNaoEncontrada extends PageLoja{
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->set_titulo('Meu Comercio 2.0 - P�gina n�o encontrada!');
		
		$url_404 = $_SESSION['url_404'];
		$_SESSION['url_404'] = "";
		$this->objSmarty->assign('url_404', $url_404);
		
		//Template Especifico
		$this->set_propertie('templatePage','modules/system/view/404.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
	}
}//Fim classe
?>