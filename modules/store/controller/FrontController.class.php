<?php
/**
 * Classe responsavel por controlar os dados da ponta do sistema
 * Gera os dados que o sistema precisa
 **/

class FrontController{
	protected static $instance = null; //Singleton instance

//METODOS_____________________________________________________________
//Classe----------------------
	// implements the 'singleton' design pattern.
	public static function &get_instance (){
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}
		return self::$instance;
	}
	
//Opera��es----------------------	
	/**
	 * Carrega os deparatamentos do site
	 * @return echo json format
	 * */
	public static function get_departamentos(){
		$filtro = null;
		$limite = null;
		
		$objDepartamento = new CategoriaLojaENT();

		$result = $objDepartamento->select_departamentos();
		
		return array('regs'=>$result);
	}
	
	/**
	 * Carrega o historico do usuario
	 * @return echo json format
	 * */
	public static function get_historico_usuario(){
		return array('produtos'=>NULL);
		
		/*$produtoEnt = new ProdutoENT();
		
		echo K13DataFormat::custom_json_encode(
			array(
				'produtos'=>array(
					$produtoEnt->get_vitrine_data()
					,$produtoEnt->get_vitrine_data()
					,$produtoEnt->get_vitrine_data()
					,$produtoEnt->get_vitrine_data()
					,$produtoEnt->get_vitrine_data()
					,$produtoEnt->get_vitrine_data()
				)
			)
		);*/
	}
	
	/**
	 * Carrega o carrinho do usuario
	 * @return echo json format
	 * */
	public static function get_user_carrinho(){
		$carrinho = CarrinhoCompras::get_instance();
/*
		$carrinho->carregar();
		//var_dump($carrinho->get_itens());
		return
			array(
				'subtotal'=>$carrinho->get_valor_subtotal()
				,'itens'=>$carrinho->get_itens()//Esta dando pau no ajax do js pois os indices dos arrays nao estao linear 0.1.2.3...
			);
*/
		return $carrinho->get_cart_data();
	}
	
	/**
	 * @deprecated
	 * Carrega a pagina completa de acordo com os parametros
	 * @return echo json format
		 *		titulo - Titulo da pagina
		 *		num - numero da pagina
		 *		BreadCrumb - Objeto bread crumb / Barra de Diretorio
		 *		Banners - Objeto de Banners
		 *		Vitrines
		 *		Menus
	 * */
	public static function get_pagina(){
		$produtoEnt = new ProdutoLojaENT();
		$limite  = 10;//temporario
		$filtro = 'AND ativo = 1';
		$produtos_result = $produtoEnt->get_dao()->listar(array('where'=> $filtro,'limit'=> $limite));
		
		$produtos_vitrine = array();
		foreach($produtos_result as $prod){
			$produtoEnt->carregar_by_array($prod);
			$produtos_vitrine[] = $produtoEnt->get_vitrine_data();
		}
		
		//Menu
		$menus = array();
		
		//Menu Categorias Da loja
		$menu_cat = array();
		$menu_cat['title'] = 'Menu Teste';
		$menu_cat['categorias'] = array();
		$menu_cat['categorias'][0] = array('title'=>'livro','url'=>K13Path::getHost().'categoria/01/Nome_da_Cat/','product_count'=>0);
		$menus['menu_categoria'] = $menu_cat;

		//breadcrumb
		$breadcrumb = array();
		$breadcrumb[] = array('url'=>K13Path::getHost(),'label'=>'In�cio');
		$breadcrumb[] = array('url'=>K13Path::getHost().'produtos','label'=>'Produtos');
		$breadcrumb[] = array('url'=>K13Path::getHost().'departamento/002/nome da categoria/','label'=>'Nome do Departamento');
		$breadcrumb[] = array('url'=>K13Path::getHost().'categoria/002/nome da categoria/','label'=>'Nome da Categoria(opcional)');
		$breadcrumb[] = array('url'=>K13Path::getHost().'categoria/002/nome da categoria/','label'=>'Nome da Sub-Categoria(opcional)');
		$breadcrumb[] = array('url'=>K13Path::getHost().'marca/002/nome da marca/','label'=>'Nome da Marca(opcional)');
		$breadcrumb[] = array('url'=>K13Path::getHost().'produto/154862/nome do produto/','label'=>'Nome do Produto(apenas quando no especifico)');
		
		//Vitrines
		$vitrines = array();
		$vitrines[] = array('title'=>'Destaques','page'=>'1','rows' => 0,'cols' => 8
			,'produtos'=>$produtos_vitrine
/*			array(
				$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
				,$produtoEnt->get_vitrine_data()
			)
*/		);
		
		//Banners
		$banners = array();
		
		
		//Pagina;
		$pagina = array();
		$pagina['page']['title'] = 'Titulo da pagina';
		//$pagina['page']['number'] = 1;
		
		$pagina['menus'] = $menus;
		$pagina['breadcrumb'] = $breadcrumb;
		$pagina['vitrines'] = $vitrines;
		$pagina['banners'] = $banners;
		
		return $pagina;
	}
	
	/**
	 * Carrega os produtos pedidos por ajax
	 * @param $parametros : array - Parametros entrados pelo ajax (GET)
			qtd: 0
			,page: 0
			,id: 0
			,title: ''
			,categoria: 0
			,marca: 0
			,destaque: false
			,oferta: false
			,frete_gratis: false
			,em_promocao: false
			,ord_id: (ASC,DESC)
			,ord_data: (ASC,DESC)
			,ord_preco: (ASC,DESC)
			,ord_nome: (ASC,DESC)
			,ord_vendas: (ASC,DESC)
			,ord_votos: (ASC,DESC)
			,ord_visitas: (ASC,DESC)
			,ord_sugestao: (ASC,DESC)
			,ord_destaque: (ASC,DESC)
			,ord_oferta: (ASC,DESC)
			,ord_frete_gratis: (ASC,DESC)
	 * @return echo json format
	 **/
	public static function get_produto($parametros,$debug = false){
		/*=========>>>>>>>>>>A FAZER
		 * Separar essa funcao como funcao interna.
		 * Qualquer produto podera ser retornado chamando esta func passando parametros
		 * E havera outra que retornara os dados em json
		 * */
		
		//Paramestros de Entrada
		//$parametros = $_REQUEST;
		//$parametros = $_REQUEST['get_produto'];
		//var_dump($parametros);
		//$parametros = json_decode($parametros);
		
		$where = '';//filtro do sql
		
		//Processa os parametros
		$params = array();//Parametros processados
		$params['where'] = '';

		//Where=====================
		if(!empty($parametros['id'])){//Apenas 1 registro
			$params['where'] .= ' AND id='.$parametros['id'];
		}
		else {
			//Titulo
			if(!empty($parametros['title'])){
				$params['where'] .= " AND title LIKE '%".$parametros['title'] ."%'";
			}
			
			//categoria
			if(!empty($parametros['categoria'])){
				$params['where'] .= ' AND id_categoria='.$parametros['categoria'];
			}
			//marca
			if(!empty($parametros['marca'])){
				$params['where'] .= ' AND id_marca='.$parametros['marca'];
			}
			//destaque
			if(!empty($parametros['destaque'])){
				$params['where'] .= ' AND em_destaque= 1';
//				if(!empty($params['order by'])) $params['order by'] .= ',';
//				$params['order by'] .= ' em_destaque DESC';
			}
			//oferta
			if(!empty($parametros['oferta'])){
				$params['where'] .= ' AND em_oferta= 1';
			}
			//frete
			if(!empty($parametros['frete_gratis'])){
				$params['where'] .= ' AND frete_gratis= 1';
			}
			//promocao
			if(!empty($parametros['em_promocao'])){
				$params['where'] .= ' AND em_promocao= 1';
			}
			
			//Order BY=====================
			//ord_preco
			if(!empty($parametros['ord_preco'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' precov '.$parametros['ord_preco'];
			}
			//ord_data
			if(!empty($parametros['ord_data'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' datacad '.$parametros['ord_data'];
				$params['order by'] .= ' ,dataedit '.$parametros['ord_data'];
			}
			//ord_vendas
			if(!empty($parametros['ord_vendas'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' qtd_vendas '.$parametros['ord_vendas'];
			}
			//ord_votos
/*			if(!empty($parametros['ord_votos'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' precov '.$parametros['ord_votos'];
			}
*/			//ord_visitas
			if(!empty($parametros['ord_visitas'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' visitas '.$parametros['ord_visitas'];
			}
			//ord_sugestao
/*			if(!empty($parametros['ord_sugestao'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' precov '.$parametros['ord_sugestao'];
			}
*/			//ord_id
			if(!empty($parametros['ord_id'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' id '.$parametros['ord_id'];
			}
			//ord_nome
			if(!empty($parametros['ord_nome'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' title '.$parametros['ord_nome'];
			}
			//ord_destaque
			if(!empty($parametros['ord_destaque'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' em_destaque '.$parametros['ord_destaque'];
			}
			//ord_oferta
			if(!empty($parametros['ord_oferta'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' em_oferta '.$parametros['ord_oferta'];
			}
			//ord_frete_gratis
			if(!empty($parametros['ord_frete_gratis'])){
				if(!empty($params['order by'])) $params['order by'] .= ',';
				$params['order by'] .= ' frete_gratis '.$parametros['ord_frete_gratis'];
			}
			
			//Limit=====================
			//var_dump($parametros['qtd']);
			if(!empty($parametros['qtd']) ){
				if(empty($parametros['page'])){
					$parametros['page'] = 0;
				}
				else {
					$parametros['page'] = ($parametros['page'] > 1 ? $parametros['page']-1 : 0); 
				}
				
				$params['limit'] = $parametros['page'].','.$parametros['qtd'];
			}
			else{
				$params['limit'] = '';
			}
		}
		
		$objProduto = new ProdutoLojaENT();
		if($debug) $objProduto->set_debug(true);
		
		$result = $objProduto->get_dao()->listar($params);
		$regs = array();
		foreach ($result as $produto){
			$objProduto->carregar_by_array($produto);
			$regs[sizeof($regs)] = $objProduto->get_vitrine_data();
		}
		$count = count($regs);
		
		return array('produtos'=>$regs,'count'=>$count,'total'=>$count);
	}

	/**
	 * Carrega o menu de categorias
	 * @param $params
		titulo
		categoria
		limit
		niveis
	 * @return array - os registros
	 * */
	public static function get_menu_categoria($params){
		$filtro = null;
		$limite = null;
		
		$objCategoria = new CategoriaLojaENT();
		
		//Titulo do menu
		$titulo = $params['titulo'];
		if(empty($params['titulo']) || empty($params['categoria'])){
			$titulo = 'Departamentos';//Carregar do admin
		}
		
		$result = $objCategoria->select_categorias($params['categoria'], $params);
//		var_dump($result);
		return array('categorias'=>$result, 'titulo' => $titulo);
	}
	
	/**
	 * Carrega o menu de marcas
	 * @return array - os registros
	 * */
	public static function get_marcas($params){
		$filtro = null;
		$limite = null;
		
		$objMarca = new MarcaENT();

		$result = $objMarca->select_marcas();
		
		return $result;
	}
	
	/**
	 * Carrega os enderecos do usuario
	 * @return array - os registros
	 * */
	public static function get_user_address($id_user){
		$filtro = null;
		$limite = null;
		
		$objEnd = new EnderecoENT();

		//$result = $objEnd->select_marcas();
		
		return $result;
	}
	
	
}//fim class
?>