<?php
//error_reporting(NULL);ini_set('display_errors',false);

/*Controlador do carrinho etapa Transporte*/
class PageCartTransporteController extends PageLoja{
	protected $objCart = null; //Entidade CarrinhoCompras
	
	public function __construct(){//construtor
		$this->objCart = CarrinhoCompras::get_instance();
		$this->objCart->carregar();
		
		$this->event_action();		
		parent::__construct();
	}
	
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->add_titulo(' - Meu carrinho de Compras - Transporte');
		
		//Js e CSS
		$this->addCssFileToHead(K13Path::getHost() .Config::get_instance()->get_propertie('system_template_url') .'css/carrinho.css');
		$this->addCssFileToHead(K13Path::getHost() .Config::get_instance()->get_propertie('system_template_url') .'css/plugins/jquery-ui-1.8.8.custom.css');
		
		$this->addJsFileToHead(K13Path::getUriFramework() .'terceiros/jquery/js/jquery-ui-1.8.8.custom.min.js');
		$this->addJsFileToHead(K13Path::getHost() .Config::get_instance()->get_propertie('system_template_url') .'js/carrinho.js');
		$this->addJsFileToHead(K13Path::getHost() .Config::get_instance()->get_propertie('system_template_url') .'js/cart_transporte.js');
		

		$this->carregar_Carrinho();
				
		//Template Especifico
		$this->set_propertie('templatePage','modules/publico/view/PageCartTransporte.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
	}
	
	/**
	 * Executa / Manipula os actions da pagina
	**/
	protected function event_action(){
		switch(K13FriendlyUrl::get_url_param(1)){
			case 'ajax':
				switch(K13FriendlyUrl::get_url_param(2)){
					case 'remove':
						$exec = $_POST['exec'];
						
						$pid = K13Funcs::anti_injection($_POST['pid']);
						$dados = array(array($pid,0));
						
						$result = $this->objCart->processar_entrada($exec,$dados);
						
						echo K13DataFormat::custom_json_encode($result);
						exit();
					break;
					case 'update':
						$exec = $_POST['exec'];
						$itens = K13Funcs::anti_injection($_POST['itens'],false);
						$dados = json_decode($itens);
						
						$result = $this->objCart->processar_entrada($exec,$dados);
						
						echo K13DataFormat::custom_json_encode($result);
						exit();
					break;
					case 'clear':
						$result = $this->objCart->processar_entrada('clear');
						
						echo K13DataFormat::custom_json_encode($result);
						exit();
					break;
				}
			break;
			default:
				if($_SERVER['REQUEST_METHOD']=='POST'){
					$exec = K13Funcs::anti_injection($_POST['exec']);
					
					$pid = K13Funcs::anti_injection($_POST['pid']);
					$qtd = K13Funcs::anti_injection($_POST['qtd']);
					$dados = array('id'=>$pid,'qtd'=>$qtd);
					
					//echo $exec .'(' .$pid .', ' .$qtd .')<BR>'; //debug
			
					$retorno_processamento = $this->objCart->processar_entrada($exec,$dados);
				}
						
		}
	}
	

}//Fim classe
?>