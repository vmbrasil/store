<?php
/*Pagina Vitrine*/

//error_reporting(NULL);ini_set('display_errors',false);
class PageVitrineController extends PageLoja{
	
	protected function doBeforeShow(){
		parent::doBeforeShow();
	}
	
	protected function carregarDadosPadrao(){
		parent::carregarDadosPadrao();
		
		$this->carregar_menu_esquerdo();
		$this->carregar_vitrines();
		$this->carregar_menu_direito();
	}
	
	/**
	 * Carrega a menu da categorias
	 * */
	protected function carregar_menu_categorias(){
		$param = array(
			'titulo'		=> 'Departamentos'
			,'niveis'		=> 2
		);
		
		$result = FrontController::get_menu_categoria(null,$param);
		$this->objSmarty->assign('menu_categoria', $result);
	}
	
	/**
	 * Carrega o menu de marcas
	 * */
	protected function carregar_menu_marcas(){
		$result = FrontController::get_marcas();
		//var_dump($result);
		$this->objSmarty->assign('menu_marcas', $result);
	}
	
	/**
	 * Carrega os produtos da vitrine
	 * @parametros - Parametros entrados pelo ajax (GET)
			qtd: 0
			,page: 0
			,id: 0
			,categoria: 0
			,marca: 0
			,destaque: false
			,oferta: false
			,frete: false
			,em_promocao: false
			,ord_id: (ASC,DESC)
			,ord_data: (ASC,DESC)
			,ord_preco: (ASC,DESC)
			,ord_nome: (ASC,DESC)
			,ord_vendas: (ASC,DESC)
			,ord_votos: (ASC,DESC)
			,ord_visitas: (ASC,DESC)
			,ord_sugestao: (ASC,DESC)
	 * @return echo json format
	 **/
	protected function carregar_vitrines(){
		$this->carregar_produtos_destaques();
		$this->carregar_produtos_ofertas();
		$this->carregar_produtos_novidades();
		$this->carregar_produtos_mais_vendidos();
		$this->carregar_produtos_mais_baratos();
		$this->carregar_produtos_visitados();
		$this->carregar_produtos_frete_gratis();
	}
	
	/**
	 * Carrega os produtos top5
	 * */
	protected function carregar_produtos_top5(){
		$params = array(
			'qtd' => 5
			,'ord_vendas' => 'DESC'
			,'ord_votos' => 'DESC'
			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		//var_dump($result);
		
		$this->objSmarty->assign('produtos_top5', $result);
	}

	/**
	 * Carrega os produtos da vitrine destaques
	**/
	protected function carregar_produtos_destaques(){
		$params = array(
			'qtd' => 4
			,'ord_destaque' => 'DESC'
//			,'ord_vendas' => 'DESC'
//			,'ord_votos' => 'DESC'
//			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		$this->objSmarty->assign('vitrine_destaques', $result);
	}
	
	/**
	 * Carrega os produtos da vitrine ofertas
	**/
	protected function carregar_produtos_ofertas(){
		$params = array(
			'qtd' => 4
			,'ord_oferta' => 'DESC'
//			,'ord_vendas' => 'DESC'
//			,'ord_votos' => 'DESC'
//			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		$this->objSmarty->assign('vitrine_ofertas', $result);
	}
	
	/**
	 * Carrega os produtos da vitrine novidades
	**/
	protected function carregar_produtos_novidades(){
		$params = array(
			'qtd' => 4
			,'ord_data' => 'DESC'
//			,'ord_votos' => 'DESC'
//			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		$this->objSmarty->assign('vitrine_novidades', $result);
	}
	
	/**
	 * Carrega os produtos da vitrine mais_vendidos
	**/
	protected function carregar_produtos_mais_vendidos(){
		$params = array(
			'qtd' => 4
			,'ord_vendas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		$this->objSmarty->assign('vitrine_mais_vendidos', $result);
	}
	
	/**
	 * Carrega os produtos da vitrine mais_vendidos
	**/
	protected function carregar_produtos_mais_baratos(){
		$params = array(
			'qtd' => 4
			,'ord_preco' => 'ASC'
		);
		$result = FrontController::get_produto($params);
		$this->objSmarty->assign('vitrine_mais_baratos', $result);
	}
	
	/**
	 * Carrega os produtos da vitrine MAIS VISITADOS
	**/
	protected function carregar_produtos_visitados(){
		$params = array(
			'qtd' => 4
			,'ord_destaque' => 'DESC'
			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		
		$this->objSmarty->assign('vitrine_mais_visitados', $result);
	}	
	
	/**
	 * Carrega os produtos da vitrine frete_gratis
	**/
	protected function carregar_produtos_frete_gratis(){
		$params = array(
			'qtd' => 4
			,'frete_gratis' => true
			,'ord_vendas' => 'DESC'
			,'ord_votos' => 'DESC'
			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		$this->objSmarty->assign('vitrine_frete_gratis', $result);
	}
	
	/**
	 * Carrega o Banner Superior
	**/
	protected function carregar_BannerSuperior(){
	}

	/**
	 * Carrega os Banners Rotativo
	**/
	protected function carregar_BannersRotativo(){
	}

	/**
	 * Carrega os Banners Menu Direito
	**/
	protected function carregar_BannersMenuDireito(){
		$objSqlSt = new K13SqlStatement();
		$objSqlSt->set_tabela('banner');
		$objSqlSt->add_where('ativo = 1');
		$objSqlSt->add_order_by('datacad DESC');
		$sql = $objSqlSt->gerar_SqlSelect("
			id
			,descricao AS description
			,url_img AS url_img
		");

		$objConexBd = K13ConnectionConfig::get_connection();
		$banners = $objConexBd->query_fetchAll($sql);
		
		$this->objSmarty->assign('banners_laterais', $banners);
	}

	/**
	 * Carrega o Menu Esquerdo
	**/
	protected function carregar_menu_esquerdo(){
		$this->carregar_menu_categorias();
	}
	
	/**
	 * Carrega o Menu Direito
	**/
	protected function carregar_menu_direito(){
		$this->carregar_produtos_top5();
	}
	
	
}//Fim classe
?>