<?php
/**
* Pagina Front Controller Publica, Classe de Interface Padrao usado em todas paginas publicas da loja
* Carrega o Carrinho de Compras
* Carrega Departamento
* Declara Metodo Tag Cloud - para carregar as tags clouds da pagina especifica
*/
class PageLoja extends PagePadrao{
	protected $departamentos = array();
	
	public function __construct(){//construtor
		parent::__construct();
	}

	/**
	 * Carrega os dados padroes da pagina(header,topo, etc)
	**/
	protected function carregarDadosPadrao(){
		parent::carregarDadosPadrao();
		
		//Carrega dados
		$this->carregar_tagCloud();
		$this->carregar_departamentos();
		$this->carregar_carrinho();
		$this->carregar_BarraDiretorios();
	}

	/**
	 * Carrega as tags Clouds
	**/
	protected function carregar_tagCloud(){
		$tags = null;
		$this->objSmarty->assign('tagCloud', $tags);
	}

	/**
	 * Carrega os departamentos
	**/
	protected function carregar_departamentos(){
		$objDepartamento = new CategoriaLojaENT(null);
		$this->departamentos = $objDepartamento->select_departamentos();
		
		$this->objSmarty->assign('list_departamento', $this->departamentos);
	}

	/**
	 * Carrega os itens do Carrinho
	**/
	protected function carregar_carrinho(){
		$objCart = CarrinhoCompras::get_instance();
		$carrinho = $objCart->get_cart_data();

		$this->objSmarty->assign('carrinho', $carrinho);
		//$this->objSmarty->debugging = 1;
		
		return $carrinho;
	}
	
	/**
	 * Carrega a Barra de Diretorios
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
/*
		$diretorios = array(
			array('label'	=>	'In�cio',	'url' => HOST)
			,array('label'	=>	'Usu�rio',	'url' => HOST.'usuario/')
			,array('label'	=>	'Identificar / Entrar',	'url' => HOST.'usuario/login/')
		);
*/
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}
	

}//Fim class
?>