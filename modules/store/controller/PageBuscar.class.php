<?php

/*Classe para Controle de Busca no site*/
class PageBuscar extends PageVitrineController{
	protected $itens = array();	//Os itens resultados da busca
	protected $objPaginador = null;	//Entidade Paginador - A entidade a ser manipulada
	
	
	/*
	Colocando o constructor como private impede que a classe seja instanciada.
	*/
	public function __construct(){//construtor
		$this->objPaginador = new K13PagerBar();
		
		parent::__construct();
	}
	
	
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->add_titulo(' - Busca: '.$_GET['txt_busca']);
		
		//Template Especifico
		$this->addJsFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'js/inicio.js');
		$this->addCssFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'css/inicio.css');
		
		$this->addJsFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'js/busca.js');
		$this->addCssFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'css/busca.css');
		
		$this->set_propertie('templatePage','modules/publico/view/buscar.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
	}
	
	/**
	 * Carrega os dados padroes da pagina(header,topo, etc)
	**/
	protected function carregarDadosPadrao(){
		parent::carregarDadosPadrao();
		$this->carregar_BarraDiretorios();
		$this->carregar_combobox();
		//Efetua a busca
		$this->buscar();
	}

	/**
	 * Carrega a Barra de Diretorios
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
		
		$diretorios = array(
			array('label'	=>	'In�cio',	'url' => HOST)
			,array('label'	=>	'Busca',	'url' => HOST.'buscar/')
			,array('label'	=>	$_GET['txt_busca'],	'url' => HOST.'buscar/?txt_busca= '.$_GET['txt_busca'])
		);
		
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}
	
	/**
	 * Carrega as opcoes dos combobox
	**/
	protected function carregar_combobox(){
		$combobox = null;
		
		//Local
		$combobox = array(
			'site'	=>	'Todo Site'
			,'departamento'	=>	'Departamento'
			,'produto'	=>	'Produto'
		);
		$this->objSmarty->assign('combobox_local', $combobox);
		
		//Categoria
		$combobox = array();
		
		foreach ($this->departamentos as $value){
			$combobox[$value['id']] = $value['title'];
		}
		$this->objSmarty->assign('combobox_categoria', $combobox);
		
		//tipo
		$combobox = array(
//			'todos'		=>	'Todos',
			'produto'	=>	'Produto'
			,'categoria'=>	'Categoria'
			,'promo��o'	=>	'Promo��o'
			,'pagina'	=>	'Pagina'
			,'arquivo'	=>	'Arquivo'
			,'foto'		=>	'Foto'
		);
		$this->objSmarty->assign('combobox_tipo', $combobox);
		
		//ordem
		$combobox = array(
			'relevante'		=>	'Mais Relevante'
			,'A-Z'			=>	'A-Z'
			,'Z-A'			=>	'Z-A'
			,'barato'		=>	'Menor Pre�o'
			,'caro'			=>	'Maior Pre�o'
			,'novidades'	=>	'Mais recente'
			,'vendidos'		=>	'Mais Vendidos'
			,'visitados'	=>	'Mais Visitados'
			,'qualificados'	=>	'Mais Qualificados'
			,'sugeridos'	=>	'Mais sugeridos'
			,'top'			=>	'TOP 10'
		);
		
		$this->objSmarty->assign('combobox_ordem', $combobox);
		
	}
	
	
	
	/**
	 * Busca os produtos ou itens
	**/
	protected function buscar(){
		//PARAMETROS----------------------------------
		$query = $this->get_param('txt_busca');

		$local = $this->get_param('local');
		$categoria = $this->get_param('categoria');
		$tipo = $this->get_param('tipo');
		$ordem = $this->get_param('ordem');
		
		$destaque = $this->get_param('opc_destaque'); $destaque = ! empty($destaque);
		$oferta = $this->get_param('opc_oferta'); $oferta = ! empty($oferta);
		$promocao = $this->get_param('opc_promocao'); $promocao = ! empty($promocao);
		$frete = $this->get_param('opc_frete'); $frete = ! empty($frete);
		
		$page = $this->get_param('pagina');
		
		//Falta Ordenar
		$ord = $this->get_param('ordem');
		
		switch ($ord){
			case 'novidades':
				$ord_data = 'DESC';
				$ord_preco = '';
				$ord_nome = '';
				$ord_vendas = '';
				$ord_destaque = 'DESC';
			break;
			
			case 'barato':
				$ord_data = '';
				$ord_preco = 'ASC';
				$ord_nome = '';
				$ord_vendas = '';
				$ord_destaque = 'DESC';
			break;
			
			case 'caro':
				$ord_data = '';
				$ord_preco = 'DESC';
				$ord_nome = '';
				$ord_vendas = '';
				$ord_destaque = 'DESC';
			break;
			
			case 'A-Z':
				$ord_data = '';
				$ord_preco = '';
				$ord_nome = 'ASC';
				$ord_vendas = '';
				$ord_destaque = '';
			break;
			
			case 'Z-A':
				$ord_data = '';
				$ord_preco = '';
				$ord_nome = 'DESC';
				$ord_vendas = '';
				$destaque = '';
			break;
			
			case 'vendidos':
				$ord_data = '';
				$ord_preco = '';
				$ord_nome = '';
				$ord_vendas = 'DESC';
				$ord_destaque = 'DESC';
			break;
			
			case 'visitados':
				$ord_data = '';
				$ord_preco = '';
				$ord_nome = '';
				$ord_visitas = 'DESC';
				$ord_destaque = 'DESC';
			break;
			
			case 'qualificados':
				$ord_data = '';
				$ord_preco = '';
				$ord_nome = '';
				$ord_votos = 'DESC';
				$ord_destaque = 'DESC';
			break;
			
			case 'sugeridos':
				$ord_data = '';
				$ord_preco = '';
				$ord_nome = '';
				$ord_sugestao = 'DESC';
				$ord_destaque = 'DESC';
			break;
			
			case 'top':
				$ord_data = 'DESC';
				$ord_sugestao = 'DESC';
				$ord_visitas = 'DESC';
				$ord_vendas = 'DESC';
				$ord_votos = 'DESC';
				$ord_destaque = 'DESC';
			break;
			
			
			default:
//				$ord_data = 'DESC';
				$ord_preco = '';
				$ord_nome = '';
				$ord_visitas = 'DESC';
				$ord_vendas = 'DESC';
				$ord_destaque = 'DESC';
		}
		
		
		$params = array(
			'qtd' => 12
			,'page' => $page
			,'title' => $query
			
			,'destaque' => $destaque
			,'oferta' => $oferta
			,'em_promocao' => $promocao
			,'frete_gratis' => $frete
			
			,'categoria' => $categoria
			,'ord_nome' => $ord_nome
			,'ord_data' => $ord_data
			,'ord_vendas' => $ord_vendas
			,'ord_preco' => $ord_preco
			,'ord_votos' => $ord_votos
			,'ord_visitas' => $ord_visitas
		);
		$result = FrontController::get_produto($params);
		$result = $result['produtos'];
		
		$this->objSmarty->assign('itensBusca', $result);
		
		$this->itens = $result;
		
		$this->gerarPaginationBar();
	}

	/**
	 * Gera a barra de Paginacao
	**/
	public function gerarPaginationBar(){
		$url = '';//Url para consulta
		$url = K13Path::getUrl();
		
		$this->objPaginador->set_url($url);
		$this->objPaginador->set_exibidos(12);//registros exibidos
		$this->objPaginador->set_pagina($this->get_param('pagina'));
		$this->objPaginador->set_total_registros(count($this->itens));
		
		$html = $this->objPaginador->get_HTML($url);
		$this->itens['pagination'] = $html;
		
		$this->objSmarty->assign('vitrine_produtos_paginacao', $html);
		return $html;
	}
	
	
}//Fim classe
?>