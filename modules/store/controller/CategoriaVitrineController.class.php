<?php
//error_reporting(NULL);ini_set('display_errors',false);
//error_reporting(E_ALL);ini_set('display_errors',true);

/**
 * Controlador da Vitrine de Produtos
 * Pagina Categoria ou Departamento da Loja
 **/
class CategoriaVitrineController extends PageVitrineController{
	protected $objCategoria = null;	//Entidade Categoria da Vitrine
	protected $objProduto = null;	//Entidade Produto
	protected $objImg = null;		//Entidade Imagem
	protected $objPaginador = null;	//Entidade Paginador - A entidade a ser manipulada
	
	protected $parametros = array();//Array de Parametros
	/*cat = categoria id
	* mar = marca id
	* ord = ordena��o()
	* */
	
	//METODOS_____________________________________________________________
	public function __construct(){//construtor
		$this->objCategoria	= new CategoriaLojaENT();
		$this->objProduto	= new ProdutoENT();
		$this->objImg		= new ImagemENT();
		$this->objPaginador = new K13PagerBar();
		
		parent::__construct();
	}
	
	/**
	 * Carrega os Dados principais da pagina antes de tudo
	**/
	protected function carregar(){
		//Categoria
		$slug = strtolower( K13Security::anti_injection( K13FriendlyUrl::get_url_param(1) ));
		
		$where = " AND slug LIKE '" .$slug ."'";
		
		if(! $this->objCategoria->get_dao()->carregar_by_where($where)){
			//---------Mensagem de erro--------------
			$this->add_erro('N�o foi possivel encontrar a Categoria informada!');
			$this->add_erro('Verifique se o endere�o foi digitado corretamente.');
			$this->add_erro('Se o problema persistir entre em contato com o administrador do site.');
			$this->objSmarty->assign('erros',$this->get_errors());
			
			$this->objSmarty->assign('categoria_informada',$slug);
			
			//------carrega lista de categorias------------------------------
			$lista = $this->objCategoria->select_categorias();
			$this->objSmarty->assign('categorias',$lista);
//			$categorias = array();
//			foreach ($lista as $key => $value){
//				$this->objCategoria->carregar_by_array($value);
//				$categorias[] = $this->objCategoria->get_serial_data();
//			}
//			$this->objSmarty->assign('categorias',$categorias);

			//-----Procura a corre��o 'Voce quis dizer'----------------------
			$correcao = '';
			for($i = 5; $i >= 2; $i--){//Cada vez diminui as restricoes de semelhanca de 5 a 3
				if($correcao == ''){
					foreach ($lista as $value){
						if(K13Funcs::string_semelhantes($slug, $value['slug'], $i)){
							$correcao = $value;
							break;
						}
						elseif(count($value['subcategorias']) > 0 && $value['subcategorias'] != false){
							foreach ($value['subcategorias'] as $sub){
								if(K13Funcs::string_semelhantes($slug, $sub['slug'], $i)){
									$correcao = $sub;
									break;
								}
							}
						}
					}
				}
			}
			
			
			$this->objSmarty->assign('correcao',$correcao);
			
			//-------------Dados da Categoria atual-----------------------
			$this->objCategoria->limparCampos();
			$this->objCategoria->set('nome', 'Categoria ' .$slug .' n�o encontrada!');
		}
		
		$this->objSmarty->assign('categoria',$this->objCategoria->get_serial_data());
		
		
		
		//===========CARREGA AS OUTRAS COISAS============
		$this->carregar_Parametros();//Primeiro a ser carregado
		
		$this->carregar_vitrines();
		$this->carregar_BarraDiretorios();
		$this->carregar_BannerSuperior();
		$this->carregar_BannersRotativo();
		$this->carregar_BannersMenuDireito();
		$this->carregar_menu_direito();
		$this->carregar_menu_esquerdo();
	}
	
	/**
	 * Carrega os dados padroes da pagina(header,topo, etc)
	**/
	protected function carregarDadosPadrao(){
		parent::carregarDadosPadrao();
		$this->carregar();
	}

	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->add_titulo(' - '.$this->objCategoria->get('nome'));
		
		//Template Especifico
		$this->addJsFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'js/categoria.js');
		$this->addCssFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'css/categoria.css');
				
		$this->set_propertie('templatePage','modules/publico/view/categoria.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
	}
	
	/**
	 * Carrega os Parametros da pagina
	**/
	protected function carregar_Parametros(){
		//Parametros
		$ord = $this->get_param('ord');
		$this->objSmarty->assign('ordem_aba',$ord);
		
		
		//categoria
/*		if($this->get_param('id_categoria') == '') $this->set_param('id_categoria',$_REQUEST['id_categoria']);
		if($this->get_param('id_categoria') == '') $this->set_param('id_categoria',K13FriendlyUrl::get_url_param(1));
		//marca
		if($this->get_param('id_marca') == '') $this->set_param('id_marca',$_REQUEST['id_marca']);
		if($this->get_param('id_marca') == '') $this->set_param('id_marca',K13FriendlyUrl::get_url_param(1));
		//ordenacao
		if($this->get_param('ord') == '') $this->set_param('ord',$_REQUEST['ord']);
		//pagina
		if($this->get_param('pag') == '') $this->set_param('ord',$_REQUEST['pag']);
		
		//FILTROS
		//Destaque
		if($this->get_param('destaque') == '') $this->set_param('destaque',$_REQUEST['destaque']);
		//Ofertas
		if($this->get_param('oferta') == '') $this->set_param('oferta',$_REQUEST['oferta']);
		//Sugest�es
		if($this->get_param('sugerido') == '') $this->set_param('sugerido',$_REQUEST['sugerido']);
		//Promo��es
		if($this->get_param('promo') == '') $this->set_param('promo',$_REQUEST['promo']);
		//frete
		if($this->get_param('frete') == '') $this->set_param('frete',$_REQUEST['frete']);
*/	}
	
	/**
	 * Carrega a Barra de Diretorios
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
		$result = $this->objCategoria->select_super_categorias($this->objCategoria->get_primaryKeyValueByPosition(0));
		
		$diretorios = array(
			array('label'	=>	'In�cio',		'url' => HOST)
//			,array('label'	=>	'Categorias',	'url' => HOST.'categorias/')
//			,array('label'	=>	$this->objCategoria->get('nome'),	'url' => HOST.'categoria/' .$this->objCategoria->get('slug').'/')
		);
		
		//Adiciona o Link Categorias
		if(count($result) <= 0) $diretorios[] = array('label'	=>	'Categorias',	'url' => HOST.'categorias/');
		
		//Formata os registros carregados
		$carregados = array();
		foreach ($result as $key => $item){
			$carregados[] = array('label'	=>	$item['title'],	'url' => HOST.'categoria/'.$item['slug'].'/');
		}
		$diretorios = array_merge($diretorios, $carregados);
		
//		var_dump($diretorios);
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}

	/**
	 * Carrega os Banners Menu Direito
	**/
	protected function carregar_BannersMenuDireito(){
		$objSqlSt = new K13SqlStatement();
		$objSqlSt->set_tabela('banner');
		$objSqlSt->add_where('ativo = 1');
		$objSqlSt->add_order_by('datacad DESC');
		$sql = $objSqlSt->gerar_SqlSelect("
			id
			,descricao AS description
			,url_img AS url_img
		");

		$objConexBd = K13ConnectionConfig::get_connection();
		$banners = $objConexBd->query_fetchAll($sql);
		
		$this->objSmarty->assign('banners_laterais', $banners);
	}

	/**
	 * Carrega a menu da categorias
	 **/
	protected function carregar_menu_categorias(){
		$id_categoria = $this->objCategoria->get_primaryKeyValueByPosition(0);
		$param = array(
			'titulo'		=> $this->objCategoria->get('nome')
			,'categoria'	=> $id_categoria
			,'niveis'		=> 2
		);
		$result = FrontController::get_menu_categoria($param);
		//var_dump($result);
		$this->objSmarty->assign('menu_categoria', $result);
	}
	
	//Prateleiras-----------------------------------------
	/**
	 * Adicina 1 prateleira
	 * @param string $descricao - descricao da prateleira
	 * @param string $id - id da prateleira
	 * @param array $produtos - produtos da prateleira
	**/
	protected function add_Prateleira($descricao, $id='', $produtos = null){
		if(empty($id))$id = $descricao;
		$this->vitrines[$id] = array('descricao' => $descricao, 'produtos' => $produtos);
	}


	/**
	 * Carrega os produtos a serem mostrados na vitrine
	**/
	protected function carregar_Produtos(){
		//PARAMETROS----------------------------------
		$ord = $this->get_param('ord');
		
		switch ($ord){
			case 'by_data':
				$ord_data = 'DESC';
				$ord_preco = '';
				$ord_nome = '';
				$ord_vendas = '';
				$destaque = '';
			break;
			
			case 'by_valor':
				$ord_data = '';
				$ord_preco = 'ASC';
				$ord_nome = '';
				$ord_vendas = '';
				$destaque = '';
			break;
			
			case 'by_nome':
				$ord_data = '';
				$ord_preco = '';
				$ord_nome = 'ASC';
				$ord_vendas = '';
				$destaque = '';
			break;
			
			case 'by_vendas':
				$ord_data = '';
				$ord_preco = '';
				$ord_nome = '';
				$ord_vendas = 'DESC';
				$destaque = true;
			break;
			
			default:
				$ord_data = 'DESC';
				$ord_preco = '';
				$ord_nome = '';
				$ord_vendas = '';
				$destaque = '';
		}

		$page = $this->get_param('pagina');
		
		$categoria = $this->objCategoria->get_primaryKeyValueByPosition(0);
		
		$params = array(
			'qtd' => 12
			,'page' => $page
			,'destaque' => $destaque
			,'categoria' => $categoria
			,'ord_nome' => $ord_nome
			,'ord_data' => $ord_data
			,'ord_vendas' => $ord_vendas
			,'ord_preco' => $ord_preco
//			,'ord_votos' => 'DESC'
//			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		
		$this->objSmarty->assign('vitrine_produtos', $result);
		
		$this->vitrines['produtos'] = $result;
		
		$this->gerarPaginationBar();
	}
	
	/**
	 * Carrega os produtos top5
	 * */
	protected function carregar_produtos_top5(){
		
		$params = array(
			'qtd' => 5
			,'ord_vendas' => 'DESC'
			,'ord_votos' => 'DESC'
			,'ord_visitas' => 'DESC'
			,'categoria' => $this->objCategoria->get_primaryKeyValueByPosition(0)
		);
		$result = FrontController::get_produto($params);
		//var_dump($result);
		
		$this->objSmarty->assign('produtos_top5', $result);
	}
	
	/**
	 * Carrega as Categorias de Prateleiras
	**/
	protected function carregar_vitrines(){
		$this->carregar_Produtos();
//		$this->carregar_PrateleiraDestaque();
//		$this->carregar_PrateleiraOfertas();
//		$this->carregar_PrateleiraSugeridos();
//		$this->carregar_PrateleiraLancamentos();
		
		$this->objSmarty->assign('vitrine', $this->vitrines);
	}
	
	/**
	 * Gera a barra de Paginacao
	**/
	public function gerarPaginationBar(){
		$url = '';//Url para consulta
		$url = K13Path::getUrl();
		
		$this->objPaginador->set_url($url);
		$this->objPaginador->set_exibidos(12);//registros exibidos
		$this->objPaginador->set_pagina($this->get_param('pagina'));
		$this->objPaginador->set_total_registros(count($this->vitrines['produtos']));
		
		$html = $this->objPaginador->get_HTML($url);
		$this->vitrines['produtos']['pagination'] = $html;
		
		$this->objSmarty->assign('vitrine_produtos_paginacao', $html);
		return $html;
	}
	

}//Fim classe

