<?php
/*Pagina Inicial*/

//error_reporting(NULL);ini_set('display_errors',false);
class PageInicial extends PageVitrineController{
	protected function doBeforeShow(){
		parent::doBeforeShow();
		$this->addJsFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'js/inicio.js');
		$this->addCssFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'css/inicio.css');
	}
	
	protected function carregarDadosPadrao(){
		parent::carregarDadosPadrao();
		
		$this->carregar_produtos_top5();
		$this->carregar_menu_categorias();
		$this->carregar_vitrines();
	}
	
}//Fim classe
?>