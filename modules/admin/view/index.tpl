{include file="head.tpl"}
{include file="header.tpl"}

{include file="rodape.tpl"}

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-br" xml:lang="pt-br">
<head>
<title>Painel De Controle</title>
<base href="http://localhost/opencart/admin/" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/ui.all.css" />
<script type="text/javascript" src="view/javascript/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.core.js"></script>
<script type="text/javascript" src="view/javascript/jquery/superfish/js/superfish.js"></script>
<script type="text/javascript" src="view/javascript/jquery/tab.js"></script>
<script type="text/javascript">
//-----------------------------------------
// Confirm Actions (delete, uninstall)
//-----------------------------------------
$(document).ready(function(){
	
    // Confirm Delete
    $('#form').submit(function(){
        if ($(this).attr('action').indexOf('delete',1) != -1) {
            if (!confirm ('Deletar/Desinstalar não pode ser desfeito! Você está certo que deseja fazer isto?')) {
                return false;
            }
        }
    });
    	
    // Confirm Uninstall
    $('a').click(function(){
        if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall',1) != -1) {
            if (!confirm ('Deletar/Desinstalar não pode ser desfeito! Você está certo que deseja fazer isto?')) {
                return false;
            }
        }
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $(".scrollbox").each(function(i) {
    	$(this).attr('id', 'scrollbox_' + i);
		sbox = '#' + $(this).attr('id');
    	$(this).after('<span><a onclick="$(\'' + sbox + ' :checkbox\').attr(\'checked\', \'checked\');"><u>Selecionar tudo</u></a> / <a onclick="$(\'' + sbox + ' :checkbox\').attr(\'checked\', \'\');"><u>Desmarcar todos</u></a></span>');
	});
});
</script>
</head>
<body>
<div id="container">
<div id="header">
  <div class="div1"><img src="view/image/logo.png" title="Administração" onclick="location = 'http://localhost/opencart/admin/index.php?route=common/home&token=45c48cce2e2d7fbdea1afc51c7c6ad26'" /></div>
    <div class="div2"><img src="view/image/lock.png" alt="" style="position: relative; top: 3px;" />&nbsp;Você está conectado como <span>admin</span></div>
  </div>
<div id="menu">
  <ul class="nav left" style="display: none;">
    <li id="dashboard"><a href="http://localhost/opencart/admin/index.php?route=common/home&token=45c48cce2e2d7fbdea1afc51c7c6ad26" class="top">Painel de controle</a></li>
    <li id="catalog"><a class="top">Catálogo</a>
      <ul>
        <li><a href="http://localhost/opencart/admin/index.php?route=catalog/category&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Categorias</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=catalog/product&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Produtos</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=catalog/manufacturer&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Fabricantes/Marcas</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=catalog/download&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Downloads</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=catalog/review&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Opiniões</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=catalog/information&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Informação</a></li>
      </ul>
    </li>
    <li id="extension"><a class="top">Extensões</a>
      <ul>
        <li><a href="http://localhost/opencart/admin/index.php?route=extension/module&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Módulos</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=extension/shipping&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Formas de envio</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=extension/payment&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Formas de pagamento</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=extension/total&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Total do pedido</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=extension/feed&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Feeds de produtos</a></li>
      </ul>
    </li>
    <li id="sale"><a class="top">Vendas</a>
      <ul>
        <li><a href="http://localhost/opencart/admin/index.php?route=sale/order&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Compras</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=sale/customer&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Clientes</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=sale/customer_group&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Grupos de cliente</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=sale/coupon&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Cupons</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=sale/contact&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Informativo</a></li>
      </ul>
    </li>
    <li id="system"><a class="top">Sistema</a>
      <ul>
        <li><a href="http://localhost/opencart/admin/index.php?route=setting/setting&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Configurações</a></li>
        <li><a class="parent">Usuários</a>
          <ul>
            <li><a href="http://localhost/opencart/admin/index.php?route=user/user&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Usuário</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=user/user_permission&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Grupos de usuários</a></li>
          </ul>
        </li>
        <li><a class="parent">Localização</a>
          <ul>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/language&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Idiomas</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/currency&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Moedas</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/stock_status&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Situação do estoque</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/order_status&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Situação das compras</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/country&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Países</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/zone&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Regiões</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/geo_zone&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Regiões geográficas</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/tax_class&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Classes de impostos</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/length_class&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Unidade de medida</a></li>
            <li><a href="http://localhost/opencart/admin/index.php?route=localisation/weight_class&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Unidade de peso</a></li>
          </ul>
        </li>
        <li><a href="http://localhost/opencart/admin/index.php?route=tool/error_log&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Registro de erros</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=tool/backup&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Backup/Restauração</a></li>
      </ul>
    </li>
    <li id="reports"><a class="top">Relatórios</a>
      <ul>
        <li><a href="http://localhost/opencart/admin/index.php?route=report/sale&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Vendas</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=report/viewed&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Produtos visualizados</a></li>
        <li><a href="http://localhost/opencart/admin/index.php?route=report/purchased&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Produtos comprados</a></li>
      </ul>
    </li>
    <li id="help"><a class="top">Ajuda</a>
      <ul>
        <li><a onclick="window.open('http://www.opencart.com');">Página inicial</a></li>
        <li><a onclick="window.open('http://www.opencart.com/index.php?route=documentation/introduction');">Documentação</a></li>
        <li><a onclick="window.open('http://forum.opencart.com');">Suporte</a></li>
      </ul>
    </li>
  </ul>
  <ul class="nav right">
    <li id="store"><a onclick="window.open('http://localhost/opencart/');" class="top">Frente da Loja</a>
      <ul>
              </ul>
    </li>
    <li id="store"><a class="top" href="http://localhost/opencart/admin/index.php?route=common/logout&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Sair</a></li>
  </ul>
  <script type="text/javascript"><!--
$(document).ready(function() {
	$('.nav').superfish({
		hoverClass	 : 'sfHover',
		pathClass	 : 'overideThisToUse',
		delay		 : 0,
		animation	 : {height: 'show'},
		speed		 : 'normal',
		autoArrows   : false,
		dropShadows  : false, 
		disableHI	 : false, /* set to true to disable hoverIntent detection */
		onInit		 : function(){},
		onBeforeShow : function(){},
		onShow		 : function(){},
		onHide		 : function(){}
	});
	
	$('.nav').css('display', 'block');
});
//--></script>
  <script type="text/javascript"><!-- 
function getURLVar(urlVarName) {
	var urlHalves = String(document.location).toLowerCase().split('?');
	var urlVarValue = '';
	
	if (urlHalves[1]) {
		var urlVars = urlHalves[1].split('&');

		for (var i = 0; i <= (urlVars.length); i++) {
			if (urlVars[i]) {
				var urlVarPair = urlVars[i].split('=');
				
				if (urlVarPair[0] && urlVarPair[0] == urlVarName.toLowerCase()) {
					urlVarValue = urlVarPair[1];
				}
			}
		}
	}
	
	return urlVarValue;
} 

$(document).ready(function() {
	route = getURLVar('route');
	
	if (!route) {
		$('#dashboard').addClass('selected');
	} else {
		part = route.split('/');
		
		url = part[0];
		
		if (part[1]) {
			url += '/' + part[1];
		}
		
		$('a[href*=\'' + url + '\']').parents('li[id]').addClass('selected');
	}
});
//--></script>
</div>
<div id="content">
<div class="breadcrumb">
    <a href="http://localhost/opencart/admin/index.php?route=common/home&token=45c48cce2e2d7fbdea1afc51c7c6ad26">Principal</a>
  </div>
<div class="warning">ATENÇÃO: A PASTA DE INSTALAÇÃO AINDA EXISTE!</div>
<div class="box">
  <div class="left"></div>
  <div class="right"></div>
  <div class="heading">
    <h1 style="background-image: url('view/image/home.png');">Painel De Controle</h1>
  </div>
  <div class="content">
    <div style="display: inline-block; width: 100%; margin-bottom: 15px; clear: both;">
      <div style="float: left; width: 49%;">
        <div style="background: #547C96; color: #FFF; border-bottom: 1px solid #8EAEC3; padding: 5px; font-size: 14px; font-weight: bold;">Visão geral</div>
        <div style="background: #FCFCFC; border: 1px solid #8EAEC3; padding: 10px; height: 180px;">
          <table cellpadding="2" style="width: 100%;">
            <tr>
              <td width="80%">Total de vendas:</td>
              <td align="right">$0,00</td>
            <tr>
              <td>Total de vendas no ano:</td>
              <td align="right">$0,00</td>
            </tr>
            <tr>
              <td>Total de compras:</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Nº de clientes:</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Clientes à espera de aprovação:</td>
              <td align="right">0</td>
            </tr>
            <tr>
              <td>Nº de produtos:</td>
              <td align="right">18</td>
            </tr>
            <tr>
              <td>Nº de opiniões:</td>
              <td align="right">3</td>
            </tr>
            <tr>
              <td>Opiniões à espera de moderação:</td>
              <td align="right">0</td>
            </tr>
          </table>
        </div>
      </div>
      <div style="float: right; width: 49%;">
        <div style="background: #547C96; color: #FFF; border-bottom: 1px solid #8EAEC3;">
          <div style="width: 100%; display: inline-block;">
            <div style="float: left; font-size: 14px; font-weight: bold; padding: 7px 0px 0px 5px; line-height: 12px;">Estatísticas</div>
            <div style="float: right; font-size: 12px; padding: 2px 5px 0px 0px;">Selecione o intervalo:              <select id="range" onchange="getSalesChart(this.value)" style="margin: 2px 3px 0 0;">
                <option value="day">Hoje</option>
                <option value="week">Esta semana</option>
                <option value="month">Este mês</option>
                <option value="year">Este ano</option>
              </select>
            </div>
          </div>
        </div>
        <div style="background: #FCFCFC; border: 1px solid #8EAEC3; padding: 10px; height: 49%;">
          <div id="report" style="width: 400px; height: 180px; margin: auto;"></div>
        </div>
      </div>
    </div>
    <div>
      <div style="background: #547C96; color: #FFF; border-bottom: 1px solid #8EAEC3; padding: 5px; font-size: 14px; font-weight: bold;">Últimas 10 compras</div>
      <div style="background: #FCFCFC; border: 1px solid #8EAEC3; padding: 10px;">
        <table class="list">
          <thead>
            <tr>
              <td class="right">Código da compra</td>
              <td class="left">Nome do cliente</td>
              <td class="left">Situação</td>
              <td class="left">Data da compra</td>
              <td class="right">Total</td>
              <td class="right">Ação</td>
            </tr>
          </thead>
          <tbody>
                        <tr>
              <td class="center" colspan="6">Sem resultados!</td>
            </tr>
                      </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!--[if IE]>
<script type="text/javascript" src="view/javascript/jquery/flot/excanvas.js"></script>
<![endif]-->
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.js"></script>
<script type="text/javascript"><!--
function getSalesChart(range) {
	$.ajax({
		type: 'GET',
		url: 'index.php?route=common/home/chart&token=45c48cce2e2d7fbdea1afc51c7c6ad26&range=' + range,
		dataType: 'json',
		async: false,
		success: function(json) {
			var option = {	
				shadowSize: 0,
				lines: { 
					show: true,
					fill: true,
					lineWidth: 1
				},
				grid: {
					backgroundColor: '#FFFFFF'
				},	
				xaxis: {
            		ticks: json.xaxis
				}
			}

			$.plot($('#report'), [json.order, json.customer], option);
		}
	});
}

getSalesChart($('#range').val());
//--></script>
</div></div>
<div id="footer">
  &copy; 2009-2011 Todos os direitos reservados.<br />Versão 1.4.9.1</div>
</body></html>