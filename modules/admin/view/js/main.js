//Evento load Principal
$(function(){
	MenuPrincipal.generate();
	
	MenuPrincipal.control_scroll();	

//	System.properties.add_interval_function(control_scroll);
//	System.events.init();
});

var MenuPrincipal = {
	/*Controla os eventos do menu*/
	generate : function (){
		/*	ddsmoothmenu.init({
			mainmenuid: "menu_principal", //menu DIV id
			orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
			classname: 'menu_principal', //class added to menu's outer DIV
			//customtheme: ["#1c5a80", "#18374a"],
			contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
		});
		*/
		
		//Inicializa todos itens escondidos
		$('#botao_menu_root .menu_sistema,.menu_sistema ul,.nivel1,.nivel2,.nivel3,.nivel4,.nivel5').each(function() {
			$(this).hide();	
		});
		
		/*Menu ROOT*/
		$('#botao_menu_root .root').click(function(e) {
			e.preventDefault();
			
			if($(this).is('.opened')){
				MenuPrincipal.close();
			}
			else{
				if($(this).is('.loading')) return;
				MenuPrincipal.open();
			}
		});
		
		//Botao Menu expansivo
		$('.menu_expand').button({
		    icons: {
		    	primary: "ui-icon-triangle-1-s"
			}
		})
		.click(function(e) {
			var $li = $(this).parent().parent('li');
			$li.find('ul:first').slideToggle(600, function(){
				if($li.find('ul:first').is(':visible')){
					$li.addClass('selected');
				}
				else{
					$li.removeClass('selected');
				}
			});
		});
		
		//Click no item
		$('.menu_sistema li a').click(function(e) {
			//Fecha o menu ao clicar no item
			$('#botao_menu_root .root').click();
			//Rola pro topo
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			
		});
		
		//Visual
		//$('.menu_sistema li a').button();
		$('.menu_sistema li .button_block').append('<div class="clear" />');
		
		
		//$('.menu_sistema li a').click(function(e) {
		//	$(this).parent('li').find('ul:first').slideToggle(600);
		//});
		
		//Atualizar Menu
		$('#atualizar_menu a').click(function(e) {
			e.preventDefault();
			
			MenuPrincipal.update();
		});
		
		//Maximizar Menu e Cadastrar nova opcao
		$('#menu_explorer a, #add_menu_item a').click(function(e) {
			//e.preventDefault();
			//Apenas fecha o menu e manda pro link
			MenuPrincipal.close();
		});

	}

	/**
	 * Controla a rolagem da pagina
	 * Fixa o menu de acordo com a rolagem
	 * */
	,control_scroll : function (){
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#menu_principal:not(.fixed)').addClass('fixed');
			} else {
				$('#menu_principal.fixed').removeClass('fixed');
			}
		});
		//console.log($('html, body').scrollTop());
	}

	/**
	 * Abre o submenu
	 * */
	,open : function (){
		var $menu = $('#botao_menu_root .menu_sistema');
		var $bt_root = $('#botao_menu_root .root');

		$menu.slideDown(600, function(){
			$bt_root.addClass('opened');
		});
	}
	/**
	 * Fecha o submenu
	 * */
	,close : function (){
		var $menu = $('#botao_menu_root .menu_sistema');
		var $bt_root = $('#botao_menu_root .root');

		$menu.slideUp(600, function(){
			$bt_root.removeClass('opened');
		});
	}
	/**
	 * Atualiza o menu
	 * */
	,update : function (){
		MenuPrincipal.close();
		$('#botao_menu_root .root').addClass('loading');
		
		$.ajax({
			url: System.properties.get_host() + 'system/get_menu_principal/'
			,type: 'GET'
//				,data: 'token=' +encodeURIComponent($('#token').val()) + '&itens=' + encodeURIComponent($input.val())
			,dataType: 'html'
			,success: function(result) {
				if(result && result.length > 0){
					$('#menu_principal').before(result).remove();
					MenuPrincipal.generate();
				}
				else{
					alert('N�o foi possivel atualizar o menu, a resposta veio nula.\nVoc� pode tentar atualizar a pagina.');
				}
			}
			,complete: function() {
				$('#botao_menu_root .root').removeClass('loading');
			}
		});
	}
};

