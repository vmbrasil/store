<?php
//error_reporting(E_ALL);ini_set('display_errors',true);

class ProdutoCompreJuntoDetalhe extends K13ControllerCadastroDetalhe{
	public function __construct(){//construtor
		$entidade = new ProdutoCompreJuntoENT();
//		$entidade->get_dao()->set_debug(1);

		$this->add_campo_mestre('id_produto','id_produto_pai');
		parent::__construct($entidade);
	}
	
	/**
	 * Intercepta antes de gerar a tela
	**/
	public function doBeforeGerar(){
		//$this->set_show_errors(true);
		//$this->get_entidade()->show_debug();
		
		$this->set_url_arquivo_consulta(HOST .'produto_consulta/');
//		$this->set_file_action(HOST .'produto_consulta/');
	}
	
}//fim class
?>