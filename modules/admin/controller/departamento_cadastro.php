<?php
//error_reporting(NULL);ini_set('display_errors',false);

/*Tipo de Movimentação de Conta*/
class Cadastro extends CadastroPadrao{
	public function __construct(){//construtor
		$entidade = new CategoriaLojaENT();
//		$entidade->get_dao()->set_debug(1);
		parent::__construct($entidade);
	}
	
	/**
	 * Intercepta antes de gerar a tela
	**/
	public function doBeforeEventAction(){
		$this->set_show_errors(true);
		//$this->get_entidade()->show_debug();
		
		$this->get_entidade()->set_nomeDescritivo('Departamento');
		$this->get_entidade()->set('departamento',true);
		$this->get_entidade()->get_campo('departamento')->set_visible(false);
		
		$this->set_url_arquivo_consulta(HOST .$this->get_module().'/' .'departamento_consulta/');
		$this->set_file_action(HOST .$this->get_module().'/' .'departamento_cadastro/');
	}
	
}//fim class

new Cadastro();
?>