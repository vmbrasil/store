<?php
/*Marca de Produto*/
class Cadastro extends CadastroPadrao{
	public function __construct(){//construtor
		$entidade = new MarcaENT();
//		$entidade->get_dao()->set_debug(1);
		parent::__construct($entidade);
	}
	
	
	/**
	 * Intercepta antes de gerar a tela
	**/
	public function doBeforeGerar(){
		$this->set_show_errors(true);
		//$this->get_entidade()->show_debug();
		
		$this->set_url_arquivo_consulta(HOST .$this->get_module().'/' .'marca_consulta/');
		$this->set_file_action(HOST .$this->get_module().'/' .'marca_cadastro/');
	}
	
}//fim class

new Cadastro();
?>