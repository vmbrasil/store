<?php
//error_reporting(NULL);ini_set('display_errors',false);
//error_reporting(E_ALL);ini_set('display_errors',true);

/*Tipo de Movimentação de Conta*/
class ProdutoLojaCadastro extends CadastroPadrao{
	public function __construct(){//construtor
		$entidade = new ProdutoLojaENT();
//		$entidade->get_dao()->set_debug(1);
		
		parent::__construct($entidade);
	}
	
	/**
	 * Intercepta antes de executar os eventos
	**/
	public function doBeforeEventAction(){
		$this->carregar();//Carrega os dados da entidade para carregar o config_status
		$this->gerar_abas();
	}
	
	
	/**
	 * Gera as abas
	**/
	protected function gerar_abas(){
		//Gera as abas
		$this->set_auto_generate_aba_mestre(false);//nao gera automatico
		
		$this->add_aba('aba_dados','Produto',null, 'Informe os dados principais do Produto');
		$this->add_aba('aba_valor','Preços');
		//$this->add_aba('aba_propridades','Propridades');
		$this->add_aba('aba_loja','Loja',null, 'Informe os dados se for necessário mostrar o produto na loja ou loja-virtual.');
		//$this->add_aba('aba_estoque','Estoque');
		$this->add_aba('aba_frete','Frete');
		$this->add_aba('aba_fotos','Fotos');
		
		$this->set_cadastro_detalhe(new ProdutoVideoDetalhe(), 'Videos', 'Informe e insira os videos do produto. Para inserir videos copie o endereço e cole o codigo no campo.');
		$this->set_cadastro_detalhe(new ProdutoCategoriaDetalhe(),'Categorias','Insira outras categorias relacionadas desse produto. Ao inserir o produto em outras categorias ele aparecerá nelas.');
		$this->set_cadastro_detalhe(new ProdutoRelacionadoDetalhe(),'Produtos Relacionados','Informe os produtos que são relacionados à este. Se nenhum produto for selecionado o sistema se encarrega de selecionar os produtos que achar mais convenientes.');
		$this->set_cadastro_detalhe(new ProdutoCompreJuntoDetalhe(),'Compre Junto','Informe os produtos ou acessorios que serão vendidos junto à este produto. O cliente poderá selecionar 2 ou mais produtos de uma vez para inserir em seu carrinho de compras de forma mais comoda. <br>Selecione os produtos que podem ser úteis se usados junto deste.');
		
		$this->add_aba('aba_config','Configurações',null,'Configurações da Tela deste produto, informe os dados que serão pedidos ou informados ao usuario na apresentação do produto.');
		$this->add_aba('aba_outros','Outros Dados');
		
		//Campos das abas
		$this->set_aba_padrao('aba_dados');
		
		//Preços
		$this->get_tab_container()->transferir_campos_to_aba('aba_dados', 'aba_valor', $this->get_entidade()->get_grupo_campos('valores'));
		
		//Dimensoes
		//$this->get_tab_container()->transferir_campos_to_aba('aba_dados', 'aba_propridades', $this->get_entidade()->get_grupo_campos('propriedade'));
		
		$this->get_tab_container()->transferir_campos_to_aba('aba_dados', 'aba_loja', $this->get_entidade()->get_grupo_campos('loja'));
		
		/*
		$this->add_campo_aba('aba_dimensoes', 'tam_x');
		$this->remove_campo_aba('aba_dados', 'tam_x');

		$this->add_campo_aba('aba_dimensoes', 'tam_y');
		$this->remove_campo_aba('aba_dados', 'tam_y');
		
		$this->add_campo_aba('aba_dimensoes', 'tam_z');
		$this->remove_campo_aba('aba_dados', 'tam_z');
		
		$this->add_campo_aba('aba_dimensoes', 'tamanho_unidade');
		$this->remove_campo_aba('aba_dados', 'tamanho_unidade');

		$this->add_campo_aba('aba_dimensoes', 'peso');
		$this->remove_campo_aba('aba_dados', 'peso');
		
		$this->add_campo_aba('aba_dimensoes', 'peso_unidade');
		$this->remove_campo_aba('aba_dados', 'peso_unidade');
		*/
		
		//Estoque
		/*
		$this->add_campo_aba('aba_estoque', 'estoque_ativo');
		$this->remove_campo_aba('aba_dados', 'estoque_ativo');

		$this->add_campo_aba('aba_estoque', 'estoque_quantidade');
		$this->remove_campo_aba('aba_dados', 'estoque_quantidade');

		$this->add_campo_aba('aba_estoque', 'estoque_minimo');
		$this->remove_campo_aba('aba_dados', 'estoque_minimo');

		$this->add_campo_aba('aba_estoque', 'estoque_subtrair');
		$this->remove_campo_aba('aba_dados', 'estoque_subtrair');

		$this->add_campo_aba('aba_estoque', 'estoque_disponivel_status');
		$this->remove_campo_aba('aba_dados', 'estoque_disponivel_status');
		
		$this->add_campo_aba('aba_estoque', 'estoque_sem_status');
		$this->remove_campo_aba('aba_dados', 'estoque_sem_status');
*/
		
		//Frete
		$this->get_tab_container()->transferir_campos_to_aba('aba_dados', 'aba_frete', $this->get_entidade()->get_grupo_campos('frete'));
		
		$this->get_tab_container()->transferir_campo_to_aba('aba_dados','aba_frete', $this->get_entidade()->get_campo('peso'));
		$this->get_tab_container()->transferir_campo_to_aba('aba_dados','aba_frete', $this->get_entidade()->get_campo('comprimento'));
		$this->get_tab_container()->transferir_campo_to_aba('aba_dados','aba_frete', $this->get_entidade()->get_campo('altura'));
		$this->get_tab_container()->transferir_campo_to_aba('aba_dados','aba_frete', $this->get_entidade()->get_campo('largura'));
/*		
		$this->add_campo_aba('aba_frete', 'frete_gratis');
		$this->remove_campo_aba('aba_dados', 'frete_gratis');
		*/
		
		//Fotos
		$this->add_campo_aba('aba_fotos', 'fotos');
		$this->remove_campo_aba('aba_dados', 'fotos');

		//Outros
		$this->add_campo_aba('aba_outros', 'visitas');
		$this->remove_campo_aba('aba_dados', 'visitas');

		$this->add_campo_aba('aba_outros', 'qtd_vendas');
		$this->remove_campo_aba('aba_dados', 'qtd_vendas');

		$this->add_campo_aba('aba_outros', 'datacad');
		$this->remove_campo_aba('aba_dados', 'datacad');

		$this->add_campo_aba('aba_outros', 'dataedit');
		$this->remove_campo_aba('aba_dados', 'dataedit');
		
		$this->add_campo_aba('aba_outros', 'id_usuario_cad');
		$this->remove_campo_aba('aba_dados', 'id_usuario_cad');

		$this->add_campo_aba('aba_outros', 'id_usuario_edit');
		$this->remove_campo_aba('aba_dados', 'id_usuario_edit');
		
		$this->add_campo_aba('aba_outros', 'ativo');
		$this->remove_campo_aba('aba_dados', 'ativo');
		
	}
	
	/**
	 * Intercepta antes de gerar a tela
	**/
	public function doBeforeGerar(){
		$this->set_show_errors(true);
		//$this->get_entidade()->show_debug();
		
		$this->set_url_arquivo_consulta(HOST .$this->get_module().'/produto_consulta/');
		$this->set_file_action(HOST .$this->get_module().'/produto_cadastro/');
		
		$this->get_entidade()->get_campo('como_material')->set_visible(false);
		$this->get_entidade()->get_campo('como_consumo')->set_visible(false);
		
		//$this->get_entidade()->set_visible_group('como_consumo', false)
	}
	
}//fim class

new ProdutoLojaCadastro();
?>