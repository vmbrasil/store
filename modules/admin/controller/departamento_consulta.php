<?php
//error_reporting(NULL);ini_set('display_errors',false);

/*Departamentos da Loja*/
class  Consulta extends ConsultaPadrao{
	public function __construct(){//construtor
		$entidade = new CategoriaLojaENT();
//		$entidade->get_dao()->set_debug(1);
		parent::__construct($entidade);
	}

	/**
	 * Intercepta antes de gerar a tela
	**/
	public function doBeforeGerar(){
		$this->get_entidade()->set_PkVisible(true);

		$this->get_entidade()->set_nomeDescritivo('Departamento');
		$this->get_entidade()->set('departamento',true);
		$this->get_entidade()->get_campo('departamento')->set_visible(false);
				
		$this->set_url_arquivo_cadastro(HOST .$this->get_module().'/' .'departamento_cadastro/');
		
		$this->set_show_errors(true);
	}

}//fim class
new Consulta();
?>