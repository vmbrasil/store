<?php
//FW Main Page
class PageAdminMeuComercio extends K13MainFrontPageController{
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		parent::doBeforeShow();
		
		//Template Padrao
		$this->set_template(K13Path::getDirFramework() .'modules/system/view/index.tpl');
		
		//Funcionamento especifico
		$this->addJsFileToHead(K13Path::getHost() .'modules/admin/view/js/main.js');
		$this->addCssFileToHead(K13Path::getHost() .'modules/admin/view/css/main.css');
	}
}

new PageAdminMeuComercio();
?>