<?php
//error_reporting(NULL);ini_set('display_errors',false);

/* Departamento ou categoria da loja / produto */
class Cadastro extends CadastroPadrao{
	public function __construct(){//construtor
		$entidade = new CategoriaLojaENT();
//		$entidade->get_dao()->set_debug(1);
		parent::__construct($entidade);
	}
	
	
	/**
	 * Intercepta antes de executar os eventos
	**/
	public function doBeforeEventAction(){
		$this->set_cadastro_detalhe(new CategoriaFilhaDetalhe(),'Categorias Relacionadas', 'Informe as Categorias Filhas (sub-categorias) que pertencem � esta.');
	}
	
	
	/**
	 * Intercepta antes de gerar a tela
	**/
	public function doBeforeGerar(){
		$this->set_show_errors(true);
		//$this->get_entidade()->show_debug();
		
		$this->set_url_arquivo_consulta(HOST .$this->get_module().'/' .'categoria_loja_consulta/');
		$this->set_file_action(HOST .$this->get_module().'/' .'categoria_loja_cadastro/');
	}
	
}//fim class

new Cadastro();
?>