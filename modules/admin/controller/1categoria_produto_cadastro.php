<?php
require_once('../controller/autoload.php');
	K13Path::setSubDir('../../');
	K13Path::setDirFramework('../../framework_k13_v1.0/');
	$smart = K13Path::getFullDirFramework() ."terceiros/smarty/Smarty.class.php";
	if(file_exists($smart))include_once($smart);//Carrega o Smarty

class NoticiaCadastro extends K13ViewCadastro{
	public function __construct(){//construtor
		$entidade = new CategoriaProdutoENT();
//		$entidade->get_dao()->set_debug(1);
		parent::__construct($entidade);
	}
	
	/**
	 * Intercepta antes de gerar a tela
	**/
	public function doBeforeGerar(){
		$this->set_template('../' .K13Path::getFullDirFramework() .'/view/cadastro.tpl');
		$this->get_objSmarty()->compile_dir = K13Path::getFullDirFramework() .'/view/templates_c';
		
		$this->set_url_arquivo_consulta('categoria_produto_consulta.php');
		
		$this->set_show_errors(true);
	}
	
	/**
	 * Intercepta antes de gerar a tela
	**/
/*	public function doBeforeGerar(){
		$this->get_entidade()->get_campo('teste2')->set_visible(false);
		
		$this->set_url_arquivo_consulta('teste_consulta.php');
		
		//Teste
//		$this->get_entidade()->get_dao()->create_table();
		
		$this->set_show_errors(true);
		//$this->objEntidade->show_debug();
	}
*/
}//fim class

new NoticiaCadastro();
?>
