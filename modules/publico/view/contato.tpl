
<div class="ContentNews">
	<div class="ContentNewsMain">
		<div class="TituliSessao">Fale conosco</div>
	</div>
</div>

<div class="ContentMain">
	<div class="contato">
		<h2>Tem alguma id�ia, d�vida ou sugest�o? Vamos conversar!</h2>
	 	<div class="column_formulario">
			{if $msg_erro != ''}
			<div class="msg_erro" >
				<h3>Ocorreu um erro ao enviar o e-mail:</h3>
				<ul> 
				{$msg_erro}
				</ul>
			</div>
			{elseif $msg_sucesso != ''}
			<div class="msg_sucesso">
				{$msg_sucesso}
			</div>
			{/if}

			{if $msg_sucesso == '' or $msg_erro != ''}
			<form name="formulario" action="{$HOST}contato" method="post">
				<fieldset>
					<legend>Nome:</legend>
					<input name="nome" id="nome" type="text" value="{$smarty.post.nome|default:''}" \>
				</fieldset>

				<fieldset>
					<legend>E-mail:</legend>
					<input name="email" id="email" type="text" value="{$smarty.post.email|default:''}" \>
				</fieldset>

				<fieldset>
					<legend>Telefone:</legend>
					<input name="telefone" id="telefone" type="text" value="{$smarty.post.telefone|default:''}" \>
				</fieldset>

				<span id="box_cidade">
					<fieldset>
						<legend>Cidade:</legend>
						<input name="cidade" id="cidade" type="text" value="{$smarty.post.cidade|default:''}" \>
					</fieldset>
				</span>

				<span id="box_uf">
					<fieldset>
						<legend>UF:</legend>
						<select name="uf" id="uf">
							<option value="AC">AC</option><option value="AL">AL</option>
							<option value="AP">AP</option><option value="AM">AM</option>
							<option value="BA">BA</option><option value="CE">CE</option>
							<option value="DF">DF</option><option value="ES">ES</option>
							<option value="GO">GO</option><option value="MA">MA</option>
							<option value="MT">MT</option><option value="MS">MS</option>
							<option value="MG">MG</option><option value="PA">PA</option>
							<option value="PB">PB</option><option value="PR">PR</option>
							<option value="PE">PE</option><option value="PI">PI</option>
							<option value="RJ">RJ</option><option value="RN">RN</option>
							<option value="RO">RO</option><option value="RR">RR</option>
							<option value="RS">RS</option><option value="SC">SC</option>
							<option value="SP">SP</option><option value="SE">SE</option>
							<option value="TO">TO</option>
						</select>
					</fieldset>
				</span>

				<fieldset>
					<legend>Tipo do Contato:</legend>
					<select name="tipo" id="tipo">
						<option value="0" disabled="disabled" selected="selected">&nbsp;</option>
						<option value="1">Atendimento ao Usu�rio</option>
						<option value="4">Canal direto com a diretoria</option>
						<option value="8">Centro de Treinamento</option>
						<option value="3">Comunica��o exclusiva para revendas</option>
						<option value="5">Contas P�blicas</option>
						<option value="2">R.H. - Sele��o  de pessoal</option>
						<option value="6">Sugest�es para o Informativo</option>
						<option value="7">Sugest�es para o Site</option>
					</select>
				</fieldset>

				<fieldset>
					<legend>Mensagem:</legend>
					<textarea name="mensagem" rows="10" id="mensagem" cols="10">{$smarty.post.mensagem|default:''}</textarea>
				</fieldset>
				
				<input id="enviar" value="Enviar" title="Enviar" type="submit">
			</form>
			{/if}
		</div><!-- fim column_formulario -->
	</div><!-- fim contato -->
</div><!-- fim ContentMain -->