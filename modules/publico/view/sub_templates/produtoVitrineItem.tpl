{*foreach from=$vitrine item=vitritem*}
<div class="vitrine">
	<h2>{$vitrine_title}</h2>
	<hr>
	{if $vitrine_produtos|@sizeof > 0}
		<ul>
		{foreach from=$vitrine_produtos item=produto}
			<li>
				{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
			</li>
		{/foreach}
		</ul>
	{else}
		<p>Nenhum Produto Encontrado!</p>
	{/if}
</div>

			<!-- BARRA DE PAGINA��O -->
			<div class="janela paginacao">
					<div style="float: left">18 de 28 produtos</div>
					<ul>
						<li><a href="#" class="selecionado">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">Pr�xima</a></li>
						<li><a href="#">Ultima</a></li>
					</ul>
			</div><!-- Fim Paginacao -->

{/foreach}