		<aside class="menu_direito">

			<!-- ======================= LISTA TOP 5=========================== -->
			{if $produtos_top5.produtos|@count > 0 and $produtos_top5.produtos != false}
			<div class="menu_box" id="menu_top5">
				<a href="{$HOST}produtos/top/"><h3 class="menu_topo_normal">Produtos TOP 5</h3></a>
				<ul class="top5">
					{foreach $produtos_top5.produtos as $produto name=array_item}
					<li {if $smarty.foreach.array_item.iteration == 1}class="first"{/if}>
						{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
						<div class="ordem">{$smarty.foreach.array_item.iteration}</div>
					</li>
					{/foreach}
				</ul>
				<div style="clear: both;"></div>
			</div>
			{/if}

			<!-- =======================MENU NEWSLETTER=========================== -->
			<div class="menu_box" id="menu_newsletter">
				<h3 class="menu_topo_normal">Receba Ofertas</h3>
				<form action="{$HOST}newsletter/" method="post" class="form_newsletter">
					<span>
						<input name="" type="text" value="Seu Nome" title="Nome" size="15">
						<input name="" type="text" value="Seu E-mail" title="E-mail" size="15">
					</span>
					<button name="" type="submit" class="newsletter_enviar">OK</button>
					<div class="clear"></div>
				</form>
				
				<ul class="lista_bt_share">
					<li><a href="{$HOST}rss/" class="rss">Assine nosso RSS</a></li>
					<li><a href="http://www.twitter.com" class="twitter">Siga-nos no twitter</a></li>
					<li><a href="http://www.facebook.com" class="facebook">Curta no facebook</a></li>
				</ul>

			</div>
	

			<!-- ======================= LISTA BANNERS LATERAIS=========================== -->
			<div class="menu_box" id="menu_banners_laterais">
				<ul class="lista_banners">
					{foreach $banners_laterais as $banner}
					<li>
						{if $banner.url}<a href="{$HOST}{$banner.url}/{$banner.description}/" title="{$banner.description}">{/if}
							<img src="{$HOST}{$banner.foto_url|default:$no_image}" alt="Link para {$banner.description}" width="158">
						{if $banner.url}</a>{/if}
					</li>
					{/foreach}
				</ul>
			</div>

		</aside><!-- Fim menu Direito -->
