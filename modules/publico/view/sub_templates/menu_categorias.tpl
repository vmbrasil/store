	<div class="menu_categorias">
	<!-- =======================MENU DE DEPARTAMENTOS E CATEGORIAS=========================== -->
		{if $menu_categoria.categorias|sizeof > 0}
		<nav class="menu_box" id="menu_categorias">
			<h3 class="menu_topo">{$menu_categoria.titulo|default:'Categorias'}</h3>
			<ul class="list">
				{foreach from=$menu_categoria.categorias item=categoria}
				<li>
					<a href="{$HOST}categoria/{$categoria.slug}/">{$categoria.title}
						{if $categoria.product_count}({$categoria.product_count}){/if}
					</a>
					{if $categoria.subcategorias|@count > 0}
					<ul>
						{foreach from=$categoria.subcategorias item=subcategoria}
						<li>
							<a href="{$HOST}categoria/{$subcategoria.slug}/">{$subcategoria.title}
								{if $subcategoria.product_count}({$subcategoria.product_count}){/if}
							</a>
						</li>
						{/foreach}
					</ul>
					{/if}
				</li>
				{/foreach}
				<li id="menu_view_all_category">
					<a href="{$HOST}categorias/">Mais Categorias</a>
				</li>
			</ul>
		</nav>
		{/if}



		<!-- =======================MENU MARCAS=========================== -->
		{if $menu_marcas|@count > 0}
		<section class="menu_box" id="menu_marcas">
			<h3 class="menu_topo_normal">Grandes Marcas</h3>
			<ul>
			{foreach $menu_marcas as $marca}
				<li>
					<a href="{$HOST}marcas/{$marca.id}/">
						<img src="{$HOST}{$marca.foto_url|default:$no_image}" alt="Marca {$marca.title}">
					</a>
				</li>
			{/foreach}
			</ul>
		</section>
		{/if}


		<!-- =======================MENU VEJA MAIS=========================== -->
		<section class="menu_box" id="menu_veja_mais">
			<h3 class="menu_topo_normal">Veja Mais</h3>
			<ul>
				<li><a href="{$HOST}produtos/destaques/">		<span class="mais">mais</span> Destaques</a></li>
				<li><a href="{$HOST}produtos/ofertas/">			<span class="mais">mais</span> Ofertas</a></li>
				<li><a href="{$HOST}produtos/lancamentos/">		<span class="mais">mais</span> Lan�amentos</a></li>
				<li><a href="{$HOST}produtos/sugeridos/">		<span class="mais">mais</span> Sugeridos</a></li>
				<li><a href="{$HOST}promocoes/">				<span class="mais">mais</span> Promo��es</a></li>
				<li><a href="{$HOST}produtos/mais-vendidos/">	<span class="mais">mais</span> Vendidos</a></li>
				<li><a href="{$HOST}produtos/mais-visitados/">	<span class="mais">mais</span> Visitados</a></li>
				<li><a href="{$HOST}produtos/mais-baratos/">	<span class="mais">mais</span> Baratos</a></li>
				<li><a href="{$HOST}produtos/mais-qualificados/"><span class="mais">mais</span> Qualificados</a></li>
				<li><a href="{$HOST}marcas/">					<span class="mais">mais</span> Marcas</a></li>
				<li><a href="{$HOST}produtos/frete-gratis/">	<span class="mais">mais</span> Frete Gr�tis</a></li>
			</ul>
		</section>



		<!-- =======================MENU INFORMAC�ES=========================== -->
		<section class="menu_box" id="menu_informacoes">
			<h3 class="menu_topo_normal">Informa��es</h3>
			<ul>
				<li><a href="{$HOST}ajuda/como-comprar/">Como Comprar</a></li>
				<li><a href="{$HOST}ajuda/devolucoes/">Fretes e Devolu��es</a></li>
				<li><a href="{$HOST}institucional/termos/">Politica de Privacidade</a></li>
				<li><a href="{$HOST}usuario/pedidos/">Rastrear Pedidos</a></li>
				<li><a href="{$HOST}site/mapa/">Mapa do Site</a></li>
				<li><a href="{$HOST}institucional/fornecedor/">Fornecedor</a></li>
				<li><a href="{$HOST}institucional/revendas/">Revendas</a></li>
				<li><a href="{$HOST}site/contato/">Fale Conosco</a></li>
				<li><a href="{$HOST}produtos/catalogo/">Baixar Cat�logo</a></li>
			</ul>
		</section>
		



	</div><!-- Fim menus esquerdo-->
