	<!-- Lista de Categorias -->
	{if $categorias|@sizeof > 0 and $categorias != false}
    <ul class="lista_categorias">
	{foreach from=$categorias item=item}
		<li>
			<a href="{$HOST}categoria/{$item.slug}/">{$item.title}</a>
			
			{if $item.subcategorias|@sizeof > 0 and $item.subcategorias != false}
			    <ul>
				{foreach from=$item.subcategorias item=subitem}
					<li>
						<a href="{$HOST}categoria/{$subitem.slug}/">{$subitem.title}</a>
					</li>
				{/foreach}
				</ul>
			{/if}
			
		</li>
	{/foreach}
	</ul>
	{/if}
