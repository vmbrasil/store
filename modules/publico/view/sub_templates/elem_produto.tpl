<section class="hproduct box_produto" id="product_{$produto.id}">
	<a href="{$HOST}produto/{$produto.slug}/" title="Produto {$produto.title}" class="detalhes" rel="product">
		<img src="{$produto.foto.mini|default:($HOST|cat:$no_image)}" class="foto product-thumb img_loader" alt="Foto do produto {$produto.title}">
		<h3 class="titulo product-title">{$produto.title}</h3>
	</a>
	
	<a href="{$HOST}categoria/{$produto.categoria.slug}/" class="categoria product-type" title="Categoria: {$produto.categoria.title}"><span>+</span>{$produto.categoria.title}</a>
	<a href="{$HOST}marca/{$produto.marca.id}" class="brand marca" title="Marca: {$produto.marca.title}"><span>Marca:</span>{$produto.marca.title}</a>
	<div class="qualificacao"><span class="q_value">{$produto.qualificacao.value}</span><span class="q_count">{$produto.qualificacao.count}</span></div>
	{if $produto.preco_anterior > $produto.preco}
	<div class="preco_anterior">De: <abbr class="currency" title="BRR" lang="pt-br">R$</abbr> <strong class="value amount currency money">{$produto.preco_anterior|default:'0,00'|number_format:2:",":"."}</strong></div>
	{/if}
	<div class="preco value price sale"><abbr class="currency" title="Real BR" lang="pt-br">R$</abbr> <strong class="value amount currency money">{$produto.preco|default:'0,00'|number_format:2:",":"."}</strong></div>
	<div class="parcela">ou <strong class="count">{$produto.parcela.max_count}x</strong> de <abbr class="currency" title="Real BR" lang="pt-br">R$</abbr> <strong class="value amount currency money">{$produto.parcela.value|default:'0,00'|number_format:2:",":"."}</strong></div>
	{if $produto.parcela.juros != ''}<div class="juros {if $produto.parcela.juros == 'sem'}sem_juros{/if}"><strong class="value">{$produto.parcela.juros}</strong> juros</div>{/if}
	{if $produto.frete.gratis}<div class="frete_gratis" title="Produto com frete GR�TIS para todas regi�es">frete gr�tis</div>{/if}
	<a href="{$HOST}produto/{$produto.slug}/" class="detalhes bt_detalhes" rel="product" title="Detalhes do produto {$produto.title}">Ver</a>
	<a href="{$HOST}produto/{$produto.slug}/" class="comprar bt_comprar" rel="product" title="Adicionar ao carrinho">Comprar</a>
</section>