<!-- CONTEUDO PRINCIPAL -->
<article id="cart">
	<section class="janela">
		<h2 class="subtitulo">Meu Carrinho</h2>
	
		<!-- Etapas -->
		<div class="cart_etapa etapa-1">
			<ol>
				<li class="atual">Carrinho</li>
				<li>Identifica��o</li>
				<li>Transporte</li>
				<li>Pagamento</li>
				<li>Finaliza��o</li>
			</ol>
			<div style="clear: both"></div>
		</div>

	{if $carrinho.itens|count > 0}
	
		<!-- Botoes -->
		{capture name=car_botoes}
		<div class="car_botoes">
			<a class="cart-bt-continuar-compra" title="Comprar mais produtos" href="{$HOST}vitrine/">Continuar comprando</a>
			<input type="button" title="Atualizar Modifica��es" class="car-bt-atualizar" value="Atualizar Carrinho">
			<a class="cart-bt-avancar" title="Pr�xima Etapa" href="{$HOST}carrinho/transporte/">Avan�ar Compra</a>
		</div>
		{/capture}
		{$smarty.capture.car_botoes}
		
		<!-- Produtos -->
		<table cellspacing="0" cellpadding="5" border="0" width="100%" class="cart_tab_produtos">
			<thead>
			<tr>
				<th width="50%">Produto</th>
				<th>Quantidade</th>
	
				<th>Valor Unit�rio</th>
				<th colspan="2">Valor Total</th>
			</tr>
			</thead>
			        
			<tbody>
			{foreach $carrinho.itens as $item}
			<tr class="item" data-pid="{$item.codigo}">
				<td height="80">
					<img class="foto" height="64" width="64" src="{$item.produto.foto.super_mini|default:($HOST|cat:$no_image)}">
					<a class="title" href="{$HOST}produto/{$item.produto.slug}/">{$item.produto.title}</a>
					{if $item.produto.categoria.title != ''}<p class="categoria">Categoria: {$item.produto.categoria.title}</p>{/if}
					{if $item.produto.marca.title != ''}<p class="marca">Marca: {$item.produto.marca.title}</p>{/if}
				</td>
				<td>
					<div class="quantidade">
						<label for="item_qtd{$item.codigo}">Quantidade do Produto</label><input type="text" class="input input_qtd" data-pid="{$item.codigo}" id="item_qtd{$item.codigo}" data-qtd="{$item.qtde}" maxlength="6" size="8" value="{$item.qtde}" name="txtqtd">
					</div>
				</td>
				<td>
					<div class="value">R$ {$item.produto.preco|number_format:2:",":"."}</div>
				</td>
				<td>
					<div class="subtotal_unit currency_format_real" data-pid="{$item.codigo}">R$ {$item.subtotal|number_format:2:",":"."}</div>
				</td>
				<td>
					<form method="post" action="{$HOST}carrinho/">
						<input type="hidden" value="rem" name="exec">
						<input type="hidden" value="{$item.codigo}" name="pid">
						<button type="submit" title="Retirar Item do carrinho" class="cart_remove_item" value="Retirar" data-pid="{$item.codigo}">Retirar</button>
					</form>
				</td>
			</tr>
			{/foreach}
		</tbody></table>
		
		<!-- Subtotal -->
		<div class="cart_subtotal">
			<form method="post" action="{$HOST}carrinho/">
				<input type="hidden" value="clear" name="exec">
				<button type="submit" class="cart_remove_all" title="Limpar todos itens do carrinho" value="Limpar" name="btLimpar">Limpar</button>
			</form>
			<span>(Subtotal) <strong class="currency_format_real">R$ {$carrinho.subtotal|number_format:2:",":"."}</strong></span>
			<div class="clear"></div>
		</div>
		
		<!-- Descontos -->
		<fieldset class="cart_valedesconto">
			<legend>Desconto</legend>
	
			<form method="post" action="{$HOST}carrinho/vale-desconto/">
				<label for="txt_desconto">
					Digite o n�mero do seu Vale-Desconto, Vale-Compra<br>
					ou Cupom e clique no bot�o OK<br>
				</label>

				<input type="text" class="input" id="txt_desconto" name="desconto" maxlength="20" value="{$carrinho.cod_desconto}">
				<button type="button" name="">OK</button>
			</form>
	
			<div class="cart_valedesconto_subtotal">
				<p>Vale-Presente, Vale-Compra ou Cupom</p>
				<strong class="currency_format_real">(R$ {$carrinho.valor_desconto|number_format:2:",":"."})</strong>
			</div>
		</fieldset>
		
		
		<!-- Transporte -->
		<fieldset class="cart_cep">
			<legend>Frete</legend>
			
			<form method="post" action="{$HOST}carrinho/cep/">
				<label for="txt_frete">Informe o seu CEP para calcular o frete<br>
					e clique no bot�o OK<br>
					<a href="#">N�o sei meu CEP</a>
				</label>
				
				<input type="text" class="input" id="txt_frete" name="frete" maxlength="8" value="{$carrinho.cep}">
				<button type="button" name="">OK</button>
			</form>
			
			<div class="cart_cep_subtotal">
				<p>Frete</p>
				<strong class="currency_format_real">(R$ {$carrinho.valor_frete|number_format:2:",":"."}) <br>� Calcular</strong>
			</div>
		</fieldset>
	
	
		<!-- Total -->
		<div class="cart_total">Valor Total	<strong class="currency_format_real">R$ {$carrinho.total|number_format:2:",":"."}</strong> </div>
		
		<!-- Possibilidade Parcelamento -->
		<div class="cart_parcelamento">ou em at� <strong class="parcelas">{$carrinho.parcelas}x</strong> de <strong class="valor currency_format_real">R$ {$carrinho.parcelas_valor|number_format:2:",":"."}</strong> <span class="juros">{$carrinho.parcelas_juros}</span></div>
		
		<!-- Hidden Atualizar Cart -->
		<form action="{$HOST}carrinho/" method="post" id="formEdit" name="formEdit">
			<input type="hidden" value="upd" name="exec">
			<input type="hidden" value="" id="qtd" name="qtd">
			<input type="hidden" value="" id="pid" name="pid">
		</form>


		<!-- Botoes -->
		{$smarty.capture.car_botoes}

		<!-- Dica -->
		<p class="msg_mensagem">
			Para finalizar seu pedido, confira a lista acima o(s) produto(s) escolhido(s) por voc�, ent�o clique no bot�o "Finalizar Compra", voc� ser� encaminhado para as etapas de conclus�o do pedido.
			Se desejar alterar a quantidade de algum produto, basta digitar o novo valor no campo "Quantidade".
			Para remover/excluir algum produto do pedido clique no bot�o "Excluir" � direita do produto correspondente.
		</p>
	


	{else}
		<!-- Carrinho Vazio -->
		<div class="carrinho_vazio">
			<h3>Seu carrinho de compras est� vazio!</h3>
			<p>Para inserir produtos no seu carrinho navegue pelos departamentos ou utilize a busca do site. Ao encontrar os produtos desejados, clique no bot�o "Comprar".</p>
			<div class="car_botoes">
				<a class="cart-bt-continuar-compra" title="Comprar mais produtos" href="{$HOST}vitrine/">Continuar comprando</a>
			</div>
		</div>
	{/if}
	</section><!-- Fim janela -->
</article>