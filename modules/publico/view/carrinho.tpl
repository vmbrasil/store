{literal}
<script language="javascript">
    function atualizar_carrinho(){
        var pid_retorno = "";
        var qtd_retorno = "";
		var array_pid = new Array(1);
		var array_qtd = new Array(1);
		array_pid[0] = 14;
		array_qtd[0] = document.getElementById("txtqtd14");

						
		for(i=0; i&lt;array_pid.length; i++){
			pid_retorno += "" + array_pid[i];
			if (i&lt;array_pid.length-1)pid_retorno += "||";

			qtd_retorno += "" + array_qtd[i].value;
			if (i&lt;array_qtd.length-1)qtd_retorno += "||";
		}
					
		document.getElementById("pid").value += pid_retorno;
		document.getElementById("qtd").value += qtd_retorno;
		document.getElementById("formEdit").submit();
	}
</script>
{/literal}		
<div class="janela">
	<div style="float: left;"><h2 class="subtitulo">Meu Carrinho</h2></div>

	<div class="car_etapa">
		<ol>
			<li class="atual">Carrinho</li>
			<li>Identifica��o</li>
			<li>Transporte</li>
			<li>Pagamento</li>
			<li>Finaliza��o</li>
		</ol>
	</div>
	<hr>
{if $carrinho.itens|count > 0}

	{capture name=car_botoes}
	<div class="car_botoes">
		<a class="car-bt-continuar-compra" title="Comprar mais produtos" href="{$HOST}vitrine/">Continuar comprando</a>
		<input type="button" onclick="atualizar_carrinho()" title="Atualizar Modifica��es" class="car-bt-atualizar" value="Atualizar Carrinho" name="btAtualizar">
		<a class="car-bt-finalizar-compra" title="Finalizar Compra" href="{$HOST}carrinho/trasporte/">Finalizar Compra</a>
	</div>
	{/capture}
	{$smarty.capture.car_botoes}
		
	<table cellspacing="0" cellpadding="5" border="0" width="100%" class="car_tab_produtos">
		<thead>
		<tr>
			<th width="50%">Produto</th>
			<th>Quantidade</th>

			<th>Valor Unit�rio</th>
			<th colspan="2">Valor Total</th>
		</tr>
		</thead>
		        
		<tbody>
		{foreach $carrinho.itens as $item}
		<tr>
			<td height="80">
				<img height="64" width="64" style="margin-right: 10px; float: left;" src="{$item.produto.thumb_url}">
				<a href="{$HOST}produto/{$item.produto.id}/{$item.produto.titulo}/">{$item.produto.titulo}</a> <p>{$item.produto.marca_descricao}</p>
			</td>
			<td>
				<div align="center">
					<input type="text" class="car_input_qtd" maxlength="6" size="8" value="{$item.qtde}" id="txtqtd14" name="txtqtd">
				</div>
			</td>
			<td> <div class="car_preco_unit">
				
				R$ {$item.produto.precov|number_format:2:",":"."}</div>
			</td>

			<td><div class="car_total_unit">R$ {($item.produto.precov*$item.qtde)|number_format:2:",":"."}</div></td>
			<td>
				<form method="post" action="{$HOST}carrinho/">
					<input type="hidden" value="rem" name="exec">
					<input type="hidden" value="14" name="pid">
					<input type="submit" onclick="return confirm('Deseja realmente remover este item?')" title="Retirar Item do carrinho" class="car_remove_item" value="Retirar" name="btremover">
				</form>
			</td>
		</tr>
		{/foreach}

		<form action="{$HOST}carrinho/" method="post" id="formEdit" name="formEdit"></form>
			<input type="hidden" value="upd" name="exec">
			<input type="hidden" value="" id="qtd" name="qtd">
			<input type="hidden" value="" id="pid" name="pid">
	</tbody></table>

	<div class="car_subtotal">
		<form method="post" action="{$HOST}carrinho/">
			<input type="hidden" value="clear" name="exec">
			<input type="submit" onclick="return confirm('ATEN��O!\nVoc� est� prestes a remover todos os itens do carrinho.\nAo confirmar n�o ser� possivel desfazer a a��o.\n\nDeseja realmente remover todos produtos do carrinho?')" title="Limpar todos itens do carrinho" class="car_remove_item" value="Limpar" name="btLimpar">
		</form>
		<span>(Subtotal) <strong>R$ {$carrinho.subtotal|number_format:2:",":"."}</strong></span>
		<div class="clear"></div>
	</div>

	<div class="car_valedesconto">

		<fieldset>
			<legend>Vale-Desconto, Vale-Compra ou Cupom: </legend>

			<label>
				Digite o n�mero do seu Vale-Presente, Vale<br>
				compra ou Cupom e clique no bot�o OK<br>
			</label>

			<form method="post" action="dgdsfg">
				<input type="text" class="car_input" maxlength="20" name="desconto" value="{$carrinho.cod_desconto}">
				<input type="button" value="OK" name="">
			</form>
		</fieldset>

		<div class="car_valedesconto_subtotal">
			<p>Vale-Presente,
			Vale-Compra ou Cupom</p>
			<strong>(R$ {$carrinho.valor_desconto|number_format:2:",":"."})</strong>
		</div>
	</div>


	<div class="car_cep">
		<fieldset>
			<legend>Calcular Valor do Frete:</legend>
			<label>Digite o CEP e clique no bot�o OK<br>
				<a href="#">N�o sei meu CEP</a>
			</label>

			<form method="post" action="{$HOST}carrinho/">
				<input type="text" class="car_input" maxlength="8" name="frete" value="{$carrinho.cep}">
				<input type="button" value="OK" name="">
			</form>
		</fieldset>

		<div class="car_cep_subtotal">
			<p>Frete</p>
			<strong>(R$ {$carrinho.valor_frete|number_format:2:",":"."}) <br>� Calcular</strong>
		</div>
	</div>

	<!-- Total -->
	<div class="car_total">Valor Total	 <strong>R$ {$carrinho.total|number_format:2:",":"."}</strong> </div>

	<div class="car_parcelamento">Ou em at� 12x de R$ {($carrinho.total/12)|number_format:2:",":"."} </div>
			
	<p class="msg_mensagem">
		Para continuar seu pedido, confira na lista acima o(s) produto(s) escolhido(s) por voc�. Se desejar alterar a quantidade de algum produto, basta digitar o novo valor no campo "Quantidade" e depois clicar no bot�o "Atualizar". Para remover/excluir algum produto do pedido clique no link "Excluir" do produto correspondente.
	</p>

			
	{$smarty.capture.car_botoes}
{else}
	<div class="carrinho_vazio">
		<h2>Seu carrinho de compras est� vazio!</h2>
		<p>Para inserir produtos no seu carrinho navegue pelos departamentos ou utilize a busca do site. Ao encontrar os produtos desejados, clique no bot�o "Comprar".</p>
		<div class="car_botoes">
			<a class="car-bt-continuar-compra" title="Comprar mais produtos" href="{$HOST}vitrine/">Continuar comprando</a>
		</div>
	</div>
{/if}
</div><!-- Fim janela -->