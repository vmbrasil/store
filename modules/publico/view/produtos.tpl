	<!--Menu ESQUERDO -->
	{include file="modules/publico/view/sub_templates/menu_categorias.tpl"}

	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<!-- DirBar -->
		{if $barra_diretorio|@sizeof > 0}
		<div class="diretorio_bar">
	    	<ul>
			{foreach from=$barra_diretorio item=diritem}
				<li><a href="{$diritem.url}">{$diritem.label}</a></li>
			{/foreach}
			</ul>
		</div>
		{/if}
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
		
			<ul>
				<li>
					<a href="#">Departameto1</a>
					<div class="destaque">
						Aqui vai o produto destaque do departamento selected
					</div>
					<ul>
						<li><a href="#">Categoria 1</a></li>
						<li><a href="#">Categoria 2</a></li>
						<li><a href="#">Categoria 3</a></li>
						<li><a href="#">Categoria 4</a></li>
						<li><a href="#">Categoria 5</a></li>
					</ul>
				</li>
				<li>
					<a href="#">Departameto2</a>
					<div class="destaque">
						Aqui vai o produto destaque do departamento selected
					</div>

					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<li>
					<a href="#">Departameto3</a>
					<div class="destaque">
						Aqui vai o produto destaque do departamento selected
					</div>

					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<li>
					<a href="#">Departameto4</a>
					<div class="destaque">
						Aqui vai o produto destaque do departamento selected
					</div>

					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<li>
					<a href="#"></a>
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<li>
					<a href="#"></a>
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<li>
					<a href="#"></a>
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<li>
					<a href="#"></a>
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<li>
					<a href="#"></a>
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
				<li>
					<a href="#"></a>
					<ul>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
			</ul>

		</div><!-- Fim Conteudo central -->

	<!-- MENU DIREITO -->
	{include file="modules/publico/view/sub_templates/coluna_secundaria.tpl"}
	</article><!-- Fim Conteudo -->


