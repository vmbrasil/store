		<!-- TAG CLOUD -->
		{if $tagCloud|@sizeof > 0}
		<div class="janela_tag_box">
			<h3 class="subtitulo">Mais Buscados</h3>
			<ul class="tags_box">
			{foreach from=$tagCloud item=tagitem}
				<li><a href="{$tagitem.url}" class="tag_size{$tagitem.size}">{$tagitem.description}</a></li>
			{/foreach}
			</ul><!-- fim tags_box-->
		</div><!-- fim janela_tags_box-->
        {/if}

		<!-- Rodape -->
		<div class="rodape">
		
			<div class="aviso_base">
				<p>Aviso : Todos os pre�os e condi��es deste site s�o v�lidos apenas para compras no site e n�o se aplicam para nossas lojas. <a href="/Vitrine/AutoAtendimento/TireSuasDuvidas.aspx?topico=SaibaMais" title="Saiba mais">Saiba mais.</a></p>
			</div>

{*include file="modules/publico/view/sub_templates/searchBar.tpl"*}

			<div class="barra_historico"></div>
			
			
			<div class="barra_rodape">

				<ul class="menu_rodape">
					<li><h4>Institucional</h4>
					<ul>
						<li><a href="#" target="_blank" title="Wal-Mart no Mundo">	Nossas Lojas</a></li>
						<li><a href="#" title="Pol�tica de Privacidade">			Pol�tica de Privacidade</a></li>
						<li><a href="#" title="Termos de Uso e Condi��es">			Termos de Uso e Condi��es</a></li>
						<li><a href="#" title="Seja nosso Fornecedor">				Seja nosso Revendedor</a></li>
						<li><a href="#" title="Seja nosso Fornecedor">				Seja nosso Fornecedor</a></li>
						<li><a href="#" target="_blank" title="Trabalhe Conosco">	Trabalhe Conosco</a></li>
					</ul>
					</li>
					<li><h4>Tire suas D�vidas</h4>
					<ul>
						<li><a href="#" title="Perguntas Frequentes">				Perguntas Frequentes</a></li>
						<li><a href="#" title="Veja como � seguro comprar">			Veja como � seguro comprar</a></li>
						<li class="quebra"><a href="#" title="Cadastro">			Cadastro</a></li>
						<li><a href="#" title="Como Navegar">						Como Navegar</a></li>
						<li class="quebra"><a href="#" title="Como Comprar">		Como Comprar</a></li>
						<li><a href="#" title="Compras por Telefone">				Compras por Telefone</a></li>
					</ul>
					</li>
					<li><h4>&nbsp;</h4>
					<ul>
						<li class="quebra"><a href="#" title="Formas de Pagamento">	Formas de Pagamento</a></li>
						<li><a href="#" title="Entregas">							Entregas</a></li>
						<li class="quebra"><a href="#" title="Trocas e Devolu��es">	Trocas e Devolu��es</a></li>
						<li><a href="#" title="Assist�ncia T�cnica">				Assist�ncia T�cnica</a></li>
						<li class="quebra"><a href="#" title="Vales e Cupons">		Vales e Cupons</a></li>
						<li><a href="#" title="Promo��es de Parceiros">				Promo��es de Parceiros</a></li>
					</ul>
					</li>
					<li><h4>Servi�os</h4>
					<ul>
						<li><a href="#" title="Central de Listas">					Central de Listas</a></li>
						<li><a href="#" title="Garantia Estendida">					Garantia Estendida</a></li>
						<li><a href="#" title="Embalagem para Presente">			Embalagem para Presente</a></li>
						<li><a href="#" title="RSS">								RSS</a></li>
						<li><a href="#" title="Localizador de Lojas">				Localizador de Lojas</a></li>
					</ul>
					</li>
				</ul>


				<ul class="banners_rodape">
					<li></li>
					<li></li>
				</ul>

				<div class="formas_pagamento_rodape">
					<ul><h4>Formas de pagamentos</h4>
        				<li><img src="{$HOST}view/images/template/forma_pagSeguro.gif"></li>
						<li><img src="{$HOST}view/images/template/forma_visa.gif"></li>
						<li><img src="{$HOST}view/images/template/forma_unibanco.png"></li>
						<li><img src="{$HOST}view/images/template/forma_master.gif"></li>
						<li><img src="{$HOST}view/images/template/forma_itau.gif"></li>
						<li><img src="{$HOST}view/images/template/forma_breal.png"></li>
						<li><img src="{$HOST}view/images/template/forma_bradesco.gif"></li>
						<li><img src="{$HOST}view/images/template/forma_club_dinner.png"></li>
						<li><img src="{$HOST}view/images/template/forma_banrisul.png"></li>
						<li><img src="{$HOST}view/images/template/forma_bb.gif"></li>
						<li><img src="{$HOST}view/images/template/forma_boleto.gif"></li>
					</ul>
				</div>

        		<div class="creditos_rodape" title="Desenvolvedor"><a href="http://www.k13web.com.br" target="_blank"><img src="images/template/spacer.gif" border="0" height="30px"></a></div>

			</div><!--Fim barra rodape -->
		</div><!--Fim rodape -->
	</div><!--Fim corpo -->
</div><!--Fim tudo -->

</body>
</html>
