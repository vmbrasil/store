<!-- CONTEUDO PRINCIPAL -->
<article id="cart">
	<section class="janela">
		<h2 class="subtitulo">Meu Carrinho &gt; Finalizar Compra</h2>
		
		<div class="cart_etapa etapa-5">
			<ol>
				<li>Carrinho</li>
				<li>Identifica��o</li>
				<li>Transporte</li>
				<li>Pagamento</li>
				<li class="atual">Finaliza��o</li>
			</ol>
			<div style="clear: both"></div>
		</div>
		
		
		<!-- Botoes -->
		<div class="car_botoes">
			<a class="cart-bt-voltar" title="Voltar para o Pagamento" href="{$HOST}carrinho/pagamento/">Voltar</a>
			<a class="cart-bt-avancar" title="Finalizar Compra" href="#car-bt-finalizar-compra">Fechar Pedido</a>
		</div>
		
		<!-- Total -->
		<div class="cart_total">Total	<strong class="currency_format_real">R$ {$carrinho.total|number_format:2:",":"."}</strong> </div>
		
		
		<!-- Produtos -->
		<h3>Produtos</h3>
		<table cellspacing="0" cellpadding="5" border="0" width="100%" class="cart_tab_produtos">
			<thead>
			<tr>
				<th width="50%">Produto</th>
				<th>Quantidade</th>
	
				<th>Valor Unit�rio</th>
				<th colspan="2">Valor Total</th>
			</tr>
			</thead>
			        
			<tbody>
			{foreach $carrinho.itens as $item}
			<tr class="item" data-pid="{$item.codigo}">
				<td height="80">
					<img class="foto" height="64" width="64" src="{$item.produto.foto.super_mini|default:($HOST|cat:$no_image)}">
					<a class="title" href="{$HOST}produto/{$item.produto.slug}/">{$item.produto.title}</a>
					<p class="categoria">Categoria: {$item.produto.categoria.title}</p>
					<p class="marca">Marca: {$item.produto.marca.title}</p>
				</td>
				<td>
					<div class="quantidade">
						<label for="item_qtd{$item.codigo}">Quantidade do Produto</label>
						<p class="input_qtd" data-pid="{$item.codigo}" id="item_qtd{$item.codigo}" data-qtd="{$item.qtde}" maxlength="6" size="8" value="{$item.qtde}" name="txtqtd">{$item.qtde}</p>
					</div>
				</td>
				<td>
					<div class="value">R$ {$item.produto.preco|number_format:2:",":"."}</div>
				</td>
				<td>
					<div class="subtotal_unit currency_format_real" data-pid="{$item.codigo}">R$ {$item.subtotal|number_format:2:",":"."}</div>
				</td>
			</tr>
			{/foreach}
		</tbody></table>
		
		<!-- Subtotal -->
		<div class="cart_subtotal">
			<span>(Subtotal) <strong class="currency_format_real">R$ {$carrinho.subtotal|number_format:2:",":"."}</strong></span>
			<div class="clear"></div>
		</div>
		
		
		<!-- Descontos -->
		<fieldset class="cart_valedesconto">
			<legend>Descontos</legend>
			<ul class="list-descontos">
				<li>
					<p>Sem desconto informado</p>
					
					<div class="cart_subvalue_list">
						<strong class="currency_format_real">R$ {$carrinho.valor_desconto|number_format:2:",":"."}</strong>
					</div>
				</li>
			</ul>
			
		</fieldset>
		
		
		<!-- Transporte -->
		<fieldset class="cart_cep">
			<legend>Transporte</legend>
			
			<div class="cart_subitem_list">
				<p>Correios - PAC</p>
				<div class="cart_subvalue_list">
					<strong class="currency_format_real">R$ {$carrinho.valor_frete|number_format:2:",":"."}</strong>
				</div>
			</div>
		</fieldset>
		
		
		<!-- Pagamento -->
		<fieldset class="cart_cep">
			<legend>Pagamento</legend>
	
			<div class="cart_subitem_list">
				<p>Cart�o de Cr�dito Visa - 3X de R$ 1528,90 (2,5% juros)</p>
				<div class="cart_subvalue_list">
					<strong class="currency_format_real">R$ {$carrinho.valor_frete|number_format:2:",":"."}</strong>
				</div>
			</div>
		</fieldset>
		
		
		<!-- Total -->
		<div class="cart_total">Total<strong class="currency_format_real">R$ {$carrinho.total|number_format:2:",":"."}</strong> </div>
		
		
		<!-- BT Finalizar -->
		<div class="car_botoes">
			<a class="car-bt-finalizar-compra" id="car-bt-finalizar-compra" name="car-bt-finalizar-compra" title="Finalizar Compra" href="{$HOST}carrinho/fechar/">Fechar Pedido</a>
		</div>
		
		
		<!-- Dica -->
		<p class="msg_mensagem">
			Para finalizar seu pedido, confira os dados acima se est� tudo correto, ent�o clique no bot�o "Fechar Pedido" para finalizar definitivamente a compra.<br>
			O pedido ser� fechado e voc� ser� encaminhado para a p�gina de pagamento, conforme voc� configurou.<br>
			<br>
			ATEN��O ao clicar em finalizar n�o ser� possivel alterar as op��es da compra, portanto confira com aten��o se est� tudo correto.<br>
			Para alterar os dados atuais clique no bot�o "Voltar", ou entre em seu carrinho de compras.
		</p>


	</section><!-- Fim janela -->
</article>