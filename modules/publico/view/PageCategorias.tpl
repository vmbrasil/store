	<!--Menu ESQUERDO -->
	{include file="modules/publico/view/sub_templates/menu_categorias.tpl"}

	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<header>
			<!-- BreadCrumb -->
			{if $barra_diretorio|@sizeof > 0}
			<div class="diretorio_bar">
		    	<ul>
		    		<li class="label">Voc� est� em:</li>
				{foreach from=$barra_diretorio item=diritem}
					<li><a href="{$diritem.url}">{$diritem.label}</a></li>
				{/foreach}
				</ul>
			</div>
			{/if}
			
			<h2>{$categoria.title|default:'Categorias da Loja'}</h2>
			
		</header>
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
			
			<!-- ===============================PRODUTOS=============================== -->

			<div id="banner_categorias">
				<a href="#"><img alt="Rolagem de produtos em sub-categorias" src="{$HOST}images/banners/banner_subcategorias.png" width="587" height="109"></a>
				<div class="clear"></div>
			</div>
			
			<!-- VITRINE DE PRODUTOS -->
			<div class="categorias">
				<p>Selecione alguma categoria desejada abaixo:</p>
				{include file="modules/publico/view/sub_templates/listar_categorias.tpl"}
				<div class="clear"></div>
			</div>

			{if $produtos_relacionados.produtos|@count > 0 and $produtos_relacionados.produtos != false}
			<!-- ===============================VEJA TAMB�M=============================== -->
			<div class="janela" id="box-acessados">
				<hr class="separador">
				<a name="aproveite"></a>
				<h2 class="subtitulo">
					<span class="icon"></span>
					<span class="text">Mais acessados</span>
				</h2>

				<ul class="list slider_mount">
					{foreach from=$produtos_relacionados.produtos item=produto}
					<li>
						{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
					</li>
					{/foreach}
				</ul>
			</div><!-- Fim Janela -->
			{/if}

		</div><!-- Fim Conteudo central -->

	</article><!-- Fim Conteudo -->


	<!-- MENU DIREITO -->
	{include file="modules/publico/view/sub_templates/coluna_secundaria.tpl"}
