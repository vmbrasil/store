	<!--Menu ESQUERDO -->
	{include file="modules/publico/view/sub_templates/menu_categorias.tpl"}


	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<!-- DirBar -->
		{include file="modules/publico/view/sub_templates/bread_crumb.tpl"}
		
		<!-- Banner -->
		<div class="banner_rotativo">
			{$banner_rotativo|default:''}
		</div>
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
		
			<h2>{$busca.title|default:'Busca'}</h2>
			
			<!-- BARRA DE BUSCA AVAN�ADA -->
			<div id="full_search_bar" class="janela">
				<form name="search_formBusca" method="get" action="{$HOST}site/buscar/">
					<label for="search_txt_busca">
						Busca:
						<input type="text" name="txt_busca" id="search_txt_busca" value="{$smarty.get.txt_busca}" placeholder="Digite o nome do produto ou palavra chave que procura">
					</label>
					
					<div class="group selects">
						<label for="search_local">
							Local:
							<select name="local" id="search_local">
								{html_options options=$combobox_local selected=$smarty.get.local}
							</select>
						</label>
						
						<label for="search_categoria">
							Categoria:
							<select name="categoria"  id="search_categoria" {if $smarty.get.categoria == '1'}selected="selected"{/if}>
								<option value="">Todas</option>
								{html_options options=$combobox_categoria selected=$smarty.get.categoria}
							</select>
						</label>
	
						<label for="search_tipo">
							Tipo:
							<select name="tipo"  id="search_tipo" {if $smarty.get.tipo == '1'}selected="selected"{/if}>
								{html_options options=$combobox_tipo selected=$smarty.get.tipo}
							</select>
						</label>
						
						<label for="search_ordem">
							Ordenar por:
							<select name="ordem"  id="search_ordem" {if $smarty.get.ordem == '1'}selected="selected"{/if}>
								{html_options options=$combobox_ordem selected=$smarty.get.ordem}
							</select>
						</label>

					</div>

					<div class="group opicional">
						<label for="opc_destaque">
							<input type="checkbox" name="opc_destaque" id="opc_destaque" value="1" placeholder="Produto em Destaque" {if $smarty.get.opc_destaque == '1'}checked="checked"{/if}>
							Em Destaque
						</label>
						<label for="opc_oferta">
							<input type="checkbox" name="opc_oferta" id="opc_oferta" value="1" placeholder="Produto em Oferta" {if $smarty.get.opc_oferta == '1'}checked="checked"{/if}>
							Em Oferta
						</label>
						<label for="opc_promocao">
							<input type="checkbox" name="opc_promocao" id="opc_promocao" value="1" placeholder="Produto em Promo��o" {if $smarty.get.opc_promocao == '1'}checked="checked"{/if}>
							Em promo��o
						</label>
						<label for="opc_frete">
							<input type="checkbox" name="opc_frete" id="opc_frete" value="1" placeholder="Produto com Frete Gr�tis" {if $smarty.get.opc_frete == '1'}checked="checked"{/if}>
							Frete Gr�tis
						</label>
					</div>
					
					<button type="submit" class="barra_busca_bt_buscar" title="Buscar no Site">Buscar</button>
					
					<div class="clear"></div>
				</form>
			</div><!-- Fim busca avancada -->
		
			<div class="janela" id="busca">
			
			{if $erros|@count > 0}
				<!-- Mensagem Erro -->
				<ul class="msg_erro">
				{foreach from=$erros item=erro}
					<li>{$erro}</li>
				{/foreach}
				</ul>
			{/if}

			{if $busca.termo_inapropriado == true}
				<p class="warning inapropriado">
					Termo inapropriado detectado. Por favor n�o utilize termos inapropriados ou palavr�es em sua pesquisa.
					<a href="">Se o termo informado estiver correto clique aqui para mostrar os resultados omitidos.</a>
				</p>
			{/if}
				
			{if $correcao != ''}
				<div class="voce_quis_dizer">
					<p>Voc� quis dizer: <a href="{$HOST}buscar/{$correcao.tag}/">query corrigida vai aqui{$correcao.tag}</a></p>
				</div>
			{/if}

			<!-- ============ RESULTADO ========== -->
			{if $itensBusca|@sizeof > 0}
			
				{if $itensBusca|@sizeof > 1}
				<!-- BARRA DE ORDENA��O -->
				<div class="bar_ordenacao">
					<ul class="ordenar_por">
						<li {if $smarty.get.ordem == '' or  $smarty.get.ordem == 'relevante'}class="selected"{/if}><a href="{$k13SelfUrl}?ordem=relevante" id="aba_ord_vitrine" title="Vitrine mais relevantes">Mais Relevante</a></li>
						<li {if $smarty.get.ordem == 'vendidos'}class="selected"{/if}><a href="{$k13SelfUrl}?ordem=vendidos" id="aba_ord_vendidos" title="Mais Vendidos">Mais Vendidos</a></li>
						<li {if $smarty.get.ordem == 'barato'}class="selected"{/if}><a href="{$k13SelfUrl}?ordem=barato" id="aba_ord_menor_preco" title="Menor Pre&ccedil;o">Menor Pre&ccedil;o</a></li>
						<li {if $smarty.get.ordem == 'novidades'}class="selected"{/if}><a href="{$k13SelfUrl}?ordem=novidades" id="aba_ord_recentes" title="Novidades">Novidades</a></li>
						<li {if $smarty.get.ordem == 'A-Z'}class="selected"{/if}><a href="{$k13SelfUrl}?ordem=A-Z" id="aba_ord_az" title="De A-Z">De A-Z</a></li>
					</ul>
					
					<div class="clear"></div>
				</div>
				{/if}

				<!-- ITENS -->
				<ul class="itens_result">
				{foreach from=$itensBusca item=item}
					<li>
					{$produto = $item}
					{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
					</li>
				{/foreach}
				</ul>
				
				<!-- BARRA DE PAGINA��O -->
				<div class="paginacao">
					{$vitrine_produtos_paginacao}
				</div>


			{else}
				<p class="nao_encontrado">Nenhum Resultado Encontrado!</p>
			{/if}
			</div><!-- Fim Janela -->
			
			<div class="clear"></div>

		</div><!-- Fim Conteudo central -->

	</article><!-- Fim Conteudo -->


	<!-- MENU DIREITO -->
	{include file="modules/publico/view/sub_templates/coluna_secundaria.tpl"}
