function elementPosition(obj)
{
	var curleft = 0, curtop = 0;

	if (obj.offsetParent)
	{
		curleft = obj.offsetLeft;
		curtop = obj.offsetTop;

		while (obj = obj.offsetParent)
		{
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		}
	}

	return { x: curleft, y: curtop };
}

function ScrollToControl(id)
{
	var elem = document.getElementById(id);
	var scrollPos = elementPosition(elem).y;
	var Up;
	scrollPos = scrollPos - document.documentElement.scrollTop;
	if (scrollPos>0){Up=1}else{Up=-1}
	var remainder = scrollPos % 50;
	var repeatTimes = (scrollPos - remainder) / 50;
	//alert('scrollPos: ' + scrollPos + ' - repeatTimes: '+repeatTimes)
	if (repeatTimes<0){repeatTimes=repeatTimes*(-1)}
	ScrollSmoothly(scrollPos,repeatTimes,Up);
	window.scrollBy(0,remainder);
}

var repeatCount = 0;
var cTimeout;
var timeoutIntervals = new Array();

var timeoutIntervalSpeed;
function ScrollSmoothly(scrollPos,repeatTimes, up)
{
	var qtd=(50 * up);
	//alert('repeatCount: ' + repeatCount + ' - repeatTimes:' + repeatTimes)
	if(repeatCount < repeatTimes)
	{
		window.scrollBy(0,qtd);
	}
	else
	{
		repeatCount = 0;
		clearTimeout(cTimeout);
		return;
	}
    repeatCount++;
	cTimeout = setTimeout("ScrollSmoothly('" + scrollPos + "','"+ repeatTimes +"',"+up+")",35);
}

//example usage :
//ScrollToControl('elementID');