$(function(){
	$('.contato input[type="text"],.contato input[type="file"],.contato textarea,.contato select').focusin(function() {
		$(this).closest('fieldset').addClass('focus');//.removeClass('focus')
    })
    .focusout(function(){
    	$('.contato fieldset').removeClass('focus');
	});

	$('#enviar').click(function(){
		var msg = '';
		//Valida
		if($('#nome').val() == '') msg += 'Nome deve ser preenchido!\n';
		if($('#email').val() == '') msg += 'E-mail deve ser preenchido!\n';
		if($('#telefone').val() == '') msg += 'Telefone deve ser preenchido!\n';
		if($('#mensagem').val() == '') msg += 'Mensagem deve ser preenchida!\n';
		
		if(msg != ''){
			alert(msg);
			return false;
		}
		
		//Envia
		$('form').submit();
	});

});