/**
	 FUN��ES PARA VALIDA��O DE DADOS
	 @Autor  Tiago Maradei   -   tiago.maradei@gmail.com
 */

function is_email(email)
{

	var reEmail1 = /^[\w!#$%&'*+\/=?^`{|}~-]+(\.[\w!#$%&'*+\/=?^`{|}~-]+)*@(([\w-]+\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
	var reEmail2 = /^[\w-]+(\.[\w-]+)*@(([\w-]{2,63}\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
	var reEmail3 = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
	var reEmail = reEmail3;

	var pFmt = 1;//livre 2-Compacto 3-restrito
	pStr = email;
	
	eval("reEmail = reEmail" + pFmt);
	if (reEmail.test(pStr)) {
		return true;
	} else if (pStr != null && pStr != "") {
		return false;
	}

/*	  er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;

	  if(er.exec(email))
		{
		  return true;
		} else {
		  return false;
		}
*/
}

function is_data(data)
	{
	  if(data.length == 10)
		{
		  er = /(0[0-9]|[12][0-9]|3[01])[-\.\/](0[0-9]|1[012])[-\.\/][0-9]{4}/;

		  if(er.exec(data))
			{
			  return true;
			} else {
			  return false;
			}

		} else {
		  return false;
		}
	}

function is_hora(hora)
	{
	  er = /(0[0-9]|1[0-9]|2[0123]):[0-5][0-9]/;

	  if(er.exec(hora))
		{
		  return true;
		} else {
		  return false;
		}
	}
