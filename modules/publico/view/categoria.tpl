	<!--Menu ESQUERDO -->
	{include file="modules/publico/view/sub_templates/menu_categorias.tpl"}

	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<header>
			{include file="modules/publico/view/sub_templates/bread_crumb.tpl"}
			
			<h2>{$categoria.title|default:'Categoria sem titulo'}</h2>
			
			{if $categoria.id > 0}
			<!-- Box banner central -->
			<div id="banner_central">
				<a href="#"><img alt="banner central" src="{$HOST}images/banners/banner_categoria_destaque.png" width="630" height="239"></a>
			</div>
			
			<div id="banner_subcategorias">
				<a href="#"><img alt="Rolagem de produtos em sub-categorias" src="{$HOST}images/banners/banner_subcategorias.png" width="587" height="109"></a>
			</div>
			{/if}

		</header>
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
		{if $categoria.id > 0}
			
			<!-- ===============================PRODUTOS=============================== -->
			<!-- BARRA DE ORDENACAO -->
			<div class="bar_ordenacao">
				<ul class="ordenar_por">
					<li {if $smarty.get.ord == ''}class="selected"{/if}><a href="{$k13SelfUrl}" id="aba_ord_vitrine" title="Vitrine mais relevantes">Vitrine</a></li>
					<li {if $smarty.get.ord == 'by_vendas'}class="selected"{/if}><a href="{$k13SelfUrl}?ord=by_vendas" id="aba_ord_vendidos" title="Mais Vendidos">Mais Vendidos</a></li>
					<li {if $smarty.get.ord == 'by_valor'}class="selected"{/if}><a href="{$k13SelfUrl}?ord=by_valor" id="aba_ord_menor_preco" title="Menor Pre&ccedil;o">Menor Pre&ccedil;o</a></li>
					<li {if $smarty.get.ord == 'by_data'}class="selected"{/if}><a href="{$k13SelfUrl}?ord=by_data" id="aba_ord_recentes" title="Novidades">Novidades</a></li>
					<li {if $smarty.get.ord == 'by_nome'}class="selected"{/if}><a href="{$k13SelfUrl}?ord=by_nome" id="aba_ord_az" title="De A-Z">De A-Z</a></li>
				</ul>
				
				<div class="clear"></div>
			</div>

			<!-- VITRINE DE PRODUTOS -->
			{if $vitrine_produtos.produtos|@sizeof > 0}
			<section class="vitrine">
				<ul>
				{foreach from=$vitrine_produtos.produtos item=produto}
					<li>
						{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
					</li>
				{/foreach}
				</ul>
			</section>
			
			<div class="paginacao">
			{$vitrine_produtos_paginacao}
			</div>
			
			{else}
			<p class="not_found">Nenhum produto encontrado nesta categoria.</p>
			{/if}
			
		{else}
			{if $erros|@count > 0}
				<!-- Mensagem Erro -->
				<ul class="msg_erro">
				{foreach from=$erros item=erro}
					<li>{$erro}</li>
				{/foreach}
				</ul>
			{/if}
			

		
			<div class="nao_encontrado">
				{if $correcao != ''}<p>Voc� quis dizer: <strong>{$correcao.title}</strong> - <a href="{$HOST}categoria/{$correcao.slug}/">{$HOST}categoria/{$correcao.slug}</a></p><br>{/if}
				
				<p>Selecione alguma categoria desejada abaixo:</p>
				{include file="modules/publico/view/sub_templates/listar_categorias.tpl"}
			</div>
		{/if}
		</div><!-- Fim Conteudo central -->

	</article><!-- Fim Conteudo -->


	<!-- MENU DIREITO -->
	{include file="modules/publico/view/sub_templates/coluna_secundaria.tpl"}
