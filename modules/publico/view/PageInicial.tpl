	<!--Menu ESQUERDO -->
	{include file="modules/publico/view/sub_templates/menu_categorias.tpl"}

	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		
		<!-- Banner -->
		<div class="banner_rotativo">
			{$banner_rotativo|default:''}
		</div>
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
		
			<!-- ===============================PRODUTOS=============================== -->
			<!-- VITRINE DE PRODUTOS DESTAQUES -->
			<div class="janela" id="vitrine">
				{if $vitrine_destaques.produtos|@sizeof > 0}
				<section class="vitrine">
					<h2><span class="icone"></span><p>Destaques</p></h2>
					<ul>
					{foreach from=$vitrine_destaques.produtos item=produto}
						<li>
							{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
						</li>
					{/foreach}
					</ul>
				</section>
				{/if}


				<!-- VITRINE DE PRODUTOS OFERTAS -->
				{if $vitrine_ofertas.produtos|@sizeof > 0}
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Ofertas</p></h2>
					<ul>
					{foreach from=$vitrine_ofertas.produtos item=produto}
						<li>
							{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
						</li>
					{/foreach}
					</ul>
				</section>
				{/if}


				<!-- VITRINE DE PRODUTOS NOVIDADES -->
				{if $vitrine_novidades.produtos|@sizeof > 0}
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Novidades</p></h2>
					<ul>
					{foreach from=$vitrine_novidades.produtos item=produto}
						<li>
							{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
						</li>
					{/foreach}
					</ul>
				</section>
				{/if}


				<!-- VITRINE DE PRODUTOS MAIS VENDIDOS -->
				{if $vitrine_mais_vendidos.produtos|@sizeof > 0}
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Mais Vendidos</p></h2>
					<ul>
					{foreach from=$vitrine_mais_vendidos.produtos item=produto}
						<li>
							{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
						</li>
					{/foreach}
					</ul>
				</section>
				{/if}



				<!-- VITRINE DE PRODUTOS MAIS BARATOS -->
				{if $vitrine_mais_baratos.produtos|@sizeof > 0}
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Menores Pre�os</p></h2>
					<ul>
					{foreach from=$vitrine_mais_baratos.produtos item=produto}
						<li>
							{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
						</li>
					{/foreach}
					</ul>
				</section>
				{/if}

				<!-- VITRINE DE PRODUTOS MAIS VISITATOS -->
				{if $vitrine_mais_visitados.produtos|@sizeof > 0}
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Mais Visitados</p></h2>
					<ul>
					{foreach from=$vitrine_mais_visitados.produtos item=produto}
						<li>
							{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
						</li>
					{/foreach}
					</ul>
				</section>
				{/if}


				<!-- VITRINE DE PRODUTOS FRETE GRATIS -->
				{if $vitrine_frete_gratis.produtos|@sizeof > 0}
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Produtos com Frete Gr�tis</p></h2>
					<ul>
					{foreach from=$vitrine_frete_gratis.produtos item=produto}
						<li>
							{include file="modules/publico/view/sub_templates/elem_produto.tpl"}
						</li>
					{/foreach}
					</ul>
				</section>
				{/if}


			</div><!-- Fim Janela -->


		</div><!-- Fim Conteudo central -->

	</article><!-- Fim Conteudo -->
	
	<!-- MENU DIREITO -->
	{include file="modules/publico/view/sub_templates/coluna_secundaria.tpl"}
