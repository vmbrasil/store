		<div class="ContentNews">
			<div class="ContentNewsMain">
				<div class="TituliSessao">Qual o seu problema? </div>
			</div>
		</div>	
			<div class="ContentMain">
				<div class="empresa">
					<h2>Problemas Complexos. Por�m n�o complicados.<br />
					<h3>Apaixonados por desafios, procuramos a melhor forma de dissolver seus problemas de gest�o com a Tecnologia da Informa��o. </h3>
					<p>Em um mercado com constantes mudan�as e atualiza��es, a OnixSeven mudou a maneira de pensar e de desenvolver software. 
					 Enquanto algumas empresas desenvolvem projetos enormes com os custos elevados, quando chegam ao mercado j� n�o atendem mais aquela necessidade,
					 n�s trabalhamos com ciclos interativos, na qual os grupos de desenvolvedores sempre est�o em contato com nossos clientes escrevendo rapidamente e
					adaptando o sistema a necessidade de cada um.</p>
					<p>Nossos especialistas sempre est�o focados naquilo que gostam: os computadores. Trabalhando com sistemas OpenSource, 
					tanto na linguagem de programa��o quanto nos bancos de dados diminuindo drasticamente os custos de implanta��o, 
					e tamb�m os riscos de viola��o dos sistemas.  <br />
<!--					Mas o que h� de mais importante em qualquer processo de neg�cios s�o as pessoas.</p>-->
					<p>Os nossos consultores oferecem suporte total e possuem as respostas que voc� precisa para investir com seguran�a em seus projetos.
					 Antes de modificar ou atualizar o sistema, voc� saber� exatamente como ocorrer� em cada etapa processo de atualiza��o ou implanta��o.</p> 
					<div class="assinatura">
						<img alt="Logo OnixSeven" src="{$HOST}view/image/onixseven2.png" width="170"><br />
						<h5>Solu��es em Gest�o P�blica</h5>
					</div>
				</div>
			</div>