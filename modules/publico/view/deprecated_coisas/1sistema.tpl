<div class="ContentNews">
	<div class="ContentNewsMain">
		<div class="TituliSessao">O nosso sistema</div>
	</div>
</div>	
<div class="ContentMain">
	<div class="empresa">
	<h2>Sistema SIFAM de Gest�o P�blica</h2>
		<p>O Sistema SIFAM de Gest�o P�blica, alicer�ado no conceito de Cart�o do Cidad�o 
		e no valios�ssimo conhecimento de nossos analistas e consultores com s�lidos conhecimentos
		 em Gest�o P�blica dentro das mais variadas rotinas de uma Prefeitura Municipal, com tecnologia 
		 totalmente WEB, nosso Sistema tornou-se uma verdadeira ferramenta de apoio a tomada de decis�o 
		 e Gest�o P�blica Municipal.</p>
		 
		<p>O aludido software consiste em um banco de dados alimentado atrav�s de uma rede remota em 
		tempo real, que unifica secretarias, autarquias e �rg�os ligados ao executivo municipal com a 
		forma��o de um cadastro eletr�nico e �nico.</p>
		
		<p>O sistema registra o momento da presta��o do servi�o p�blico ou da concess�o de um benef�cio
		 municipal, estadual ou federal, de forma eletr�nica, em um conjunto de dados, informa��es acerca 
		 do beneficiado, com data, hor�rio, servidor respons�vel pelo atendimento e o local da presta��o 
		 ou atendimento. O escopo de tal registro � o de estruturar um cadastro eletr�nico completo para 
		 o servi�o municipal, que impossibilita a exist�ncia de dados n�o condizentes com a realidade ou 
		 fraudulentos, no intuito de melhor otimiza��o dos servi�os p�blicos. � necess�rio destacar que
		 uma vez enviados os dados atrav�s de "login" e senha do servidor, n�o mais ser� poss�vel a altera��o 
		 dos registros, eis que os mesmos ficam gravados no servidor em que est�o concentradas as informa��es,
		 sem acesso sen�o por pessoal autorizado pelo executivo municipal.</p>
		 
		 <p>O presente registro eletr�nico tamb�m se destina ao gerenciamento das despesas, express�o do princ�pio
		  da probidade e da responsabilidade do administrador p�blico, de sorte que a partir da identifica��o 
		  do p�blico alvo usu�rio e dos servi�os prestados, identifica-se o endere�amento e a efetiva presta��o 
		  de recursos e servi�os p�blicos. Com isto, comprova-se a demanda e identifica mais especificamente os 
		  benefici�rios atrav�s de informa��es atuais, mediante cruzamento de dados para os diferentes campos de 
		  atua��o do servi�o p�blico, inclusive proporcionando a estrat�gia de a��es futuras, com base em estat�sticas 
		  tomadas a partir dos pr�prios registros. Dentro deste jaez, a programa��o de demandas pode ser mais 
		  facilmente detectada, com vistas aos pleitos de recursos estaduais ou federais, demonstrados a partir 
		  dos aludidos registros, que identificam os servi�os, benef�cios e benefici�rios.</p>
		<br />
		<h3>Tecnologia</h3>
		<ul>
		<li>Tr�s Camadas (Banco de Dados, Cliente, Servidor)</li>
		<li>Tecnologia WEB, linguagem PHP/J2EE;</li>
		<li>Sistema Gerenciador de Banco de Dados - (POSTGRESQL, ORACLE, SQL SERVER, ETC);</li>
		</ul>
		<p>A tecnologia utilizada garante integridade e confiabilidade dos dados, seguran�a na troca de informa��es 
		e total compatibilidade entre os produtos;</p>
		<br/>
		<h3>IMPORTANTE:</h3> 
		<p>O Sistema SIFAM de Gest�o P�blica � uma Solu��o t�o perfeitamente concebida para a gest�o municipal e de
		 r�pida e f�cil parametriza��o e adequa��o �s rotinas de qualquer munic�pio que a prefeitura em que fizermos 
		 uma simples demonstra��o, nesse momento essa prefeitura j� poder� iniciar a utiliza��o do Sistema.</p>
			<br/>
				<br/>
		<h3>VANTAGENS do Sistema SIFAM de Gest�o P�blica</h3>
		<p>
			<ul>
				<li>Gerenciamento da maquina administrativa em tempo real,�com melhor�aloca��o do recurso</li>
				<li> Capta��o de recursos na esfera estadual e federal</li> 
				<li>Gest�o Social</li>
				<li>Planejamento estrat�gico, t�tico e operacional.</li>
				<li>Gest�o Pol�tica</li>
			</ul>
�		</p>
�		<h3>OBJETIVOS DO REGISTRO ELETR�NICO <br />(CART�O CIDAD�O)</h3> 
			<p>Informatizar o processo de concess�o e controle de benef�cios, sendo estes, municipais, estaduais ou federais; 
			Monitoramento on-line das movimenta��es ocorridas; 
			Levantamento estat�stico e socioecon�mico do Munic�pio. 
			A base de dados do sistema possui informa��es vitais para concess�o de benef�cios sobre fam�lias e mun�cipes, auxiliando 
			na escolha dos beneficiados; 
			Facilidade para identificar as reais necessidades de cada fam�lia/mun�cipe, possibilitando maior justi�a social; 
			Capacidade de fornecer dados gerenciais para aux�lio nas tomadas de decis�es, como relat�rio de custos por fam�lia, 
			secretaria e benef�cios, proporcionando melhor aloca��o dos recursos; 
			Um cart�o para cada fam�lia. Em caso de extravio, haver� o cancelamento e substitui��o do mesmo; </p>
	</div>
</div>