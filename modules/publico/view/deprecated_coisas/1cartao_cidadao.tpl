		<div class="ContentNews">
			<div class="ContentNewsMain">
				<div class="TituliSessao">Cart�o cidad�o </div>
			</div>
		</div>	
			<div class="ContentMain">
				<div class="empresa">
					<h2>Problemas Complexos. Por�m n�o complicados.<br /></h2>
					<h3>Apaixonados por desafios, procuramos a melhor forma de dissolver seus problemas de gest�o com a Tecnologia da Informa��o. </h3>
					<p>A �nix do Brasil Solu��es Tecnol�gicas viabiliza, definitivamente, a Solu��o Tecnol�gica mais desejada pelos Executivos Municipais
					 e suas Secretarias: o "Cart�o do Cidad�o" possibilita ao Gestor conhecer efetivamente e em tempo real as verdadeiras necessidades 
					 do mun�cipe, bem como extrair n�meros reais do custo do cidad�o e de sua Fam�lia para o Munic�pio, n�meros esses que servir�o de base
					  para a busca dos recursos devidos ao mesmo Munic�pio.<br /> <br />
					  
					  Na pr�tica, isso acontece atrav�s do lan�amento, do cruzamento de informa��es 
					  e da disponibilidade de relat�rios de ABSOLUTAMENTE qualquer setor da  Prefeitura, atrav�s de uma rotina simples, pratica e que apesar
					   do alto grau tecnol�gico aplicado, ainda assim leve, exigindo poucos recursos de hardware, em geral utilizando o parque de maquinas
					    j� existente na Prefeitura.<br /><br />
					  
					  Em resumo, o Sistema REGISTRA as a��es externas e internas, ou seja, o Gestor administra em tempo real
					     tanto o que acontece com o cidad�o como as atividades internas dos funcion�rios da Prefeitura.</p> 
					<div class="assinatura">
						<img alt="Logo OnixSeven" src="{$HOST}view/image/onixseven2.png" width="170"><br />
						<h5>Solu��es em Gest�o P�blica</h5>
					</div>
				</div>
			</div>