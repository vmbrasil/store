<div class="ContentNews">
	<div class="ContentNewsMain">
		<div class="TituliSessao">Treinamento & capacita��o</div>
	</div>
</div>	
<div class="ContentMain">
	<div class="empresa">
	<h2></h2> 
	<h3>Nossa equipe de especialistas em gest�o p�blica, com habilidade fora do comum... </h3>
			<h2>O que � Treinamento?</h2>
			<p>� um processo de assimila��o cultural a curto prazo, que objetiva repassar ou
			reciclar conhecimentos, habilidades ou atitudes relacionados diretamente �
			execu��o de tarefas e � sua otimiza��o no trabalho.</p><br />
			<h3>Qual o objetivo do Treinamento?</h3>
			<p>O treinamento tem por finalidade ajuda-lp a alcan�ar seus objetivos,
			proporcionando oportunidade aos funcion�rios de todos os n�veis do sistema.<br />
			O treinamento produz um estado de mudan�a no conjunto de CONHECIMENTOS,
			HABILIDADES e ATITUDES, de cada trabalhador, modificando a bagagem
			particular de cada um.</p><br />
			<h3>Quem corrige as distor��es correspondente ao perfil exigido pela Organiza��o?</h3>
			<p>Cada um de n�s temos habilidades adquiridas ao longo de
			nossa trajet�ria, por�m, precisamos estar em cientes com a posi��o que
			ocupamos numa estrutura organizacional e com as devidas responsabilidades.<br />
			As eventuais diferen�as existentes devem ser corrigidas por meio do Treinamento.</p><br />
			<h3>Qual o resultado esperado com um Programa de Treinamento?</h3>
			<ul>
			<li>Aumento da produtividade</li>
			<li>Melhoria na qualidade dos resultados</li>
			<li>Redu��o de custos, retrabalho, etc.</li>
			<li>Otimiza��o da efici�ncia</li>
			<li>Eleva��o do saber</li>
			<li>Aumento das habilidades</li>
			<li>Melhoria do clima organizacional</li>
		</ul>
		</p>
		<div class="assinatura">
			<img alt="Logo OnixSeven" src="{$HOST}view/image/onixseven2.png" width="170"><br />
			<h5>Solu��es em Gest�o P�blica</h5>
		</div>
	</div>
</div>