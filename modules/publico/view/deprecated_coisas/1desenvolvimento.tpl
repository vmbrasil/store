<div class="ContentNews">
	<div class="ContentNewsMain">
		<div class="TituliSessao">Compromisso com a simplicidade</div>
	</div>
</div>	
<div class="ContentMain">
	<div class="empresa">
		<h2>Simplismente simples!</h2>
		 
		<h3>Encontraremos a melhor maneira de resolver seus problemas em Gest�o. </h3>
		<p>
			O cliente, aquele que tem uma necessidade a ser atendida, por meio de uma ferramenta de software que precisa ser desenvolvida dentro de um contexto de custo e prazo com qualidade e condi��es de continuidade e manuten��o dentro da vida �til desta ferramenta, o desenvolvedor que precisa entender essa necessidade do cliente dentro de contextos funcionais, identificar� os demais requisitos n�o funcionais para ent�o escolhendo uma ferramenta de desenvolvimento, desenvolver esta solu��o para o cliente, e por fim existe algu�m no meio do caminho (um gestor de projetos ou um SCRUM Master como caracteria o m�todo SCRUM) que trabalha para gerenciar a expectativa do cliente, a produ��o do desenvolvedor com o objetivo de chegar a conclus�o do projeto com uma ferramenta de software entregue dentro das especifica��es/necessidades do cliente.
		</p>
		<p>
			Em meio a tudo isso, falando especificamente da proposta do SCRUM, temos a promessa de que por meio de um modelo de gest�o �gil, o cliente ter� aos poucos (a cada sprint) um elemento de software palp�vel, capaz de ser avaliado e se aprovado, devidamente publicado em produ��o, isso em um processo c�clico, cont�nuo at� a conclus�o do projeto, ou seja, o cliente tem sempre algo pra ver, testar, mudar se quiser e com isso a din�mica do projeto muda a vis�o dos envolvidos muda, a realidade de desenvolvimento do projeto muda com muito mais foco no que � necess�rio, muito mais foco no que � utiliz�vel, muito menos foco no que � burocr�tico.
		</p>
		
		<h3>Ao contr�rio do que muitos dizem, n�o vemos SCRUM como um modismo.</h3>
		<p>
			Na forma de se desenvolver projetos de software, mas principalmente na forma de se relacionar entre os 3 principais envolvidos no processo de desenvolvimento de software, o desenvolvedor, o gestor e o cliente, pois o SCRUM os coloca em um contexto de colabora��o, cumplicidade, e n�o mais naquela rivalidade do "s� vou fazer o que foi especificado l� tr�s e pronto" ou "se mudar o escopo vai ter que mudar o prazo, e vai ficar mais caro..." como se v� na maioria dos contextos.
		</p>
		
		<h3>Vemos como uma evolu��o na forma de se pensar projetos de software.</h3>
		<p>
			SCRUM ajuda a dar a TI a visibilidade de apoiador, viabilizador, aliado, e com isso todos n�s que trabalhamos com desenvolvimento de software porque gostamos disso, teremos al�m do prazer de ver um produto conclu�do e entregue a satisfa��o do nosso cliente n�o somente no que ele est� recebendo, mas tamb�m na forma como ele participou tamb�m no desenvolvimento deste produto.
		</p>
		
		<div class="assinatura">
			<img alt="Logo OnixSeven" src="{$HOST}view/image/onixseven2.png" width="170"><br />
			<h5>Solu��es em Gest�o P�blica</h5>
		</div>

	</div>
</div>