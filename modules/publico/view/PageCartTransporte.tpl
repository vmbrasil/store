<!-- CONTEUDO PRINCIPAL -->
<article id="shipping">
	<section class="janela">
		<h2 class="subtitulo">Meu Carrinho &gt; Transporte</h2>
	
		<!-- Etapas -->
		<div class="cart_etapa etapa-3">
			<ol>
				<li>Carrinho</li>
				<li>Identifica��o</li>
				<li class="atual">Transporte</li>
				<li>Pagamento</li>
				<li>Finaliza��o</li>
			</ol>
			<div style="clear: both"></div>
		</div>

		{capture name=car_botoes}
		<div class="car_botoes">
			<a class="cart-bt-voltar" title="Voltar para o carrinho" href="{$HOST}carrinho/">Voltar</a>
			<a class="cart-bt-avancar" title="Ir para Pagamento" href="{$HOST}carrinho/pagamento/">Avan�ar</a>
		</div>
		{/capture}
		{$smarty.capture.car_botoes}
			
		<h3>1 - Endere�o de Entrega</h3>
		<div class="cart_end_entrega" id="abas">
			<ul>
				<li><a href="#tabs-selecionado" title="Endere�o selecionado">Endere�o Selecionado</a></li>
				<li><a href="#tabs-meus" title="Selecionar em Meus Endere�os">Meus Endere�os</a></li>
				<li style="display:none"><a href="#tabs-novo" title="Criar novo endere�o">Novo</a></li>
			</ul>
			<div id="tabs-selecionado">
				<div class="ui-state-error ui-corner-all"> 
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
					<strong>Aten��o!</strong> Voc� n�o possui nenhum endere�o de entrega cadastrado. Cadastre um endere�o clicando abaixo ou entre em: Meu Perfil > Meus Endere�os >  Endere�os de Entrega</p>
				</div>
				<br>

				<!-- Endereco selecionado -->
				<form action="{$HOST}usuario/enderecos/salvar/" method="post" class="form_endereco">
					<div class="campo campo_cep">
						<label for="txt_cep">CEP</label>
						<input type="text" class="input" id="txt_cep" name="cep" maxlength="10" value="" required="required" readonly="readonly">
					</div>
					<div class="campo campo_destinatario">
						<label for="txt_destinatario">Nome do destinat�rio</label>
						<input type="text" class="input" id="txt_destinatario" name="destinatario" maxlength="60" value="" required="required" readonly="readonly">
					</div>
					<div class="campo campo_pais">
						<label for="txt_pais">Pais</label>
						<input type="text" class="input" id="txt_pais" name="pais" maxlength="60" value="" required="required" readonly="readonly">
					</div>
					<div class="campo campo_endereco">
						<label for="txt_endereco">Endere�o Completo (Rua, numero - Bairro)</label>
						<input type="text" class="input" id="txt_endereco" name="endereco" maxlength="500" value="" required="required" readonly="readonly">
					</div>
					<div class="campo campo_estado">
						<label for="txt_estado">Estado</label>
						<input type="text" class="input" id="txt_estado" name="estado" maxlength="60" value="" required="required" readonly="readonly" readonly="readonly">
					</div>
					<div class="campo campo_cidade">
						<label for="txt_cidade">Cidade</label>
						<input type="text" class="input" id="txt_cidade" name="cidade" maxlength="60" value="" required="required" readonly="readonly">
					</div>
					
					<div class="botoes">
						<button type="button" class="bt_mudar bt_vermelho" title="Mudar o endere�o">Mudar</button>
					</div>
				</form>
			</div>

			<div id="tabs-meus">

				<div class="ui-state-highlight ui-corner-all"> 
					<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
					Clique sobre um endere�o para seleciona-lo.</p>
				</div>
				<br>


				<table cellspacing="0">
					<thead>
						<tr>
							<th>Endere�o</th>
							<th>Cidade</th>
							<th>Estado</th>
							<th>Pa�s</th>
							<th>CEP</th>
							<th>Destinat�rio</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Endere�o</td>
							<td>Cidade</td>
							<td>Estado</td>
							<td>Pa�s</td>
							<td>CEP</td>
							<td>Destinat�rio</td>
						</tr>
					</tbody>
					
				</table>
			</div>
			
			<div id="tabs-novo">
				<!-- Novo Endereco -->
				<form action="{$HOST}usuario/enderecos/novo/" method="post" class="form_endereco">
					<div class="campo campo_cep">
						<label for="txt_new_cep">CEP</label>
						<input type="text" class="input" id="txt_new_cep" name="cep" maxlength="10" value="" required="required">
						<a href="#">N�o sei meu CEP</a>
					</div>
					<div class="campo campo_destinatario">
						<label for="txt_new_destinatario">Nome do destinat�rio</label>
						<input type="text" class="input" id="txt_new_destinatario" name="destinatario" maxlength="60" value="" required="required">
					</div>
					<div class="campo campo_pais">
						<label for="txt_new_pais">Pais</label>
						<input type="text" class="input" id="txt_new_pais" name="pais" maxlength="60" value="" required="required">
					</div>
					<div class="campo campo_endereco">
						<label for="txt_new_endereco">Endere�o Completo (Rua, numero - Bairro)</label>
						<input type="text" class="input" id="txt_new_endereco" name="endereco" maxlength="500" value="" required="required">
					</div>
					<div class="campo campo_estado">
						<label for="txt_new_estado">Estado</label>
						<input type="text" class="input" id="txt_new_estado" name="estado" maxlength="60" value="" required="required">
					</div>
					<div class="campo campo_cidade">
						<label for="txt_new_cidade">Cidade</label>
						<input type="text" class="input" id="txt_new_cidade" name="cidade" maxlength="60" value="" required="required">
					</div>
					<div class="campo campo_padrao">
						<label for="txt_new_padrao">Endere�o Padr�o</label>
						<input type="checkbox" class="checkbox" id="txt_new_padrao" name="padrao" value="1">
					</div>
					
					<div class="botoes">
						<button type="submit" class="bt_salvar">Salvar</button>
						<button type="button" class="bt_cancelar">Cancelar</button>
					</div>
				</form>
			</div>
		</div>

		
		<h3>2 - Forma de Entrega</h3>
		<div class="cart_forma_entrega" id="forma_envio">
			<table cellspacing="0">
				<thead><tr>
					<th>Forma de Envio</th>
					<th>Valor</th>
				</tr></thead>
				<tbody>
					<tr>
						<td class="forma">
							<span class="title">Motoboy</span>
							<span class="tempo">(1 a 3 dias �teis)</span></td>
						<td class="valor">R$ 5,00</td>
					</tr>
					<tr>
						<td class="forma">
							<span class="title">Taxi</span>
							<span class="tempo">(1 a 3 dias �teis)</span></td>
						<td class="valor">R$ 8,00</td>
					</tr>
					<tr>
						<td class="forma">
							<span class="title">Encomenda normal</span>
							<span class="tempo">(7 a 14 dias �teis)</span></td>
						<td class="valor">R$ 11,00</td>
					</tr>
					<tr>
						<td class="forma">
							<span class="title">Sedex</span>
							<span class="tempo">(3 a 7 dias �teis)</span></td>
						<td class="valor">R$ 15,00</td>
					</tr>
					<tr>
						<td class="forma">
							<span class="title">Sedex 10</span>
							<span class="tempo">(1 a 3 dias �teis)</span></td>
						</td>
						<td class="valor">R$ 25,00</td>
					</tr>
					<tr>
						<td class="forma">
							<span class="title">Transportadora</span>
							<span class="tempo">(7 a 18 dias �teis)</span></td>
						</td>
						<td class="valor">R$ 12,00</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="cart_subtotal">
			<span>(Subtotal) <strong class="currency_format_real">R$ {$carrinho.subtotal|number_format:2:",":"."}</strong></span>
			<div class="clear"></div>
		</div>
	
		<!-- Total -->
		<div class="cart_total">Valor Total	<strong class="currency_format_real">R$ {$carrinho.total|number_format:2:",":"."}</strong> </div>
	
		<div class="cart_parcelamento">ou em at� <strong class="parcelas">{$carrinho.parcelas}x</strong> de <strong class="valor currency_format_real">R$ {$carrinho.parcelas_valor|number_format:2:",":"."}</strong> <span class="juros">{$carrinho.parcelas_juros}</span></div>
				
		<form action="{$HOST}carrinho/" method="post" id="formEdit" name="formEdit">
			<input type="hidden" value="upd" name="exec">
			<input type="hidden" value="" id="qtd" name="qtd">
			<input type="hidden" value="" id="pid" name="pid">
		</form>

		{$smarty.capture.car_botoes}


		<p class="msg_mensagem">
			Informe seu endere�o, em seguida escolha uma das formas de transporte no quadro abaixo.
			Voc� pode inserir novo endere�o de envio, clicando em "Novo Endere�o", ou selecionar um endere�o j� existente em seu cadastro, clicando em "Meus Endere�os".
			Para ir para a pr�xima etapa do pedido, clique no bot�o "Avan�ar", se estiver tudo certo, voc� ser� encaminhado para a etapa de escolha da forma de pagamento.
		</p>
	

	</section><!-- Fim janela -->
</article>