	<!--Menu ESQUERDO -->
	{include file="modules/publico/view/sub_templates/menu_categorias.tpl"}

	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<!-- DirBar -->
		{if $barra_diretorio|@sizeof > 0}
		<div class="diretorio_bar">
	    	<ul>
			{foreach from=$barra_diretorio item=diritem}
				<li><a href="{$diritem.url}">{$diritem.label}</a></li>
			{/foreach}
			</ul>
		</div>
		{/if}
		<div class="modo_exibicao" title="Modo de Exibi��o"></div>
		
		<!-- Banner -->
		<div class="banner_rotativo">
			{$banner_rotativo|default:''}
		</div>
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
		
			<!-- BARRA DE FILTRAGEM -->
			{$html_barra_filtragem|default:''}
		
			<!-- ===============================PRODUTOS=============================== -->
			<!-- BARRA DE ORDENACAO -->
			<div class="bar_ordenacao">
				<ul class="ordenar_por">
					<li class="selected"><a href="#" id="aba_ord_vitrine">Vitrine</a></li>
					<li><a href="#" id="aba_ord_vendidos">Mais Vendidos</a></li>
					<li><a href="#" id="aba_ord_menor_preco">Menor Pre&ccedil;o</a></li>
				</ul>
				
				<div class="clear"></div>
			</div>

			<!-- VITRINE DE PRODUTOS -->
			<div class="janela" id="vitrine">
				{include file="modules/publico/view/sub_templates/produtoVitrineItem.tpl"}
			</div><!-- Fim Janela -->

		</div><!-- Fim Conteudo central -->

	</article><!-- Fim Conteudo -->
	
	<!-- MENU DIREITO -->
	{include file="modules/publico/view/sub_templates/coluna_secundaria.tpl"}



