<!-- CONTEUDO PRINCIPAL -->
<article id="payment">
	<section class="janela">
		<h2 class="subtitulo">Meu Carrinho &gt; Pagamento</h2>
		
		<!-- Etapas -->
		<div class="cart_etapa etapa-4">
			<ol>
				<li>Carrinho</li>
				<li>Identifica��o</li>
				<li>Transporte</li>
				<li class="atual">Pagamento</li>
				<li>Finaliza��o</li>
			</ol>
			<div style="clear: both"></div>
		</div>

		<!-- Botoes -->
		{capture name=car_botoes}
		<div class="car_botoes">
			<a class="cart-bt-voltar" title="Voltar para o transporte" href="{$HOST}carrinho/transporte/">Voltar</a>
			<a class="cart-bt-avancar" title="Ir para Finaliza��o" href="{$HOST}carrinho/finalizar/">Avan�ar</a>
		</div>
		{/capture}
		{$smarty.capture.car_botoes}
		
		<!-- Mensagem -->
		<div style="clear: both"></div>
		<div class="ui-state-highlight ui-corner-all"> 
			<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
			Selecione um forma de pagamento na listagem abaixo. Para selecionar clique no bot�o "SELECIONAR".</p>
		</div>
		<br>
		
		<!-- Formas de Pagamento -->
		<div class="formas-pgto-select">
			<ul>
				<li class="">
					<div class="title">Dinheiro</div>
					<div class="icone"><img src="{$HOST}view/templates/template1/images/pgto/dinheiro.png" width="128" height="128"></div>
					<div class="parcelamento">� Vista</div>
					<div class="adicional"></div>
					<button class="selecionar">Selecionar</button>
					<div class="clear"></div>
				</li>
				<li class="">
					<div class="title">Transferencia em Conta Corrente</div>
					<div class="icone"><img src="{$HOST}view/templates/template1/images/pgto/transferencia.png" width="128" height="128"></div>
					<div class="parcelamento">� Vista</div>
					<div class="adicional">* Desconto de 5% sobre o valor da compra</div>
					<button class="selecionar">Selecionar</button>
					<div class="clear"></div>
				</li>
				<li class="">
					<div class="title">Boleto Banc�rio</div>
					<div class="icone"><img src="{$HOST}view/templates/template1/images/pgto/boleto.png" width="128" height="128"></div>
					<div class="parcelamento">� Vista</div>
					<div class="adicional">* Desconto de 5% sobre o valor da compra</div>
					<button class="selecionar">Selecionar</button>
					<div class="clear"></div>
				</li>
				<li class="">
					<div class="title">Pag Seguro</div>
					<div class="icone"><img src="{$HOST}view/templates/template1/images/pgto/pagseguro.png" width="128" height="128"></div>
					<div class="parcelamento">Em at� 12x</div>
					<div class="adicional">* Adi��o de 5% sobre o valor da compra</div>
					<button class="selecionar">Selecionar</button>
					<div class="clear"></div>
				</li>
				<li class="">
					<div class="title">Pagamento Digital</div>
					<div class="icone"><img src="{$HOST}view/templates/template1/images/pgto/pagamento_digital.png" width="128" height="128"></div>
					<div class="parcelamento">Em at� 12x</div>
					<div class="adicional">* Adi��o de 5% sobre o valor da compra</div>
					<button class="selecionar">Selecionar</button>
					<div class="clear"></div>
				</li>
				<li class="">
					<div class="title">Paypal</div>
					<div class="icone"><img src="{$HOST}view/templates/template1/images/pgto/paypal.png" width="128" height="128"></div>
					<div class="parcelamento">Em at� 12x</div>
					<div class="adicional">* Adi��o de 5% sobre o valor da compra</div>
					<button class="selecionar">Selecionar</button>
					<div class="clear"></div>
				</li>
				<li class="">
					<div class="title">Cart�o Visa</div>
					<div class="icone"><img src="{$HOST}view/templates/template1/images/pgto/visa.png" width="128" height="128"></div>
					<div class="parcelamento">Em at� 12x</div>
					<div class="adicional">* Adi��o de 5% sobre o valor da compra</div>
					<button class="selecionar">Selecionar</button>
					<div class="clear"></div>
				</li>

			</ul>
		</div>
		
		
		<!-- Total -->
		<div class="cart_total">Valor Total	<strong class="currency_format_real">R$ {$carrinho.total|number_format:2:",":"."}</strong> </div>
		
		<!-- Botoes -->
		{$smarty.capture.car_botoes}

		<!-- Dica -->
		<p class="msg_mensagem">
			Informe uma forma de pagamento dispon�vel que mais lhe conv�m, informe os dados pedidos. Em seguida, clique em "Avan�ar" para ir para a etapa final de sua compra.
		</p>

	</section><!-- Fim janela -->
</article>