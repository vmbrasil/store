<?php
ini_set('SMTP', 'smtp.gmail.com');
ini_set('sendmail_from', 'onixdobrasilmail@gmail.com');
ini_set('smtp_port', '465');

//error_reporting(NULL);ini_set('display_errors',false);

//phpinfo();exit;

/**
 * Classe Controlador de contato
 * Gerencia os eventos do formulario
 * @author Alex Klen
 * */
class ContatoController{
	protected $objContato = null;	//Obj model -  contato
	protected $msg_erro = '';		//String - Mensagem de erros que podem ocorrer
	protected $msg_sucesso = '';	//String - Mensagem de sucesso ao enviar o formulario
	
	public function __construct(){
		$this->objContato = new Contato();
		
		$this->do_action();
		$this->gerarTemplate();
	}
	
	/*Executa/Processa as a��es da pagina*/
	protected function do_action(){
		if($_SERVER['REQUEST_METHOD']=='POST'){
//			var_dump($_REQUEST);
			
			$this->recebeDados($this->objContato);
			
			if($this->validar()){
				$this->enviarEmail();
			}
//			else {
//				$this->msg_erro .= '<li>Ocorreu um erro ao validar os dados!</li>';
//			}
		}
	}
	
	/*Gera o template da pagina*/
	protected function gerarTemplate(){
		global $smarty;
		
		$smarty->assign('backgroud','contentContato');
		
		$smarty->assign('css','<link rel="stylesheet" href="view/style/formulario.css" type="text/css" media="screen" />');
		$smarty->assign('javascript','<script src="view/script/contato.js"></script>');
		
		$smarty->assign('msg_erro',$this->msg_erro);
		$smarty->assign('msg_sucesso',$this->msg_sucesso);
		
		//die('Continuar a implementar as mensagens de resposta, inserir condi��es no tpl, implementar enviarEmail, implementar validar. Estou na linha 54');
	}
	
	/**
	 * Recebe os dados enviados de formul�rios
	 * @param OnModelDAOLIB $parDAO Objeto DAO a processar
	 * @author Alex Klen
	 */
	protected function recebeDados(&$objENT = null) {
		foreach ($_POST as $key => $value){
			$this->objContato->set($key, $value);
		}
	}
	
	/*Valida os dados de envio do email
	 * TODO:
	 * retorno
	 * */
	protected function validar(){
		$retorno = true;
		if(empty($_POST['nome'])){
			$this->msg_erro .= '<li>Nome deve ser preenchido!</li>';
			$retorno = false;
		}
		if(empty($_POST['email'])){
			$this->msg_erro .= '<li>E-mail deve ser preenchido!</li>';
			$retorno = false;
		}
		if(empty($_POST['mensagem'])){
			$this->msg_erro .= '<li>Mensagem deve ser preenchida!</li>';
			$retorno = false;
		}
		
		return $retorno;
	}
	
	/**
	 * Envia a mensagem de contato para o email
	 * @author Alex Klen
	 * @return boolean - sucesso
	 */
	protected function enviarEmail() {
		$mail = new PHPMailerLite();
		
		//config-----------------------------------------
		$mail->IsSMTP = true;// telling the class to use SMTP
		$mail->IsMAIL();
		
		$mail->SMTPSecure = 'ssl';
		$mail->SMTPAuth   = true;// enable SMTP authentication
		$mail->Mailer   = "smtp";
		
		$mail->Port = 465;//587
		$mail->Host       = "smtp.mail.yahoo.com";// Ip do GMAIL - telnet smtp.gmail.com 587
		//gmail-smtp-in.l.google.com
		//smtp.gmail.com

		$mail->SMTPKeepAlive = 'true'; 
		$mail->Username   = "onixdobrasilmail@gmail.com";	// GMAIL username
		$mail->Password   = "@asdfgh#";            			// GMAIL password

//		$mail->SetFrom('contato@onixseven.com.br', 'Onix Seven');
//		$this->AddReplyTo(contato@onixseven.com.br', 'Onix Seven');
		
		//dados-----------------------------------------
		$subject = 'Contato Realizado Pelo Site';
		$mail->Subject		= $subject;

		//Definimos que o e-mail ser� enviado no formato HTML
		$mail->IsHTML(true);
		
		$mail->AltBody		= "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		$body				= '';
		$body				.= '<h3>Nome: ' .$this->objContato->get('nome') .'</h3>';
		$body				.= '<h3>Email: ' .$this->objContato->get('email') .'</h3>';
		$body				.= '<h3>Telefone:' .$this->objContato->get('telefone') .'</h3>';
		$body				.= '<h3>Cidade:' .$this->objContato->get('cidade') .'</h3>';
		$body				.= '<h3>UF:' .$this->objContato->get('uf') .'</h3>';
		$body				.= '<h3>Tipo:' .$this->objContato->get('tipo') .'</h3>';
		$body				.= '<h3>Mensagem:' .$this->objContato->get('mensagem') .'</h3>';
//		echo $body;
		
		$mail->MsgHTML($body);
		
		//De
		$mail->AddReplyTo($this->objContato->get('email'), $this->objContato->get('nome'));
		$mail->SetFrom($this->objContato->get('email'), $this->objContato->get('nome'));
		
		$mail->From = $this->objContato->get('email');
		$mail->FromName = $this->objContato->get('nome');
		
		//Para
		$mail->AddAddress('contato@onixseven.com.br', 'Onix Seven');
		
//		$mail->SMTPDebug = 2;//Debug
		
		//if(! empty($doc_url)) $mail->AddAttachment($doc_url);// attachment

		if(! $mail->Send()) {
			$this->msg_erro .= $mail->ErrorInfo;
			return false;
		}
		else {
			$this->msg_sucesso = 'Email Enviado com sucesso';
			return true;
		}
		
	}
	
}//fim classe

new ContatoController();
?>