<?php
//error_reporting(NULL);ini_set('display_errors',false);
//error_reporting(E_ALL);ini_set('display_errors',true);

/**
 * Constroi a pagina especifica do site de acordo com o parametro
 **/
class SiteFactory{

	public function __construct(){//construtor
		$this->fabricar();
	}
	
	/**
	 * Fabrica as paginas 
	**/
	protected function fabricar(){
		$param = strtolower( K13Security::anti_injection( K13FriendlyUrl::get_url_param(1) ));
		
		switch ($param) {
			case 'buscar':
				new PageBuscar();
			break;
			
			default:
				//header('location:'.HOST.'404/');
				new PaginaNaoEncontrada();
			break;
		}
	}
}//Fim classe

new SiteFactory();
?>