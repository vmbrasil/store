<?php
ini_set('SMTP', 'smtp.gmail.com');
ini_set('sendmail_from', 'onixdobrasilmail@gmail.com');
ini_set('smtp_port', '465');

error_reporting(NULL);ini_set('display_errors',false);

//phpinfo();exit;

/**
 * Classe Controlador do Formulario Trabalhe Conosco
 * Gerencia os eventos do formulario
 * @author Alex Klen
 * */
class TrabalheController{
	protected $objTrabalhe = null;	//Obj model -  contato
	protected $msg_erro = '';		//String - Mensagem de erros que podem ocorrer
	protected $msg_sucesso = '';	//String - Mensagem de sucesso ao enviar o formulario
	
	public function __construct(){
		$this->objTrabalhe = new Trabalhe();
		
		$this->do_action();
		$this->gerarTemplate();
	}
	
	/*Executa/Processa as a��es da pagina*/
	protected function do_action(){
		if($_SERVER['REQUEST_METHOD']=='POST'){
//			var_dump($_REQUEST);
			
			$this->recebeDados($this->objTrabalhe);
			
			if($this->validar()){
				$this->receberArquivo();
				$this->enviarEmail();
			}
//			else {
//				$this->msg_erro .= '<li>Ocorreu um erro ao validar os dados!</li>';
//			}
		}
	}
	
	/*Gera o template da pagina*/
	protected function gerarTemplate(){
		global $smarty;
		
//		$smarty->debugging = true;
		$smarty->assign('backgroud','contentTrabalho');
		
		$smarty->assign('css','<link rel="stylesheet" href="view/style/formulario.css" type="text/css" media="screen" />');
		$smarty->assign('javascript','<script src="view/script/trabalhe.js"></script>');
		
		$smarty->assign('msg_erro',$this->msg_erro);
		$smarty->assign('msg_sucesso',$this->msg_sucesso);
		
		//die('Continuar a implementar as mensagens de resposta, inserir condi��es no tpl, implementar enviarEmail, implementar validar. Estou na linha 54');
	}
	
	/**
	 * Recebe os dados enviados de formul�rios
	 * @param OnModelDAOLIB $parDAO Objeto DAO a processar
	 * @author Alex Klen
	 */
	protected function recebeDados(&$objENT = null) {
		foreach ($_POST as $key => $value){
			$this->objTrabalhe->set($key, $value);
		}
	}
	
	/*Valida os dados de envio do email
	 * @return boolean - sucesso
	 * */
	protected function validar(){
		$retorno = true;
		if(empty($_POST['nome'])){
			$this->msg_erro .= '<li>Nome deve ser preenchido!</li>';
			$retorno = false;
		}
		if(empty($_POST['email'])){
			$this->msg_erro .= '<li>E-mail deve ser preenchido!</li>';
			$retorno = false;
		}
		if(empty($_POST['telefone'])){
			$this->msg_erro .= '<li>Telefone deve ser preenchido!</li>';
			$retorno = false;
		}
		if(empty($_POST['mensagem'])){
			$this->msg_erro .= '<li>Mensagem deve ser preenchida!</li>';
			$retorno = false;
		}
		
		return $retorno;
	}
	
	/**
	 * Envia a mensagem de contato para o email
	 * @author Alex Klen
	 * @return boolean - sucesso
	 */
	protected function enviarEmail() {
		$mail = new PHPMailerLite();
		
		//config-----------------------------------------
		$mail->IsSMTP = true;// telling the class to use SMTP
		$mail->IsMAIL();
		
		$mail->SMTPSecure = 'ssl';
		$mail->SMTPAuth   = true;// enable SMTP authentication
		$mail->Mailer   = "smtp";
		
		$mail->Port = 465;//587
		$mail->Host       = "smtp.mail.yahoo.com";// Ip do GMAIL - telnet smtp.gmail.com 587
		//gmail-smtp-in.l.google.com
		//smtp.gmail.com

		$mail->SMTPKeepAlive = 'true'; 
		$mail->Username   = "onixdobrasilmail@gmail.com";	// GMAIL username
		$mail->Password   = "@asdfgh#";            			// GMAIL password

//		$mail->SetFrom('contato@onixseven.com.br', 'Onix Seven');
//		$this->AddReplyTo(contato@onixseven.com.br', 'Onix Seven');
		
		//dados-----------------------------------------
		$subject = 'Contato Realizado Pelo Site';
		$mail->Subject		= $subject;

		//Definimos que o e-mail ser� enviado no formato HTML
		$mail->IsHTML(true);
		
		$mail->AltBody		= "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		$body				= '';
		$body				.= '<h3>Nome: ' .$this->objTrabalhe->get('nome') .'</h3>';
		$body				.= '<h3>Email: ' .$this->objTrabalhe->get('email') .'</h3>';
		$body				.= '<h3>Telefone:' .$this->objTrabalhe->get('telefone') .'</h3>';
		$body				.= '<h3>Cidade:' .$this->objTrabalhe->get('cidade') .'</h3>';
		$body				.= '<h3>UF:' .$this->objTrabalhe->get('uf') .'</h3>';
		$body				.= '<h3>Curriculum: <a href="' .HOST.$this->objTrabalhe->get('arquivo_upload') .'">' .($this->objTrabalhe->get('arquivo_upload') != '' ? $this->objTrabalhe->get('arquivo_upload'): '') .'</a></h3>';
		$body				.= '<h3>Mensagem:' .$this->objTrabalhe->get('mensagem') .'</h3>';
//		echo $body;
		
		$mail->MsgHTML($body);
		
		//De
		$mail->AddReplyTo($this->objTrabalhe->get('email'), $this->objTrabalhe->get('nome'));
		$mail->SetFrom($this->objTrabalhe->get('email'), $this->objTrabalhe->get('nome'));
		
		$mail->From = $this->objTrabalhe->get('email');
		$mail->FromName = $this->objTrabalhe->get('nome');
		
		//Para
		$mail->AddAddress('contato@onixseven.com.br', 'Onix Seven');
		
//		$mail->SMTPDebug = 2;//Debug
		
		//if(! empty($doc_url)) $mail->AddAttachment($doc_url);// attachment

		if(! $mail->Send()) {
			$this->msg_erro .= $mail->ErrorInfo;
			//Deleta o arquivo se foi enviado
			if($this->objTrabalhe->get('arquivo_upload') != '') unlink($this->objTrabalhe->get('arquivo_upload'));
			
			return false;
		}
		else {
			$this->msg_sucesso = 'Email Enviado com sucesso';
			return true;
		}
		
	}
	
	/**
	 * Recebe o arquivo enviado por upload
	 * Armazena em: ?
	 * @author Alex Klen
	 * @return boolean - sucesso
	 */
	protected function receberArquivo(){
//		var_dump($_FILES);
		//Verifica se algum arquivo foi enviado
		if(empty($_FILES['arquivo']['name']) || !is_file($_FILES['arquivo']['tmp_name'])) return false;
		
		// Pasta onde o arquivo vai ser salvo
		$_UP['pasta'] = 'upload/';
		
		// Tamanho m�ximo do arquivo (em Bytes)
		$tamanho_maximo = 2; //2Mb
		$_UP['tamanho'] = 1024 * 1024 * $tamanho_maximo;
		
		// Array com as extens�es permitidas
		$_UP['extensoes'] = array('doc', 'docx', 'htm', 'html', 'pdf', 'txt');
		
		//Renomeia o arquivo? (Se true, o arquivo ser� salvo com um nome �nico do time que foi enviado + exten�ao)
		$_UP['renomeia'] = true;
		
		// Array com os tipos de erros de upload do PHP
		$_UP['erros'][0] = 'N�o houve erro';
		$_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
		$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
		$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
		$_UP['erros'][4] = 'N�o foi feito o upload do arquivo';
		
		// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
		if ($_FILES['arquivo']['error'] != 0 && $_FILES['arquivo']['error'] != 4) {
			$this->msg_erro .= '<li>N�o foi poss�vel fazer o upload</li>';
			$this->msg_erro .= '<li>' .$_UP['erros'][$_FILES['arquivo']['error']] .'</li>';
			return false;
		}
		
		if ($_FILES['arquivo']['size'] <= 0 || !is_file($_FILES['arquivo']['tmp_name'])) {
			$this->msg_erro .= '<li>O arquivo recebido est� corrompido!</li>';
			return false;
		}
		
		// Caso script chegue a esse ponto, n�o houve erro com o upload e o PHP pode continuar
		// Faz a verifica��o da extens�o do arquivo
		$extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
		if (!in_array($extensao, $_UP['extensoes'])) {
			$ext = '';
			foreach ($_UP['extensoes'] as $extensao){$ext .= (empty($ext) ? $extensao : ','.$extensao);}
			$this->msg_erro .= '<li>Por favor, envie arquivos com as seguintes extens�es: ' .$ext .'.</li>';
			return false;
		}
		
		// Faz a verifica��o do tamanho do arquivo
		else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
			$this->msg_erro .= "<li>O arquivo enviado � muito grande, envie arquivos de at� {$tamanho_maximo}Mb.</li>";
			return false;
		}
		
		

		// O arquivo passou em todas as verifica��es, hora de tentar mov�-lo para a pasta
		
		// Primeiro verifica se deve trocar o nome do arquivo
		if ($_UP['renomeia'] == true) {
			// Cria um nome baseado no UNIX TIMESTAMP atual e com sua extens�o
			$nome_final = time() .'.' .strtolower(end(explode(".", $_FILES['arquivo']['name'])));
		}
		else {// Mant�m o nome original do arquivo
			//Faz o tratamento dos caracteres
			$nome_final = strtolower($_FILES['arquivo']['name']);
			$caracteres = array("�","~","^","]","[","{","}",";",":","�",",",">","<","-","/","|","@","$","%","�","�","�","�","�","�","�","�","+","=","*","&","(",")","!","#","?","`","�"," ","�");
			$arquivo_tratado = str_replace($caracteres,"",$nome_final);
			$nome_final = $arquivo_tratado;
		}
		
		// Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
		if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] .$nome_final)) {
			// Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
			$this->msg_sucesso .= 'Arquivo recebido com sucesso!';
			$this->objTrabalhe->set('arquivo_upload', $_UP['pasta'] .$nome_final);
			return true;
		}
		else {
			// N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
			$this->msg_erro .= '<li>N�o foi poss�vel receber o arquivo, verifique se o caminho est� correto!</li>';
			return false;
		}
	}
	
}//fim classe

new TrabalheController();
?>