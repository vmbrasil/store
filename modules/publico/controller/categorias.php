<?php
/**
 * Essa pagina lista todas as categorias
 * Lista alguuns produtos mais importantes das principais categorias, configurado no admin
 **/
class CategoriasController extends PageVitrineController{
	protected $objCategoria = null;	//Entidade Categoria da Vitrine
	protected $objProduto = null;	//Entidade Produto
	protected $objImg = null;		//Entidade Imagem
	protected $objPaginador = null;	//Entidade Paginador - A entidade a ser manipulada
	
	protected $parametros = array();//Array de Parametros
	/*cat = categoria id
	* mar = marca id
	* ord = ordena��o()
	* */
	
	//METODOS_____________________________________________________________
	public function __construct(){//construtor
		$this->objCategoria	= new CategoriaLojaENT();
		$this->objProduto	= new ProdutoENT();
		$this->objImg		= new ImagemENT();
		$this->objPaginador = new K13PagerBar();
		
		parent::__construct();
	}
	
	/**
	 * Carrega os Dados principais da pagina antes de tudo
	**/
	protected function carregar(){
		//------carrega lista de categorias------------
		$lista = $this->objCategoria->select_categorias(null, array('niveis' => 4));
		$this->objSmarty->assign('categorias',$lista);
		
		//------------ CARREGA AS OUTRAS COISAS ------------
		$this->carregar_produtos_visitados();
		$this->carregar_BarraDiretorios();
	}
	
	/**
	 * Carrega os dados padroes da pagina(header,topo, etc)
	**/
	protected function carregarDadosPadrao(){
		parent::carregarDadosPadrao();
		$this->carregar();
	}

	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->add_titulo(' - Categorias');
		
		//Template Especifico
		$this->addCssFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'css/plugins/infinit-slider.css');
		$this->addJsFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'js/plugins/jquery.infinit-slider.js');
		
		$this->addJsFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'js/categorias.js');
		$this->addCssFileToHead(K13Path::getHost().Config::get_instance()->get_propertie('system_template_url') .'css/categorias.css');
		
		$this->set_propertie('templatePage','modules/publico/view/PageCategorias.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
	}
	
	/**
	 * Carrega a Barra de Diretorios
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
		
		$diretorios = array(
			array('label'	=>	'In�cio',		'url' => HOST)
			,array('label'	=>	'Categorias',	'url' => HOST.'categorias/')
		);
		
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}

	/**
	 * Carrega as Categorias de Prateleiras
	**/
	protected function carregar_vitrines(){
	}
	
	protected function carregar_produtos_visitados(){
		$params = array(
			'qtd' => 12
			,'page' => $page
//			,'destaque' => $destaque
//			,'categoria' => $categoria
//			,'ord_nome' => $ord_nome
			,'ord_data' => $ord_data
//			,'ord_vendas' => $ord_vendas
//			,'ord_preco' => $ord_preco
//			,'ord_votos' => 'DESC'
			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		
		$this->objSmarty->assign('produtos_relacionados', $result);
	}	
	
	

}//Fim classe

new CategoriasController();