<?php
//error_reporting(E_ALL);ini_set('display_errors',true);
error_reporting(NULL);ini_set('display_errors',false);

header('Content-Type: application/json; charset=iso-8859-1');

/*Fun�oes do Ajax para o site consumir os servi�os*/
class Ajax{
	public function __construct(){//construtor
		$param = K13FriendlyUrl::get_url_param(1);
		switch($param){
			case 'departamentos':
				$this->get_departamentos();
			break;
			//USER
			case 'user_historico':
				$this->get_historico();
			break;
			
			case 'user_carrinho':
				$this->get_user_carrinho();
			break;
			//Pagina/Vitrine
			case 'get_pagina':
				$this->get_pagina();
			break;
			case 'get_produto':
				$this->get_produto();
			break;
			
			
			default:
				echo 'null';
		}
	}
	
	/**
	 * Carrega os deparatamentos do site
	 * @return echo json format
	 * */
	protected function get_departamentos(){
		$result = FrontController::get_departamentos();
		echo K13DataFormat::custom_json_encode($result);
	}
	
	/**
	 * Carrega o historico do usuario
	 * @return echo json format
	 * */
	protected function get_historico(){
		$result = FrontController::get_historico_usuario();
		echo K13DataFormat::custom_json_encode($result);
	}
	
	/**
	 * Carrega o carrinho do usuario
	 * @return echo json format
	 * */
	protected function get_user_carrinho(){
		$result = FrontController::get_user_carrinho();
		echo K13DataFormat::custom_json_encode($result);
	}
	
	/**
	 * Carrega a pagina completa de acordo com os parametros
	 * @return echo json format
		 *		titulo - Titulo da pagina
		 *		num - numero da pagina
		 *		BreadCrumb - Objeto bread crumb / Barra de Diretorio
		 *		Banners - Objeto de Banners
		 *		Vitrines
		 *		Menus
	 * */
	protected function get_pagina(){
		$result = FrontController::get_pagina();
		echo K13DataFormat::custom_json_encode($result);
	}
	
	/**
	 * Carrega os produtos pedidos por ajax
	 * @parametros - Parametros entrados pelo ajax (GET)
			qtd: 0
			,page: 0
			,id: 0
			,categoria: 0
			,marca: 0
			,destaque: false
			,oferta: false
			,frete: false
			,em_promocao: false
			,ord_id: (ASC,DESC)
			,ord_data: (ASC,DESC)
			,ord_preco: (ASC,DESC)
			,ord_nome: (ASC,DESC)
			,ord_vendas: (ASC,DESC)
			,ord_votos: (ASC,DESC)
			,ord_visitas: (ASC,DESC)
			,ord_sugestao: (ASC,DESC)
	 * @return echo json format
	 * */
	protected function get_produto(){
		$result = FrontController::get_produto($_GET);
		echo K13DataFormat::custom_json_encode($result);
	}
	
	/*Pega os enderecos do usuario logado*/
	protected function get_user_address(){
		$result = FrontController::get_user_address();
		echo K13DataFormat::custom_json_encode($result);
	}
	
}// fim class
new Ajax();
?>