<?php
//error_reporting(NULL);ini_set('display_errors',false);
//error_reporting(E_ALL);ini_set('display_errors',true);

/**
 * Controlador da Vitrine de Produtos
 **/
class VitrineController extends PageLoja{
	protected $objProduto = null;	//Entidade Produto
	protected $objImg = null;		//Entidade Imagem
	protected $parametros = array();//Array de Parametros
	/*cat = categoria id
	* mar = marca id
	* ord = ordena��o()
	* */
	
	//METODOS_____________________________________________________________
	public function __construct(){//construtor
		$this->objProduto	= new ProdutoENT();
		$this->objImg		= new ImagemENT();
		
		parent::__construct();
	}

	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//Titulo especifico da Pagina
		$this->set_titulo('Meu Comercio 2.0 - '.'Nome da categoria especifica');
		
		$this->carregar_Parametros();//Primeiro a ser carregado
		
		$this->carregar_Prateleiras();
		$this->carregar_BarraDiretorios();
		$this->carregar_BannerSuperior();
		$this->carregar_BannersRotativo();
		$this->carregar_BannersMenuDireito();
		$this->carregar_MenuDireito();
		$this->carregar_MenuEsquerdo();
		
		//Template Especifico
		//$this->addCssFileToHead(HOST.'modules/publico/view/style/vitrine.css');
		//$this->addJsFileToHead(HOST.'modules/publico/view/script/vitrine.js');
		
		$this->set_propertie('templatePage','modules/publico/view/promocoes.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
	}
	
	/**
	 * Carrega os Parametros da pagina
	**/
	protected function carregar_Parametros(){
		//categoria
/*		if($this->get_param('id_categoria') == '') $this->set_param('id_categoria',$_REQUEST['id_categoria']);
		if($this->get_param('id_categoria') == '') $this->set_param('id_categoria',K13FriendlyUrl::get_url_param(1));
		//marca
		if($this->get_param('id_marca') == '') $this->set_param('id_marca',$_REQUEST['id_marca']);
		if($this->get_param('id_marca') == '') $this->set_param('id_marca',K13FriendlyUrl::get_url_param(1));
		//ordenacao
		if($this->get_param('ord') == '') $this->set_param('ord',$_REQUEST['ord']);
		//pagina
		if($this->get_param('pag') == '') $this->set_param('ord',$_REQUEST['pag']);
		
		//FILTROS
		//Destaque
		if($this->get_param('destaque') == '') $this->set_param('destaque',$_REQUEST['destaque']);
		//Ofertas
		if($this->get_param('oferta') == '') $this->set_param('oferta',$_REQUEST['oferta']);
		//Sugest�es
		if($this->get_param('sugerido') == '') $this->set_param('sugerido',$_REQUEST['sugerido']);
		//Promo��es
		if($this->get_param('promo') == '') $this->set_param('promo',$_REQUEST['promo']);
		//frete
		if($this->get_param('frete') == '') $this->set_param('frete',$_REQUEST['frete']);
*/	}
	
	/**
	 * Carrega a Barra de Diretorios
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
		$diretorios = array(
			array('label'	=>	'In�cio',		'url' => HOST)
			,array('label'	=>	'Produtos',	'url' => HOST.'produtos/')
//			,array('label'	=>	'Departamento',	'url' => HOST.'categoria/12/Nome-Escolhido')
//			,array('label'	=>	'Categoria',	'url' => HOST.'categoria/54/Nome-Escolhido')
		);
		
		/*Departamento Atual*/
		if($this->get_param('id_categoria') != '')
			$diretorios[] =  array('label'	=>	'Carregar Departamento',	'url' => HOST.'categoria/'.$this->get_param('id_categoria').'/Nome-Escolhido');
		
		/*Categoria Atual*/
		if($this->get_param('id_categoria') != '') 
			$diretorios[] =  array('label'	=>	'Carregar Categoria',	'url' => HOST.'categoria/'.$this->get_param('id_categoria').'/Nome-Escolhido');
		
		/*Marca Selecionada*/
		if($this->get_param('id_marca') != ''){
			if($this->get_param('id_categoria') != '')//Categoria selecionada
				$diretorios[] =  array('label'	=>	'Carregar Categoria',	'url' => HOST.'categoria/'.$this->get_param('id_categoria').'/Nome-Escolhido/?id_marca='.$this->get_param('id_marca'));
			else//Sem categoria selecionanda
				$diretorios[] =  array('label'	=>	'Carregar Marca',	'url' => HOST.'marca/'.$this->get_param('id_marca').'/Nome-Escolhido');
		}
		
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}

	/**
	 * Carrega o Banner Superior
	**/
	protected function carregar_BannerSuperior(){
	}

	/**
	 * Carrega os Banners Rotativo
	**/
	protected function carregar_BannersRotativo(){
	}

	/**
	 * Carrega os Banners Menu Direito
	**/
	protected function carregar_BannersMenuDireito(){
		$objSqlSt = new K13SqlStatement();
		$objSqlSt->set_tabela('banner');
		$objSqlSt->add_where('ativo = 1');
		$objSqlSt->add_order_by('datacad DESC');
		$sql = $objSqlSt->gerar_SqlSelect("
			id
			,descricao AS description
			,url_img AS url_img
		");

		$objConexBd = K13ConnectionConfig::get_connection();
		$banners = $objConexBd->query_fetchAll($sql);
		
		$this->objSmarty->assign('banners_laterais', $banners);
	}

	/**
	 * Carrega o Menu Esquerdo
	**/
	protected function carregar_MenuEsquerdo(){
		/*
		 * Parametros esperados:
		 * categoria id
		 * */
		$id_categoria = $_REQUEST['id_categoria'];
		if(empty($id_categoria)) $id_categoria = 
		//Continuar a implementar
		$objDepartamento = new CategoriaLojaENT(array('where'=>'','ord'=>''));
		$departamentos = $objDepartamento->select_categorias();
		$this->objSmarty->assign('list_departamento', $departamentos);
		
		
		
		
		$objDepartamento = new CategoriaLojaENT();
		$departamentos = $objDepartamento->select_departamentos();
		$this->objSmarty->assign('list_departamento', $departamentos);
		
		$objSqlSt = new K13SqlStatement();
		$objSqlSt->set_tabela('departamento AS D');
		$objSqlSt->add_where('D.ativo = 1');
		$objSqlSt->add_order_by('D.ordem DESC');
		$objSqlSt->add_order_by('D.destaque DESC');
		$sql = $objSqlSt->gerar_SqlSelect("
			D.id
			,D.descricao AS description
			,(SELECT COUNT(*) FROM meu_comercio.produto P
				INNER JOIN meu_comercio.categoria AS C ON C.id = P.categoria_id
				INNER JOIN meu_comercio.departamento AS DP ON DP.id = C.departamento_id
				WHERE (DP.id = D.id)
			) AS pcount
			,'' AS categorias
		");
		
		$objConexBd = K13ConnectionConfig::get_connection();
		$departamentos = $objConexBd->query_fetchAll($sql);
		
		foreach($departamentos as $key => $dep){
			$objSqlSt->limparClasse();
			$objSqlSt->set_tabela('categoria AS C');
			$objSqlSt->add_where('C.ativo = 1');
			$objSqlSt->add_where('C.departamento_id = '.$dep['id']);
			$objSqlSt->add_order_by('C.visitas DESC');
			$objSqlSt->set_limit('0,4');
			$sql = $objSqlSt->gerar_SqlSelect("
				C.id
				,C.descricao AS description
				,(SELECT COUNT(*) FROM produto WHERE categoria_id = C.id
				) AS pcount
			");
			
			$objConexBd->executar_query($sql);
	
			$categorias = $objConexBd->get_fetch_array();
			
			$departamentos[$key]['categorias'] = $categorias;
		}
		
		$menu = array('titulo' => 'Produtos', 'departamentos' => $departamentos);
		$this->objSmarty->assign('menu_departamentos', $menu);
	}

	/**
	 * Carrega o Menu Direito
	**/
	protected function carregar_MenuDireito(){
	}

	//Prateleiras-----------------------------------------
	/**
	 * Adicina 1 prateleira
	 * @param string $descricao - descricao da prateleira
	 * @param string $id - id da prateleira
	 * @param array $produtos - produtos da prateleira
	**/
	protected function add_Prateleira($descricao, $id='', $produtos = null){
		if(empty($id))$id = $descricao;
		$this->vitrines[$id] = array('descricao' => $descricao, 'produtos' => $produtos);
	}


	/**
	 * Carrega os produtos a serem mostrados na vitrine
	**/
	protected function carregar_Produtos(){
		//PARAMETROS----------------------------------
		$objSql_produtos = new K13SqlStatement();
		$objSql_produtos->set_tabela('produto AS P');
		$objSql_produtos->add_where('P.ativo = 1');
		$objSql_produtos->add_order_by('datacad DESC');
		//$objSql_produtos->set_limit('0,4');
		$objSql_produtos->add_join('INNER JOIN categoria AS C ON P.id_categoria = C.id_categoria');
		$sql = $objSql_produtos->gerar_SqlSelect("
			P.id
			,P.titulo
			,P.precoa AS preco_anterior
			,P.precov AS preco_venda
			,P.precov/12 AS valor_parcela
			,P.categoria_id AS categoria_id
			,C.descricao AS categoria_descricao
		");
		var_dump($sql);
				
		$objConexBd = K13Connection::get_connection();
		$objConexBd->executar_query($sql);

		$tupla_produtos[0] = $objConexBd->get_fetch_array();
		
		$this->vitrines[0]['produtos'] = $tupla_produtos[0];
	}
	
	/**
	 * Carrega as Categorias de Prateleiras
	**/
	protected function carregar_Prateleiras(){
//		$this->carregar_Produtos();
//		$this->carregar_PrateleiraDestaque();
//		$this->carregar_PrateleiraOfertas();
//		$this->carregar_PrateleiraSugeridos();
//		$this->carregar_PrateleiraLancamentos();
		
		$this->objSmarty->assign('vitrine', $this->vitrines);
	}
	
	/**
	 * Carrega os Destaques
	**/
	protected function carregar_PrateleiraDestaque(){
		$objSql_produtos = new K13SqlStatement();
		$objSql_produtos->set_tabela('produto AS P');
		$objSql_produtos->add_where('P.ativo = 1');
		$objSql_produtos->add_order_by('P.destaque DESC');
		$objSql_produtos->set_limit('0,4');
		$objSql_produtos->add_join('INNER JOIN categoria AS C ON P.categoria_id = C.id');
		$sql = $objSql_produtos->gerar_SqlSelect("
			P.id
			,P.titulo
			,P.fotos
			,P.precoa AS preco_anterior
			,P.precov AS preco_venda
			,P.precov/12 AS valor_parcela
			,P.categoria_id AS categoria_id
			,C.descricao AS categoria_descricao
		");
				
		$objConexBd = K13Connection::get_connection();
		$objConexBd->executar_query($sql);

		$produtos = $objConexBd->get_fetch_array();
		
		//Carrega as imagens dos produtos
		foreach($produtos as $key => $value){
			$this->objProduto->carregar_by_array($value);
			
			$this->objImg->get_dao()->carregar( array('id' => $this->objProduto->get_foto(0)) );
			
			$produtos[$key] = array_merge( $produtos[$key], array('img_url' => $this->objImg->get('img_url'), 'thumb_url' => $this->objImg->get('thumb_url')) );
		}

		
		$this->add_Prateleira('Destaques', 'destaques', $produtos);
	}

	/**
	 * Carrega Ofertas
	**/
	protected function carregar_PrateleiraOfertas(){
		$objSql_produtos = new K13SqlStatement();
		$objSql_produtos->set_tabela('produto AS P');
		$objSql_produtos->add_where('P.ativo = 1');
		$objSql_produtos->add_order_by('P.oferta DESC');
		$objSql_produtos->set_limit('0,4');
		$objSql_produtos->add_join('INNER JOIN categoria AS C ON P.categoria_id = C.id');
		$sql = $objSql_produtos->gerar_SqlSelect("
			P.id
			,P.titulo
			,P.fotos
			,P.precoa AS preco_anterior
			,P.precov AS preco_venda
			,P.precov/12 AS valor_parcela
			,P.categoria_id AS categoria_id
			,C.descricao AS categoria_descricao
		");
				
		$objConexBd = K13Connection::get_connection();
		$objConexBd->executar_query($sql);

		$produtos = $objConexBd->get_fetch_array();

		//Carrega as imagens dos produtos
		foreach($produtos as $key => $value){
			$this->objProduto->carregar_by_array($value);
			
			$this->objImg->get_dao()->carregar( array('id' => $this->objProduto->get_foto(0)) );
			
			$produtos[$key] = array_merge( $produtos[$key], array('img_url' => $this->objImg->get('img_url'), 'thumb_url' => $this->objImg->get('thumb_url')) );
		}
		
		$this->add_Prateleira('Ofertas', 'ofertas', $produtos);
	}

	/**
	 * Carrega Ofertas
	**/
	protected function carregar_PrateleiraSugeridos(){
		$objSql_produtos = new K13SqlStatement();
		$objSql_produtos->set_tabela('produto AS P');
		$objSql_produtos->add_where('P.ativo = 1');
		$objSql_produtos->add_order_by('P.sugestao DESC');
		$objSql_produtos->set_limit('0,4');
		$objSql_produtos->add_join('INNER JOIN categoria AS C ON P.categoria_id = C.id');
		$sql = $objSql_produtos->gerar_SqlSelect("
			P.id
			,P.titulo
			,P.fotos
			,P.precoa AS preco_anterior
			,P.precov AS preco_venda
			,P.precov/12 AS valor_parcela
			,P.categoria_id AS categoria_id
			,C.descricao AS categoria_descricao
		");
				
		$objConexBd = K13Connection::get_connection();
		$objConexBd->executar_query($sql);

		$produtos = $objConexBd->get_fetch_array();

		//Carrega as imagens dos produtos
		foreach($produtos as $key => $value){
			$this->objProduto->carregar_by_array($value);
			
			$this->objImg->get_dao()->carregar( array('id' => $this->objProduto->get_foto(0)) );
			
			$produtos[$key] = array_merge( $produtos[$key], array('img_url' => $this->objImg->get('img_url'), 'thumb_url' => $this->objImg->get('thumb_url')) );
		}
		
		$this->add_Prateleira('Sugest�es', 'sugeridos', $produtos);
	}

	/**
	 * Carrega Lancamentos
	**/
	protected function carregar_PrateleiraLancamentos(){
		$objSql_produtos = new K13SqlStatement();
		$objSql_produtos->set_tabela('produto AS P');
		$objSql_produtos->add_where('P.ativo = 1');
		$objSql_produtos->add_order_by('P.datacad DESC');
		$objSql_produtos->set_limit('0,4');
		$objSql_produtos->add_join('INNER JOIN categoria AS C ON P.categoria_id = C.id');
		$sql = $objSql_produtos->gerar_SqlSelect("
			P.id
			,P.titulo
			,P.fotos
			,P.precoa AS preco_anterior
			,P.precov AS preco_venda
			,P.precov/12 AS valor_parcela
			,P.categoria_id AS categoria_id
			,C.descricao AS categoria_descricao
		");
		
		$objConexBd = K13Connection::get_connection();
		$objConexBd->executar_query($sql);

		$produtos = $objConexBd->get_fetch_array();

		//Carrega as imagens dos produtos
		foreach($produtos as $key => $value){
			$this->objProduto->carregar_by_array($value);
			
			$this->objImg->get_dao()->carregar( array('id' => $this->objProduto->get_foto(0)) );
			
			$produtos[$key] = array_merge( $produtos[$key], array('img_url' => $this->objImg->get('img_url'), 'thumb_url' => $this->objImg->get('thumb_url')) );
		}
		
		$this->add_Prateleira('Lan�amentos', 'Lancamentos', $produtos);
	}

}//Fim classe

new VitrineController();
?>