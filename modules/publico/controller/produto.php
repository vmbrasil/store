<?php
//error_reporting(NULL);ini_set('display_errors',false);
//error_reporting(E_ALL);ini_set('display_errors',true);

/*Controlador da Visualizacao do Produto*/
class ProdutoController extends PageLoja{
	protected $objProduto = null;	//Entidade Produto
	protected $objCategoria = null;	//Entidade Categoria do Produto
	protected $objMarca = null;	//Entidade Marca do Produto
	
	protected $objImg = null;		//Entidade Imagem

	//METODOS_____________________________________________________________
	public function __construct(){//construtor
		$this->objProduto	= new ProdutoENT();
		$this->objCategoria	= new CategoriaLojaENT();
		$this->objMarca	= new MarcaENT();
		$this->objImg		= new ImagemENT();
		
		parent::__construct();
	}
	
	/**
	 * Carrega os dados padroes da pagina(header,topo, etc)
	**/
	protected function carregarDadosPadrao(){
		parent::carregarDadosPadrao();
		$this->carregar();
	}
	
	/**
	 * Carrega os Dados principais da pagina antes de tudo
	**/
	protected function carregar(){
		$this->carregarProduto();
		$this->carregar_BarraDiretorios();
		$this->carregar_compre_junto();
		$this->carregar_outros_produtos();
	}
	
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		
		//Titulo especifico da Pagina
		$this->set_titulo('Meu Comercio 2.0 - ' .$this->objProduto->get('titulo'));
		
		$this->addCssFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'css/plugins/jquery.jqzoom.css');
		$this->addCssFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'css/plugins/jquery.rating.css');
		$this->addCssFileToHead(K13Path::getHost() .Config::get_instance()->get_propertie('system_template_url') .'css/plugins/jquery-ui-1.8.8.custom.css');
		$this->addCssFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'css/plugins/infinit-slider.css');
		
		$this->addCssFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'css/produto.css');
		$this->addCssFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'css/comments.css');
		
		
		$this->addJsFileToHead(K13Path::getUriFramework() .'terceiros/jquery/js/jquery-ui-1.8.8.custom.min.js');
		$this->addJsFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'js/plugins/jquery.easing.1.2.js');
		$this->addJsFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'js/plugins/jquery.rating.js');
		$this->addJsFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'js/plugins/jquery.jqzoom-core.js');
		$this->addJsFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'js/plugins/jquery.infinit-slider.js');
		$this->addJsFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'js/plugins/jquery.tweetbutton.js');
		$this->addJsFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'js/plugins/jquery.likebutton.js');
		
		$this->addJsFileToHead(HOST.Config::get_instance()->get_propertie('system_template_url').'js/produto.js');
		
		//Template Especifico
		$this->set_propertie('templatePage','modules/publico/view/produto.tpl');
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
	}

	/**
	 * Carrega os dados do Produto
	**/
	protected function carregarProduto(){
		//Carrega o Produto
		$slug = strtolower( K13Security::anti_injection( K13FriendlyUrl::get_url_param(1) ));
		
		$carregou = $this->objProduto->get_dao()->consultar("AND slug LIKE '" .$slug ."'");
		if($carregou) $this->objProduto->carregar_by_array($carregou[0]);
		
		//Valida�oes
		if(empty($slug) || !$carregou){
			//header('location:'.HOST.'404/');
			new PaginaNaoEncontrada();
		}
		
		//Carrega a Categoria
		$this->objCategoria->get_dao()->carregar( array('id_categoria' => $this->objProduto->get('id_categoria') ) );
		
		//Carrega a Marca
		$this->objMarca->get_dao()->carregar( array('id_marca' => $this->objProduto->get('id_marca') ) );
		
		//Carrega As fotos
		$this->objImg->get_dao()->carregar( array('id' => $this->objProduto->get_foto(0)) );
		$tupla = $this->objProduto->get_tupla();
		
		//Formata os dados para a interface
		$tupla = array_merge($tupla,array(
			'id' => $this->objProduto->get_primaryKeyValueByPosition(0)
			,'img_url' => $this->objImg->get('img_url')
			,'thumb_url' => $this->objImg->get('thumb_url')
			,'avisos_decoded' => K13TListComponent::decode_tlist($this->objProduto->get('avisos'))
			,'itens_decoded' => K13TListComponent::decode_tlist($this->objProduto->get('itens_inclusos'))
			,'tags_decoded' => K13TListComponent::decode_tlist($this->objProduto->get('tags'))
		));
		
		//Marca
		if($this->objMarca->get_primaryKeyValueByPosition(0) > 0){
			$tupla['marca'] = array(
				'id' => $this->objMarca->get_primaryKeyValueByPosition(0)
				,'title' => $this->objMarca->get('nome')
				,'slug' => $this->objMarca->get('slug')
			);
			
			$this->objSmarty->assign('mais', array(
				'label' => $this->objMarca->get('nome')
				,'url' => HOST.'marca/'.$this->objMarca->get('slug').'/'
			));
		}
		else{//Se nao tiver Marca carrega Categoria
			$this->objSmarty->assign('mais', array(
				'label' => $this->objCategoria->get('nome')
				,'url' => HOST.'categoria/'.$this->objCategoria->get('slug').'/'
			));
		}
		
		$this->objSmarty->assign('produto_atual', $tupla);
	}
	
	/**
	 * Carrega a Barra de Diretorios
	**/
//	protected function carregar_BarraDiretorios(){
//		$diretorios = array(
//			array('label'	=>	'In�cio',	'url' => HOST)
//			,array('label' => 'Departamento','url' => HOST.'departamento/12/Nome-Escolhido')
//			,array('label' => 'Categoria','url' => HOST.'categoria/54/Nome-Escolhido')
//			,array('label' => 'SubCategoria','url' => HOST.'subcategoria/54/Nome-Escolhido')
//			,array('label' => 'Marca','url' => HOST.'subcategoria/54/Nome-Escolhido')
//		);
//		
//		$this->objSmarty->assign('barra_diretorio', $diretorios);
//	}
	
	/**
	 * Carrega a Barra de Diretorios (BreadCrumb)
	**/
	protected function carregar_BarraDiretorios(){
		$diretorios = null;
		$result = $this->objCategoria->select_super_categorias($this->objProduto->get('id_categoria'));
		
		$diretorios = array(
			array('label'	=>	'In�cio',		'url' => HOST)
//			,array('label'	=>	'Categorias',	'url' => HOST.'categorias/')
//			,array('label'	=>	$this->objCategoria->get('nome'),	'url' => HOST.'categoria/' .$this->objCategoria->get('slug').'/')
		);
		
		//Formata os registros carregados
		$carregados = array();
		foreach ($result as $key => $item){
			$carregados[] = array('label'	=>	$item['title'],	'url' => HOST.'categoria/'.$item['slug'].'/');
		}
		$diretorios = array_merge($diretorios, $carregados);
		
		//Adiciona a marca
		if($this->objMarca->get_primaryKeyValueByPosition(0) > 0) $diretorios[] = array('label' => $this->objMarca->get('nome'),'url' => HOST.'marca/'.$this->objMarca->get('slug').'/');
		
//		var_dump($diretorios);
		//Template
		$this->objSmarty->assign('barra_diretorio', $diretorios);
		return $diretorios;
	}
	
	
	/**
	 * Carrega outros produtos da vitrine 'Veja Tambem'
	**/
	protected function carregar_outros_produtos(){
		$params = array(
			'qtd' => 12
			,'page' => $page
//			,'destaque' => $destaque
//			,'categoria' => $categoria
//			,'ord_nome' => $ord_nome
//			,'ord_data' => $ord_data
//			,'ord_vendas' => $ord_vendas
//			,'ord_preco' => $ord_preco
//			,'ord_votos' => 'DESC'
//			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		
		$this->objSmarty->assign('produtos_relacionados', $result);
		
		return;
		/* TODO
		 * usar a logica abaixo para montar a lista de produtos relacionados automaticos(Sem intervencao do usuario no admin)
		 * */
		//Produtos da SUBCATEGORIA
		$objSqlSt = new K13SqlStatement();
		$objSqlSt->set_tabela('produto AS P');
		$objSqlSt->add_where('P.ativo = 1');
		$objSqlSt->add_where('P.id != '.$this->objProduto->get_value('id'));
		$objSqlSt->add_where('P.subcategoria_id = '.$this->objProduto->get_value('subcategoria_id'));
		$objSqlSt->add_order_by('RAND()');
		$objSqlSt->set_limit('0,5');
		$objSqlSt->add_join('INNER JOIN categoria AS C ON P.categoria_id = C.id');
		$sql = $objSqlSt->gerar_SqlSelect("
			P.id
			,P.titulo
			,P.fotos
			,P.precoa AS preco_anterior
			,P.precov AS preco_venda
			,P.precov/12 AS valor_parcela
			,P.categoria_id AS categoria_id
			,C.descricao AS categoria_descricao
		");
		
		$objConexBd = K13ConnectionConfig::get_connection();
		
		$produtos = $objConexBd->query_fetchAll($sql);
		
		if(! $produtos){
			//CARREGA OUTROS DA CATEGORIA
			$objSqlSt->limparClasse();
			$objSqlSt->set_tabela('produto AS P');
			$objSqlSt->add_where('P.ativo = 1');
			$objSqlSt->add_where('P.id != '.$this->objProduto->get_value('id'));
			$objSqlSt->add_where('P.categoria_id = '.$this->objProduto->get_value('categoria_id'));
			$objSqlSt->add_order_by('RAND()');
			$objSqlSt->set_limit('0,5');
			$objSqlSt->add_join('INNER JOIN categoria AS C ON P.categoria_id = C.id');
			$sql = $objSqlSt->gerar_SqlSelect("
				P.id
				,P.titulo
				,P.fotos
				,P.precoa AS preco_anterior
				,P.precov AS preco_venda
				,P.precov/12 AS valor_parcela
				,P.categoria_id AS categoria_id
				,C.descricao AS categoria_descricao
			");
			
			$produtos = $objConexBd->query_fetchAll($sql);
			
			if(! $produtos){
				//SEN�O SE N�O TIVER MAIS PRODUTOS DA MESMA CATEGORIA CARREGA QUALQUER UM
				$objSqlSt->limparClasse();
				$objSqlSt->set_tabela('produto AS P');
				$objSqlSt->add_where('P.ativo = 1');
				$objSqlSt->add_where('P.id != '.$this->objProduto->get_value('id'));
				$objSqlSt->add_order_by('RAND()');
				$objSqlSt->set_limit('0,5');
				$objSqlSt->add_join('INNER JOIN categoria AS C ON P.categoria_id = C.id');
				$sql = $objSqlSt->gerar_SqlSelect("
					P.id
					,P.titulo
					,P.fotos
					,P.precoa AS preco_anterior
					,P.precov AS preco_venda
					,P.precov/12 AS valor_parcela
					,P.categoria_id AS categoria_id
					,C.descricao AS categoria_descricao
				");
				
				$produtos = $objConexBd->query_fetchAll($sql);
				
				$this->objSmarty->assign('vitrine_produtos', $produtos);
			}
		}
		
		//Carrega as imagens dos produtos
		foreach($produtos as $key => $value){
			$this->objProduto->carregar_by_array($value);
			
			$this->objImg->get_dao()->carregar( array('id' => $this->objProduto->get_foto(0)) );
			
			$produtos[$key] = array_merge( $produtos[$key], array('img_url' => $this->objImg->get('img_url'), 'thumb_url' => $this->objImg->get('thumb_url')) );
		}
		
		$this->objSmarty->assign('vitrine_produtos', $produtos);
	}


	
	/**
	 * Carrega produtos da vitrine 'Compre Junto'
	**/
	protected function carregar_compre_junto(){
		//TODO
		$params = array(
			'qtd' => 6
			,'page' => $page
//			,'destaque' => $destaque
//			,'categoria' => $categoria
//			,'ord_nome' => $ord_nome
//			,'ord_data' => $ord_data
//			,'ord_vendas' => $ord_vendas
//			,'ord_preco' => $ord_preco
//			,'ord_votos' => 'DESC'
//			,'ord_visitas' => 'DESC'
		);
		$result = FrontController::get_produto($params);
		
		$this->objSmarty->assign('produtos_compre_junto', $result);
	}
	
}//Fim classe

new ProdutoController();
?>