<?php

/*Classe para controle dos itens do menu principal*/
class MenuItemENT extends K13Entidade{
	public function __construct($schema_table = null, &$Dao = null){
		parent::__construct(array('schema' => 'meu_comercio', 'table'=>'menu_principal'),new K13DAO());
		$this->set_nomeDescritivo('Itens do Menu Principal');
		$this->set_campoDescritivo('label');

		$this->set_campo(array(
			'name' => 'idmenu'
			, 'value' => ''
			, 'label' => 'ID'
			, 'type' => K13Campo::TINT
		));
		$this->define_primaryKey('idmenu');
		
		$this->set_campo(array(
			'name' => 'label'
			, 'value' => ''
			, 'label' => 'Label Descritivo'
			, 'size' => 100
			, 'type' => K13Campo::TSTRING
		));

		$this->set_campo(array(
			'name' => 'descricao'
			, 'value' => ''
			, 'label' => 'Descricao'
			, 'size' => 200
			, 'type' => K13Campo::TSTRING
		));

		$this->set_campo(array(
			'name' => 'url'
			, 'value' => ''
			, 'label' => 'URL do Link'
			, 'size' => 400
			, 'type' => K13Campo::TSTRING
		));

		$this->set_campo(array(
			'name' => 'ativo'
			, 'value' => 'TRUE'
			, 'default' => 'TRUE'
			, 'label' => 'Ativo'
			, 'type' => K13Campo::TBOOLEAN
		));

		$this->set_campo(array(
			'name' => 'ordem'
			, 'value' => 0
			, 'default' => 0
			, 'label' => 'Ordem de Visualizacao'
			, 'type' => K13Campo::TINT
		));

	}
}//fim class

