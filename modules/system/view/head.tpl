<!DOCTYPE html>
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
	<title>{$titulo|default:''}</title>
	
	<link rel="shortcut icon" href="{$HOST}view/images/favicon.ico" />
	<link rel="stylesheet" type="text/css" media="screen" href="{$HOST}view/templates/{$template_slug}/css/system.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="{$HOST}view/templates/{$template_slug}/css/menu_topo.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="{$HOST}view/templates/{$template_slug}/css/geral.css" />

	<script type="text/javascript" src="{$k13fwJqueryUrl}"></script>
	<script type="text/javascript" src="{$HOST}view/templates/{$template_slug}/js/plugins/json2.js"></script>
	<script type="text/javascript" src="{$HOST}view/templates/{$template_slug}/js/plugins/jquery.price_format.1.0.js"></script>
	<script type="text/javascript" src="{$HOST}view/templates/{$template_slug}/js/plugins/jquery.jcarousel.js"></script>

	<script language="JavaScript" src="{$HOST}view/templates/{$template_slug}/js/menu_topo.js"></script>

	{include file="modules/system/view/system_script.tpl"}

	<script type="text/javascript" src="{$HOST}view/templates/{$template_slug}/js/geral.js"></script>

	{$metatags|default:''}
	{$css|default:''}
	{$js|default:''}
	{$htmlHead|default:''}
</head>


