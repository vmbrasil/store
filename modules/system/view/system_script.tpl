{literal}
<script type="text/javascript">

	/* ######################### SISTEMA ######################### */
	var Global = {};
	Global.System = {
		properties : {
{/literal}
			host : '{$HOST}'	//o host principal do sistema
			,debug : {if $DEBUG == true}true{else}false{/if}	//debug do sistema
{literal}
			//properties Metods
			,get_host : function(){
				return this.host;
			}
			,get_debug : function(){
				return this.debug;
			}
		}//fim properties
		,events : {
			/*Inicia o objeto
			 * Carrega os valores
			 */
			init : function(){
				$.ajax({
					type: 'GET',
					url: 'ajax/system/',
					data: JSON.stringify(params),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response, status) {
						//alert(JSON.stringify(response.d));
			
						if (response == null) {
							return false;
						}
			
						for (var i in response.menu){
							atual = response.d[i];
							var id 	= atual.ID;
							var data = atual.Data;
							var dia = atual.Data.substr(0, 2); // pega os 2 primeiros caracteres
							var mes = atual.Data.substr(3,2);
							var ano = atual.Data.substr(8,2);
							var cidade = atual.Cidade;
							var estado = atual.UF;
							var local = atual.Local;
							var horario = atual.Horario;
			
							var evento  = '<span class="data">'+dia+'/'+mes+'/'+ano+'</span>';
							evento += '<div class="dados local"><strong>LOCAL:</strong> '+local+'</div>';
							evento += '<div class="dados cidade"><strong>CIDADE / ESTADO:</strong> '+cidade+' - '+estado+'</div>';
							evento += '<div class="dados horario"><strong>HOR�RIO:</strong> '+horario+'</div>';
			
							$('#jcarousel-agenda').append('<li>'+evento+'</li>');
							i++;
						}
			
						$('#menu_categorias h3').text(response.titulo);
			
						$('#menu_categorias li:nth(2n-1)').addClass('linha1');
						$('#menu_categorias li:nth(2n)').addClass('linha2');
		
					},
					error: function (xhr, msg, e) {
						alert("Erro! " +e);
					}
				});

			}//fim init
		
		}//fim events
	}//fim system
	
	var System = Global.System;
</script>
{/literal}