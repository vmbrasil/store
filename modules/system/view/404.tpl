<div class="janela">
	<div class="error">
		<h2 class="supertitulo">Ops! P�gina n�o encontrada</h2>
		<p class="subtitulo">
		Erro 404! <br />
		{if $url_404 != ""}O endere�o <a href="{$url_404}">{$url_404}</a> n�o pode ser acessado.<br />{/if}
		Verifique a barra de menus, onde encontrar� o que deseja.<br />
		Se tiver alcan�ado esta p�gina  por algum de nossos links,<br />
		por favor, entre em contato.
		</p>
	</div>
</div>