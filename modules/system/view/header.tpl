
<body>
<!-- Toda Pagina -->
<div class="tudo">
	<!-- Topo -->
	<div class="topo">
		<h1 class="logo"><a href="{$HOST}"><img src="{$HOST}view/templates/template1/images/logo.png" alt="{$titulo|default:''}" title="{$titulo|default:''}" width="266" height="91" /></a></h1>
		<div class="topo_identificacao">{$identificacao|default:'Bem Vindo Visitante. <a href="'|cat:$HOST|cat:'usuario/login/">Entrar | Cadastrar</a>'}</div>

		<div class="menu_institucional">
			<ul>
				<li id="home"><a href="{$HOST}">						Inicial</a></li>
				<li id="lojas"><a href="{$HOST}usuario/perfil/">		Minha Conta</a></li>
				<li id="quemsomos"><a href="{$HOST}site/sobre/">		Quem Somos</a></li>
				<li id="atendimento"><a href="{$HOST}site/contato/">	Central de Atendimento</a></li>
				<li id="duvidas"><a href="{$HOST}site/ajuda/">			D�vidas</a></li>
			</ul>
		</div>


		<!-- ===============================MENU PRINCIPAL================================-->
		<nav id="menu_principal" class="menu_principal">
			<ul class="bt_menu_top">
				<li id="bt_menu_top_departamento">Departamentos
					<ul class="submenu">
						<li></li>
					</ul>
				</li>

				<li id="bt_menu_top_atendimento">Atendimento
					<ul class="submenu">
						<li></li>
					</ul>
				</li>

				<li id="bt_menu_top_dados">Meus Dados
					<ul class="submenu">
						<li></li>
					</ul>
				</li>

			</ul><!-- Fim menu Principal-->

			<!-- Carrinho -->
			<div class="botao_carrinho">
				<a href="{$HOST}carrinho/" title="Ir para o Carrinho de Compras">Meu Carrinho</a>
					<div class="box_carrinho">
						<ol>
						{if $carrinho.itens|count > 0}
							{foreach $carrinho.itens as $item}
							<li>
								<a href="{$HOST}produto/{$item.produto.slug}/">
									<span class="foto"><img src="{$item.produto.foto.super_mini|default:($HOST|cat:$no_image)}" width="64" height="64"></span>
									<span class="title">{$item.produto.title}</span>
									<div class="somatoria">
										<p class="qtde">{$item.qtde}</p>
										<p> X </p>
										<p class="currency_format_real">R$ {$item.produto.preco|number_format:2:",":"."}</p>
									</div>
								</a>
								<div class="clear"></div>
							</li>
							{/foreach}
						{else}
							<li><span class="title">Carrinho Vazio</span></li>
						{/if}
						</ol>
						<a class="carrinho" href="{$HOST}carrinho/" title="Ir para o Carrinho de Compras">Ir para o Carrinho</a>
						<p class="amount">Sub-Total: <strong>R$ {$carrinho.subtotal|number_format:2:",":"."}</strong></p>
						<div class="close" title="Fechar">Fechar</div>
					</div>
			</div><!-- Fim Carrinho -->
		</nav><!-- Fim menu Principal-->

		<!-- Box SubMenus -->
		<div class="box_submenus">
			<!-- Departamentos-->
			<div class="box_menu" id="menu_top_departamento">
				{if $list_departamento|@sizeof > 0}
			    <ul>
					{foreach from=$list_departamento item=depitem}
					<li><a href="{$HOST}departamento/{$depitem.slug}/">{$depitem.title}</a></li>
					{/foreach}
			    </ul>
			    {/if}
				<div class="clear"></div>
			</div>

			<!-- Atendimento -->
			<div class="box_menu" id="menu_top_atendimento">
				<div class="box_atendimento">
					<h3><a href="{$HOST}site/contato/">Central de Atendimento</a></h3>
					<p>Acesse a nossa Central de Atendimento e entre em contato conosco em
					todos os canais de relacionamento.</p>
				</div>
				<div class="box_atendimento">
					<h3><a href="{$HOST}site/contato/">Telefone</a></h3>
					<p>55 5555-5555.<br>De segunda � sexta das 9:00 �s 18:00</p>
				</div>
				<div class="box_atendimento">
					<h3><a href="{$HOST}site/ajuda/">D�vidas (F.A.C)</a></h3>
					<p>Se voc� tem alguma d�vida entre em nosso F.A.Q. ou entre em contato para solucionar sua d�vida.</p>
				</div>

				<div class="clear"></div>
			</div>

			<!-- Meus Dados -->
			<div class="box_menu" id="menu_top_dados">
				<ul>
					<li><a href="{$HOST}usuario/perfil/" title="Entrar no meu Perfil">Meu Perfil</a></li>
					<li><a href="{$HOST}usuario/senha/" title="Mudar a senha de minha conta">Mudar Senha</a></li>
					<li><a href="{$HOST}usuario/enderecos/" title="Meus endere�os de Entrega">Meus Endere�os de Entrega</a></li>
				</ul>
				<ul>
					<li><a href="{$HOST}usuario/pedidos/" title="Pedidos em andamento">Meus Pedidos</a></li>
					<li><a href="{$HOST}usuario/compras/" title="Minhas compras finalizadas">Minhas Compras</a></li>
					<li><a href="{$HOST}usuario/carrinho/" title="Meu carrinho de Compras">Meu Carrinho</a></li>
				</ul>
				<ul>
					<li><a href="{$HOST}usuario/desejos/" title="Lista de Produtos Preferidos">Minha Lista de Desejos</a></li>
					<li><a href="{$HOST}usuario/notificacao/" title="Produtos que voc� marcou">Notifica��o de Produtos</a></li>
					<li><a href="{$HOST}usuario/config/" title="Configura��es da Conta">Configura��es</a></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
<!-- Conteudo do menu -->

{include file="modules/publico/view/sub_templates/searchBar.tpl"}
	</div><!-- Fim topo -->

		<!-- Banner Topo -->
		{if $banner_topo|default:'' != ''}
		<div class="banner_topo">
			{$banner_topo}
		</div>
		{/if}

	<!-- Corpo -->
	<div class="corpo">
