	<!--Menu ESQUERDO -->
	{include file="modules/publico/view/sub_templates/menu_categorias.tpl"}

	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		
		<!-- Banner -->
		<div class="banner_rotativo">
			{$banner_rotativo|default:''}
		</div>
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
		
			<!-- ===============================PRODUTOS=============================== -->
			<!-- VITRINE DE PRODUTOS -->
			<div class="janela" id="vitrine">
				{include file="modules/publico/view/sub_templates/produtoVitrineItem.tpl"}
			</div><!-- Fim Janela -->


		</div><!-- Fim Conteudo central -->

	</article><!-- Fim Conteudo -->
	
	<!-- MENU DIREITO -->
	{include file="modules/publico/view/sub_templates/coluna_secundaria.tpl"}
