<?php
//error_reporting(NULL);ini_set('display_errors',false);
//error_reporting(E_ALL);ini_set('display_errors',1);

/**
 * Instala o sistema
 * */
class Install extends K13InstallPage{
	
	public function __construct(){
		
		$classes = array(
			//Sistema
//			'K13SetorTipoENT'
//			,'K13SetorENT'
			'K13UserGroupENT'
			,'K13UserENT'
			,'K13UserGroupUserENT'
			,'K13ParamSystemENT'
			,'K13FolderENT'
			,'K13FileENT'
			,'K13ImagemENT'
			,'K13ModuloENT'
			,'K13LocalENT'
			,'K13PermissaoENT'
			,'K13MenuSistemaENT'
			
			//GeoLocation
			,'PaisENT'
			,'UfENT'
			,'CidadeENT'
			,'BairroENT'
			,'LogradouroENT'
			,'RuaENT'
			,'EnderecoENT'
			
			//Loja
			,'CategoriaLojaENT'
			,'CategoriaFilhaENT'
//			,'MenuItemENT'
			,'MarcaENT'
			,'StatusProdutoENT'
			,'ProdutoENT'
			,'ProdutoCategoriaENT'
			,'ProdutoVideoENT'
			,'ProdutoRelacionadoENT'
			,'ProdutoCompreJuntoENT'
			,'ComentarioENT'
			
			/* N�o vai existir mais
			 * ,'MenuSistemaPrincipalENT'
			//,'NoticiaENT'
			,'DepartamentoENT'
			,'VendaENT'
			,'ItemVendaENT'*/
		);
		if(DEBUG) var_dump($classes);
		
		//Instala
		$install = new K13Install($classes,true,DEBUG);
		$this->result = $install->get_result();
		$this->count_errors = $install->get_count_errors();
		$this->count_succes = $install->get_count_succes();
		
		//Gera a interface
		parent::__construct();
	}
	
}//Fim classe

new Install();
?>