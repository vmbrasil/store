<?php
/**
* Consulta Controller Padrao
* Para Adiministracao
*/
class ConsultaPadrao extends K13ControllerConsulta{

	public function __construct(&$DAOEntidade){//construtor
		parent::__construct($DAOEntidade);
	}

	/**
	 * Carrega os dados padroes
	**/
	public function carregar_dados_padrao(){
		parent::carregar_dados_padrao();

//		$this->set_template(K13Path::getDirFramework() .'view/K13ViewConsulta.tpl');
		$this->get_objSmarty()->compile_dir = K13Path::getDirFramework() .'view/templates_c';
	}

	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//$this->addCssFileToHead(HOST.'view/style/frameworkStyle.css');
	}

}//Fim class
?>