<?php
/**
* Pagina Controller Padrao
* Seta o template Padrao
* Carrega dados padrao
*/
class PagePadrao extends K13Page{

	public function __construct($params = null){//construtor
		parent::__construct();
		
		//Css Padrao
//		$this->addCssFileToHead('view/css/estilo.css');
		
		//Template Padrao
		$this->set_template(K13Path::getSubDir().'modules/system/view/index.tpl');

		//dados padrao
		$this->carregarDadosPadrao();

		$show = (is_array($params) && isset($params['show']) ? $params['show'] : true);
		$this->show($show);
	}
	
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		$this->objSmarty->assign('titulo', $this->get_propertie('titulo'));
		
		//Template Especifico
		$this->objSmarty->assign('page',$this->get_propertie('templatePage'));
	}
	
	/**
	 * Carrega os dados padroes da pagina(header, topo, etc)
	**/
	protected function carregarDadosPadrao(){
		//dados padrao
		$this->set_propertie('templatePage','modules/publico/view/PageInicial.tpl');
		
		$this->objSmarty->setTemplateDir(TEMPLATES);
		$this->objSmarty->compile_dir = 'view/templates_c/';
		$this->objSmarty->assign('HOST',HOST);
		$this->objSmarty->assign('FRAMEWORK',FRAMEWORK);
		
		//Titulo especifico da Pagina
		$config = Config::get_instance();//Manda as configurações para o Singleton responsavel do sistema
		$config->set_propertie('SiteNome','Meu Comercio 2.0.1');
		$config->set_propertie('logomarca',' - Betha test 1.0');
		$config->set_propertie('SiteDescricao','Descricao teste');
		$this->set_titulo('Meu Comercio 2.0.1');
		
		$this->objSmarty->assign('nomeSite', $config->get_propertie('SiteNome'));
		$this->objSmarty->assign('logomarca', $config->get_propertie('logomarca'));
		
		//Carrega o template configurado para o sistema
		$config->set_propertie('system_template',SYSTEM_TEMPLATE);
		$config->set_propertie('system_template_url','view/skins/'.SYSTEM_TEMPLATE.'/');//url do diretorio de templates
		$system_template = $config->get_propertie('system_template');
		
		//O nome do template usado pelo sistema. Pasta dos templates: view/templates
		$this->objSmarty->assign('template_slug', $system_template);
		
		//Produto sem imagem - futuramente sera dinamico escolhido nas configuracoes
		$this->objSmarty->assign('no_image', 'view/skins/'.$system_template.'/images/system/sem_foto.png');
		
	}

	/**
	 * Seta o subtitulo da pagina especifica
	 * */
	public function set_subtitulo($value){
		$this->set_propertie('subtitulo',$value);
	}
	/**
	 * Seta o subtitulo da pagina especifica
	 * */
	public function get_subtitulo(){
		return $this->get_propertie('subtitulo');
	}
	
}//Fim class
?>