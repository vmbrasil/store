<?php
/**
* Cadastro Controller Padrao
* Para Adiministracao
*/
class CadastroPadrao extends K13ControllerCadastro{

	public function __construct(&$DAOEntidade){//construtor
		K13Security::verify_full_security();
		
		parent::__construct($DAOEntidade);
	}

	/**
	 * Carrega os dados padroes
	**/
	public function carregar_dados_padrao(){
		parent::carregar_dados_padrao();
//		$this->set_template(K13Path::getDirFramework() .'view/K13ViewCadastro.tpl');
		//$this->get_objSmarty()->compile_dir = K13Path::getDirFramework() .'view/templates_c';
		$this->get_objSmarty()->compile_dir = 'view/templates_c';
	}
	
	/**
	 * Metodo abstrato para controle do usuario/programador interceptar os dados
	**/
	protected function doBeforeShow(){
		//$this->addCssFileToHead(HOST.'view/style/frameworkStyle.css');
	}
	
}//Fim class
?>