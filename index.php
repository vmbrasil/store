<?php
header('Content-Type: text/html; charset=utf-8');//charset=iso-8859-1

session_start();

//Configurações
require_once ('config.php');

//Auto load-----------------------------------------------------------------------
require_once (FRAMEWORK.'class/controller/K13FriendlyUrl.class.php');

/** Autoload
	* Carrega as classes automaticam
 * */
function __autoload($className){
	FrontControllerFactory::autoload($className);
}//Fim autoload



/**
 * Classe responsavel por controlar a ponta do sistema
 * Fabrica as classes controladoras necessarias
 * Age acordo com os parametros da pagina
 * */
class FrontControllerFactory{
	public static $param = array('','','');	//array de parametros da pagina
	public static $include_path = array();	//array dos caminhos para includes das classes autoload
	public static $secure = false;	//verifica a seguranca
	
	//System version
	public static $system_version = 0.1;	//versao do sistema
	
	
	/** construtor */
	public function __construct(){
		$this->init_autoload();
		$this->get_params();
		$this->configure();
		$this->secure_system();
		
		if(K13Security::get_system_secured()){
			$this->fabricar();
		}
	}
	
	/** 
	 * Carrega as configurações do sistema
	 **/
	protected function configure(){
		//Escolhe Mostrar Erros do Sistema
		if(DEBUG){
			error_reporting(E_ALL);ini_set('display_errors',true);
		}
		else{
			error_reporting(NULL);ini_set('display_errors',false);
		}
		
		//Carrega as configurações do sistema e conexao com o banco
		Config::get_instance()->configure();
//			K13Path::setSubDir('../');
	}
	
	/** 
	 * Carrega os parametros da url amigavel da pagina
	 **/
	protected function get_params(){
		/*
		$gets = explode("/", str_replace(strrchr($_SERVER["REQUEST_URI"], "?"), "", $_SERVER["REQUEST_URI"]));
		array_shift($gets);#:: Elimita o 1º tem da array que é sempre vazio
		self::$param = $gets;
		*/
		K13FriendlyUrl::process_Url(INITIAL_PARAM);//1-local,0-servidor
	}
	
	/**
	* Fabrica as instancias controladoras
	* Dá include nos arquivos.
	* 
	* Regra: 1-Pagina inicial: 'modules/system/controller/main.php'
	* 2-Pagina menu subpastas. Verifica se a pasta existe,exibe lista.
	* 3-Arquivo controlador. Verifica se o arquivo existe.
	**/
	protected function fabricar(){
		/* ESTRUTURA DOS PARAMETROS
		 * 0 - O modulo desejado
		 * 1 - A pagina do modulo
		 * 1.1 - Caso a pagina nao seja selecionada:
		 * 1.1.1 - se existir pagina padrao
		 * 1.1.2 - senao o menu padrao sera aberto
		 * */
		$module_controller = K13FriendlyUrl::get_url_param(0);
		
		/* Nao esta usando swith
		switch($module_controller){
//			case 'modulo_especifico':
//				new ModuloFactory();
//			break;
			//Não foi definido o modulo ou Chamou o inicial
			default:
			{
		*/
				//Pagina Inicial do Sistema------------
				if( empty($module_controller) || $module_controller == 'index.php'){
					include(INDEX_CONTROLLER);//'modules/system/controller/main.php';
				}
				//Pagina Especificada pela URL------------
				else {
					$url_controller = K13FriendlyUrl::get_url_param(1);
					//Menu Controlador - pagina não informada
					if( (empty($url_controller) && is_dir('modules/' .$module_controller) )
						|| ($url_controller == 'index.php' && is_dir('modules/' .$module_controller))
					){
						//Se existir o arquivo index no modulo entao executa
						if(file_exists('modules/' .$module_controller .'/index.php') ){
							include ('modules/' .$module_controller .'/index.php');
						}
						elseif(file_exists('modules/' .$module_controller .'/controller/index.php')){
							include ('modules/' .$module_controller .'/controller/index.php');
						}
						else{//Se nao existir o index tenta carregar o menu explorer
							header('location:' .K13Path::getHost().'system/menu_explorer/' .$module_controller .'/');
							//K13Funcs::force_redirect(K13Path::getHost().'system/menu_explorer/' .$module_controller .'/');
						}
					}
					//Pagina Informada
					else{
						if(file_exists('modules/' .$module_controller .'/controller/' .$url_controller .'.php')){
							include ('modules/' .$module_controller .'/controller/' .$url_controller .'.php');
						}
						elseif(file_exists('modules/' .$module_controller .'/' .$url_controller .'.php')){
							include ('modules/' .$module_controller .'/' .$url_controller .'.php');
						}
						//Submodulo
						elseif(file_exists('modules/' .$module_controller .'/' .$url_controller .'/controller/index.php')){
							include ('modules/' .$module_controller .'/' .$url_controller .'/controller/index.php');
						}
						//Aceita primeiro parametro como uma pagina de system
						elseif(file_exists('modules/system/controller/' .$module_controller .'.php')){
							include ('modules/system/controller/' .$module_controller .'.php');
						}
						//Aceita primeiro parametro como uma pagina de public
						elseif(file_exists('modules/publico/controller/' .$module_controller .'.php')){
							include ('modules/publico/controller/' .$module_controller .'.php');
						}
						//Aceita parametro como uma pagina do FW/modules
						elseif(file_exists(FRAMEWORK.'modules/' .$module_controller .'/controller/' .$url_controller .'.php')){
							include (FRAMEWORK.'modules/' .$module_controller .'/controller/' .$url_controller .'.php');
						}
						//Carrega qualquer arquivo
						elseif(file_exists($_SERVER['REQUEST_URI'])){
							include ($_SERVER['REQUEST_URI']);
						}
						//Pagina Não encontrada
						else {
							include ('modules/system/controller/404.php');
						}
					}
				}//fim pag especifica
		/* Nao esta usando swith
			}//fim default
		}//fim switch modules
		*/
	}//fim func fabricar
	
	
	/** 
	 * Inicializa o autoload
	**/
	protected function init_autoload(){
		//-------Carrega o include Patch---------
		self::$include_path = array();
		//2-Home. Depois as 3 pastas: controller,model,view
		self::$include_path[] = 'controller';
		self::$include_path[] = 'model';
		self::$include_path[] = 'class';
		self::$include_path[] = 'config';
		self::$include_path[] = 'view';
		
		//3-Modulo Atual. Depois as 3 pastas: controller,model,view
		/*
		$gets = explode("/", str_replace(strrchr($_SERVER["REQUEST_URI"], "?"), "", $_SERVER["REQUEST_URI"]));
		array_shift($gets);#:: Elimita o 1º tem da array que é sempre vazio
		self::$param = $gets;
		*/
		$page_param = K13FriendlyUrl::get_url_param(0);
		if(!empty($page_param)){
			self::$include_path[] = 'modules/'.$page_param.'/model';
			self::$include_path[] = 'modules/'.$page_param.'/controller';
			self::$include_path[] = 'modules/'.$page_param.'/view';
		}
		
		//4-Modulo System. Depois as 3 pastas: controller,model,view
		self::$include_path[] = 'modules/system/model';
		self::$include_path[] = 'modules/system/controller';
		self::$include_path[] = 'modules/system/view';
		self::$include_path[] = 'modules/system';
		
		//5-Modulo publico. Depois as 3 pastas: controller,model,view
		self::$include_path[] = 'modules/publico/model';
		self::$include_path[] = 'modules/publico/controller';
		self::$include_path[] = 'modules/publico/view';
		self::$include_path[] = 'modules/publico';
		
		//6-Todos modulos. Propria pasta. Depois as 3 pastas: controller,model,view
		foreach(new DirectoryIterator(ROOT.'/modules') as $file){
			$filename = $file->getFilename() ;
			if ($file->isDir() && $filename != "." && $filename != "..") {//retira-se os dir . e ..
				if(is_dir(ROOT.'/modules/'.$filename.'/model')) self::$include_path[] = 'modules/'.$filename.'/model';
				if(is_dir(ROOT.'/modules/'.$filename.'/controller')) self::$include_path[] = 'modules/'.$filename.'/controller';
				self::$include_path[] = 'modules/'.$filename;//Propria pasta
				if(is_dir(ROOT.'/modules/'.$filename.'/view')) self::$include_path[] = 'modules/'.$filename.'/view';
				
				//Submodulos
				foreach(new DirectoryIterator(ROOT.'/modules/'.$filename) as $submodulo){
					$subname = $submodulo->getFilename() ;
					if ($submodulo->isDir()
					&& $subname != "."
					&& $subname != ".."
					&& $subname != "controller"
					&& $subname != "model"
					&& $subname != "view"
					&& $subname != "dsv"
					) {//retira-se os dir . e ..
						if(is_dir(ROOT.'/modules/'.$filename.'/'.$subname.'/controller')) self::$include_path[] = 'modules/'.$filename.'/'.$subname.'/controller';
						self::$include_path[] = 'modules/'.$filename.'/'.$subname;//Propria pasta
					}
				}
				
			}
		}
		
		
		//7-Framework
		//class
		self::$include_path[] = FRAMEWORK.'class/controller';
		self::$include_path[] = FRAMEWORK.'class/model';
		self::$include_path[] = FRAMEWORK.'class/libs';
		self::$include_path[] = FRAMEWORK.'class/view';
		
		self::$include_path[] = FRAMEWORK.'class/view/componentes';
		
		self::$include_path[] = FRAMEWORK.'class';//Se tornou ultima prioridade
		
		
		//SubClasses
/*		foreach(new DirectoryIterator(FRAMEWORK.'/class') as $file){
			if ($file->isDir() && $file->getFilename() != "." && $file->getFilename() != "..") {//retira-se os dir . e ..
				self::$include_path[] = FRAMEWORK.'class/'.$file->getFilename().'/controller';
				self::$include_path[] = FRAMEWORK.'class/controller'.$file->getFilename().'/model';
				self::$include_path[] = FRAMEWORK.'class/model'.$file->getFilename();//Propria pasta
				self::$include_path[] = FRAMEWORK.'class/view'.$file->getFilename();
			}
		}
*/
		

		self::$include_path[] = FRAMEWORK.'configs';
		//self::$include_path[] = FRAMEWORK.'entidade';//deprecated
		
		self::$include_path[] = FRAMEWORK.'modules/controller';
		self::$include_path[] = FRAMEWORK.'modules/model';
		
		
		//8-Todos modulos do framework. Propria pasta. Depois as 3 pastas: controller,model,view
		foreach(new DirectoryIterator(FRAMEWORK.'/modules') as $file){
			$filename = $file->getFilename();
			if ($file->isDir() && $filename != "." && $filename != "..") {//retira-se os dir . e ..
				if(is_dir(FRAMEWORK.'modules/'.$filename.'/controller')) self::$include_path[] = FRAMEWORK.'modules/'.$filename.'/controller';
				if(is_dir(FRAMEWORK.'modules/'.$filename.'/model')) self::$include_path[] = FRAMEWORK.'modules/'.$filename.'/model';
				self::$include_path[] = FRAMEWORK.'modules/'.$filename;//Propria pasta
				if(is_dir(FRAMEWORK.'modules/'.$filename.'/view')) self::$include_path[] = FRAMEWORK.'modules/'.$filename.'/view';
			}
		}
		
		//9-Libs & Smarty
		self::$include_path[] = TERCEIROS;
		self::$include_path[] = FRAMEWORK_TERCEIROS;
		self::$include_path[] = SMARTY_DIR;
		//self::$include_path[] = FRAMEWORK_TERCEIROS .'QueryPath/';
		
//		self::$include_path[] = TERCEIROS .$className;//Terceiros
//		self::$include_path[] = SMARTY_DIR .$className;//Smarty
//		self::$include_path[] = TERCEIROS .'QueryPath/' .$className;//QueryPath
		
		
		if(DEBUG) {
			echo 'include_path::<pre>';
			print_r(self::$include_path);
			echo '</pre>';
		}
	}
	
	/** Autoload
		 *  Prioridade:
		 * 1-Mesma Pasta. Depois as 3 pastas: controller,model,view
		 * 2-Home. Depois as 3 pastas: controller,model,view
		 * 3-Modulo Atual. Depois as 3 pastas: controller,model,view
		 * 4-Modulo System. Depois as 3 pastas: controller,model,view
		 * 5-Modulo publico. Depois as 3 pastas: controller,model,view
		 * 6-Todos modulos. Depois as 3 pastas: controller,model,view
		 * 7-Framework
		 * 8-Libs & Smarty
	 **/
	public static function autoload($className){
		/* ALGORITMO:
		 * Busca o arquivo php na mesma pasta
		 * Busca o arquivo nas pastas configuradas(include paths)
		 * Busca 3 tipos de pastas mvc: controller,model,view
		 * Busca 3 tipos de extensoes: .class.php,.php,.cls.php; nessa ordem de prioridade
		 * Busca até 5 diretorios acima. '../'
		 * Por fim busca classes do Smarty se satisfazer as condiçoes
		 * */
		 
		 /*
		if($className !='K13Security' 
		&& $className !='K13Session' 
		&& $className !='K13Cript' 
		&& $className !='K13Path' 
		
		
		&& ! K13Security::verify_security() ){
			if(DEBUG){
				$msg = 'Erro! Não foi possivel carregar a classe do sistema! Contacte o administrador.';
				throw new Exception($msg);
				trigger_error($msg, E_USER_ERROR);
			}
			return false;
		}
		*/
		//1-Mesma Pasta. Depois as 3 pastas: controller,model,view
		if(file_exists($className .'.php')) include_once($className .'.php');
		
//		self::$include_path[] = TERCEIROS .$className;//Terceiros
//		self::$include_path[] = SMARTY_DIR .$className;//Smarty
//		self::$include_path[] = TERCEIROS .'QueryPath/' .$className;//QueryPath
		
		//Processa----------------------
		foreach(self::$include_path as $prefix){//include paths
			$path[0] = $prefix .'/' .$className .'.class.php';
			$path[1] = $prefix .'/' .$className .'.php';
			$path[2] = $prefix .'/' .$className .'.cls.php';
			$path[3] = $prefix .'/' .$className .'/' .$className .'.php';
	
			foreach($path as $thisPath){//cada sufix
				$subdirs = '';
	//			echo $thisPath .'<br>';//Debug
	
				$thisPath = str_replace('//', '/', $thisPath);
				if(file_exists($thisPath)){
					require_once($thisPath);
					if(DEBUG) var_dump($thisPath);
					return $thisPath;
				}
	
				/*
				for($i=0; $i<5; $i++){//5 subdirs
					$thisPath = $subdirs .$thisPath;
					$subdirs = '../';
					$thisPath = str_replace('//', '/', $thisPath);
					if(file_exists($thisPath)){
						require_once($thisPath);
						if(DEBUG) var_dump($thisPath);
						return $thisPath;
					}
				}//subdirs
				*/
			}//sufix
		}//paths
	
		//Smarty Template-------------------
		$_class = strtolower($className);
		if (substr($_class, 0, 16) === 'smarty_internal_' || $_class == 'smarty_security') {
			$thisPath = SMARTY_SYSPLUGINS_DIR .$_class .'.php';
			$thisPath = str_replace('\\', '/', $thisPath);
			$thisPath = str_replace('//', '/', $thisPath);
			if(file_exists($thisPath)){
				include ($thisPath);
				return $thisPath;
			}
		}
		
		//Não carregou
		echo '<div class="error">Erro! Não foi possivel carregar a classe ' .$className ."</div><br /><pre> \n";
		if(DEBUG) debug_print_backtrace();
		echo '</pre>';
		trigger_error('Erro! Não foi possivel carregar a classe ' .$className, E_USER_ERROR);
	}//Fim autoload
	
	protected function secure_system(){
		K13Security::system_security();
	}

}//fim class

new FrontControllerFactory();
?>