<?php
/**
* Pagina Para carregar configurações do Sistema
* Singleton pattern
*/
class Config{
	protected static $debug = false;//Se o debug do Sistema esta ativo
	public static $instance; //Instancia unica (Singleton)
	protected static $arr_properties = array();		//String array - Array de Propriedades do Objeto
	
	
	public function __construct(){//construtor
		//self::configure();
	}
	
	/**
	 * Configura o sistema
	 * */
	public static function configure(){//construtor
		
		//Seta o timezone
		date_default_timezone_set('Brazil/East');//Brazil/East
		
		//Seta os dados de conexao com o banco
		K13ConnectionConfig::set_data_connection(
		    array(
				'host' => DB_HOST
				,'database' => DB_DATABASE
		    	,'login' => DB_USER
				,'senha' => DB_PASSWORD
				,'type' => DB_TYPE
				,'port' => DB_PORT
			)
		);

		/*
		//Configura os caminhos do sistema
		K13Path::setHost(HOST);
		
		//Configura o squema padrao do banco
		K13SystemVar::set_default_squema('public');
		
		//Configura o tipo do banco
		K13DBHandler::set_db_type(DB_TYPE);
		
		//Configura o caminho do autocomplete
		K13Path::setUriAutocomplete(AUTOCOMPLETE);
		
		//Configura Diretorio DO FW
		K13Path::setDirFramework(FRAMEWORK);
		
		//Configura URL DO FW
		K13Path::setUriFramework(FRAMEWORK_URL);
		
		//Configura o parametro inicial da url amigavel
		K13FriendlyUrl::set_root_start(INITIAL_PARAM);
		
		//Configura o caminho do formulario de login
		K13Security::get_instance()->set_url_login_form(URL_LOGIN_FORM);

		//Configura o caminho de saida do sistema
		K13SystemVar::set_default_url_exit(K13Security::get_instance()->get_url_login_form());

		//Configura a pagina de entrado do sistema depois de logar
		K13SystemVar::set_default_url_entrance(URL_LOGIN_ENTRANCE);
		
		//Configura a url para selecionar qual grupo entrar depois de logar
		K13SystemVar::set_default_url_group_selection(URL_GROUP_SELECTION);
		
		//Seta o debug do sistema
		Config::set_debug(DEBUG);

		//Seta a url do jquery + atual do fw
		Config::set_propertie('k13fwJqueryUrl', JQUERY_URL);
		
		//Seta a url dE UPLOAD DE IMAGENS
		K13SystemVar::set_propertie('k13SysUploadImages', UPLOAD_IMAGE);
		
		//Seta a url dE UPLOAD DE ARQUIVOS
		K13SystemVar::set_propertie('k13SysUploadFiles', UPLOAD_FILES);
		
		//Seta a URL da pagina HOME
		Config::set_propertie('k13UrlHomePage', HOME);
		
		//Configura o nome/caminho do template usado
		K13SystemVar::setFWTemplate(FRAMEWORK_TEMPLATE);
		
		K13SystemVar::setSysTemplate(SYSTEM_TEMPLATE);
		*/
		
		//Configura os caminhos do sistema
		K13Path::setHost(HOST);
		
		//Configura o squema padrao do banco
		K13SystemVar::set_default_squema(DB_DATABASE);
		
		//Configura o tipo do banco
		K13DBHandler::set_db_type(DB_TYPE);
		
		//Configura o caminho do autocomplete
		K13Path::setUriAutocomplete('"'.HOST.'system/autocomplete/"');
//		K13Path::setUriAutocomplete(AUTOCOMPLETE);
		
		//Configura o nome/caminho do template usado
		K13SystemVar::setFWTemplate('template1');
		
//		K13Path::setUriFWTemplate(SYSTEM_TEMPLATE);
		
		//Configura Diretorio DO FW
		K13Path::setDirFramework(FRAMEWORK);
		
		//Configura URL DO FW
		K13Path::setUriFramework(FRAMEWORK_URL);
		
		//Configura o parametro inicial da url amigavel
		K13FriendlyUrl::set_root_start(INITIAL_PARAM);
		
		
		
		//Configura o caminho do formulario de login
		K13Security::get_instance()->set_url_login_form(URL_LOGIN_FORM);

		//Configura o caminho de saida do sistema
		K13SystemVar::set_default_url_exit(K13Security::get_instance()->get_url_login_form());

		//Configura a pagina de entrado do sistema depois de logar
		K13SystemVar::set_default_url_entrance(URL_LOGIN_ENTRANCE);
		
		//Configura a url para selecionar qual grupo entrar depois de logar
		K13SystemVar::set_default_url_group_selection(URL_GROUP_SELECTION);
		
		
		
		//Seta o debug do sistema
		Config::set_debug(DEBUG);
		
		//Seta a url do jquery + atual do fw
		Config::set_propertie('k13fwJqueryUrl', JQUERY_URL);
		
		//Seta a url dE UPLOAD DE IMAGENS
		K13SystemVar::set_propertie('k13SysUploadImages', UPLOAD_IMAGE);
		
		//Seta a url dE UPLOAD DE ARQUIVOS
		K13SystemVar::set_propertie('k13SysUploadFiles', UPLOAD_FILES);
		
	}
	
	/**
	* Geta o estado debug do Sistema
	* @return $debug : boolean - O estado debug
	**/
	public static function get_debug(){
		return self::$debug;
	}

	/**
	* Seta o estado debug do Sistema
	* @param $bool : boolean - O estado debug a ser setado
	**/
	public static function set_debug($bool){
		self::$debug = $bool;
	}
	
	/**
	* Pega a instancia unica desta classe
	* Singleton Pattern
	*/
	public static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new Config();
		}
		return self::$instance;
	}

	/**
	 * Seta uma propriedade
	 * @param $prop : string - a propriedade a ser setada
	 * @param $value : String - o valor
	**/
	public static function set_propertie($prop, $value){
		self::$arr_properties[$prop] = $value;
	}
	/**
	 * Geta uma propriedade
	 * @param $prop : string - a propriedade a ser setada
	 * @return String - a propriedade
	**/
	public static function get_propertie($prop){
		return self::$arr_properties[$prop];
	}
	
	

}//Fim Pagina
?>