$(function(){
	carregar_menu_categoria();
	
	carregar_produtos_top5({
		qtd: 5
		,page: 0
		,categoria: 0
		,ord_vendas: 'DESC'
		,ord_votos: 'DESC'
		,ord_visitas: 'DESC'
	});

});

/*Carrega o menu de categorias
 * -Passando o parametro null Retorna o padrao
 * */
function carregar_menu_categoria(){
	Pagina.montar_menu_categoria(null);
}

