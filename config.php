<?php
//BANCO DE DADOS
/*
 * Mysql DB
*/
define('DB_TYPE'		,'mysql');							//TIPO DO BANCO
define('DB_PORT'		,'3306');							//PORTA DO BANCO
define('DB_HOST'		,'127.0.0.1');						//HOST DO BANCO
define('DB_DATABASE'	,'gastos_pessoais');				//DATABASE DO BANCO
define('DB_USER'		,'root');							//USER DO BANCO
define('DB_PASSWORD'	,'1');								//PASS DO BANCO

/*
 * Mysql DB

define('DB_TYPE'		,'mysql');						//TIPO DO BANCO
define('DB_PORT'		,'3306');							//PORTA DO BANCO
define('DB_HOST'		,'localhost');					//HOST DO BANCO
define('DB_DATABASE'	,'meu_comercio');		//DATABASE DO BANCO
define('DB_USER'		,'root');							//USER DO BANCO
define('DB_PASSWORD'	,'1');							//PASS DO BANCO
*/
/*
 * PostGreSql DB
 */
/*
define('DB_TYPE'		,'pgsql');						//TIPO DO BANCO
define('DB_PORT'		,'5432');							//PORTA DO BANCO
define('DB_HOST'		,'localhost');					//HOST DO BANCO
define('DB_DATABASE'	,'k13system');			//DATABASE DO BANCO
define('DB_USER'		,'postgres');					//USER DO BANCO
define('DB_PASSWORD'	,'1');						//PASS DO BANCO
*/

//CAMINHOS
define('INITIAL_PARAM', 0);							//O Parametro inicial do qual comeca a pegar depois do HOST. EX: http://localhost/site_produtos/. HOST=http://localhost/. PARAM[0] = site_produtos/

define('HOST','http://localhost/store/');	//URL PRINCIPAL DO SISTEMA
define('HOME','');									//URL DA PAGINA INICIAL
define('INDEX_CONTROLLER','modules/publico/controller/index.php');	//Diretorio do controlador do index (Pagina inicial). Se o modulo for indicado vazio ou index.

define("FRAMEWORK", '../framework/');		//CAMINHO DO FRAMEWORK
define("FRAMEWORK_URL", 'http://localhost/framework/');//CAMINHO URL DO FRAMEWORK

define("FRAMEWORK_TERCEIROS", FRAMEWORK.'terceiros/');//CAMINHO DE BIBLIOTECAS DE TERCEIROS
define("TERCEIROS", 'library/');					//CAMINHO DE BIBLIOTECAS DE TERCEIROS

define("JQUERY_URL", FRAMEWORK_URL.'terceiros/jquery/js/jquery-1.6.2.min.js');//CAMINHO DO JQUERY + ATUAL NO FW

define("UPLOAD_IMAGE", 'upload/images/');//CAMINHO DO UPLOAD DE IMAGENS
define("UPLOAD_FILES", 'upload/files/');//CAMINHO DO UPLOAD DE ARQUIVOS

//AUTOCOMPLETE
define("AUTOCOMPLETE", '"'.HOST.'system/autocomplete/"');//CAMINHO DO AUTOCOMPLETE

//CAMINHOS DO LOGIN
define("URL_LOGIN_FORM", HOST.'system/login/');	//CAMINHO DO FORMULARIO DE LOGIN NO SISTEMA
define("URL_LOGIN_ENTRANCE", HOST);				//CAMINHO DA PAGINA QUANDO ENTRAR (LOGIN) NO SISTEMA
define("URL_LOGOUT_EXIT", HOST.'vitrine/');			//CAMINHO DA PAGINA QUANDO SAIR (LOGOUT) DO SISTEMA
define("URL_GROUP_SELECTION", HOST.'system/select_user_group/');	//CAMINHO DA PAGINA DE SELE�AO DO GRUPO DE USUARIO DEPOIS DE LOGAR

//CONFIGURA��O DO SMARTY
define("SMARTY_DIR", FRAMEWORK_TERCEIROS."smarty/");	//CAMINHO DO SMARTY - no futuro usar FRAMEWORK_TERCEIROS
define("TEMPLATES", "view/");						//CAMINHO DOS TEMPLATES DO SMARTY

//SKINS DO SISTEMA
define("FRAMEWORK_TEMPLATE",	"simples");				//NOME DO TEMPLATE VISUAL DO FRAMEWORK - ENCONTRADO NA PASTA FRAMEWORK/modules/view/skins
define("SYSTEM_TEMPLATE",		"template1");				//NOME DO TEMPLATE VISUAL DO SISTEMA - ENCONTRADO NA PASTA view/skins

define("TMP", TERCEIROS."tmp/");					//CAMINHO DOS TEMPLATES GERADOS
define("TMP_CACHE", TERCEIROS."cache/");			//CAMINHO DO CACHE DOS TEMPLATES GERADOS

//PASTA FISICA
$path = dirname(__FILE__);//realpath("");
//$path = str_replace( "\\", "/", $path);
define('ROOT',$path);								//CAMINHO DA PASTA FISICA INICIAL

//DEBUG do sistema
$sysDebug = (isset($_REQUEST['sysDebug'])?$_REQUEST['sysDebug']:0);
define('DEBUG', $sysDebug);							//DEBUG do sistema

