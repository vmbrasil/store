$(function () {

//	$('form.rating').rating();
	
	$('#prod_album_abas').tabs();

	$('#abas_detalhes').tabs();
	
	//Compre junto
	create_compre_junto();
	
	//Veja Mais
	create_veja_mais();
	
	//Fotos
	create_fotos();
	
	//Videos
	create_videos();
	
	//Social Buttons
	create_social_buttons();
	
	//Avaliacao
	create_rating();
});

/**
 * Cria os produtos Compre junto
 */
function create_compre_junto(){
	$('#box-junto .slider_mount').infinite_slider({speed:200,min_itens: 5});
}

/**
 * Cria os produtos Veja Mais
 */
function create_veja_mais(){
	$('#box-veja .slider_mount').infinite_slider({speed:100,frames:5,min_itens: 5});
}

/**
 * Cria o album de fotos
 */
function create_fotos(){
	if($('#tab-produto-album-fotos .slider_mount li').size() > 3){
		$('#tab-produto-album-fotos .slider_mount').infinite_slider({
			speed:200
			,frames:1
			//,auto_scroll: 6000
			,width:75
			,height:80
			,min_itens: 3
		});
	}
	else{
		//$('#tab-produto-album-fotos .slider_mount').
	}

	$('#tab-produto-album-fotos .prod_album .big_zoom').jqzoom({
		zoomType: 'standard'
		,lens:true
		,preloadImages: false
		,alwaysOn:false
		,zoomWidth: 580
		,zoomHeight: 580
		,xOffset:8
		,yOffset:-53
		,position:'right'
		,preloadText: 'Carregando Zoom'
	});
}

/**
 * Cria o album de videos
 */
function create_videos(){
	if($('#tab-produto-album-videos .slider_mount li').size() > 3){
		$('#tab-produto-album-videos .slider_mount').infinite_slider({
			speed:200
			,frames:1
			,width:75
			,height:80
			,min_itens: 3
		});
		
		//Seleciona o Segundo, ja que o primeiro foi colocado para tras para fazer animacao suave
		selecionar_video($('#tab-produto-album-videos .slider_mount li:nth-child(2) a'));
	}
	else {
		//Seleciona o Primeiro
		selecionar_video($('#tab-produto-album-videos .slider_mount li:first a'));
	}
	//Evento
	$('#tab-produto-album-videos .slider_mount li a').live('click',function(e){
		e.preventDefault();
		selecionar_video($(this));
	});

}

/**
 * Seleciona o video
 * @param $video : Jquery Object - o link com os dados do video
 */
function selecionar_video($video){
	var width = '360';
	var height = '360';
	$('#tab-produto-album-videos .prod_album').html('<div class="video"><object width="'+width+'" height="'+height+'"><param name="movie" value="'+$video.attr('href')+'"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="wmode" value="opaque"></param><embed src="'+$video.attr('href')+'" type="application/x-shockwave-flash" width="'+width+'" height="'+height+'" allowscriptaccess="always" allowfullscreen="true" wmode="opaque"></embed></object></div>');
}

/**
 * Cria os botoes das redes sociais de compartilhamento
 */
function create_social_buttons(){
	$('#share_tweetbutton').tweetButton({
		text: 'Vejam s� que barato esse produto'
		,via: 'nome_da_loja_no_twitter'
		,lang: 'pt'
		,count: 'horizontal'
		,url: location.href
	});
	
	$('#share_likebutton').likebutton({
		layout : 'button_count'
		,width : 100
		,height : 20
		,url: location.href
	});
	
//	// Load Tweet Button Script
//	var e = document.createElement('script');
//	e.type="text/javascript"; e.async = true;
//	e.src = 'http://platform.twitter.com/widgets.js';
//	document.getElementsByTagName('head')[0].appendChild(e);								
	// Load LinkedIn button
//	var e = document.createElement('script');
//	e.type="text/javascript"; e.async = true;
//	e.src = 'http://platform.linkedin.com/in.js';
//	document.getElementsByTagName('head')[0].appendChild(e);
	// Load Plus One Button
	var e = document.createElement('script');
	e.type="text/javascript"; e.async = true;
	e.src = 'https://apis.google.com/js/plusone.js';
	document.getElementsByTagName('head')[0].appendChild(e);
	// Load StumbleUpon button
//	var e = document.createElement('script');
//	e.type="text/javascript"; e.async = true;
//	e.src = 'http://www.stumbleupon.com/hostedbadge.php?s=1&a=1&d=stumbleupon-button';
//	document.getElementsByTagName('head')[0].appendChild(e);

}


/**
 * Cria os eventos e plugins da Avaliacao
 */
function create_rating(){
	
	$('#stars-rating .star').rating({
		focus: function(value, link){
			var tip = $('#rating-hover-label');
			tip[0].data = tip[0].data || tip.html();
			tip.html(link.title || 'value: '+value);
		}
		,blur: function(value, link){
			var tip = $('#rating-hover-label');
			tip.html('');
		}
		,callback: function(value, link){
			$('#rating-value').val(value);
			alert($('#rating-value').val());
			
			var tip = $('#rating-hover-label');
			tip[0].data = tip[0].data || tip.html();
			tip.html(link.title || 'value: '+value);
		}
	});
}


