//---------------------------------------------
/**!
 * jQuery infinite_slider 1.0 - 10/06/2012
 * @autor 2012 Alex Klen (www.alexklen.com.br)
 * 
 * 
 * HOW TO USE:
 * Get the id or class of the UL or OL element and it is done, the plugin do the work.
 * Example:
 * $('#ul_elem').infinite_slider();
 *
 *
 * SETINGS:
 *	speed : int				- Speed of animation scroll
 *	beforeCreate : function	- Function that execute before Create
 *	beforeSlide : function	- Function that execute before Slide
 *	afterSlide : function	- Function that execute after Slide
 *	create_arrows : bool	- Auto create the html arrows tags
 *	auto_create : bool		- Auto mount the html structure tags
 *	auto_scroll : int		- Time to auto scroll. 0 - stoped; +1 - Time to next scroll to next; -1 - Time to next scroll to previous
 *	width : int				- Width of the box. 'auto' - dont resize. Css style content
 *	height : int			- Height of the box. 'auto' - dont resize. Css style content
 *	frames : int			- Number of Frames to scroll
 *	pause_on_over : bool	- Pause autoscroll on mouse over
 *	min_itens :	int			- Minima quantidade de itens para o plugn ser ativado. Se a quantidade de itens for menor que o informado o plugn fica inativo. 0 - Sempre ativa, +1 - Quantidade minima
 *
 *
 * Based on infinite JQuery carousel tutorial of (Name of Author) Version: 1.0 (10-JUNE-2012) http://web.enavu.com/tutorials/making-an-infinite-jquery-carousel/
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Requires:
 * jQuery v1.3.2 or later
 * 
 */
(function($) {
	// Default configuration properties.
	var defaults = {
		speed :			500		//Speed of animation scroll
		,beforeCreate:		null	//Function that execute before Create
		,beforeSlide:		null	//Function that execute before Slide
		,afterSlide:		null	//Function that execute after Slide
		,create_arrows:		true	//Auto create the html arrows tags
		,auto_create :		true	//Auto mount the html structure tags
		,auto_scroll:		0		//Time to auto scroll. 0 - stoped; +1 - Time to next scroll to next; -1 - Time to next scroll to previous
		,width:				'auto'	//Width of the box. 'auto' - dont resize.
		,height:			'auto'	//Height of the box. 'auto' - dont resize.
		,frames:			1		//Number of Frames to scroll
		,pause_on_over:		true	//Pause autoscroll on mouse over
		,stop_on_scroll:	false	//Stop autoscroll on click scroll
		,min_itens:			0		//Minima quantidade de itens para o plugn ser ativado. Se a quantidade de itens for menor que o informado o plugn fica inativo. 0 - Sempre ativa, +1 - Quantidade minima
	};
	
	var InfiniteSliderHanddler = {
		/**
		* Cria o Plugn
		* @param settings - object data - os dados (option) preferidos pelo usuario
		* @param $elem - Jquery obj - O elemento UL que esta sendo pluginsado
		*/
		create: function(settings, $elem){
			//Verfica se existe a estrutura necessaria para o plugn
			if(! $elem.parent().is('.infslider-inner')){
				//Cria a estrutura
				$elem.wrap('<div class="infslider-container" />').wrap('<div class="infslider-inner" />');
			}
			
			if(! $elem.is('.infslider-list')) $elem.addClass('infslider-list');
			
			//Elementos
			var $inner = $elem.parent('.infslider-inner');
			var $container = $elem.parent('.infslider-inner').parent('.infslider-container');
			var $previous = $container.find('.infslider-scroll-previous');
			var $next = $container.find('.infslider-scroll-next');
			
			//Inicializa Dados
			var width = (settings.width != 'auto' && settings.width.length > 0 ? settings.width : $elem.find('li').outerWidth() );
			var height = (settings.height != 'auto' && settings.height.length > 0 ? settings.height : $elem.find('li').outerHeight() );
			//var left = ($elem.find('li').size() > 1 ? '-'+width+'px' : 0);
			
			$elem.css({
				'height':height
				,'width': '99999px'
				,'position': 'relative'
				//,'left':left //Inicializa com 1 elemento para tras (para que a animacao nao fique seca(sumir 1))
				,'left':'0'
			});
			$elem.find('li').css({
				'float':'left'
				,'height':height
			});
			$container.css('height',height);
			$inner.css('height',height);
			
			//Tamanho
			if(settings.width != 'auto' && settings.width.length > 0){
				$container.css('width',settings.width +'px !important');
				$inner.css('width',settings.width +'px !important');
			}
			
			//Cria os Scroll
			if(settings.create_arrows){
				$inner.before('<div class="infslider-scroll-previous" />').after('<div class="infslider-scroll-next" />');

				$previous = $container.find('.infslider-scroll-previous');
				$next = $container.find('.infslider-scroll-next');
				
				//Eventos nos scrolls
				//Previous
				$previous.click(function(e){
					e.preventDefault();
					
					if(! $elem.is(':animated') && $elem.find('li').size() > 1){
						InfiniteSliderHanddler.previous($elem, settings, settings.frames);
						//Cancel autoscroll if true
						if(settings.stop_on_scroll){
							InfiniteSliderHanddler.stop_autoscroll($elem, settings);
						}
					}

				});
				
				//Next
				$next.click(function(e){
					e.preventDefault();
					
					if(! $elem.is(':animated') && $elem.find('li').size() > 1){
						InfiniteSliderHanddler.next($elem, settings, settings.frames);
						//Cancel autoscroll if true
						if(settings.stop_on_scroll){
							InfiniteSliderHanddler.stop_autoscroll($elem, settings);
						}
					}
				});
				
				//Coloca aquele que ficou atras para frente para aparecer como primeiro
				//if($elem.find('li').size() > 1) $elem.find('li:first').before($elem.find('li:last'));
				
				//AutoScroll
				var interval = InfiniteSliderHanddler.start_autoscroll($elem, settings, settings.frames);
				
				if(settings.pause_on_over){
					$container.mouseenter(function(){
						//Stop AutoScroll
						InfiniteSliderHanddler.pause_autoscroll($elem);
					})
					.mouseleave(function(){
						interval = InfiniteSliderHanddler.start_autoscroll($elem, settings, settings.frames);
					});
				}
			}
			
			//Salva os dados no elemento
			var slider = new jQuery.infinite_slider($elem,settings);
			$elem.data('infinite_slider',slider);
		}
		
		/**
		* Goto previous slide
		* @param $elem - Jquery obj - O elemento UL que esta sendo pluginsado
		* @param settings - object data - os dados (option) preferidos pelo usuario
		* @param frames - frames to animate
		*/
		,previous : function($elem, settings, frames){
			//Valida
			if($elem.find('li').size() <= 2){
				return;
			}
			
			/*
			 * Algoritmo:
			 * Manda o ultimo para o primeiro
			 * Coloca a lista para tras - para esconder este q chegou
			 * Faz a animacao para frente
			 * Termina por zerar a localizacao da lista (ponto 0)
			 * */
			
			//get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
			var item_width = $elem.find('li').outerWidth() + 10;
			
			//calculae the new left indent of the unordered list
			var left_indent = parseInt($elem.css('left')) + item_width;
			
			//Manda o ultimo para o primeiro
			// when sliding to left we are moving the last item before the first list item
			$elem.find('li:first').before($elem.find('li:last'));

			//Coloca a lista para tras - para esconder este q chegou
			$elem.css({'left' : -left_indent});
			
			//Faz a animacao para frente
			//make the sliding effect using jquery's animate function '
			$elem.animate({'left' : 0},settings.speed,function(){    
				
				//and get the left indent to the default -210px
//				$elem.css({'left' : '-'+$elem.find('li').outerWidth()+'px'});
				//Termina por zerar a localizacao da lista (ponto 0)
				$elem.css({'left' : 0});
				
				if(--frames > 0) InfiniteSliderHanddler.previous($elem, settings, frames);
			}); 
		}
		
		/**
		* Goto next slide
		* @param $elem - Jquery obj - O elemento UL que esta sendo pluginsado
		* @param settings - object data - os dados (option) preferidos pelo usuario
		* @param frames - frames to animate
		*/
		,next : function($elem, settings, frames){
			//Valida
			if($elem.find('li').size() <= 2){
				return;
			}
			/*
			 * Algoritmo:
			 * Faz a animacao para tras
			 * Manda o primeiro para o ultimo
			 * Termina por zerar a localizacao da lista (ponto 0)
			 * */

			//get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
			var item_width = $elem.find('li').outerWidth() + 10;
			
			//calculae the new left indent of the unordered list
			var left_indent = parseInt($elem.css('left')) - item_width;
			
			//Faz a animacao para tras
			//make the sliding effect using jquery's anumate function '
			$elem.animate({'left' : left_indent},settings.speed,function(){
				//Manda o primeiro para o ultimo
				//get the first list item and put it after the last list item (that's how the infinite effects is made)
				$(this).find('li:last').after($(this).find('li:first'));
				
				//and get the left indent to the default -210px
//				$elem.css({'left' : '-'+$elem.find('li').outerWidth()+'px'});
				//Termina por zerar a localizacao da lista (ponto 0)
				$elem.css({'left' : 0});
				
				if(--frames > 0) InfiniteSliderHanddler.next($elem, settings, frames);
			});
		}

		/**
		* Start AutoScroll
		* @param $elem - Jquery obj - O elemento UL que esta sendo pluginsado
		* @param settings - object data - os dados (option) preferidos pelo usuario
		* @param frames - frames to animate
		*/
		,start_autoscroll : function($elem, settings, frames){
			var interval = null;
			if(settings.auto_scroll > 0){//Positive autoscroll
				interval = setInterval(function(){
					if(! $elem.is(':animated')){
						InfiniteSliderHanddler.next($elem, settings, settings.frames);
					}
				}
				,parseInt(settings.auto_scroll));
			}
			else if(settings.auto_scroll < 0){//Negative autoscroll
				interval = setInterval(function(){
					if(! $elem.is(':animated')){
						InfiniteSliderHanddler.previous($elem, settings, settings.frames);
					}
				}
				,Math.abs(parseInt(settings.auto_scroll)));
				
			}
			
			//Clear AutoScroll to not overload
			clearInterval(jQuery.data($elem,'autoscroll_interval'));

			//Armazena o iterval
			jQuery.data($elem,'autoscroll_interval',interval);
			
			//retorna o interval
			return interval;
		}
		
		/**
		* Pause AutoScroll
		* @param $elem - Jquery obj - O elemento UL que esta sendo pluginsado
		*/
		,pause_autoscroll : function($elem){
			clearInterval(jQuery.data($elem,'autoscroll_interval'));
		}
		
		
		/**
		* Stop AutoScroll
		* @param $elem - Jquery obj - O elemento UL que esta sendo pluginsado
		* @param settings - object data - os dados (option) preferidos pelo usuario
		*/
		,stop_autoscroll : function($elem, settings){
			settings.auto_scroll = 0;
			InfiniteSliderHanddler.stop_autoscroll($elem);
		}
		
	};//fim InfiniteSliderHanddler
		
	/**
	 * The infinite_slider object.
	 *
	 * @constructor
	 * @class infinite_slider
	 * @param e {HTMLElement} The element to create the carousel for.
	 * @param o {Object} A set of key/value pairs to set as configuration properties.
	 * @cat Plugins/jCarousel
	 */
	jQuery.infinite_slider = function(e, o) {
		this.elem = e;//Elemento jquery
		this.options	= $.extend({}, defaults, o || {});
		
		this.previous = function(frames){
			InfiniteSliderHanddler.previous(this.elem, this.options, frames);
		};
		this.next = function(frames){
			InfiniteSliderHanddler.next(this.elem, this.options, frames);
		};
	};
		
	jQuery.fn.infinite_slider = function(settings) {
		return this.each(function(){
			var meliante = $(this);			//O cara que esta sendo manipulado
			
			var instance = meliante.data('infinite_slider');
			if (instance) {
				if (settings) {//se tiver settings altera os dados e reseta o comportamento do componente
					jQuery.extend(instance.options, settings);
				}
				//instance.reload();
			}
			else {
				InfiniteSliderHanddler.create($.extend({}, defaults, settings || {}), meliante);//Cria
			}
		});	
	};

})(jQuery);