/*
**	@plugin-name : jQuery likeButton
**	@author : Alex A. Klen (alexklen.com.br)
**	@date : 07/20/2011
**	@copyright : Free to use
**
**	This plugin was tested in jQuery 1.5.2 version
**
**
**	Example:
**	Use the tag <DIV> with an ID to call the likeButton (Best way)
 * <div class="like_button"></div>
 * 
	$('.like_button').likebutton({
		layout : 'button_count'
		,width : 100
		,height : 20
		,url : 'http://www.site.com'
	});
**
**
**	See complete Twitter API Documentations: https://dev.twitter.com/docs/tweet-button
*/
(function($) {

	$.fn.likebutton = function(options) {
	
		options = $.extend({
			layout : 'default' //default,button-count, box-count
			,width : 250
			,height : 27
			,url : window.location.href
			,show_faces: 'false'
		}, options);
		
		return this.each(function() {
		
		 	var $this = $(this);
		 	
		 	$('<iframe src="http://www.facebook.com/plugins/like.php?layout='+options.layout+'&show_faces=false&width='+options.width+'&action=like&colorscheme=light&height='+options.height+'&locale=pt_BR&href='+options.url+'" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:'+options.width+'px; height:'+options.height+'px;" allowTransparency="true"></iframe>').appendTo(this);
		
		}); // end each
	
	};

})(jQuery);