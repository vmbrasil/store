/*
**	@plugin-name : jQuery tweetButton
**	@author : Rennan M. Rodrigues
**	@date : 07/20/2011
**	@copyright : Free to use // questions and/or suggestions send-me a mail: martini.rennan@gmail.com
**
**	This plugin was tested in jQuery 1.5.2 version
**
**
**	Example:
**	Use the tag <DIV> with an ID to call the tweetButton (Best way)
**	$('#tweetbutton').tweetButton({
**		text: Default Tweet text (max. 140 characters)
**		via: Screen name of the user to attribute the Tweet to
**		lang: The language for the Tweet Button (values: de=deutsch; co=corean; es=spañol; fr=french; en=english; it=italian; ja=japanese; pt=portuguese; ru=russian; tr=turk)
**		count: This is the type of data-count will or not appears in the tweet button (values: vertical; horizontal; none)
**		url: URL of the page to share
**	});
**
**
**	See complete Twitter API Documentations: https://dev.twitter.com/docs/tweet-button
*/

(function($) {
	$.fn.tweetButton = function(options) {
		options = $.extend({
			text: 'My first jQuery plugin, the jQuery.tweetButton();'
			, via: 'rennan_martini'
			, lang: 'pt'
			, count: 'vertical'
			, url: 'http://about.me/rennan'
		}, options);
		
		return this.each(function() {
			var $this = $(this);
			$this.empty();
			var text = options.text + ' ' + options.url;
			$(this).append('<a href="http://twitter.com/share" class="twitter-share-button" data-url="'+options.url+'" data-text="'+text+'" data-count="'+options.count+'" data-via="'+options.via+'" data-lang="'+options.lang+'"></a>');
			$.getScript('http://platform.twitter.com/widgets.js');
		});
	};
})(jQuery);