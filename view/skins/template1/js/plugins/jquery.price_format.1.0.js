/*
* Price Format jQuery Plugin
* By Eduardo Cuducos
* cuducos [at] gmail [dot] com
* Version: 1.0
* Release: 2009-01-21
*/

(function($) {

	$.fn.priceFormat = function(options) {  

		var defaults = {  
			prefix: 'US$ ',
			centsSeparator: '.',  
			thousandsSeparator: ','
		};  
		var options = $.extend(defaults, options);
		
		return this.each(function() {
			
			var obj = $(this);

			function price_format () {

				// format definitions
				var prefix = options.prefix;
				var centsSeparator = options.centsSeparator;
				var thousandsSeparator = options.thousandsSeparator;
				var formatted = '';
				var thousandsFormatted = '';
				var str = obj.text();
				var values = str.split('.');//Separa o integer dos cents
				var integerVal = (values.length >0 ? values[0]: '0');
				var centsVal = (values.length >1 ? values[1]: '00');
				
				// Verify Integers value
				// skip everything that isn't a number
				// and skip left 0
				var isNumber = /[0-9]/;
				formatted = '';
				for (var i=0;i<(integerVal.length);i++) {
					char = integerVal.substr(i,1);
					//if (formatted.length==0 && char==0) char = false;
					if (char && char.match(isNumber)) formatted = formatted+char;
				}
				integerVal = formatted;

				// Verify Cents value
				// skip everything that isn't a number
				// and skip left 0
				formatted = '';
				for (var i=0;i<(centsVal.length);i++) {
					char = centsVal.substr(i,1);
					if (char && char.match(isNumber)) formatted = formatted+char;
				}
				centsVal = formatted;
				if(centsVal.length > 2) centsVal = centsVal.substr(0,2);//limita numero de centavos
				
				
				// format to fill with zeros when < 100
				while (centsVal.length<2) centsVal = centsVal+'0';

/*
				centsVal = formatted.substr(formatted.length-2,2);
				integerVal = formatted.substr(0,formatted.length-2);
*/
				// apply cents pontuation
				formatted = integerVal+centsSeparator+centsVal;
//				console.log(str +': '+ integerVal + ',' +centsVal+'; ' +formatted);
//				console.log(values);

				// apply thousands pontuation
				if (thousandsSeparator) {
					var thousandsCount = 0;
					for (var j=integerVal.length;j>0;j--) {
						char = integerVal.substr(j-1,1);
						thousandsCount++;
						if (thousandsCount%3==0) char = thousandsSeparator+char;
						thousandsFormatted = char+thousandsFormatted;
					}
					if (thousandsFormatted.substr(0,1)==thousandsSeparator) thousandsFormatted = thousandsFormatted.substring(1,thousandsFormatted.length);
					formatted = thousandsFormatted+centsSeparator+centsVal;
				}
				
				// apply the prefix
				if (prefix) formatted = prefix+formatted;
				
				// replace the value
				obj.text(formatted);
			
			}

			$(this).bind('keyup',price_format);
			if ($(this).text().length>0) price_format();

		});

	}; 		
		
})(jQuery);