$(function(){

	//Submit
	$('#form_cadastro').submit(function(event){
		event.preventDefault();

		var $form_det = $(this);
		$('.message_box .msg_erro').slideUp();
		
		//Valida
		if(validar_form($form_det)){
			var $modal = create_modal_box('Aguarde...');
			$.ajax({
				url: $form_det.attr('action')//location.href
				,data: $form_det.serialize()+'&type=json'
				,dataType: 'json'
				,type:'post'
				,success: function(data){
					try{
						if(data.sucesso == true){
							/* Limpa o form
							 * redireciona
							 */
							//msgbox_show_sucess(['Bem Vindo!<br>Aguarde Redirecionando...'], $('#form_login') );
							alert('Cadastrado com sucesso!\n' +data.mensagem +'\nAguarde enquanto voc� � redirecionado.');
							//Redireciona
							if(!data.url) data.url = System.properties.get_host();
							setTimeout(function(){
								window.location.href = data.url;
							},2000);
							
							//Em caso de demora - Apresenta link de entrada
							setTimeout(function(){
								$('#form_login_section form').after('<p class="demora_pra_entrar">Caso esteja demorando muito para entrar clique em </p><a id="link_forcar_entrada" class="k13_bt_acrilico" href="'+data.url+'" title="Entrar no sistema">ENTRAR</a>')
							},5000);
						}
						else{
							/* Mostra a mensagem de erro */
							var msg = '';
							if(data.errors){
								for(var i in data.errors){
									msg += data.errors[i] +'\n';
								}
							}
							alert('Erro, verifique!\n' +msg);
						}
					}
					catch(e){
						msgbox_show_errors(['Ocorreu um erro ao recuperar a resposta!','Resposta Nula.'], $form_det);
					}
				}
				,error: function(){
					msgbox_show_errors(['Ocorreu um erro ao recuperar a resposta!','Resposta inv�lida do servidor.'], $form_det);
				}
				,complete: function(){
					destroy_modal_box($modal);
				}
			});
		}
	});

});

