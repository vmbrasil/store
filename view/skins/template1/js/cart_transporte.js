$(function(){
	// Abas
	$('#abas').tabs();
});

/*Carrega todos end do user*/
function carregar_enderecos(){
	//Manda por ajax
	$.ajax({
		type: 'POST',
		url: System.properties.get_host()+'ajax/get_user_address/',
		//data: {itens: JSON.stringify(dados), exec: 'upd'},
		//contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (response, status) {
			if(response.success){
				//Atualiza os valores
				set_valores_tela(response.data);
				Usuario.carregar_carrinho();
				currency_format_real('#cart .currency_format_real');
			}
			else{
				alert('Ocorreu um erro!\n'+response.message);
			}
		},
		error: function (xhr, msg, e) {
			//alert("Erro ao retornar resposta! " +msg);
			console.log("Erro ao retornar resposta! " +msg);
		}
	});

}