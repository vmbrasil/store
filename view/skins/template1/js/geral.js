$(function (){
	//alert('Continuar a fazer:\n\ncarregar o menu de categorias\nCarregar os produtos cadastrados no banco\nCarregar os banners cadastrados\ncarregar e terminar as outras coisas');

	ProdutoVitrine.load_template();
	Vitrine.load_template();
	
	//Pagina.carregar_departamentos();/*O proprio php carrega*/
	
	//Pagina.montar.pagina();
	
	Usuario.carregar_usuario();
	
	//imageLoader('img.img_loader');//sera chamado individualmente
});

/* ######################### PRODUTO ######################### */
//Cada Produto da Vitrine
var ProdutoVitrine = {
	template : ''//Tempplate html do produto
	//METODOS
	,load_template:
		/*Carrega o template html do produto*/
		function(){
			var html = '';
			html += '<div>';
			html += '<div class="box_produto hproduct">';
			html += '	<a href="#" title="" class="detalhes" rel="product">';
			html += '		<img src="#" class="foto product-thumb img_loader">';
			html += '		<div class="titulo product-title"></div>';
			html += '	</a>';
			html += '	<a href="#" class="categoria product-type">Categoria</a>';
			html += '	<div class="qualificacao"><span class="q_value">0</span><span class="q_count"></span></div>';
			html += '	<div class="preco_anterior">De: R$ <strong class="value amount currency money">0,00</strong></div>';
			html += '	<div class="preco value"><span>R$ </span><strong class="value amount currency money">0,00</strong></div>';
			html += '	<div class="parcela"><strong class="count">12x</strong> R$ <strong class="value amount currency money">0,00</strong></div>';
			html += '	<div class="juros"><strong class="value">sem</strong> juros</div>';
			html += '	<div class="frete">frete gratis</div>';
			html += '	<a href="#" class="detalhes bt_detalhes" rel="product" title="Mais Detalhes do Produto">Ver Detalhes</a>';
			html += '</div>';
			html += '</div>';
			
			this.template = html;
			return html;
		}
	,get_html_from_json:
		/*Monta o html dos itens de produtos da vitrine atraves do json
		 * @return : string - html */
		function(j_produto){
			var $produto = $(this.template);
			
			//Dados Padroes
			j_produto= $.extend({
				id: 0
				//,classe: ''
				,url: ''
				,title: 'Sem titulo'
				,qualificacao: {value:0,count:0}
				,precoa: 0
				,precov: 0
				,frete_gratis: 0
				,parcela: {min_count: 2,max_count: 12,value: 0,juros:0}
				,categoria: {titulo: '',url: ''}
			}, j_produto);
			
			$produto.attr('id','hproduct_'+j_produto.id);
			//if(j_produto.classe) $produto.addClass(j_produto.classe);
			$produto.find('.detalhes').attr('href',j_produto.url).attr('title',j_produto.title);
			$produto.find('.titulo').html(j_produto.title);
			//Qualificacao
			$produto.find('.qualificacao').find('.q_value').text(j_produto.qualificacao.value);
			$produto.find('.qualificacao').find('.q_count').text(j_produto.qualificacao.count);
			//Pre�o Anterior
			$produto.find('.preco_anterior').find('.value').text( j_produto.precoa.toFixed(2).replace('.',',') );
			if(parseFloat(j_produto.precoa) == 0.0) $produto.find('.preco_anterior').hide();
			//Pre�o
			$produto.find('.preco').find('.value').text( j_produto.preco.toFixed(2).replace('.',',') );
			
			//Parcela
			$produto.find('.parcela').find('.count').text(j_produto.parcela.max_count+'x');
			$produto.find('.parcela').find('.value').text( parseFloat(j_produto.parcela.value).toFixed(2).replace('.',',') );
			if(parseFloat(j_produto.parcela.value) == 0.0){
				$produto.find('.parcela').remove();
				$produto.find('.juros').text( '� vista' );
			}
			else{
				$produto.find('.juros').find('.value').text( (j_produto.juros == '0'? 'Sem': j_produto.juros) );
			}
			//Frete
			if(! j_produto.frete_gratis) $produto.find('.frete').remove();
			//Categoria
			$produto.find('.categoria').attr('href',j_produto.categoria.url).attr('title',j_produto.categoria.title).text(j_produto.categoria.title);
			
			return $produto.html();
		}
	,carregar_produtos :
	/**
	 * Carrega produtos por ajax
	 * @param callback:function(response, status) - funcao a ser executada depois de carregar - parametros mesmo de sucess do $.ajax
	 * 		response: resposta do ajax
	 * 		status: status do ajax
	 * @param params: (Object) - parametros para pedir os dados
		 * qtd(int): quantidade de registros (0 para default)
		 * page(int): pagina que esta (0 para default)
		 * categoria(int): filtragem dos produtos por categoria
		 * class(string): a classe css a ser aplicada
		 * marca(int): filtragem dos produtos por marca
		 * id(int): o id de 1 produto especifico
		 * destaque(bool): se vai filtrar apenas destaques
		 * oferta(bool): se vai filtrar apenas ofertas
		 * frete(bool): se vai ser frete gratis
		 * promocao(bool): se vai filtrar apenas promocoes
		 * Ordenacao
			,ord_id: (ASC,DESC)
			,ord_data: (ASC,DESC)
			,ord_preco: (ASC,DESC)
			,ord_nome: (ASC,DESC)
			,ord_vendas: (ASC,DESC)
			,ord_votos: (ASC,DESC)
			,ord_visitas: (ASC,DESC)
			,ord_sugestao: (ASC,DESC)
	 * Estrutura Json:
	 *	Produto:
	 *		id - Titulo da pagina
	 *		titulo - Titulo da pagina
	 *		precoa - numero da pagina
	 *		preco - Objeto bread crumb / Barra de Diretorio
	 *		url - Objeto de Banners
	 *		qualificacao = {value,count}
	 *		frete = {value,tipo,gratis,description}
	 *		categoria = {id,title,url}
	 *		parcela = {value,max_count,min_count}
	 * */
	function(callback,params){
		params = $.extend({
			qtd: 0
			,page: 0
			,id: 0
			,categoria: 0
			//,classe: ''
			,marca: 0
			,destaque: false
			,oferta: false
			,frete: false
			,promocao: false
			,ord_id: ''
			,ord_data: ''
			,ord_preco: ''
			,ord_nome: ''
			,ord_vendas: ''
			,ord_votos: ''
			,ord_visitas: ''
			,ord_sugestao: ''
		}, params);
		
		//params = {get_produto:params};
		//console.log(params);
		
		$.ajax({
			type: 'POST',
			url: System.properties.get_host()+'ajax/get_produto/',
			data: params,//JSON.stringify(params),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (response, status) {
				//alert(JSON.stringify(response.d));
				if(callback && typeof(callback) == 'function'){
					callback(response, status);
				}
			},
			error: function (xhr, msg, e) {
				console.log("Erro a retornar produto! " +e);
			}
		});
		
		if(System.properties.get_debug()) console.log('carregar_produto()');
	}
};

/* ######################### VITRINE ######################### */
//Cada Grupo de Vitrine
var Vitrine = {
	template : ''//Tempplate html da vitrine
	//METODOS
	,load_template:
		/*Carrega o template html da vitrine*/
		function(){
			var html = '';
			html += '<div>';//gamb
			html += '<div class="vitrine">';
			html += '	<h2>{$vitritem.descricao}</h2>';
//			html += '	<hr>';
			html += '	<ul></ul>';
			html += '</div>';
			html += '</div>';
			
			this.template = html;
			return html;
		}
	,get_html_from_json:
		/*Monta o html de cada vitrine atraves do json
		 * @return : string - html */
		function(j_vitri){
			//Dados Padroes
			j_vitri= $.extend({
				title: ''
				,page: '1'
				,rows: '0'
				,cols: '0'
				,produtos: null
			}, j_vitri);
			
			var $vitrine = $(this.template);
			var $vitrine_list = $vitrine.find('ul')

			
			//Titulo
			$vitrine.find('h2').text(j_vitri.title);
			if(j_vitri.title == '') $vitrine.find('h2').hide();//Apaga
			
			//Produtos
			var $li = null;
			for (var i in j_vitri.produtos){
/*				var produto = j_vitri.produtos[i];
				
				//Cria cada vitrine
				var html  = '';
				html += ProdutoVitrine.get_html_from_json(produto);
				
				$vitrine.find('ul').append('<li>'+html+'</li>');
				i++;
*/
////////////////////////////				
				var produto = j_vitri.produtos[i];
				//Cria a nova linha da Lista
				if($vitrine_list.find('li').size()<=0
				|| ($vitrine_list.find('li:last').children().size() >= j_vitri.cols
				&& j_vitri.cols != 0)){
					if($li != null)$vitrine_list.append($li);//Adiciona a cheia
					$li = $('<li></li>');//cria uma nova e adiciona para encher
					$vitrine_list.append($li);
				}
				
				//Cria cada vitrine
				var item  = '';
				item += ProdutoVitrine.get_html_from_json(produto);
				
				$li.append(item);//enche de produtos
				
				i++;
				
				////////////////////////////

			}
			//console.log($vitrine.html());
			return $vitrine.html();
		}
};//Fim Vitrine

//A Pagina Principal
var Pagina = {
	/*Monta a estrutura da interface*/
	montar: {
		/**
		 * Monta o menu de categorias/departamentos da pagina por parametro
		 * Coloca visual
		 * 100% completed
		 * @param j_menu : json - estrutura de dados do menu vinda do ajax
		 * */
		menu_categoria :
		function(j_menu){
			j_menu= $.extend({//dados padrao
				title: 'produtos'
				,categorias: null
			}, j_menu);

			var $menu = $('#menu_categorias');
			var $menu_lista_itens = $menu.find('ul.list');

			$menu.find('h3.menu_topo').text(j_menu.title);
			
			//Limpa
			//$menu_lista_itens.slideUp('fast',function(){$(this).empty()});
			$menu_lista_itens.empty();
			
			//Cada item
			for (var i in j_menu.categorias){//Categorias
				var categoria = j_menu.categorias[i];
				categoria= $.extend({//dados padrao
					title: ''
					,url:''
					,subcategorias: null
					,product_count: 0
				}, categoria);
				
				//Item do menu
				var $item  = '<li><a href="'+categoria.url+'">'+categoria.title+' ('+categoria.product_count+')</a></li>';
				$item = $($item);
				
				//SubCategorias
				if( categoria.subcategorias){
					$item.append('<ul></ul>');
					for (var y in categoria.subcategorias){
						var subcategoria = categoria.subcategorias[y];
						subcategoria= $.extend({//dados padrao
							title: ''
							,url:''
							,product_count: 0
						}, subcategoria);

						var subitem  = '<li><a href="'+subcategoria.url+'">'+subcategoria.title+' ('+subcategoria.product_count+')</a></li>';
						
						$item.find('ul').append(subitem);
						y++;
					}
				}//fim subcategorias
				
				$menu_lista_itens.append($item);
					
				i++;
			}//fim categorias
			
			//Mostra o menu
			$menu_lista_itens.slideDown('fast');

			if(System.properties.get_debug()) console.log('carregar_menu()');
		}
		
		,vitrine :
		/**
		 * Carrega a vitrine de produtos da pagina por ajax
		 * 100% completed
		 * @param j_vitri : json - estrutura da vitrine
		 * */
		function(j_vitri){
		
			$('#vitrine').empty();//Limpa todas vitrines
			
			var $li = null;
			for (var i in j_vitri){
				var vitrine = j_vitri[i];
				
				//Cria cada vitrine
				var item = Vitrine.get_html_from_json(vitrine);
				
				$('#vitrine').append(item);
				i++;
			}
			
			//Mostra
			currency_format_real();
			
			imageLoader('#vitrine img.img_loader');
			
			gerar_estrelas_qualify('#vitrine .qualificacao');
			
			$('#vitrine').fadeIn('fast');

		}

		/**
		 * Monta a barra de diretorio
		 * 100% completed
		 * @param j_dir : json - estrutura do menu de diretoerios 
		 * */
		,breadCrumb :
		function(j_dir){
		
			var $breadcrumb = $('.conteudo .diretorio_bar');
			var $breadcrumb_lista = $breadcrumb.find('ul');
	
			//Limpa
			//$menu_lista_itens.slideUp('fast',function(){$(this).empty()});
			$breadcrumb_lista.empty();
			
			//Cada item
			for (var i in j_dir){//Categorias
				var bread = j_dir[i];

				bread = $.extend({//dados padrao
					label: ''
					,url: ''
				}, bread);

				//Item do menu
				var item  = '<li><a href="'+bread.url+'">'+bread.label+'</a></li>';
				
				$breadcrumb_lista.append(item);
					
				i++;
			}//fim categorias
			
			//Mostra o menu
			$breadcrumb_lista.show('fast');

			if(System.properties.get_debug()) console.log('carregar_breadCrumb()');
		}
		
		/**
		 * Carrega os Banners
		 * 1% completed
		 * */
		,banners :
		function(){
			if(System.properties.get_debug()) console.log('carregar_banners()');
		}
		
		,produtos_top:
			function(){
	
				//$('#menu_top5 ul li .box_produto').addClass('top5');
				$('#menu_top5 ul li:first').addClass('first');
				$('#menu_top5 ul li').each(function(i,o){
					$(this).append('<div class="ordem">'+(i+1)+'</div>');
				});
				
				//Mostra
				currency_format_real();
				imageLoader('#menu_top5 img.img_loader');
				gerar_estrelas_qualify('#menu_top5 .qualificacao');
		}


	}
	
	/*Carrega os dados*/
	,carregar:{
		/**
		 * @deprecated
		 * 100% completed
		 * Carrega a PAGINA DA VITRINE inteira por ajax
		 * Envolve os dados do menu, dos banners, do breadcrumb e da vitrines de produtos
		 * ELEMENTOS DA PAGINA VITRINE:
			 * -Menu de Categorias
			 * -Menu de Marcas
			 * -Vitrines de Produtos
			 * -Banner Global (Topo total)
			 * -Banner Local (Topo conteudo)
			 * -Banner List(lista de mini-banners do menu)
			 * -Banner Content (mini-banner do conteudo intercalado com vitrines)
			 * -BreadCrumb (Diretorios)
			 * -Titulo
			 * -Departamento atual
			 * -Categoria atual
			 * -Tag Cloud
		 * Estrutura Json:
		 *	Pagina
		 *		titulo - Titulo da pagina
		 *		num - numero da pagina
		 *		BreadCrumb - Objeto bread crumb / Barra de Diretorio
		 *		Banners - Objeto de Banners
		 *		Vitrines
		 *		Menus
		 * */
		pagina:
		function(){
			var params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id do registro
			};

			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/get_pagina/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
					//alert(JSON.stringify(response.d));
				
					if (response == null) {
						if(System.properties.get_debug()) alert('N�o foi possivel carregar a p�gina!');
						return false;
					}
					
					//dados padrao
					response= $.extend({
						page: {title:'',number: ''}
						,menus: {menu_categoria:null}
						,breadcrumb: null
						,vitrines: null
						,banners: null
					}, response);

					
					Pagina.montar_menu_categoria(response.menus.menu_categoria);
					Pagina.carregar_breadCrumb(response.breadcrumb);
					Pagina.carregar_vitrine(response.vitrines);
					Pagina.carregar_banners(response.banners);
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			if(System.properties.get_debug()) console.log('carregar_pagina()');
		}
		
		/**
		 * 100% completed
		 * Carrega os departamentos da Loja
		 * Carrega a aba departamentos
		 * */
		,departamentos:
		function(){
			var params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id do registro
			};

			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/departamentos/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
					//alert(JSON.stringify(response.d));
					
					if (response == null) {
						if(System.properties.get_debug()) alert('Departamentos veio vazio!');
						return false;
					}
					
					$('#menu_top_departamento ul').empty();
					
					for (var i in response.regs){
						registro = response.regs[i];
						var id 	= registro.id;
						var nome = registro.title;
						
						var html  = '<a href="'+System.properties.get_host()+'departamento/'+id+'/'+nome+'/">'+nome+'</a>';
						
						$('#menu_top_departamento ul').append('<li>'+html+'</li>');
						i++;
					}
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			if(System.properties.get_debug()) console.log('carregar_departamentos()');
		}
		
		/**
		 * Carrega a lista de produtos top5
		 * 99% completed
		 * -Passando os parametros necessarios
		 * */
		,produtos_top:
		function (params){
			params = $.extend({
				qtd: 5
				,page: 0
				,categoria: 0
				,ord_vendas: 'DESC'
				,ord_votos: 'DESC'
				,ord_visitas: 'DESC'
			}, params);

			var success = function(result){
				
				$('#menu_top5 ul').empty();//Limpa todas vitrines
				
				for (var i in result.produtos){
					var vitrine = result.produtos[i];
					
					//Cria cada vitrine
					var item = ProdutoVitrine.get_html_from_json(vitrine);
					
					$('#menu_top5 ul').append('<li>'+item+'</li>');
					i++;
				}
				
				Pagina.montar.produtos_top();
			};
			
			ProdutoVitrine.carregar_produtos(success,params);
		}

		/**
		 * Carrega os banners Laterais
		 * 90% completed
		**/
		,banners_laterais:
		function(){
			var params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id do registro
			};

			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/get_banners_laterais/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
					//alert(JSON.stringify(response.d));
				
					if (response == null) {
						if(System.properties.get_debug()) alert('N�o foi possivel carregar a p�gina!');
						return false;
					}
					
					//dados padrao
					response = $.extend({
						page: {title:'',number: ''}
						,menus: {menu_categoria:null}
						,breadcrumb: null
						,vitrines: null
						,banners: null
					}, response);

					
					Pagina.montar_banners(response.banners);
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			if(System.properties.get_debug()) console.log('carregar_pagina()');
		}

		/**
		 * Carrega o menu principal de departamentos e categorias
		 * @param params : Object - {qtde:quantidade de registros,id: id da categoria atual, sub_qtde: quantidade de subcategorias}
		 * 1% completed
		*/
		,menu_categoria:
		function(params){
			params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id da categoria atual
			};

			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/get_banners_laterais/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
					//alert(JSON.stringify(response.d));
				
					if (response == null) {
						if(System.properties.get_debug()) alert('N�o foi possivel carregar a p�gina!');
						return false;
					}
					
					//dados padrao
					response = $.extend({
						page: {title:'',number: ''}
						,menus: {menu_categoria:null}
						,breadcrumb: null
						,vitrines: null
						,banners: null
					}, response);

					
					Pagina.montar_banners(response.banners);
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			if(System.properties.get_debug()) console.log('carregar_pagina()');
		}

		
		/**
		 * Carrega as marcas
		 * 1% completed
		 */
		,menu_marcas:
		function(params){
			params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id da categoria atual
			};

			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/get_banners_laterais/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
					//alert(JSON.stringify(response.d));
				
					if (response == null) {
						if(System.properties.get_debug()) alert('N�o foi possivel carregar a p�gina!');
						return false;
					}
					
					//dados padrao
					response = $.extend({
						page: {title:'',number: ''}
						,menus: {menu_categoria:null}
						,breadcrumb: null
						,vitrines: null
						,banners: null
					}, response);

					
					Pagina.montar_banners(response.banners);
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			if(System.properties.get_debug()) console.log('carregar_pagina()');
		}

		/**
		 * Carrega os produtos em destaque
		 * 1% completed
		*/
		,vitrine_destaques:
		function(params){
			params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id da categoria atual
			};

			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/get_banners_laterais/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
					//alert(JSON.stringify(response.d));
				
					if (response == null) {
						if(System.properties.get_debug()) alert('N�o foi possivel carregar a p�gina!');
						return false;
					}
					
					//dados padrao
					response = $.extend({
						page: {title:'',number: ''}
						,menus: {menu_categoria:null}
						,breadcrumb: null
						,vitrines: null
						,banners: null
					}, response);

					
					Pagina.montar_banners(response.banners);
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			if(System.properties.get_debug()) console.log('carregar_pagina()');
		}

		/**
		 * Carrega os produtos em oferta
		 * 1% completed
		*/
		,vitrine_ofertas:
		function(params){
			params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id da categoria atual
			};

			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/get_banners_laterais/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
					//alert(JSON.stringify(response.d));
				
					if (response == null) {
						if(System.properties.get_debug()) alert('N�o foi possivel carregar a p�gina!');
						return false;
					}
					
					//dados padrao
					response = $.extend({
						page: {title:'',number: ''}
						,menus: {menu_categoria:null}
						,breadcrumb: null
						,vitrines: null
						,banners: null
					}, response);

					
					Pagina.montar_banners(response.banners);
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			if(System.properties.get_debug()) console.log('carregar_pagina()');
		}

		/**
		 * Carrega os produtos lancamentos
		 * 1% completed
		*/
		,vitrine_novidades:
		function(params){
			params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id da categoria atual
			};

			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/get_banners_laterais/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
					//alert(JSON.stringify(response.d));
				
					if (response == null) {
						if(System.properties.get_debug()) alert('N�o foi possivel carregar a p�gina!');
						return false;
					}
					
					//dados padrao
					response = $.extend({
						page: {title:'',number: ''}
						,menus: {menu_categoria:null}
						,breadcrumb: null
						,vitrines: null
						,banners: null
					}, response);

					
					Pagina.montar_banners(response.banners);
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			if(System.properties.get_debug()) console.log('carregar_pagina()');
		}

	}//fim carregar

};//Fim Pagina


/* ######################### USUARIO ######################### */
var Usuario = {
	carregar_usuario :
		/**
		 * Carrega os dados do usuario
		 * */
		function(){
			//this.carregar_carrinho();//comentado pois esta sendo carregado pelo php
			this.carregar_historico();
			if(System.properties.get_debug()) console.log('carregar_usuario()');
		}

	,carregar_historico :
		/**
		 * Carrega o historico de navegacao do usuario que andou pela loja
		 * */
		function(){
			var params = {
				count : 0 //quantidade de registros a retornar
				,id : 0 //id do registro especifico
			};

			$.ajax({
				type: 'GET',
				url: System.properties.get_host()+'ajax/user_historico/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (j_historico, status) {
				//alert(JSON.stringify(response.d));
				
					if (j_historico == null || j_historico.produtos == null || j_historico.produtos.length <= 0) {
						if(System.properties.get_debug()) alert('Historico do Usuario veio vazio!');
						$('.barra_historico').fadeOut('fast');//esconde
						return false;
					}
					
					//Cria a barra se nao existir
					if( $('.barra_historico ul').size() <= 0 ) $('.barra_historico').append('<ul></ul>');
					
					$('.barra_historico ul').empty();
					
					for (var i in j_historico.produtos){
						var produto = j_historico.produtos[i];
						
						var html  = '';
						html += ProdutoVitrine.get_html_from_json(produto);
						
						$('.barra_historico ul').append('<li>'+html+'</li>');
						i++;
					}
					
					//Mostra
					currency_format_real();
					
					imageLoader('.barra_historico img.img_loader');
					
					$('.barra_historico').fadeIn('fast');
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
		});

		if(System.properties.get_debug()) console.log('carregar_carrinho()');
	}
	,carregar_carrinho :
		/**
		 * Carrega o carrinho de compras do usuario por ajax
		 * Coloca visual
		 * @param j_carrinho json do carrinho
		 * */
		function(j_carrinho){

			var params = {
				count : 0 //quantidade de registros a retornar
				,id : 0 //id do registro especifico
			};

			$.ajax({
				type: 'GET',
				url: System.properties.get_host()+'ajax/user_carrinho/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (j_carrinho, status) {
				//alert(JSON.stringify(response.d));
					
					$('#menu_principal .box_carrinho ol').empty();
					
					if (! j_carrinho || $(j_carrinho.itens).size() <= 0) {
						if(System.properties.get_debug()) alert('Carrinho do Usuario veio vazio!');
						$('#menu_principal .box_carrinho ol').append('<li><span class="title">Carrinho Vazio</span></li>');
						$('#menu_principal .box_carrinho .amount').html('Sub-Total: <strong>R$ 0,00</strong>');
						//currency_format_real('#menu_principal .box_carrinho .currency_format_real');
						return false;
					}
					
					for (var i in j_carrinho.itens){
						var produto = j_carrinho.itens[i].produto;
						var item = j_carrinho.itens[i];
						
						var html  = '';
						html += '<li>';
						html += '	<a href="'+System.properties.get_host()+'produto/'+produto.slug+'">';
						html += '		<span class="foto"><img src="'+produto.foto.super_mini+'" width="64" height="64"></span>';
						html += '		<span class="title">'+produto.title+'</span>';
						html += '		<div class="somatoria"><p class="qtde">'+item.qtde+'</p><p> X </p><p class="currency_format_real">'+produto.preco+'</p></div>';
						html += '	</a>';
						html += '</li>';
						
						$('#menu_principal .box_carrinho ol').append(html);
						i++;
					}
				$('#menu_principal .box_carrinho .amount').html('Sub-Total: <strong class="currency_format_real">'+j_carrinho.subtotal+'</strong>');
				
				currency_format_real('#menu_principal .box_carrinho .currency_format_real');
				
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});

			if(System.properties.get_debug()) console.log('carregar_carrinho()');
		}
	,add_to_cart :
		/**
		 * Adiciona um produto no Carrinho de compras por ajax
		 * Pergunta se Deseja ir para o carrinho
		 * Recarrega carrinho Topo
		 * 
		 * @param prod_id integer - id do produto a ser adicionado
		 * @return MessageBox - Se sucesso Pergunta, sen�o Mensagem se erro.
		 * */
		function(prod_id){
			var params = {
				qtde : 0 //quantidade de registros a retornar
				,id : 0 //id do registro
			};
		
			$.ajax({
				type: 'POST',
				url: System.properties.get_host()+'ajax/user_carrinho/add/',
				data: JSON.stringify(params),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (response, status) {
				//alert(JSON.stringify(response.d));
						
					if (response == null) {
						if(System.properties.get_debug()) alert('Carrinho do Usuario veio vazio!');
						return false;
					}
					
					for (var i in response.menu){
						atual = response.d[i];
						var id 	= atual.ID;
						var data = atual.Data;
						var dia = atual.Data.substr(0, 2); // pega os 2 primeiros caracteres
						var mes = atual.Data.substr(3,2);
						var ano = atual.Data.substr(8,2);
						var cidade = atual.Cidade;
						var estado = atual.UF;
						var local = atual.Local;
						var horario = atual.Horario;

						var evento  = '<span class="data">'+dia+'/'+mes+'/'+ano+'</span>';
							evento += '<div class="dados local"><strong>LOCAL:</strong> '+local+'</div>';
							evento += '<div class="dados cidade"><strong>CIDADE / ESTADO:</strong> '+cidade+' - '+estado+'</div>';
							evento += '<div class="dados horario"><strong>HOR�RIO:</strong> '+horario+'</div>';

						$('#jcarousel-agenda').append('<li>'+evento+'</li>');
						i++;
					}
				},
				error: function (xhr, msg, e) {
					if(System.properties.debug) alert("Erro! " +e);
					console.log("Erro! " +e);
				}
			});
			
			alert('scr geral.add_to_cart');
		}
};//Fim Usuario

/*Formata o valor monetario para real*/
function currency_format_real(selector){
	if(! selector) $selector = '.currency_format_real';
	$(selector).priceFormat({
		prefix: 'R$ ',
		centsSeparator: ',',
		thousandsSeparator: '.'
	});
}

/*Loader da foto do produto*/
function imageLoader(selector){
	if(! selector) selector = 'img.img_loader';//seletor padrao

	$(selector).each(function(){
		var self = $(this);
		var src = $(this).attr('src');
		var src_sem_foto = System.properties.get_host()+'images/produtos/sem_foto.png';
		var $loading = $('<div class="loading"></div>');
			$loading.width(self.width());
			$loading.height(self.height());
		
		$(this).after($loading);
		
		$(this)
		// once the image has loaded, execute this code
		.load(function () {
			// with the holding div #loader, apply:
			//$(this).unwrap();
			$loading.remove();
			
			// fade our image in to create a nice effect
			$(this).fadeIn().removeClass('img_loader');
		})
		// if there was an error loading the image, react accordingly
		.error(function () {
			// notify the user that the image could not be loaded
			self.attr('src', src_sem_foto);
			$loading.remove();
			$(this).fadeIn().removeClass('img_loader');
		})
		// *finally*, set the src attribute of the new image to our image
		.attr('src', src).hide();
		
	});
}

/*Gera as estrelas de qualificacao
 * @param selector : string - o seletor dos elementos que ser�o modificados
 * O seletor deve obrigatoriamente selecionar a estrutura:
	<div class="qualificacao"><span class="q_value">0</span><span class="q_count">0</span></div>
 * */
function gerar_estrelas_qualify(selector){
	if(! selector) return;
	
	$(selector).each(function(i,o){
		var $self = $(this);
		var valor = $(this).find('.q_value').text();
		var votos = $(this).find('.q_count').text();
		
		var $estrelas = $('<span class="star_rate"></span>');
		//Processa o valor
		var class_value = 'rate_0';
		switch(Math.floor(valor)){
			case 1:
				class_value = 'rate_1';
			break;
			case 2:
				class_value = 'rate_2';
			break;
			case 3:
				class_value = 'rate_3';
			break;
			case 4:
				class_value = 'rate_4';
			break;
			case 5:
				class_value = 'rate_5';
			break;
		}
		//alert(class_value);
		$estrelas.addClass(class_value).attr('title',valor + '/' +votos +' votos');
		if($self.find('.star_rate')) $self.find('.star_rate').remove();
		$self.append($estrelas);
	});
}

function serialize(obj)
{
	var returnVal;
	if(obj != undefined){
		switch(obj.constructor)
		{
		case Array:
			var vArr="[";
			for(var i=0;i<obj.length;i++)
			{
				if(i>0) vArr += ",";
				vArr += serialize(obj[i]);
			}
			vArr += "]"
				return vArr;
		case String:
			returnVal = escape("'" + obj + "'");
			return returnVal;
		case Number:
			returnVal = isFinite(obj) ? obj.toString() : null;
			return returnVal;    
		case Date:
			returnVal = "#" + obj + "#";
			return returnVal;  
		default:
			if(typeof obj == "object"){
				var vobj=[];
				for(attr in obj)
				{
					if(typeof obj[attr] != "function")
					{
						vobj.push('"' + attr + '":' + serialize(obj[attr]));
					}
				}
				if(vobj.length >0)
					return "{" + vobj.join(",") + "}";
				else
					return "{}";
			}  
			else
			{
				return obj.toString();
			}
		}
	}
	return null;
}

/* ========================================================*
 * 						BIBLIOTECAS
 * ========================================================*/

/*Adiciona metodo reset para os forms*/
jQuery.fn.reset = function () {
	$(this).each(function() { this.reset();$(this).val(''); });
}

/*Adiciona metodo checkValidity() para os forms*/
jQuery.fn.checkValidity = function () {
	var retorno = true;
	if($(this).is(':input')) return $(this).get(0).checkValidity()
	$(this).find(':input').each(function(i) {
		try{//Gambi para o IE
			var $ob = $(this);
			if(! $ob.get(0).checkValidity())
				retorno = false;
		}catch(e){}
	});
	return retorno;
}

/*Limita a quantidade dos textarea*/
function limit_textarea(){
	var textarea = $("textarea[maxlength]");
	var maxLength = textarea.attr("maxlength");
	var length = (textarea.size() > 0 ? textarea.get(0).value.length : 0);

	$("textarea[maxlength]").keyup(function(event){
	    var key = event.which;
		maxLength = textarea.attr("maxlength");
		length = textarea.get(0).value.length;

	
	    //todas as teclas incluindo enter
	    if(key >= 33 || key == 13) {
	        if(length >= maxLength) {
	            event.preventDefault();
	        }
	    }
	    
		var restantes = maxLength - length;
		$('.contagem_text[rel="'+$(this).attr('id')+'"]').text(restantes);
	})
	.keypress(function(event){//atualizacao
		maxLength = textarea.attr("maxlength");
		length = textarea.get(0).value.length;
		
		var restantes = maxLength - length;
		$('.contagem_text[rel="'+$(this).attr('id')+'"]').text(restantes);
	});


	var restantes = maxLength - length;
	$('.contagem_text[rel="'+textarea.attr('id')+'"]').text(restantes);
}

//vai pegar um parametro da query string q foi passado pelo GET
function getqs(name) {
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp(regexS);
	var results=regex.exec( window.location.href );
	if (results == null) {
		return "";
	} else {
		return decodeURIComponent(results[1].replace(/\+/g, " "));
	}
}
