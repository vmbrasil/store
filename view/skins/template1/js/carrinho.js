$(function(){
	/*Eventos*/
	
	//Atualizar o carrinho
	$('.car-bt-atualizar').click(atualizar_carrinho);
	$('.input_qtd').blur(function(){
		atualizar_item($(this));
	});
	
	//Remover Item
	$('.cart_remove_item').click(function(e){
		e.preventDefault();
		remover_item($(this));
	});
	
	//Limpar Carrinho
	$('.cart_remove_all').click(function(e){
		e.preventDefault();
		limpar_carrinho();
	});
	
});

/*Atualiza o carrinho*/
function atualizar_carrinho(){
	var dados = [];//Os itens do carrinho
	
	if($('.input_qtd').size()<=0){//Nao existe itens
		window.location = window.location;
		return;
	}
	
	$('.input_qtd').each(function(){
		dados[dados.length] = [$(this).attr('data-pid'),$(this).val()];
	});
	
	//alert('atualizar_carrinho(): '+ JSON.stringify(item));
	
	//Manda por ajax
	$.ajax({
		type: 'POST',
		url: System.properties.get_host()+'carrinho/ajax/update/',
		data: {itens: JSON.stringify(dados), exec: 'upd'},
		//contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (response, status) {
			if(response.success){
				//Atualiza os valores
				set_valores_tela(response.data);
				Usuario.carregar_carrinho();
				currency_format_real('#cart .currency_format_real');
			}
			else{
				alert('Ocorreu um erro!\n'+response.message);
			}
		},
		error: function (xhr, msg, e) {
			alert("Erro ao retornar resposta! " +msg);
		}
	});

}

/**
 * Atualiza um item especifico
 * @param $item : jquery - o input alterado
 * */
function atualizar_item($item){
	var $status_ico = $item.parent('div').find('.status');//o icone de status do item
	var dados = [];//Os itens do carrinho
	
	//Valida
	if($('.input_qtd').size()<=0){//Nao existe itens
		window.location = window.location;
		return;
	}
	
	//Valida Numero correto
	if($item.val() <= 0){//N�o executa pois os dados n�o foram alterados
		add_icon_status_qtd($item, 'error', 'Informe um numero intero maior que 0 (zero).\nO valor anterior foi retornado.');
		$item.val($item.attr('data-qtd'));
		return;
	}
	
	//Verifica se houve alteracao
	if($item.val() == $item.attr('data-qtd')){//N�o executa pois os dados n�o foram alterados
		return;
	}
	
	dados[dados.length] = [$item.attr('data-pid'),$item.val()];
	
	//alert('atualizar_carrinho(): '+ JSON.stringify(item));
	
	//Status Loading
	$status_ico = add_icon_status_qtd($item, 'loading');
//	if($item.parent('div').find('.status').size()<=0) $item.parent('div').append('<div class="status loading"/>');
//	var $status_ico = $item.parent('div').find('.status').html('');//.attr('class','status')
	
	//Manda por ajax
	$.ajax({
		type: 'POST',
		url: System.properties.get_host()+'carrinho/ajax/update/',
		data: {itens: JSON.stringify(dados), exec: 'upd'},
		//contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (response, status) {
			if(response.success){
				//Atualiza os valores
				set_valores_tela(response.data);
				Usuario.carregar_carrinho();
				currency_format_real('#cart .currency_format_real');
				//Altera icone
				add_icon_status_qtd($item, 'success');
				setTimeout(
					function(){
						if($status_ico.is('.success')){
							$status_ico.fadeOut(500,function(){
								if($status_ico.is('.success'))$status_ico.remove();
								else $status_ico.show();
							});
						}
					}
					,3000
				);
			}
			else{
				alert('Erro!\n'+response.message);
				add_icon_status_qtd($item, 'error', '<h5>Aten��o!</h5>\n' +response.message);
			}
		},
		error: function (xhr, msg, e) {
			alert("Erro ao retornar resposta! " +msg);
			add_icon_status_qtd($item, 'error', 'Erro ao retornar resposta! \n'+msg);
		}
	});
}

/**
 * Atualiza os valores da tela
 * @param data : Object - o objeto de dados
 * */
function set_valores_tela(data){
	$('#txt_frete').val(data.cep);
	$('.cart_cep_subtotal strong').text('R$ ' +data.valor_frete);
	$('#txt_desconto').val(data.cod_desconto);
	$('.cart_valedesconto_subtotal strong').text('R$ ' +data.valor_desconto);
	$('.cart_subtotal span strong').text('R$ ' +data.subtotal);
	$('.cart_total strong').text('R$ ' +data.total);
	$('.cart_parcelamento .parcelas').text(data.parcelas +'x');
	$('.cart_parcelamento .valor').text(data.parcelas_valor);
	$('.cart_parcelamento .juros').text(data.parcelas_juros);
	
	for(var i in data.itens){
		var sub_atual = $('.subtotal_unit[data-pid="'+data.itens[i].codigo+'"]').text();
		sub_atual = sub_atual.replace('R$ ','').replace('.','').replace(',','.');
		//if(sub_atual != data.itens[i].subtotal)
		$('.subtotal_unit[data-pid="'+data.itens[i].codigo+'"]').text('R$ ' +data.itens[i].subtotal);
		$('.input_qtd[data-pid="'+data.itens[i].codigo+'"]').val(data.itens[i].qtde).attr('data-qtd',data.itens[i].qtde);
	}
}

/**
 * Remove 1 item do carrinho
 * @param $item : jquery - o botao clicado
 * */
function remover_item($item){
	if(! confirm('Deseja realmente remover este item?')) return;
	
	var id = $item.attr('data-pid');
	
	$.ajax({
		type: 'POST',
		url: System.properties.get_host()+'carrinho/ajax/remove/',
		data: {pid: id, exec: 'rem'},
		//contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (response, status) {
			if(response.success){
				$('tr[data-pid="'+id+'"]').remove();
				atualizar_carrinho();
			}
			else{
				alert('Ocorreu um erro!\n'+response.message);
			}
		},
		error: function (xhr, msg, e) {
			alert("Erro ao retornar resposta! " +msg);
		}
	});

}

/**
 * Remove todos itens do carrinho
 * */
function limpar_carrinho(){
	if(! confirm('ATEN��O!\nVoc� est� prestes a remover todos os itens do carrinho.\nAo confirmar n�o ser� possivel desfazer a a��o.\n\nDeseja realmente remover todos produtos do carrinho?')) return;

	$.post(System.properties.get_host()+'carrinho/ajax/clear/',function(){
		window.location = window.location;
	});
}

/**
 * Adiciona um icone de status no item qtd
 * @param $item : Jquery o item que vai receber o icone
 * @param classe : string - a classe css do icone
 * @param msg : string  - a mensagem que aparecera 
 * */
function add_icon_status_qtd($item, classe, msg){
	//Seleciona o icone e limpa dados
	var $status_ico = $item.parent('div').find('.status').attr('class','status').html('');//limpa os dados
	//Se nao encontrar cria
	if($item.parent('div').find('.status').size() <= 0) $status_ico = $('<div class="status"/>').appendTo($item.parent('div'));
	
	//Adiciona a classe
	if(classe && classe.length > 0) $status_ico.addClass(classe);
	else $status_ico.addClass('loading');//Adiciona a classe loading default
	
//	alert($status_ico.size('class'));
	
	//Adiciona mensagem
	if(msg && msg.length > 0){
		$status_ico.html('<div class="msg">' +msg +'</div>');
	}
	
	return $status_ico;
}