function esconderQuadro(exceto, botao){
	//DeSeleciona os botoes
	$('.bt_menu_top li').addClass('').removeClass('bt_menu_top_selecionado');

	//Seleciona o Quadro
	$('.box_submenus .box_menu').each(function(index) {
		if($(this).attr('id') != ''){
			//$(this).css('display','block');
			if(exceto.attr('id') != $(this).attr('id') || (exceto.css('display') != 'none')) $(this).stop(true, true).slideUp();//.animate({"opacity": "0"}, "fast")
			else {//Mostra
				exceto.stop(true, true).slideDown();//.animate({"opacity": "1"}, "fast")
				botao.addClass('bt_menu_top_selecionado');
			}
		}
	});
}

$(function(){
	/*Padrao Menu PopUp*/
	$("ul.bt_menu_top li").mouseenter(function() {
		$(this).find("ul.submenu").stop(true, true).show('fast');
	}).mouseleave(function() {
		$(this).find("ul").hide('fast');
	});


	/*Departamentos Menu PopUp*/
	$("li#bt_menu_top_departamento").click(function() {
		esconderQuadro($('#menu_top_departamento'),$(this));
	});
	/*Atendimento Menu PopUp*/
	$("li#bt_menu_top_atendimento").click(function() {
		esconderQuadro($('#menu_top_atendimento'),$(this));
	});
	/*Dados Menu PopUp*/
	$("li#bt_menu_top_dados").click(function() {
		esconderQuadro($('#menu_top_dados'),$(this));
	});
	
	/*Carrinho*/
	$("div.botao_carrinho a").mouseenter(function() {
		$(".box_carrinho").stop(true, true).show('slow');
	});
	$(".botao_carrinho").mouseleave(function() {
		$(".box_carrinho").stop(false, true).hide('fast');
	});
	//Fechar
	$(".box_carrinho .close").click(function() {
		$(".box_carrinho").mouseleave();
	});
//	$(".botao_carrinho").mouseleave(function() {
//		$(".box_carrinho").mouseleave();
//	});

});
