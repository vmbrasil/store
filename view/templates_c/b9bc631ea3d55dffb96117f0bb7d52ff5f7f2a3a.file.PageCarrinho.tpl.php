<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 12:09:45
         compiled from "modules/publico/view/PageCarrinho.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3257355648cb9594762-62665141%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b9bc631ea3d55dffb96117f0bb7d52ff5f7f2a3a' => 
    array (
      0 => 'modules/publico/view/PageCarrinho.tpl',
      1 => 1343057812,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3257355648cb9594762-62665141',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- CONTEUDO PRINCIPAL -->
<article id="cart">
	<section class="janela">
		<h2 class="subtitulo">Meu Carrinho</h2>
	
		<!-- Etapas -->
		<div class="cart_etapa etapa-1">
			<ol>
				<li class="atual">Carrinho</li>
				<li>Identifica��o</li>
				<li>Transporte</li>
				<li>Pagamento</li>
				<li>Finaliza��o</li>
			</ol>
			<div style="clear: both"></div>
		</div>

	<?php if (count($_smarty_tpl->getVariable('carrinho')->value['itens'])>0){?>
	
		<!-- Botoes -->
		<?php ob_start(); ?>
		<div class="car_botoes">
			<a class="cart-bt-continuar-compra" title="Comprar mais produtos" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
vitrine/">Continuar comprando</a>
			<input type="button" title="Atualizar Modifica��es" class="car-bt-atualizar" value="Atualizar Carrinho">
			<a class="cart-bt-avancar" title="Pr�xima Etapa" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/transporte/">Avan�ar Compra</a>
		</div>
		<?php  Smarty::$_smarty_vars['capture']['car_botoes']=ob_get_clean();?>
		<?php echo Smarty::$_smarty_vars['capture']['car_botoes'];?>

		
		<!-- Produtos -->
		<table cellspacing="0" cellpadding="5" border="0" width="100%" class="cart_tab_produtos">
			<thead>
			<tr>
				<th width="50%">Produto</th>
				<th>Quantidade</th>
	
				<th>Valor Unit�rio</th>
				<th colspan="2">Valor Total</th>
			</tr>
			</thead>
			        
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('carrinho')->value['itens']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
			<tr class="item" data-pid="<?php echo $_smarty_tpl->tpl_vars['item']->value['codigo'];?>
">
				<td height="80">
					<img class="foto" height="64" width="64" src="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['produto']['foto']['super_mini'])===null||$tmp==='' ? (($_smarty_tpl->getVariable('HOST')->value).($_smarty_tpl->getVariable('no_image')->value)) : $tmp);?>
">
					<a class="title" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produto/<?php echo $_smarty_tpl->tpl_vars['item']->value['produto']['slug'];?>
/"><?php echo $_smarty_tpl->tpl_vars['item']->value['produto']['title'];?>
</a>
					<?php if ($_smarty_tpl->tpl_vars['item']->value['produto']['categoria']['title']!=''){?><p class="categoria">Categoria: <?php echo $_smarty_tpl->tpl_vars['item']->value['produto']['categoria']['title'];?>
</p><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['item']->value['produto']['marca']['title']!=''){?><p class="marca">Marca: <?php echo $_smarty_tpl->tpl_vars['item']->value['produto']['marca']['title'];?>
</p><?php }?>
				</td>
				<td>
					<div class="quantidade">
						<label for="item_qtd<?php echo $_smarty_tpl->tpl_vars['item']->value['codigo'];?>
">Quantidade do Produto</label><input type="text" class="input input_qtd" data-pid="<?php echo $_smarty_tpl->tpl_vars['item']->value['codigo'];?>
" id="item_qtd<?php echo $_smarty_tpl->tpl_vars['item']->value['codigo'];?>
" data-qtd="<?php echo $_smarty_tpl->tpl_vars['item']->value['qtde'];?>
" maxlength="6" size="8" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['qtde'];?>
" name="txtqtd">
					</div>
				</td>
				<td>
					<div class="value">R$ <?php echo number_format($_smarty_tpl->tpl_vars['item']->value['produto']['preco'],2,",",".");?>
</div>
				</td>
				<td>
					<div class="subtotal_unit currency_format_real" data-pid="<?php echo $_smarty_tpl->tpl_vars['item']->value['codigo'];?>
">R$ <?php echo number_format($_smarty_tpl->tpl_vars['item']->value['subtotal'],2,",",".");?>
</div>
				</td>
				<td>
					<form method="post" action="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/">
						<input type="hidden" value="rem" name="exec">
						<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['codigo'];?>
" name="pid">
						<button type="submit" title="Retirar Item do carrinho" class="cart_remove_item" value="Retirar" data-pid="<?php echo $_smarty_tpl->tpl_vars['item']->value['codigo'];?>
">Retirar</button>
					</form>
				</td>
			</tr>
			<?php }} ?>
		</tbody></table>
		
		<!-- Subtotal -->
		<div class="cart_subtotal">
			<form method="post" action="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/">
				<input type="hidden" value="clear" name="exec">
				<button type="submit" class="cart_remove_all" title="Limpar todos itens do carrinho" value="Limpar" name="btLimpar">Limpar</button>
			</form>
			<span>(Subtotal) <strong class="currency_format_real">R$ <?php echo number_format($_smarty_tpl->getVariable('carrinho')->value['subtotal'],2,",",".");?>
</strong></span>
			<div class="clear"></div>
		</div>
		
		<!-- Descontos -->
		<fieldset class="cart_valedesconto">
			<legend>Desconto</legend>
	
			<form method="post" action="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/vale-desconto/">
				<label for="txt_desconto">
					Digite o n�mero do seu Vale-Desconto, Vale-Compra<br>
					ou Cupom e clique no bot�o OK<br>
				</label>

				<input type="text" class="input" id="txt_desconto" name="desconto" maxlength="20" value="<?php echo $_smarty_tpl->getVariable('carrinho')->value['cod_desconto'];?>
">
				<button type="button" name="">OK</button>
			</form>
	
			<div class="cart_valedesconto_subtotal">
				<p>Vale-Presente, Vale-Compra ou Cupom</p>
				<strong class="currency_format_real">(R$ <?php echo number_format($_smarty_tpl->getVariable('carrinho')->value['valor_desconto'],2,",",".");?>
)</strong>
			</div>
		</fieldset>
		
		
		<!-- Transporte -->
		<fieldset class="cart_cep">
			<legend>Frete</legend>
			
			<form method="post" action="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/cep/">
				<label for="txt_frete">Informe o seu CEP para calcular o frete<br>
					e clique no bot�o OK<br>
					<a href="#">N�o sei meu CEP</a>
				</label>
				
				<input type="text" class="input" id="txt_frete" name="frete" maxlength="8" value="<?php echo $_smarty_tpl->getVariable('carrinho')->value['cep'];?>
">
				<button type="button" name="">OK</button>
			</form>
			
			<div class="cart_cep_subtotal">
				<p>Frete</p>
				<strong class="currency_format_real">(R$ <?php echo number_format($_smarty_tpl->getVariable('carrinho')->value['valor_frete'],2,",",".");?>
) <br>� Calcular</strong>
			</div>
		</fieldset>
	
	
		<!-- Total -->
		<div class="cart_total">Valor Total	<strong class="currency_format_real">R$ <?php echo number_format($_smarty_tpl->getVariable('carrinho')->value['total'],2,",",".");?>
</strong> </div>
		
		<!-- Possibilidade Parcelamento -->
		<div class="cart_parcelamento">ou em at� <strong class="parcelas"><?php echo $_smarty_tpl->getVariable('carrinho')->value['parcelas'];?>
x</strong> de <strong class="valor currency_format_real">R$ <?php echo number_format($_smarty_tpl->getVariable('carrinho')->value['parcelas_valor'],2,",",".");?>
</strong> <span class="juros"><?php echo $_smarty_tpl->getVariable('carrinho')->value['parcelas_juros'];?>
</span></div>
		
		<!-- Hidden Atualizar Cart -->
		<form action="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/" method="post" id="formEdit" name="formEdit">
			<input type="hidden" value="upd" name="exec">
			<input type="hidden" value="" id="qtd" name="qtd">
			<input type="hidden" value="" id="pid" name="pid">
		</form>


		<!-- Botoes -->
		<?php echo Smarty::$_smarty_vars['capture']['car_botoes'];?>


		<!-- Dica -->
		<p class="msg_mensagem">
			Para finalizar seu pedido, confira a lista acima o(s) produto(s) escolhido(s) por voc�, ent�o clique no bot�o "Finalizar Compra", voc� ser� encaminhado para as etapas de conclus�o do pedido.
			Se desejar alterar a quantidade de algum produto, basta digitar o novo valor no campo "Quantidade".
			Para remover/excluir algum produto do pedido clique no bot�o "Excluir" � direita do produto correspondente.
		</p>
	


	<?php }else{ ?>
		<!-- Carrinho Vazio -->
		<div class="carrinho_vazio">
			<h3>Seu carrinho de compras est� vazio!</h3>
			<p>Para inserir produtos no seu carrinho navegue pelos departamentos ou utilize a busca do site. Ao encontrar os produtos desejados, clique no bot�o "Comprar".</p>
			<div class="car_botoes">
				<a class="cart-bt-continuar-compra" title="Comprar mais produtos" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
vitrine/">Continuar comprando</a>
			</div>
		</div>
	<?php }?>
	</section><!-- Fim janela -->
</article>