<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 14:14:13
         compiled from "modules/publico/view/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3208355689e653cfcd7-77410104%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '995ad3851cc65500e8810b0c6d53987262b87168' => 
    array (
      0 => 'modules/publico/view/header.tpl',
      1 => 1432919649,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3208355689e653cfcd7-77410104',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<body>
<!-- Toda Pagina -->
<div class="tudo">
	<!-- Topo -->
	<div class="topo">
		<h1 class="logo"><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
"><img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/template1/images/logo.png" alt="<?php echo (($tmp = @$_smarty_tpl->getVariable('titulo')->value)===null||$tmp==='' ? '' : $tmp);?>
" title="<?php echo (($tmp = @$_smarty_tpl->getVariable('titulo')->value)===null||$tmp==='' ? '' : $tmp);?>
" width="266" height="91" /></a></h1>
		<div class="topo_identificacao"><?php echo (((((($tmp = @$_smarty_tpl->getVariable('identificacao')->value)===null||$tmp==='' ? 'Bem Vindo Visitante. <a href="' : $tmp)).($_smarty_tpl->getVariable('HOST')->value)).('usuario/login/">Entrar</a> | <a href="')).($_smarty_tpl->getVariable('HOST')->value)).('usuario/cadastrar/">Cadastrar</a>');?>
</div>

		<div class="menu_institucional">
			<ul>
				<li id="home"><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
">Inicial</a></li>
				<li id="lojas"><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/perfil/">Minha Conta</a></li>
				<li id="quemsomos"><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
site/sobre/">Quem Somos</a></li>
				<li id="atendimento"><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
site/contato/">Central de Atendimento</a></li>
				<li id="duvidas"><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
site/ajuda/">D�vidas</a></li>
			</ul>
		</div>


		<!-- ===============================MENU PRINCIPAL================================-->
		<nav id="menu_principal" class="menu_principal">
			<ul class="bt_menu_top">
				<li id="bt_menu_top_departamento">Departamentos
					<ul class="submenu">
						<li></li>
					</ul>
				</li>

				<li id="bt_menu_top_atendimento">Atendimento
					<ul class="submenu">
						<li></li>
					</ul>
				</li>

				<li id="bt_menu_top_dados">Meus Dados
					<ul class="submenu">
						<li></li>
					</ul>
				</li>

			</ul><!-- Fim menu Principal-->

			<!-- Carrinho -->
			<div class="botao_carrinho">
				<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/" title="Ir para o Carrinho de Compras">Meu Carrinho</a>
					<div class="box_carrinho">
						<ol>
						<?php if (count($_smarty_tpl->getVariable('carrinho')->value['itens'])>0&&$_smarty_tpl->getVariable('carrinho')->value['itens']!=false){?>
							<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('carrinho')->value['itens']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
							<li>
								<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produto/<?php echo $_smarty_tpl->tpl_vars['item']->value['produto']['slug'];?>
/">
									<span class="foto"><img src="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['produto']['foto']['super_mini'])===null||$tmp==='' ? (($_smarty_tpl->getVariable('HOST')->value).($_smarty_tpl->getVariable('no_image')->value)) : $tmp);?>
" width="64" height="64"></span>
									<span class="title"><?php echo $_smarty_tpl->tpl_vars['item']->value['produto']['title'];?>
</span>
									<div class="somatoria">
										<p class="qtde"><?php echo $_smarty_tpl->tpl_vars['item']->value['qtde'];?>
</p>
										<p> X </p>
										<p class="currency_format_real">R$ <?php echo number_format($_smarty_tpl->tpl_vars['item']->value['produto']['preco'],2,",",".");?>
</p>
									</div>
								</a>
								<div class="clear"></div>
							</li>
							<?php }} ?>
						<?php }else{ ?>
							<li><span class="title">Carrinho Vazio</span></li>
						<?php }?>
						</ol>
						<a class="carrinho" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/" title="Ir para o Carrinho de Compras">Ir para o Carrinho</a>
						<p class="amount">Sub-Total: <strong>R$ <?php echo number_format($_smarty_tpl->getVariable('carrinho')->value['subtotal'],2,",",".");?>
</strong></p>
						<div class="close" title="Fechar">Fechar</div>
					</div>
			</div><!-- Fim Carrinho -->
		</nav><!-- Fim menu Principal-->

		<!-- Box SubMenus -->
		<div class="box_submenus">
			<!-- Departamentos-->
			<div class="box_menu" id="menu_top_departamento">
				<?php if (sizeof($_smarty_tpl->getVariable('list_departamento')->value)>0&&$_smarty_tpl->getVariable('list_departamento')->value!=false){?><?php }?>
			    <ul>
					<?php  $_smarty_tpl->tpl_vars['depitem'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('list_departamento')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['depitem']->key => $_smarty_tpl->tpl_vars['depitem']->value){
?><?php if ($_smarty_tpl->tpl_vars['depitem']->value!=false){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
departamento/<?php echo $_smarty_tpl->tpl_vars['depitem']->value['slug'];?>
/"><?php echo $_smarty_tpl->tpl_vars['depitem']->value['title'];?>
</a></li>
					<?php }?><?php }} ?>
					<?php if (sizeof($_smarty_tpl->getVariable('list_departamento')->value)<=0||$_smarty_tpl->getVariable('list_departamento')->value==false){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
vitrine/">Loja</a></li>
					<?php }?>
			    </ul>
			    
				<div class="clear"></div>
			</div>

			<!-- Atendimento -->
			<div class="box_menu" id="menu_top_atendimento">
				<div class="box_atendimento">
					<h3><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
site/contato/">Central de Atendimento</a></h3>
					<p>Acesse a nossa Central de Atendimento e entre em contato conosco em
					todos os canais de relacionamento.</p>
				</div>
				<div class="box_atendimento">
					<h3><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
site/contato/">Telefone</a></h3>
					<p>55 5555-5555.<br>De segunda � sexta das 9:00 �s 18:00</p>
				</div>
				<div class="box_atendimento">
					<h3><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
site/ajuda/">D�vidas (F.A.C)</a></h3>
					<p>Se voc� tem alguma d�vida entre em nosso F.A.Q. ou entre em contato para solucionar sua d�vida.</p>
				</div>

				<div class="clear"></div>
			</div>

			<!-- Meus Dados -->
			<div class="box_menu" id="menu_top_dados">
				<ul>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/perfil/" title="Entrar no meu Perfil">Meu Perfil</a></li>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/senha/" title="Mudar a senha de minha conta">Mudar Senha</a></li>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/enderecos/" title="Meus endere�os de Entrega">Meus Endere�os de Entrega</a></li>
				</ul>
				<ul>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/pedidos/" title="Pedidos em andamento">Meus Pedidos</a></li>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/compras/" title="Minhas compras finalizadas">Minhas Compras</a></li>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/carrinho/" title="Meu carrinho de Compras">Meu Carrinho</a></li>
				</ul>
				<ul>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/desejos/" title="Lista de Produtos Preferidos">Minha Lista de Desejos</a></li>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/notificacao/" title="Produtos que voc� marcou">Notifica��o de Produtos</a></li>
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/config/" title="Configura��es da Conta">Configura��es</a></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
<!-- Conteudo do menu -->

<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/searchBar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	</div><!-- Fim topo -->

		<!-- Banner Topo -->
		<?php if ((($tmp = @$_smarty_tpl->getVariable('banner_topo')->value)===null||$tmp==='' ? '' : $tmp)!=''){?>
		<div class="banner_topo">
			<?php echo $_smarty_tpl->getVariable('banner_topo')->value;?>

		</div>
		<?php }?>

	<!-- Corpo -->
	<div class="corpo">
