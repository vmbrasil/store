<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 12:04:36
         compiled from "modules/publico/view/sub_templates/listar_categorias.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1384355648b84ca1fe3-54574667%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbdffb9a4db982877bc783e1c432b0b8053e845a' => 
    array (
      0 => 'modules/publico/view/sub_templates/listar_categorias.tpl',
      1 => 1337045078,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1384355648b84ca1fe3-54574667',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<!-- Lista de Categorias -->
	<?php if (sizeof($_smarty_tpl->getVariable('categorias')->value)>0&&$_smarty_tpl->getVariable('categorias')->value!=false){?>
    <ul class="lista_categorias">
	<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('categorias')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
		<li>
			<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
/"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a>
			
			<?php if (sizeof($_smarty_tpl->tpl_vars['item']->value['subcategorias'])>0&&$_smarty_tpl->tpl_vars['item']->value['subcategorias']!=false){?>
			    <ul>
				<?php  $_smarty_tpl->tpl_vars['subitem'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item']->value['subcategorias']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['subitem']->key => $_smarty_tpl->tpl_vars['subitem']->value){
?>
					<li>
						<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['subitem']->value['slug'];?>
/"><?php echo $_smarty_tpl->tpl_vars['subitem']->value['title'];?>
</a>
					</li>
				<?php }} ?>
				</ul>
			<?php }?>
			
		</li>
	<?php }} ?>
	</ul>
	<?php }?>
