<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 11:56:47
         compiled from "modules/usuario/view/perfil.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1078556489afda06d4-48303106%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '446aeb26230959665fb39758d033960389f15b10' => 
    array (
      0 => 'modules/usuario/view/perfil.tpl',
      1 => 1343410726,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1078556489afda06d4-48303106',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<!--Menu ESQUERDO -->
	<?php $_template = new Smarty_Internal_Template("modules/usuario/view/menu_perfil_usuario.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	
	
	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<!-- DirBar -->
		<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/bread_crumb.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
		
		<h2>Perfil do Usu�rio</h2>
		
		<div class="janela" id="wperfil">

				<!-- Dados Pessoais -->
				<fieldset>
					<legend>Dados Pessoais</legend>
					
					<div rel="cad_nome" class="linha_campo">
						<label rel="cad_nome" class="notnull" for="cad_nome">Nome Completo:</label>
						<input type="text" rel="cad_nome" id="cad_nome" name="nome" class="tstring notnull" value="" required="required" autofocus="autofocus">
					</div>
					
					<div rel="cad_email" class="linha_campo">
						<label rel="cad_email" class="notnull" for="cad_email">Data Nascimento:</label>
						<input type="text" rel="cad_email" id="cad_email" name="email" class="tstring notnull" value="" required="required">
					</div>
					
					<div rel="sexo_4aad7ac268c82ba01102cea37e15344c554626f4" class="linha_campo">
						<label data-text="Sexo" rel="sexo_4aad7ac268c82ba01102cea37e15344c554626f4" class="   notnull notnull" for="sexo_4aad7ac268c82ba01102cea37e15344c554626f4">Sexo:</label>
						<select form="4905e049bfd15ff858d9033877a89c77e027e77a" required="true" title="Sexo" class="tselect   notnull notnull" rel="sexo_4aad7ac268c82ba01102cea37e15344c554626f4" id="sexo_4aad7ac268c82ba01102cea37e15344c554626f4" name="sexo">
				 			<option selected="selected" value="M">Masculino</option>
							<option value="F">Feminino</option>
						</select>
					</div>

					<div rel="cad_nome" class="linha_campo">
						<label rel="cad_nome" class="notnull" for="cad_nome">CPF:</label>
						<input type="text" rel="cad_nome" id="cad_nome" name="nome" class="tstring notnull" value="" required="required" autofocus="autofocus">
					</div>

					<div rel="cad_nome" class="linha_campo">
						<label rel="cad_nome" class="notnull" for="cad_nome">RG:</label>
						<input type="text" rel="cad_nome" id="cad_nome" name="nome" class="tstring notnull" value="" required="required" autofocus="autofocus">
					</div>

					<div rel="estado_civil_4aad7ac268c82ba01102cea37e15344c554626f4" class="linha_campo">
						<label data-text="Estado Civil" rel="estado_civil_4aad7ac268c82ba01102cea37e15344c554626f4" class="   notnull notnull" for="estado_civil_4aad7ac268c82ba01102cea37e15344c554626f4">Estado Civil:</label>
						<select form="4905e049bfd15ff858d9033877a89c77e027e77a" required="true" title="Estado Civil" class="tselect   notnull notnull" rel="estado_civil_4aad7ac268c82ba01102cea37e15344c554626f4" id="estado_civil_4aad7ac268c82ba01102cea37e15344c554626f4" name="estado_civil">
			 				<option value="Solteiro(a)">Solteiro(a)</option>
							<option value="Casado(a)">Casado(a)</option>
							<option value="Separado(a)">Separado(a)</option>
							<option value="Divorciado(a)">Divorciado(a)</option>
							<option value="Vi�vo(a)">Vi�vo(a)</option>
							<option value="Outro(a)">Outro(a)</option>
						</select>
					</div>
					
					<div rel="escolaridade_4aad7ac268c82ba01102cea37e15344c554626f4" class="linha_campo">
						<label data-text="Escolaridade" rel="escolaridade_4aad7ac268c82ba01102cea37e15344c554626f4" class="  " for="escolaridade_4aad7ac268c82ba01102cea37e15344c554626f4">Escolaridade:</label>
						<select form="4905e049bfd15ff858d9033877a89c77e027e77a" title="Escolaridade" class="tselect" rel="escolaridade_4aad7ac268c82ba01102cea37e15344c554626f4" id="escolaridade_4aad7ac268c82ba01102cea37e15344c554626f4" name="escolaridade">
						 	<option value="">--Selecione--</option>			<option value="Analfabeto">Analfabeto</option>
							<option value="Semi-analfabeto">Semi-analfabeto</option>
							<option value="Ensino basico incompleto">Ensino basico incompleto</option>
							<option value="Ensino basico completo">Ensino basico completo</option>
							<option value="Ensino medio incompleto">Ensino medio incompleto</option>
							<option value="Ensino medio completo">Ensino medio completo</option>
							<option value="Ensino t�cnico incompleto">Ensino t�cnico incompleto</option>
							<option value="Ensino t�cnico completo">Ensino t�cnico completo</option>
							<option value="Ensino superior incompleto">Ensino superior incompleto</option>
							<option value="Ensino superior completo">Ensino superior completo</option>
							<option value="Especializa��o incompleto">Especializa��o incompleto</option>
							<option value="Especializa��o completo">Especializa��o completo</option>
							<option value="Mestrado incompleto">Mestrado incompleto</option>
							<option value="Mestrado completo">Mestrado completo</option>
							<option value="Doutorado incompleto">Doutorado incompleto</option>
							<option value="Doutorado completo">Doutorado completo</option>
							<option value="P�s-Doutorado incompleto">P�s-Doutorado incompleto</option>
							<option value="P�s-Doutorado completo">P�s-Doutorado completo</option>
							<option value="PHD">PHD</option>
						</select>
					</div>

				</fieldset>
				
				<fieldset>
					<legend>Contato</legend>
					

					<div rel="cad_email" class="linha_campo">
						<label rel="cad_email" class="notnull" for="cad_email">E-mail principal:</label>
						<input type="text" rel="cad_email" id="cad_email" name="email" class="tstring notnull" value="" required="required">
					</div>

					<div rel="cad_site" class="linha_campo">
						<label rel="cad_site" for="cad_site">Site:</label>
						<input type="text" rel="cad_site" id="cad_site" name="site" class="tstring" value="">
					</div>

					<div rel="cad_telefone" class="linha_campo">
						<label rel="cad_telefone" for="cad_telefone">Telefone:</label>
						<input type="text" rel="cad_telefone" id="cad_telefone" name="telefone" class="tstring" value="">
					</div>
					
					<div rel="cad_cel" class="linha_campo">
						<label rel="cad_cel" for="cad_cel">Celular:</label>
						<input type="text" rel="cad_cel" id="cad_cel" name="celular" class="tstring" value="">
					</div>
					
					
				</fieldset>
				
				<!-- Endere�o -->
				<fieldset>
					<legend>Endere�o Principal</legend>
					
					<div rel="cad_end" class="linha_campo">
						<label rel="cad_end" class="notnull" for="cad_end">Endere�o:</label>
						<input type="text" rel="cad_end" id="cad_end" name="endereco" class="tstring notnull" value="" required="required">
					</div>
					
					<div rel="cad_pais" class="linha_campo">
						<label rel="cad_pais" class="notnull" for="cad_pais">Pa�s:</label>
						<input type="text" rel="cad_pais" id="cad_pais" name="pais" class="tstring notnull" value="" required="required">
					</div>
					
					<div rel="cad_uf" class="linha_campo">
						<label rel="cad_uf" class="notnull" for="cad_uf">Regi�o / Estado:</label>
						<input type="text" rel="cad_uf" id="cad_uf" name="uf" class="tstring notnull" value="" required="required">
					</div>
					
					<div rel="cad_cid" class="linha_campo">
						<label rel="cad_cid" class="notnull" for="cad_cid">Cidade:</label>
						<input type="text" rel="cad_cid" id="cad_cid" name="cidade" class="tstring notnull" value="" required="required">
					</div>
					
					<div rel="cad_cep" class="linha_campo">
						<label rel="cad_cep" class="notnull" for="cad_cep">CEP:</label>
						<input type="text" rel="cad_cep" id="cad_cep" name="cep" class="tstring notnull" value="" required="required">
					</div>
					
				</fieldset>
				
		</div><!-- Fim Janela -->
		
		
		<div class="janela" id="wpendencias">
		
			<h2>Pendencias</h2>
			
			<section>
				<h3>Ultimos Pedidos</h3>
				lista dos pedidos aguardando finalizar
			</section>
			
			<section>
			<h3>Carrinho em Aberto</h3>
			lista dos carrinhos deixado em aberto
			</section>
			
			<section>
				<h3>Perguntas Respondidas</h3>
				lista das ultimas Perguntas Respondidas
			</section>
		</div><!-- Fim Janela -->

	</article><!-- Fim Conteudo -->
	
	<!-- MENU DIREITO -->
	<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/coluna_secundaria.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
