<?php /* Smarty version Smarty-3.0.6, created on 2015-06-01 15:34:11
         compiled from "modules/publico/view/sub_templates/elem_produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15546556ca5a346f163-18113203%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e0d8b7d361c24320541e85499c573a1edeb8b6bf' => 
    array (
      0 => 'modules/publico/view/sub_templates/elem_produto.tpl',
      1 => 1433183631,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15546556ca5a346f163-18113203',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<section class="hproduct box_produto" id="product_<?php echo $_smarty_tpl->getVariable('produto')->value['id'];?>
">
	<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produto/<?php echo $_smarty_tpl->getVariable('produto')->value['slug'];?>
/" title="Produto <?php echo $_smarty_tpl->getVariable('produto')->value['title'];?>
" class="detalhes" rel="product">
		<img src="<?php echo (($tmp = @$_smarty_tpl->getVariable('produto')->value['foto']['mini'])===null||$tmp==='' ? (($_smarty_tpl->getVariable('HOST')->value).($_smarty_tpl->getVariable('no_image')->value)) : $tmp);?>
" class="foto product-thumb img_loader" alt="Foto do produto <?php echo $_smarty_tpl->getVariable('produto')->value['title'];?>
">
		<h3 class="titulo product-title"><?php echo $_smarty_tpl->getVariable('produto')->value['title'];?>
</h3>
	</a>
	
	<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
categoria/<?php echo $_smarty_tpl->getVariable('produto')->value['categoria']['slug'];?>
/" class="categoria product-type" title="Categoria: <?php echo $_smarty_tpl->getVariable('produto')->value['categoria']['title'];?>
"><span>+</span><?php echo $_smarty_tpl->getVariable('produto')->value['categoria']['title'];?>
</a>
	<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
marca/<?php echo $_smarty_tpl->getVariable('produto')->value['marca']['id'];?>
" class="brand marca" title="Marca: <?php echo $_smarty_tpl->getVariable('produto')->value['marca']['title'];?>
"><span>Marca:</span><?php echo $_smarty_tpl->getVariable('produto')->value['marca']['title'];?>
</a>
	<div class="qualificacao"><span class="q_value"><?php echo $_smarty_tpl->getVariable('produto')->value['qualificacao']['value'];?>
</span><span class="q_count"><?php echo $_smarty_tpl->getVariable('produto')->value['qualificacao']['count'];?>
</span></div>
	<?php if ($_smarty_tpl->getVariable('produto')->value['preco_anterior']>$_smarty_tpl->getVariable('produto')->value['preco']){?>
	<div class="preco_anterior">De: <abbr class="currency" title="BRR" lang="pt-br">R$</abbr> <strong class="value amount currency money"><?php echo number_format((($tmp = @$_smarty_tpl->getVariable('produto')->value['preco_anterior'])===null||$tmp==='' ? '0,00' : $tmp),2,",",".");?>
</strong></div>
	<?php }?>
	<div class="preco value price sale"><abbr class="currency" title="Real BR" lang="pt-br">R$</abbr> <strong class="value amount currency money"><?php echo number_format((($tmp = @$_smarty_tpl->getVariable('produto')->value['preco'])===null||$tmp==='' ? '0,00' : $tmp),2,",",".");?>
</strong></div>
	<div class="parcela">ou <strong class="count"><?php echo $_smarty_tpl->getVariable('produto')->value['parcela']['max_count'];?>
x</strong> de <abbr class="currency" title="Real BR" lang="pt-br">R$</abbr> <strong class="value amount currency money"><?php echo number_format((($tmp = @$_smarty_tpl->getVariable('produto')->value['parcela']['value'])===null||$tmp==='' ? '0,00' : $tmp),2,",",".");?>
</strong></div>
	<?php if ($_smarty_tpl->getVariable('produto')->value['parcela']['juros']!=''){?><div class="juros <?php if ($_smarty_tpl->getVariable('produto')->value['parcela']['juros']=='sem'){?>sem_juros<?php }?>"><strong class="value"><?php echo $_smarty_tpl->getVariable('produto')->value['parcela']['juros'];?>
</strong> juros</div><?php }?>
	<?php if ($_smarty_tpl->getVariable('produto')->value['frete']['gratis']){?><div class="frete_gratis" title="Produto com frete GR�TIS para todas regi�es">frete gr�tis</div><?php }?>
	<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produto/<?php echo $_smarty_tpl->getVariable('produto')->value['slug'];?>
/" class="detalhes bt_detalhes" rel="product" title="Detalhes do produto <?php echo $_smarty_tpl->getVariable('produto')->value['title'];?>
">Ver</a>
	<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produto/<?php echo $_smarty_tpl->getVariable('produto')->value['slug'];?>
/" class="comprar bt_comprar" rel="product" title="Adicionar ao carrinho">Comprar</a>
</section>