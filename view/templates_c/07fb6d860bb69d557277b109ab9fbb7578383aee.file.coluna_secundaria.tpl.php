<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 10:51:15
         compiled from "modules/publico/view/sub_templates/coluna_secundaria.tpl" */ ?>
<?php /*%%SmartyHeaderCode:511555647a535872f7-42236852%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '07fb6d860bb69d557277b109ab9fbb7578383aee' => 
    array (
      0 => 'modules/publico/view/sub_templates/coluna_secundaria.tpl',
      1 => 1337137628,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '511555647a535872f7-42236852',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
		<aside class="menu_direito">

			<!-- ======================= LISTA TOP 5=========================== -->
			<?php if (count($_smarty_tpl->getVariable('produtos_top5')->value['produtos'])>0&&$_smarty_tpl->getVariable('produtos_top5')->value['produtos']!=false){?>
			<div class="menu_box" id="menu_top5">
				<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/top/"><h3 class="menu_topo_normal">Produtos TOP 5</h3></a>
				<ul class="top5">
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('produtos_top5')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['array_item']['iteration']=0;
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['array_item']['iteration']++;
?>
					<li <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['array_item']['iteration']==1){?>class="first"<?php }?>>
						<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						<div class="ordem"><?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['array_item']['iteration'];?>
</div>
					</li>
					<?php }} ?>
				</ul>
				<div style="clear: both;"></div>
			</div>
			<?php }?>

			<!-- =======================MENU NEWSLETTER=========================== -->
			<div class="menu_box" id="menu_newsletter">
				<h3 class="menu_topo_normal">Receba Ofertas</h3>
				<form action="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
newsletter/" method="post" class="form_newsletter">
					<span>
						<input name="" type="text" value="Seu Nome" title="Nome" size="15">
						<input name="" type="text" value="Seu E-mail" title="E-mail" size="15">
					</span>
					<button name="" type="submit" class="newsletter_enviar">OK</button>
					<div class="clear"></div>
				</form>
				
				<ul class="lista_bt_share">
					<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
rss/" class="rss">Assine nosso RSS</a></li>
					<li><a href="http://www.twitter.com" class="twitter">Siga-nos no twitter</a></li>
					<li><a href="http://www.facebook.com" class="facebook">Curta no facebook</a></li>
				</ul>

			</div>
	

			<!-- ======================= LISTA BANNERS LATERAIS=========================== -->
			<div class="menu_box" id="menu_banners_laterais">
				<ul class="lista_banners">
					<?php  $_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('banners_laterais')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['banner']->key => $_smarty_tpl->tpl_vars['banner']->value){
?>
					<li>
						<?php if ($_smarty_tpl->tpl_vars['banner']->value['url']){?><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
<?php echo $_smarty_tpl->tpl_vars['banner']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['banner']->value['description'];?>
/" title="<?php echo $_smarty_tpl->tpl_vars['banner']->value['description'];?>
"><?php }?>
							<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
<?php echo (($tmp = @$_smarty_tpl->tpl_vars['banner']->value['foto_url'])===null||$tmp==='' ? $_smarty_tpl->getVariable('no_image')->value : $tmp);?>
" alt="Link para <?php echo $_smarty_tpl->tpl_vars['banner']->value['description'];?>
" width="158">
						<?php if ($_smarty_tpl->tpl_vars['banner']->value['url']){?></a><?php }?>
					</li>
					<?php }} ?>
				</ul>
			</div>

		</aside><!-- Fim menu Direito -->
