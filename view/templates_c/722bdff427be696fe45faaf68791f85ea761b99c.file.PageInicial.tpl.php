<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 10:51:14
         compiled from "modules/publico/view/PageInicial.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1054055647a52be50e0-09742122%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '722bdff427be696fe45faaf68791f85ea761b99c' => 
    array (
      0 => 'modules/publico/view/PageInicial.tpl',
      1 => 1340916322,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1054055647a52be50e0-09742122',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<!--Menu ESQUERDO -->
	<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/menu_categorias.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		
		<!-- Banner -->
		<div class="banner_rotativo">
			<?php echo (($tmp = @$_smarty_tpl->getVariable('banner_rotativo')->value)===null||$tmp==='' ? '' : $tmp);?>

		</div>
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
		
			<!-- ===============================PRODUTOS=============================== -->
			<!-- VITRINE DE PRODUTOS DESTAQUES -->
			<div class="janela" id="vitrine">
				<?php if (sizeof($_smarty_tpl->getVariable('vitrine_destaques')->value['produtos'])>0){?>
				<section class="vitrine">
					<h2><span class="icone"></span><p>Destaques</p></h2>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('vitrine_destaques')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
						<li>
							<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						</li>
					<?php }} ?>
					</ul>
				</section>
				<?php }?>


				<!-- VITRINE DE PRODUTOS OFERTAS -->
				<?php if (sizeof($_smarty_tpl->getVariable('vitrine_ofertas')->value['produtos'])>0){?>
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Ofertas</p></h2>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('vitrine_ofertas')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
						<li>
							<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						</li>
					<?php }} ?>
					</ul>
				</section>
				<?php }?>


				<!-- VITRINE DE PRODUTOS NOVIDADES -->
				<?php if (sizeof($_smarty_tpl->getVariable('vitrine_novidades')->value['produtos'])>0){?>
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Novidades</p></h2>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('vitrine_novidades')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
						<li>
							<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						</li>
					<?php }} ?>
					</ul>
				</section>
				<?php }?>


				<!-- VITRINE DE PRODUTOS MAIS VENDIDOS -->
				<?php if (sizeof($_smarty_tpl->getVariable('vitrine_mais_vendidos')->value['produtos'])>0){?>
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Mais Vendidos</p></h2>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('vitrine_mais_vendidos')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
						<li>
							<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						</li>
					<?php }} ?>
					</ul>
				</section>
				<?php }?>



				<!-- VITRINE DE PRODUTOS MAIS BARATOS -->
				<?php if (sizeof($_smarty_tpl->getVariable('vitrine_mais_baratos')->value['produtos'])>0){?>
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Menores Pre�os</p></h2>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('vitrine_mais_baratos')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
						<li>
							<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						</li>
					<?php }} ?>
					</ul>
				</section>
				<?php }?>

				<!-- VITRINE DE PRODUTOS MAIS VISITATOS -->
				<?php if (sizeof($_smarty_tpl->getVariable('vitrine_mais_visitados')->value['produtos'])>0){?>
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Mais Visitados</p></h2>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('vitrine_mais_visitados')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
						<li>
							<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						</li>
					<?php }} ?>
					</ul>
				</section>
				<?php }?>


				<!-- VITRINE DE PRODUTOS FRETE GRATIS -->
				<?php if (sizeof($_smarty_tpl->getVariable('vitrine_frete_gratis')->value['produtos'])>0){?>
				<section class="vitrine">
					<hr>
					<h2><span class="icone"></span><p>Produtos com Frete Gr�tis</p></h2>
					<ul>
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('vitrine_frete_gratis')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
						<li>
							<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						</li>
					<?php }} ?>
					</ul>
				</section>
				<?php }?>


			</div><!-- Fim Janela -->


		</div><!-- Fim Conteudo central -->

	</article><!-- Fim Conteudo -->
	
	<!-- MENU DIREITO -->
	<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/coluna_secundaria.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
