<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 10:51:14
         compiled from "modules/system/view/system_script.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1055355647a5254a8c8-05713403%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '294426236a5a0c9097e4d115313abb0e7343dd27' => 
    array (
      0 => 'modules/system/view/system_script.tpl',
      1 => 1309104298,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1055355647a5254a8c8-05713403',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>

<script type="text/javascript">

	/* ######################### SISTEMA ######################### */
	var Global = {};
	Global.System = {
		properties : {

			host : '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
'	//o host principal do sistema
			,debug : <?php if ($_smarty_tpl->getVariable('DEBUG')->value==true){?>true<?php }else{ ?>false<?php }?>	//debug do sistema

			//properties Metods
			,get_host : function(){
				return this.host;
			}
			,get_debug : function(){
				return this.debug;
			}
		}//fim properties
		,events : {
			/*Inicia o objeto
			 * Carrega os valores
			 */
			init : function(){
				$.ajax({
					type: 'GET',
					url: 'ajax/system/',
					data: JSON.stringify(params),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (response, status) {
						//alert(JSON.stringify(response.d));
			
						if (response == null) {
							return false;
						}
			
						for (var i in response.menu){
							atual = response.d[i];
							var id 	= atual.ID;
							var data = atual.Data;
							var dia = atual.Data.substr(0, 2); // pega os 2 primeiros caracteres
							var mes = atual.Data.substr(3,2);
							var ano = atual.Data.substr(8,2);
							var cidade = atual.Cidade;
							var estado = atual.UF;
							var local = atual.Local;
							var horario = atual.Horario;
			
							var evento  = '<span class="data">'+dia+'/'+mes+'/'+ano+'</span>';
							evento += '<div class="dados local"><strong>LOCAL:</strong> '+local+'</div>';
							evento += '<div class="dados cidade"><strong>CIDADE / ESTADO:</strong> '+cidade+' - '+estado+'</div>';
							evento += '<div class="dados horario"><strong>HOR�RIO:</strong> '+horario+'</div>';
			
							$('#jcarousel-agenda').append('<li>'+evento+'</li>');
							i++;
						}
			
						$('#menu_categorias h3').text(response.titulo);
			
						$('#menu_categorias li:nth(2n-1)').addClass('linha1');
						$('#menu_categorias li:nth(2n)').addClass('linha2');
		
					},
					error: function (xhr, msg, e) {
						alert("Erro! " +e);
					}
				});

			}//fim init
		
		}//fim events
	}//fim system
	
	var System = Global.System;
</script>
