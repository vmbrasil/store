<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 12:13:00
         compiled from "modules/publico/view/head.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3141555648d7cd01827-46710431%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '02acdbc763cb0642a38e46fc148fd04c85c54e69' => 
    array (
      0 => 'modules/publico/view/head.tpl',
      1 => 1432653176,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3141555648d7cd01827-46710431',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
 <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
	<title><?php echo (($tmp = @$_smarty_tpl->getVariable('titulo')->value)===null||$tmp==='' ? '' : $tmp);?>
</title>
	
	<link rel="shortcut icon" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/images/favicon.ico" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo $_smarty_tpl->getVariable('template_slug')->value;?>
/css/system.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo $_smarty_tpl->getVariable('template_slug')->value;?>
/css/menu_topo.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo $_smarty_tpl->getVariable('template_slug')->value;?>
/css/geral.css" />

	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13fwJqueryUrl')->value;?>
"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo $_smarty_tpl->getVariable('template_slug')->value;?>
/js/plugins/json2.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo $_smarty_tpl->getVariable('template_slug')->value;?>
/js/plugins/jquery.price_format.1.0.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo $_smarty_tpl->getVariable('template_slug')->value;?>
/js/plugins/jquery.jcarousel.js"></script>

	<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo $_smarty_tpl->getVariable('template_slug')->value;?>
/js/menu_topo.js"></script>

	<?php $_template = new Smarty_Internal_Template("modules/system/view/system_script.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo $_smarty_tpl->getVariable('template_slug')->value;?>
/js/geral.js"></script>

	<?php echo (($tmp = @$_smarty_tpl->getVariable('metatags')->value)===null||$tmp==='' ? '' : $tmp);?>

	<?php echo (($tmp = @$_smarty_tpl->getVariable('css')->value)===null||$tmp==='' ? '' : $tmp);?>

	<?php echo (($tmp = @$_smarty_tpl->getVariable('js')->value)===null||$tmp==='' ? '' : $tmp);?>

	<?php echo (($tmp = @$_smarty_tpl->getVariable('htmlHead')->value)===null||$tmp==='' ? '' : $tmp);?>

</head>


