<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 12:24:56
         compiled from "modules/publico/view/produto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:243565564904845c343-72097025%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd1c1fbbf4c72de2da6938064cef1d45ad16d3d1b' => 
    array (
      0 => 'modules/publico/view/produto.tpl',
      1 => 1343400400,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '243565564904845c343-72097025',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/bread_crumb.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	
<div id="veja_categoria_departamento">
	Mais Produtos em <a href="<?php echo $_smarty_tpl->getVariable('mais')->value['url'];?>
"><?php echo $_smarty_tpl->getVariable('mais')->value['label'];?>
</a>
</div>	
				<!-- ===============================PRODUTO=============================== -->
				<div class="janela" id="produto">
					<a name="preco"></a>
					<h1 class="prod_nome_titulo"><?php echo $_smarty_tpl->getVariable('produto_atual')->value['title'];?>
</h1>

					<div class="prod_qualificacao">Avalia��o dos consumidores: <?php echo $_smarty_tpl->getVariable('qualificacao_estrelas')->value;?>
 <img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
modules/publico/view/images/template/rating-40.png" title="4 de 5 (Bom)"></div>
					
					<div class="prod_social_plugns">
						<ul class="social_buttons">
							<li>
								<div id="share_likebutton"></div>
							</li>
							<li>
								<div id="share_tweetbutton"></div>
							</li>
							<li>
								<g:plusone size="medium" callback="plusone_vote"></g:plusone>
							</li>
							<li><a href="#" id="share_product_email" title="Recomendar por e-mail">Recomendar por e-mail</a></li>
						</ul>
					</div>

					<!-- Dados -->
					<div class="prod_dados">

						<!-- Album esquerda Produto -->
						<div class="prod_esquerda_principal">
							<div id="prod_album_abas">
							
								<ul class="tabs">
									<li><a href="#tab-produto-album-fotos" title="Fotos">Fotos</a></li>
									<li><a href="#tab-produto-album-videos" title="Videos">Videos</a></li>
								</ul>
								
								<!-- Album Fotos -->
								<div id="tab-produto-album-fotos">
									<div class="prod_album">
										<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545.jpg<?php echo $_smarty_tpl->getVariable('produto_atual')->value['img_url'];?>
" title="" class="big_zoom" id="big_zoom" rel="fotos-galeria">
											<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545_sm.jpg" alt="" width="360" class="foto" />
										</a>
									</div>
									<div class="prod_album_miniaturas">
										<ul class="list slider_mount">
											<li>
												<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545_sm.jpg" title="" class="zoomThumbActive" rel="
												{gallery: 'fotos-galeria', smallimage: '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545_sm.jpg',largeimage: '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545.jpg'}
												">
													<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545_sm.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/01234567_md.jpg" title="" rel="
												{gallery: 'fotos-galeria', smallimage: '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/01234567_md.jpg',largeimage: '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/01234567_lg.jpg'}
												">
													<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/01234567_sm.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="http://www.lorempixum.com/350/350/" title="" rel="
												{gallery: 'fotos-galeria', smallimage: 'http://www.lorempixum.com/350/350/',largeimage: 'http://www.lorempixum.com/1024/768/'}
												">
													<img src="http://www.lorempixum.com/80/80/" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545_sm.jpg" title="" rel="
												{gallery: 'fotos-galeria', smallimage: '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545_sm.jpg',largeimage: '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545.jpg'}
												">
													<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/45545343545_sm.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/01234567_md.jpg" title="" rel="
												{gallery: 'fotos-galeria', smallimage: '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/01234567_md.jpg',largeimage: '<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/01234567_lg.jpg'}
												">
													<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
/images/produtos/01234567_sm.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
										</ul>
									</div><!-- Fim prod_album_miniaturas -->
								</div><!-- Fim Fotos-->
								
								<!-- Album Videos -->
								<div id="tab-produto-album-videos">
									<div class="prod_album">
										<div class="video"><object width="360" height="360"><param name="movie" value="http://www.youtube.com/v/o9pe57TCqTo"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="wmode" value="opaque"></param><embed src="http://www.youtube.com/v/o9pe57TCqTo" type="application/x-shockwave-flash" width="360" height="360" allowscriptaccess="always" allowfullscreen="true" wmode="opaque"></embed></object></div>
									</div>
									<div class="prod_album_miniaturas">
										<ul class="list slider_mount">
											<li>
												<a href="http://www.youtube.com/v/o9pe57TCqTo" title="" class="zoomThumbActive">
													<img src="http://i.ytimg.com/vi/o9pe57TCqTo/hqdefault.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="http://www.youtube.com/v/HHaaavc-mJs" title="Utra-Fly o mais barato do Brasil ">
													<img src="http://i.ytimg.com/vi/HHaaavc-mJs/hqdefault.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="http://www.youtube.com/v/NXcgGAPUATE" title="MONTAGEM AVIAO TUCANO PASSO A PASSO">
													<img src="http://i.ytimg.com/vi/NXcgGAPUATE/hqdefault.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="http://www.youtube.com/v/SYA9n1ovvtY" title="Eletra aviao de garrafa pet">
													<img src="http://i.ytimg.com/vi/SYA9n1ovvtY/hqdefault.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="http://www.youtube.com/v/IHyH6DKU2NQ" title="First test of the DXflyer-G4 Prototype Dagonfly">
													<img src="http://i.ytimg.com/vi/IHyH6DKU2NQ/hqdefault.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
											<li>
												<a href="http://www.youtube.com/v/Nc4biATS5sc" title="Hobby - Conhe�a os kits dos Aeromodelos">
													<img src="http://i.ytimg.com/vi/Nc4biATS5sc/hqdefault.jpg" class="thumb" alt="" width="75" height="75" />
												</a>
											</li>
										</ul>
									</div><!-- Fim prod_album_miniaturas -->
								</div><!-- Fim Videos-->
								
							</div><!-- Fim Abas-->

						</div><!-- Fim esquerda-->


						<!-- Dados direita Produto -->
						<div class="prod_direita_principal">
							<div class="prod_box_principal">
								<div class="prod_preco_box">
									<?php if ($_smarty_tpl->getVariable('produto_atual')->value['precoa']>$_smarty_tpl->getVariable('produto_atual')->value['precov']){?><div class="prod_preco_de">De: <strong>R$ <?php echo number_format((($tmp = @$_smarty_tpl->getVariable('produto_atual')->value['precoa'])===null||$tmp==='' ? '0,00' : $tmp),2,",",".");?>
</strong></div><?php }else{ ?><br /><?php }?>
									<div class="prod_preco_por" title="Apenas R$ <?php echo number_format($_smarty_tpl->getVariable('produto_atual')->value['precov'],2,",",".");?>
">Por: <strong>R$ <?php echo number_format($_smarty_tpl->getVariable('produto_atual')->value['precov'],2,",",".");?>
</strong></div>
									<div class="prod_preco_parcela">ou <strong>12X</strong> de <strong>R$ <?php echo number_format($_smarty_tpl->getVariable('produto_atual')->value['valor_parcela'],2,",",".");?>
</strong> sem juros</div>
									<?php if ($_smarty_tpl->getVariable('produto_atual')->value['precoa']>$_smarty_tpl->getVariable('produto_atual')->value['precov']){?><div class="prod_preco_economize">Economize: <strong>R$ <?php echo number_format(($_smarty_tpl->getVariable('produto_atual')->value['precoa']-$_smarty_tpl->getVariable('produto_atual')->value['precov']),2,",",".");?>
</strong> <span class="off">(-26% <b>OFF</b>)</span></div><?php }else{ ?><br /><?php }?>
								</div>
								
								<!-- =========== COMPRAR - COLOCAR NO CARRINHO ========= -->
								<div class="prod_comprar_box">
									<form action="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
carrinho/" method="post">
										<input type="hidden" name="exec" value="add">
										<input type="hidden" name="pid" value="<?php echo $_smarty_tpl->getVariable('produto_atual')->value['id'];?>
">
										<button name="btcomprar" type="submit" title="Adicionar item ao carrinho" class="prod_comprar_botao">Comprar</button>
									</form>
								</div>
								<div class="clear"></div>
							</div><!-- Fim box_principal-->


							<div class="prod_box_outros_dados">
							
								<!-- ======================== Formas de Pagamento =============================== -->
								<a name="pagamento"></a>
								<fieldset id="box-pagamento">
									<legend>Formas de pagamento</legend>
									<ul class="icones-pgto">
										<li class="pgto-dinheiro" title="Dinheiro">Dinheiro</li>
										<li class="pgto-boleto" title="Boleto">Boleto</li>
										<li class="pgto-tbb" title="Transferencia em conta Banco do Brasil">Transferencia em conta Banco do Brasil</li>
										<li class="pgto-ti" title="Transferencia em conta Itau">Transferencia em conta Itau</li>
										<li class="pgto-tcx" title="Transferencia em conta Caixa Economica">Transferencia em conta Caixa Economica</li>
										<li class="pgto-ps" title="Pag Seguro">Pag Seguro</li>
										<li class="pgto-pd" title="Pagamento Digital">Pagamento Digital</li>
										<li class="pgto-pp" title="Paypal">Paypal</li>
										<li class="pgto-cvisa" title="Cart�o Visa">Cart�o Visa</li>
										<li class="pgto-cmc" title="Cart�o Master Card">Cart�o Master Card</li>
										<li class="pgto-cdc" title="Cart�o Dinner Club">Cart�o Dinner Club</li>
										<li class="pgto-cae" title="Cart�o American Express">Cart�o American Express</li>
										<li class="pgto-chc" title="Cart�o Hipercard">Cart�o Hipercard</li>
									</ul>
									
									<ul class="prod_desconto_aviso">
										<li title="5% de desconto NO BOLETO"><span class="value">5%</span> de desconto <span class="forma">NO BOLETO</span></li>
										<li title="5% de desconto TRANSFERENCIA EM CONTA"><span class="value">5%</span> de desconto <span class="forma">TRANSFERENCIA EM CONTA</span></li>
									</ul>

								</fieldset>
								<div class="prod_box_mais_pagamentos"><a href="#">Mais formas de pagamento</a></div>

							
								<!-- ======================== FRETE ==================== -->
								<a name="entrega"></a>
								<fieldset id="box_frete">
									<legend>Formas de Envio</legend>
									
									<div class="prod_frete_gratis-full" title="Produto com FRETE GR�TIS para todas regi�es"><span class="ico"></span><span class="label">Frete Gr�tis para todas regi�es</span></div>
									<div class="prod_frete_gratis" title="Regi�es com frete gr�tis"><span class="label">Frete Gr�tis:</span><span class="value">ES - PR - SP</span></div>
									<div class="prod_frete_fixo" title="Frete fixo por unidade para todas regi�es"><span class="label">Frete Fixo:</span><span class="value">R$ 10,00</span></div>
									<form id="form_calcular_frete">
										<label for="cep" title="Informe seu CEP para calcular o valor do FRETE">
											<span class="label1">Calcular o Frete</span>
											<span class="label2">Informe seu CEP</span>
										</label>
										<input type="text" name="cep" id="cep" title="Informe seu CEP para calcular o Frete para sua regi�o">
										<button type="submit" title="Calcular frete">Calcular</button>
										<a href="#" id="descobrir_cep">N�o sei meu CEP</a>
									</form>
									<fieldset id="result_calcular_frete">
										<legend>Valores de Entrega</legend>
										<ul class="list">
											<li>
												<span class="description" title="Forma de Entrega"><span class="frete-icon-retirar"></span>Retirar no Local</span>
												<span class="prazo" title="Prazo de Entrega">Mesmo instante</span>
												<span class="value" title="Valor do Frete">GR�TIS</span>
											</li>
											<li>
												<span class="description" title="Forma de Entrega"><span class="frete-icon-local"></span>Entrega Local</span>
												<span class="prazo" title="Prazo de Entrega">1 dia �til</span>
												<span class="value" title="Valor do Frete">R$ 5,00</span>
											</li>
											<li>
												<span class="description" title="Forma de Entrega"><span class="frete-icon-pac"></span>PAC</span>
												<span class="prazo" title="Prazo de Entrega">10 dias �teis</span>
												<span class="value" title="Valor do Frete">R$ 8,36</span>
											</li>
											<li>
												<span class="description" title="Forma de Entrega"><span class="frete-icon-sedex"></span>Sedex</span>
												<span class="prazo" title="Prazo de Entrega">4 dias �teis</span>
												<span class="value" title="Valor do Frete">R$ 13,46</span>
											</li>
											<li>
												<span class="description" title="Forma de Entrega"><span class="frete-icon-transportadora"></span>Transportadora</span>
												<span class="prazo" title="Prazo de Entrega">6 dias �teis</span>
												<span class="value" title="Valor do Frete">R$ 29,15</span>
											</li>
										</ul>
									</fieldset><!-- Fim calculo result -->
									
									<fieldset id="result_calcular_frete">
										<legend>Refer�ncia de envio:</legend>
										<ul class="list">
											<li>
												<span class="description" title="Regi�o">SP - RJ - MG - PR - SC</span>
												<span class="prazo" title="Prazo de Entrega">4 dias �teis</span>
												<span class="value" title="Valor do Frete">R$ 9,99</span>
											</li>
											<li>
												<span class="description" title="Regi�o">DF -ES -GO -MS -RS</span>
												<span class="prazo" title="Prazo de Entrega">5 dias �teis</span>
												<span class="value" title="Valor do Frete">R$ 11,99</span>
											</li>
											<li>
												<span class="description" title="Regi�o">BA - ES - TO</span>
												<span class="prazo" title="Prazo de Entrega">8 dias �teis</span>
												<span class="value" title="Valor do Frete">R$ 12,99</span>
											</li>
											<li>
												<span class="description" title="Regi�o">AL CE MA PB PE PI RN RO</span>
												<span class="prazo" title="Prazo de Entrega">Mesmo dia</span>
												<span class="value" title="Valor do Frete">R$ 14,99</span>
											</li>
											<li>
												<span class="description" title="Regi�o">AC -AM -AP -PA -RO -RR</span>
												<span class="prazo" title="Prazo de Entrega">14 dias �teis</span>
												<span class="value" title="Valor do Frete">R$ 10,99</span>
											</li>
										</ul>
									</fieldset><!-- Fim Referencia Frete -->
    

								</fieldset><!-- Fim FRETE -->
								
								<!-- ======================== DADOS DA OFERTA ==================== -->
								<a name="pagamento"></a>
								<fieldset id="box_oferta">
									<legend>Oferta</legend>
									
									<div class="dados_oferta esquerda">
										<dl>
											<dt class="oferta-visitas">Visualizado:</dt>
												<dd class="oferta-visitas">1</dd>
											<dt class="oferta-vendidos">Vendidos:</dt>
												<dd class="oferta-vendidos">36</dd>
											<dt class="oferta-minimo-pedidos">Minimo de Pedidos:</dt>
												<dd class="oferta-minimo">36</dd>
											<dt class="oferta-minimo-produtos">Minimo por Pedido:</dt>
												<dd class="oferta-minimo">1</dd>
											<dt class="oferta-disponibilidade">Disponibilidade:</dt>
												<dd class="oferta-disponibilidade">Em estoque</dd>
											<dt class="oferta-estoque">Disponiveis:</dt>
												<dd class="oferta-estoque">36</dd>
											<dt class="oferta-iniciado">Iniciado em:</dt>
												<dd class="oferta-iniciado">15/08/2011 11 : 50 : 27</dd>
											<dt class="oferta-acabar">Tempo para Acabar:</dt>
												<dd class="oferta-acabar">28 : 50 : 27</dd>
										</dl>
										<p class="oferta-status valendo">Oferta Valendo</p>
									</div>
									
									<div class="box_prod_lists direita">
										<p class="title">Adicionar � listas:</p>
										<ul id="prod_add_lists">
											<li><a href="#" id="bt-add-list-fav" title="adicionar � lista de favoritos">Favoritos</a></li>
											<li><a href="#" id="bt-add-list-desejos" title="adicionar � lista de desejos">Lista de Desejos</a></li>
											<li><a href="#" id="bt-add-list-lembretes" title="adicionar � lista de lembretes">Lista de Lembretes</a></li>
										</ul>
										
										<div class="clear"></div>
										
										<div class="prod_cod">Cod.: <?php echo $_smarty_tpl->getVariable('produto_atual')->value['id_produto'];?>
</div>
									</div>
									
									<div class="clear"></div>

								</fieldset>

							</div>

						</div><!-- fim direita -->
					</div><!-- fim dados produto -->
				</div><!-- Fim Janela produto -->

				<br class="clear" />

				<!-- ===============================INDICE=============================== -->
				<div class="janela" id="box-indice">
					<hr>
					<h2 class="subtitulo">
						<span class="icon"></span>
						<span class="text">Veja nesta p�gina</span>
					</h2>
					<ul class="prod_indice_pag">
						<li><a href="#preco">Pre�o do produto</a></li>
						<li><a href="#pagamento">Formas de Pagamento</a></li>
						<li><a href="#entrega">Formas de Entrega</a></li>
						<li><a href="#oferta">Dados da Ofera</a></li>
						<li><a href="#detalhes">Detalhes do Produto</a></li>
						<li><a href="#junto">Aproveite e compre junto</a></li>
						<li><a href="#qualifica">Avalia��o dos consumidores</a></li>
						<li><a href="#opiniao">Opini�o dos Usu�rios</a></li>
						<li><a href="#duvidas">D�vidas dos internautas</a></li>
						<li><a href="#aproveite">Veja tamb�m</a></li>
					</ul>
				</div>
				
				


				<!-- =============================== COMPRE JUNTO =============================== -->

				<div class="janela" id="box-junto">
					<hr class="separador">
					<a name="junto"></a>
					<h2 class="subtitulo">
						<span class="icon"></span>
						<span class="text">Aproveite e Compre Junto</span>
					</h2>
					<div class="prod_box_veja_tambem">
						<?php if (sizeof($_smarty_tpl->getVariable('produtos_compre_junto')->value)>0&&$_smarty_tpl->getVariable('produtos_compre_junto')->value['produtos']!=false){?>
						<ul class="list slider_mount">
							<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('produtos_relacionados')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
							<li>
								<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto_compre_junto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
							</li>
							<?php }} ?>
						</ul>
						<?php }?>
					</div>
				</div><!-- Fim Janela -->




				<!-- ===============================DETALHES=============================== -->
				
				<div class="janela" id="box-detalhes">
					<hr class="separador">
					<a name="detalhes"></a>
					<h2 class="subtitulo">
						<span class="icon"></span>
						<span class="text">Detalhes do produto</span>
					</h2>

					<div id="abas_detalhes">
						<ul>
							<li><a href="#tab-produto-detalhes" title="Descri��o do Produto">Descri��o do Produto</a></li>
							<li><a href="#tab-produto-info" title="Informa��es Adicionais">Informa��es Adicionais</a></li>
							<li><a href="#tab-produto-caracteristica" title="Fun��es e Recursos">Caracteristicas</a></li>
						</ul>
						
						<div id="tab-produto-detalhes" class="prod_box_detalhes">
							<h3>Descri��o detalhada do Produto</h3>
							<?php echo (($tmp = @html_entity_decode($_smarty_tpl->getVariable('produto_atual')->value['detalhes']))===null||$tmp==='' ? 'Sem descri��o.' : $tmp);?>

						</div>
						
						<div id="tab-produto-info" class="prod_box_detalhes">
							<?php if ($_smarty_tpl->getVariable('produto_atual')->value['informacoes']!=''){?>
							<h3>Detalhes:</h3>
							<?php echo html_entity_decode($_smarty_tpl->getVariable('produto_atual')->value['informacoes']);?>

							<hr>
							<?php }?>
							
							<?php if (sizeof($_smarty_tpl->getVariable('produto_atual')->value['itens_decoded'])>0&&$_smarty_tpl->getVariable('produto_atual')->value['itens_decoded']!=false){?>
							<h3>Itens Inclusos:</h3>
							<ul>
							<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('produto_atual')->value['itens_decoded']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
								<li><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</li>
							<?php }} ?>
							</ul>
							<hr>
							<?php }?>
							
							<?php if ($_smarty_tpl->getVariable('produto_atual')->value['informacoes']!=''){?>
							<h3>Garantia:</h3>
							<?php echo html_entity_decode($_smarty_tpl->getVariable('produto_atual')->value['garantia_description']);?>

							<hr>
							<?php }?>
							
							<?php if (sizeof($_smarty_tpl->getVariable('produto_atual')->value['avisos_decoded'])>0&&$_smarty_tpl->getVariable('produto_atual')->value['avisos_decoded']!=false){?>
							<h3>Aviso:</h3>
							<ul>
							<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('produto_atual')->value['avisos_decoded']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
?>
								<li><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</li>
							<?php }} ?>
							</ul>
							<?php }?>
						</div>
						
						<div id="tab-produto-caracteristica" class="prod_box_detalhes">
							<h3>Caracteristicas:</h3>
							<dl>
								<dt>Bateria</dt><dd>800MHz</dd>
								<dt>Bateria</dt><dd>800MHz</dd>
								<dt>Bateria</dt><dd>800MHz</dd>
								<dt>Bateria</dt><dd>800MHz</dd>
								<dt>Bateria</dt><dd>800MHz</dd>
							</dl>
							<hr>
							
							<h3>Fun��es e Recursos:</h3>
							<dl>
								<dt>Bateria</dt><dd>800MHz</dd>
								<dt>Bateria</dt><dd>800MHz</dd>
								<dt>Bateria</dt><dd>800MHz</dd>
								<dt>Bateria</dt><dd>800MHz</dd>
								<dt>Bateria</dt><dd>800MHz</dd>
							</dl>
						</div>
						
					</div><!-- Fim abas -->
				</div><!-- Fim detalhes -->



				<!-- ===============================QUALIFICA��O=============================== -->
				<div class="janela"  id="box-qualifica">
					<hr class="separador">
					<a name="qualifica"></a>
					<h2 class="subtitulo">Avalia��o de Consumidores</h2>

					<div class="prod_box_qualificacao">
						<div class="media">
							<p class="label" style="float: left">M&eacute;dia de avalia&ccedil;&otilde;es</p>
							
							<em>
								<strong class="rating-produto avaliacao45"></strong>
								<span>4,5 estrelas em 33 Votos</span>
							</em>
						</div>

						<ul class="list_avaliacao">
							<li><span class="icone rating-50" title="Excelente"></span><span class="prod_grap_box"><span class="prod_grap_barra" style="width: <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star5']['porcentagem'])===null||$tmp==='' ? '1' : $tmp);?>
%;">&nbsp;</span></span> <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star5']['votos'])===null||$tmp==='' ? '0' : $tmp);?>
 votos (<?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star5']['porcentagem'])===null||$tmp==='' ? '0' : $tmp);?>
%)</li>
							<li><span class="icone rating-40" title="Otimo"></span><span class="prod_grap_box"><span class="prod_grap_barra" style="width: <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star4']['porcentagem'])===null||$tmp==='' ? '1' : $tmp);?>
%;">&nbsp;</span></span> <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star4']['votos'])===null||$tmp==='' ? '0' : $tmp);?>
 votos (<?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star4']['porcentagem'])===null||$tmp==='' ? '0' : $tmp);?>
%)</li>
							<li><span class="icone rating-30" title="Bom"></span><span class="prod_grap_box"><span class="prod_grap_barra" style="width: <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star3']['porcentagem'])===null||$tmp==='' ? '1' : $tmp);?>
%;">&nbsp;</span></span> <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star3']['votos'])===null||$tmp==='' ? '0' : $tmp);?>
 votos (<?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star3']['porcentagem'])===null||$tmp==='' ? '0' : $tmp);?>
%)</li>
							<li><span class="icone rating-20" title="Regular"></span><span class="prod_grap_box"><span class="prod_grap_barra" style="width: <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star2']['porcentagem'])===null||$tmp==='' ? '1' : $tmp);?>
%;">&nbsp;</span></span> <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star2']['votos'])===null||$tmp==='' ? '0' : $tmp);?>
 votos (<?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star2']['porcentagem'])===null||$tmp==='' ? '0' : $tmp);?>
%)</li>
							<li><span class="icone rating-10" title="Ruim"></span><span class="prod_grap_box"><span class="prod_grap_barra" style="width: <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star1']['porcentagem'])===null||$tmp==='' ? '1' : $tmp);?>
%;">&nbsp;</span></span> <?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star1']['votos'])===null||$tmp==='' ? '0' : $tmp);?>
 votos (<?php echo (($tmp = @$_smarty_tpl->getVariable('avaliacao')->value['star1']['porcentagem'])===null||$tmp==='' ? '0' : $tmp);?>
%)</li>
						</ul>
					</div>

					<div class="prod_box_votacao">
						<h4>Avalie esse produto:</h4>
						<form class="rating" action="rate.php">
							<input type="hidden" name="rating-value" id="rating-value" value="5" />
							
							<div id="stars-rating">
								<input name="star1" type="radio" value="1" class="star required" title="Ruim" />
								<input name="star1" type="radio" value="2" class="star" title="Regular" />
								<input name="star1" type="radio" value="3" class="star" title="Bom" />
								<input name="star1" type="radio" value="4" class="star" title="�timo" />
								<input name="star1" type="radio" value="5" class="star" title="Excelente" checked="checked" />
								<span id="rating-hover-label"></span>
								<div class="clear"></div>
							</div>
							
							<div class="opt_interesse">
								<label><input type="radio" name="interesse_produto" value="1">Tenho interesse</label>
								<label><input type="radio" name="interesse_produto" value="0">N�o Tenho interesse</label>
								<label><input type="radio" name="interesse_produto" value="2">J� tenho esse produto</label>
							</div>
							
							<div class="comentario">
								<label for="rating-text-comentario">Comentario:</label>
								<input type="text" name="rating-text-comentario" id="rating-text-comentario" value="">
							</div>
							
							<button id="rating-bt-avaliar">AVALIAR</button>
						</form>
						
						<a href="#" id="rating-ver-comentarios">ver coment�rios</a>

					</div>
				</div><!-- Fim Janela -->

				<div class="clear"><br /></div>



				<!-- ===============================OPINI�ES=============================== -->
				<div class="janela" id="box-opniao">
					<a name="opiniao"></a>
					<h2 class="subtitulo">
						<span class="icon"></span>
						<span class="text">Opni�o de Usu�rios</span>
						<button id="show_more_opiniao" class="bt_round_more_detail" title="Mostrar mais opini�es sobre o produto">
							<span class="text">+</span>
						</button>
					</h2>
					<ul class="prod_opiniao_especialistas">
						<li>
							<div class="comment-user-box">
								<h3><span class="comment-title">Nome do Cara</span><span class="comment-data">Data: 11/4/2010</span></h3>
								<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
images/avatar/544545102012.png" class="comment-foto" alt="Nome do cara" width="100" height="115">
								<p class="comment-text">Opni�o do cara</p>
								<div class="comment-toolbar">
									<a href="#" class="comment-tool-responder" title="Responder">Responder</a>
									<div class="comment-opnion-io">
										<span class="label">Este coment�rio ajudou?</span>
										<a href="#" class="comment-tool-positive" title="Cometario Positivo">Positivo</a>
										<a href="#" class="comment-tool-negative" title="Cometario Negativo">Negativo</a>
									</div>
									<a href="#" class="comment-tool-denuciar" title="Denunciar como Impr�prio">Denunciar</a>
								</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</li>
					</ul>
				</div><!-- Fim Janela -->


				<!-- ===============================DUVIDAS=============================== -->
				<div class="janela" id="box-duvidas">
					<a name="duvidas"></a>
					<h2 class="subtitulo">
						<span class="icon"></span>
						<span class="text">D�vidas dos internautas</span>
						<button id="show_more_duvidas" class="bt_round_more_detail" title="Mostrar mais d�vidas sobre o produto">
							<span class="text">+</span>
						</button>
					</h2>
					<ul class="prod_perguntas_internauta">
						<li>
							<fieldset>
								<legend>Pergunta</legend>
								<div class="comment-user-box">
									<h3><span class="comment-title">Nome do Cara</span><span class="comment-data">Data: 11/4/2010</span></h3>
									<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
images/avatar/02534035405.png" class="comment-foto" alt="Nome do cara" width="100" height="115">
									<p class="comment-text">Pergunta do cara. dsfsdfsd sdfsdfsdfsd sdfsf sdfsdfsdf sdffsdf sdfsdf.</p>
									<div class="comment-toolbar">
										<a href="#" class="comment-tool-responder" title="Responder">Responder</a>
										<div class="comment-opnion-io">
											<span class="label">Este coment�rio ajudou?</span>
											<a href="#" class="comment-tool-positive" title="Cometario Positivo">Positivo</a>
											<a href="#" class="comment-tool-negative" title="Cometario Negativo">Negativo</a>
										</div>
										<a href="#" class="comment-tool-denuciar" title="Denunciar como Impr�prio">Denunciar</a>
									</div>
									<div class="clear"></div>
								</div>

								<fieldset>
									<legend>Resposta</legend>
									<div class="comment-user-box">
										<h3><span class="comment-title">Nome do Cara</span><span class="comment-data">Data: 11/4/2010</span></h3>
										<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
images/avatar/544545102012.png" class="comment-foto" alt="Nome do cara" width="100" height="115">
										<p class="comment-text">Resposta do cara</p>
										<div class="comment-toolbar">
											<a href="#" class="comment-tool-responder" title="Responder">Responder</a>
											<div class="comment-opnion-io">
												<span class="label">Este coment�rio ajudou?</span>
												<a href="#" class="comment-tool-positive" title="Cometario Positivo">Positivo</a>
												<a href="#" class="comment-tool-negative" title="Cometario Negativo">Negativo</a>
											</div>
											<a href="#" class="comment-tool-denuciar" title="Denunciar como Impr�prio">Denunciar</a>
										</div>
										<div class="clear"></div>
									</div>
									<div class="clear"></div>
								</fieldset>

								<div class="clear"></div>
							</fieldset>
							<div class="clear"></div>
						</li>
					</ul>
					
					<button id="bt_perguntar" title="Fazer uma pergunta sobre o produto">Perguntar</button>
				</div><!-- Fim Janela -->

				
				<!-- ===============================VEJA TAMB�M=============================== -->
				<div class="janela" id="box-veja">
					<hr class="separador">
					<a name="aproveite"></a>
					<h2 class="subtitulo">
						<span class="icon"></span>
						<span class="text">Veja Tamb�m</span>
					</h2>

					<?php if (count($_smarty_tpl->getVariable('produtos_relacionados')->value['produtos'])>0&&$_smarty_tpl->getVariable('produtos_relacionados')->value['produtos']!=false){?>
					<ul class="list slider_mount">
						<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('produtos_relacionados')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
						<li>
							<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
						</li>
						<?php }} ?>
					</ul>
					<?php }?>

				</div><!-- Fim Janela -->


				<!-- ===============================TAGS=============================== -->
				<div class="janela box-tags" id="box-tags">
					<hr class="separador">
					<a name="tags"></a>
					<h2 class="subtitulo">
						<span class="icon"></span>
						<span class="text">Tags do Produto</span>
					</h2>
					<p class="explicacao">* O que s�o Tags? Tags s�o palavras-chaves associadas aos produtos pelos pr�prios consumidores. Ao clicar em uma Tag voc� poder� ver todos os produtos associados a ela.</p>
					<div class="tags_list">
					<?php if (count($_smarty_tpl->getVariable('produto_atual')->value['tags_decoded'])>0&&$_smarty_tpl->getVariable('produto_atual')->value['tags_decoded']!=false){?>
						<?php  $_smarty_tpl->tpl_vars['tag'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('produto_atual')->value['tags_decoded']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['tag']->key => $_smarty_tpl->tpl_vars['tag']->value){
?>
						<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
tag/<?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
/"><?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
</a>
						<?php }} ?>
					<?php }else{ ?>
						<p class="mensagem">Sem tags no momento. Adicione tags clicando abaixo.</p>
					<?php }?>
					</div>
					<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produto/<?php echo $_smarty_tpl->getVariable('produto_atual')->value['slug'];?>
/add-tag/" id="add_tag" title="Adicione uma Tag para este produto">Adicione uma Tag</a>
					<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
tags/populares/" id="more_tags" title="Veja todas as Tags mais populares">Veja as Tags mais populares</a>
					
					<div class="clear"></div>
				</div><!-- Fim Janela -->





				