<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 10:51:15
         compiled from "modules/publico/view/sub_templates/menu_categorias.tpl" */ ?>
<?php /*%%SmartyHeaderCode:818155647a53047489-92702852%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5e109d65b023000044122ae601b1ad00e98292d' => 
    array (
      0 => 'modules/publico/view/sub_templates/menu_categorias.tpl',
      1 => 1343231358,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '818155647a53047489-92702852',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<div class="menu_categorias">
	<!-- =======================MENU DE DEPARTAMENTOS E CATEGORIAS=========================== -->
		<?php if (sizeof($_smarty_tpl->getVariable('menu_categoria')->value['categorias'])>0){?>
		<nav class="menu_box" id="menu_categorias">
			<h3 class="menu_topo"><?php echo (($tmp = @$_smarty_tpl->getVariable('menu_categoria')->value['titulo'])===null||$tmp==='' ? 'Categorias' : $tmp);?>
</h3>
			<ul class="list">
				<?php  $_smarty_tpl->tpl_vars['categoria'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('menu_categoria')->value['categorias']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['categoria']->key => $_smarty_tpl->tpl_vars['categoria']->value){
?>
				<li>
					<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['categoria']->value['slug'];?>
/"><?php echo $_smarty_tpl->tpl_vars['categoria']->value['title'];?>

						<?php if ($_smarty_tpl->tpl_vars['categoria']->value['product_count']){?>(<?php echo $_smarty_tpl->tpl_vars['categoria']->value['product_count'];?>
)<?php }?>
					</a>
					<?php if (count($_smarty_tpl->tpl_vars['categoria']->value['subcategorias'])>0){?>
					<ul>
						<?php  $_smarty_tpl->tpl_vars['subcategoria'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['categoria']->value['subcategorias']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['subcategoria']->key => $_smarty_tpl->tpl_vars['subcategoria']->value){
?>
						<li>
							<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
categoria/<?php echo $_smarty_tpl->tpl_vars['subcategoria']->value['slug'];?>
/"><?php echo $_smarty_tpl->tpl_vars['subcategoria']->value['title'];?>

								<?php if ($_smarty_tpl->tpl_vars['subcategoria']->value['product_count']){?>(<?php echo $_smarty_tpl->tpl_vars['subcategoria']->value['product_count'];?>
)<?php }?>
							</a>
						</li>
						<?php }} ?>
					</ul>
					<?php }?>
				</li>
				<?php }} ?>
				<li id="menu_view_all_category">
					<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
categorias/">Mais Categorias</a>
				</li>
			</ul>
		</nav>
		<?php }?>



		<!-- =======================MENU MARCAS=========================== -->
		<?php if (count($_smarty_tpl->getVariable('menu_marcas')->value)>0){?>
		<section class="menu_box" id="menu_marcas">
			<h3 class="menu_topo_normal">Grandes Marcas</h3>
			<ul>
			<?php  $_smarty_tpl->tpl_vars['marca'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('menu_marcas')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['marca']->key => $_smarty_tpl->tpl_vars['marca']->value){
?>
				<li>
					<a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
marcas/<?php echo $_smarty_tpl->tpl_vars['marca']->value['id'];?>
/">
						<img src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
<?php echo (($tmp = @$_smarty_tpl->tpl_vars['marca']->value['foto_url'])===null||$tmp==='' ? $_smarty_tpl->getVariable('no_image')->value : $tmp);?>
" alt="Marca <?php echo $_smarty_tpl->tpl_vars['marca']->value['title'];?>
">
					</a>
				</li>
			<?php }} ?>
			</ul>
		</section>
		<?php }?>


		<!-- =======================MENU VEJA MAIS=========================== -->
		<section class="menu_box" id="menu_veja_mais">
			<h3 class="menu_topo_normal">Veja Mais</h3>
			<ul>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/destaques/">		<span class="mais">mais</span> Destaques</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/ofertas/">			<span class="mais">mais</span> Ofertas</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/lancamentos/">		<span class="mais">mais</span> Lan�amentos</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/sugeridos/">		<span class="mais">mais</span> Sugeridos</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
promocoes/">				<span class="mais">mais</span> Promo��es</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/mais-vendidos/">	<span class="mais">mais</span> Vendidos</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/mais-visitados/">	<span class="mais">mais</span> Visitados</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/mais-baratos/">	<span class="mais">mais</span> Baratos</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/mais-qualificados/"><span class="mais">mais</span> Qualificados</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
marcas/">					<span class="mais">mais</span> Marcas</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/frete-gratis/">	<span class="mais">mais</span> Frete Gr�tis</a></li>
			</ul>
		</section>



		<!-- =======================MENU INFORMAC�ES=========================== -->
		<section class="menu_box" id="menu_informacoes">
			<h3 class="menu_topo_normal">Informa��es</h3>
			<ul>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
ajuda/como-comprar/">Como Comprar</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
ajuda/devolucoes/">Fretes e Devolu��es</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
institucional/termos/">Politica de Privacidade</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
usuario/pedidos/">Rastrear Pedidos</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
site/mapa/">Mapa do Site</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
institucional/fornecedor/">Fornecedor</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
institucional/revendas/">Revendas</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
site/contato/">Fale Conosco</a></li>
				<li><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
produtos/catalogo/">Baixar Cat�logo</a></li>
			</ul>
		</section>
		



	</div><!-- Fim menus esquerdo-->
