<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 12:04:36
         compiled from "modules/publico/view/PageCategorias.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2790655648b8491f9c8-88697380%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b63bfebc5786c7f5809a5e3f27160fb3c1d006b0' => 
    array (
      0 => 'modules/publico/view/PageCategorias.tpl',
      1 => 1340895588,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2790655648b8491f9c8-88697380',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<!--Menu ESQUERDO -->
	<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/menu_categorias.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

	<!-- CONTEUDO PRINCIPAL -->
	<article class="conteudo">
		<header>
			<!-- BreadCrumb -->
			<?php if (sizeof($_smarty_tpl->getVariable('barra_diretorio')->value)>0){?>
			<div class="diretorio_bar">
		    	<ul>
		    		<li class="label">Voc� est� em:</li>
				<?php  $_smarty_tpl->tpl_vars['diritem'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('barra_diretorio')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['diritem']->key => $_smarty_tpl->tpl_vars['diritem']->value){
?>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['diritem']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['diritem']->value['label'];?>
</a></li>
				<?php }} ?>
				</ul>
			</div>
			<?php }?>
			
			<h2><?php echo (($tmp = @$_smarty_tpl->getVariable('categoria')->value['title'])===null||$tmp==='' ? 'Categorias da Loja' : $tmp);?>
</h2>
			
		</header>
		
		<!-- BOX CENTRAL -->
		<div class="conteudo_central">
			
			<!-- ===============================PRODUTOS=============================== -->

			<div id="banner_categorias">
				<a href="#"><img alt="Rolagem de produtos em sub-categorias" src="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
images/banners/banner_subcategorias.png" width="587" height="109"></a>
				<div class="clear"></div>
			</div>
			
			<!-- VITRINE DE PRODUTOS -->
			<div class="categorias">
				<p>Selecione alguma categoria desejada abaixo:</p>
				<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/listar_categorias.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
				<div class="clear"></div>
			</div>

			<?php if (count($_smarty_tpl->getVariable('produtos_relacionados')->value['produtos'])>0&&$_smarty_tpl->getVariable('produtos_relacionados')->value['produtos']!=false){?>
			<!-- ===============================VEJA TAMB�M=============================== -->
			<div class="janela" id="box-acessados">
				<hr class="separador">
				<a name="aproveite"></a>
				<h2 class="subtitulo">
					<span class="icon"></span>
					<span class="text">Mais acessados</span>
				</h2>

				<ul class="list slider_mount">
					<?php  $_smarty_tpl->tpl_vars['produto'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('produtos_relacionados')->value['produtos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['produto']->key => $_smarty_tpl->tpl_vars['produto']->value){
?>
					<li>
						<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/elem_produto.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
					</li>
					<?php }} ?>
				</ul>
			</div><!-- Fim Janela -->
			<?php }?>

		</div><!-- Fim Conteudo central -->

	</article><!-- Fim Conteudo -->


	<!-- MENU DIREITO -->
	<?php $_template = new Smarty_Internal_Template("modules/publico/view/sub_templates/coluna_secundaria.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
