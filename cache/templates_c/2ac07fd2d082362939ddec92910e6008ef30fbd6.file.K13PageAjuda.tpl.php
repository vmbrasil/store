<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 14:42:51
         compiled from "../framework/modules/view/K13PageAjuda.tpl" */ ?>
<?php /*%%SmartyHeaderCode:202325568a51bdaf080-77672092%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2ac07fd2d082362939ddec92910e6008ef30fbd6' => 
    array (
      0 => '../framework/modules/view/K13PageAjuda.tpl',
      1 => 1390472098,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '202325568a51bdaf080-77672092',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<ul id="listbutom-page-usabilidade">
	<li class="page-bt-back">
		<a href="javascript:history.go(-1);" id="page-bt-back" title="Voltar pagina">
			Voltar
		</a>
	</li>
	
	<li class="page-bt-update">
		<a href="javascript:document.location.reload();" id="page-bt-update" title="Atualizar pagina atual">
			Atualizar
		</a>
	</li>
	
	<li class="page-bt-maximize">
		<a href="#" id="page-bt-maximize" title="Abrir pagina atual em nova janela." target="_blank">
			Nova janela
		</a>
	</li>
	
	<?php if (count($_smarty_tpl->getVariable('page_ajuda')->value)>0&&$_smarty_tpl->getVariable('page_ajuda')->value['descricao']!=''){?>
	<li class="page-bt-ajuda">
		<span id="page-bt-ajuda" title="<?php echo $_smarty_tpl->getVariable('page_ajuda')->value['descricao'];?>
">
			<?php if ($_smarty_tpl->getVariable('page_ajuda')->value['link']!=''){?><a href="<?php echo $_smarty_tpl->getVariable('page_ajuda')->value['link'];?>
" target="blank">Ajuda</a><?php }?>
			
			<div id="box_ajuda">
				<span class="balao_bico"></span>
				<p><?php echo $_smarty_tpl->getVariable('page_ajuda')->value['descricao'];?>
</p>
				<a href="<?php echo $_smarty_tpl->getVariable('page_ajuda')->value['link'];?>
" class="link_ajuda" target="_blank">Ler Mais</a>
			</div>
		</span>
	</li>
	<?php }?>
</ul>