<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 13:31:56
         compiled from "../../framework_k13_v1.0/modules/view/header_libs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1089255649ffc588200-37031367%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e0d30f3b2a7478bd8f7303591e5d7be2669f266b' => 
    array (
      0 => '../../framework_k13_v1.0/modules/view/header_libs.tpl',
      1 => 1373740210,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1089255649ffc588200-37031367',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
modules/view/css/system_debug.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
modules/view/skins/<?php echo $_smarty_tpl->getVariable('k13fwSkinTemplate')->value;?>
/css/jquery/jquery-ui.custom.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/infinit-slider/infinit-slider.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/gzoom/css/jquery.gzoom_k13web.css" />
	
	<!-- Plugins de terceiros -->
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/jquery/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/jquery/js/jquery-ui-1.8.8.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/jquery/js/jquery.ui.autocomplete-modificado.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/jquery/js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/colorpicker/js/colorpicker.js"></script>
<!--	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/jcarousel/jquery.jcarousel.js"></script>-->
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/infinit-slider/jquery.infinit-slider.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/gzoom/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/gzoom/js/jquery.gzoom.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/jquery.slug-modificado.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/jquery.maskedinput-1.3.min.js"></script>
	
	<!-- FW System -->
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
modules/view/js/mascaras.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
modules/view/js/fw_system_lib.js"></script>
	

<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13_system_script.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
