<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 17:03:59
         compiled from "../framework/modules/view/K13ViewGridList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:38995568c62f5c7d01-75398811%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ff3f07388950d6d74ea9061d9ff127edb9d54af9' => 
    array (
      0 => '../framework/modules/view/K13ViewGridList.tpl',
      1 => 1368451300,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '38995568c62f5c7d01-75398811',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="grid_<?php echo $_smarty_tpl->getVariable('grid')->value['id'];?>
" data-id="<?php echo $_smarty_tpl->getVariable('grid')->value['id'];?>
" class="tb_grid_result">
		<thead>
			<tr>
			<?php ob_start(); ?>
				<?php if ($_smarty_tpl->getVariable('grid')->value['column_selector']==true){?><th width="24px"><input type="checkbox" title="Clique para selecionar todos itens" class="ck_all_selector" id="ck_all_selector_<?php echo $_smarty_tpl->getVariable('header')->value['id'];?>
"></th><?php }?>
				<?php  $_smarty_tpl->tpl_vars['header'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('grid')->value['header']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['header']->key => $_smarty_tpl->tpl_vars['header']->value){
?>
					<th title="<?php echo $_smarty_tpl->tpl_vars['header']->value['description'];?>
"><?php echo $_smarty_tpl->tpl_vars['header']->value['label'];?>
</th>
				<?php }} ?>
				<?php if ($_smarty_tpl->getVariable('grid')->value['column_acao']==true){?><th colspam="2">Ação</th><?php }?>
			<?php  Smarty::$_smarty_vars['capture']['grid_header']=ob_get_clean();?>
			<?php echo Smarty::$_smarty_vars['capture']['grid_header'];?>

			</tr>
		</thead>

		<tfoot>
			<tr>
				<?php echo Smarty::$_smarty_vars['capture']['grid_header'];?>

			</tr>
		</tfoot>
		
		<tbody>
				<?php  $_smarty_tpl->tpl_vars['registro'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('grid')->value['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['registro']->key => $_smarty_tpl->tpl_vars['registro']->value){
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['registro']->key;
?>
				<tr data-id="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">
					<?php if ($_smarty_tpl->getVariable('grid')->value['column_selector']==true){?>
						<td align="center"><input type="checkbox" title="Clique para selecionar esse item" pk="<?php echo $_smarty_tpl->tpl_vars['registro']->value['pk_value'];?>
" id="$registro.pk_value" class="ck_selecionador" name="ck_selecionador"></td>
					<?php }?>
					<?php  $_smarty_tpl->tpl_vars['campo'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['registro']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['campo']->key => $_smarty_tpl->tpl_vars['campo']->value){
?>
						<?php if ($_smarty_tpl->tpl_vars['campo']->value['visible']==true){?><td><?php echo $_smarty_tpl->tpl_vars['campo']->value['value'];?>
</td><?php }?>
					<?php }} ?>
					<?php if ($_smarty_tpl->getVariable('grid')->value['column_acao']==true){?>
						<td align="center" colspam="2" class="botao_acao">
						<?php  $_smarty_tpl->tpl_vars['botao'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['registro']->value['botoes_acao']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['botao']->key => $_smarty_tpl->tpl_vars['botao']->value){
?>
							<a title="<?php echo $_smarty_tpl->tpl_vars['botao']->value['description'];?>
" class="botao_acao <?php echo $_smarty_tpl->tpl_vars['botao']->value['class'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['botao']->value['url'];?>
"><span class="icon"></span><span class="text"><?php echo $_smarty_tpl->tpl_vars['botao']->value['label'];?>
</span></a>
						<?php }} ?>
						</td>
					<?php }?>
				</tr>
				<?php }} else { ?>
				<tr>
					<td colspan="100%" class="grid_none">Não foi encontrado nenhum registro.</td>
				</tr>
				<?php } ?>
		</tbody>
	</table>