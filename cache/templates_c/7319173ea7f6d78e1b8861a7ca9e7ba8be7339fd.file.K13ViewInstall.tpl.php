<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 13:36:09
         compiled from "../../framework_k13_v1.0/modules/view/K13ViewInstall.tpl" */ ?>
<?php /*%%SmartyHeaderCode:168825564a0f902f184-46570663%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7319173ea7f6d78e1b8861a7ca9e7ba8be7339fd' => 
    array (
      0 => '../../framework_k13_v1.0/modules/view/K13ViewInstall.tpl',
      1 => 1362257815,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168825564a0f902f184-46570663',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<html>
<head>

	<title><?php echo (($tmp = @$_smarty_tpl->getVariable('titulo')->value)===null||$tmp==='' ? 'Instalação do Sistema' : $tmp);?>
</title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
modules/view/css/system_debug.css" />
	<?php echo $_smarty_tpl->getVariable('css')->value;?>

	<?php echo $_smarty_tpl->getVariable('js')->value;?>

	<?php echo $_smarty_tpl->getVariable('htmlHead')->value;?>


</head>
<body>

	<header class="title_box">
		<h1>Resultado da Instalação</h1>
	</header>
	<section class="install_result">
		<div class="install_result_resumo">
			<span>Sucessos: <?php echo $_smarty_tpl->getVariable('count_success')->value;?>
</span>
			<span>Erros: <?php echo $_smarty_tpl->getVariable('count_errors')->value;?>
</span>
			<div class="clear"></div>
		</div>
		
		<ul class="install_result_list">
		<?php echo $_smarty_tpl->getVariable('result')->value;?>

		</ul>
	</section>

</body>
</html>