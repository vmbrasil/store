<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 14:42:51
         compiled from "../framework/modules/interface/view/tpl/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:320285568a51b73ab47-75754827%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cddb5f959f83b626dd9d6eb1f89c9016b57662f3' => 
    array (
      0 => '../framework/modules/interface/view/tpl/login.tpl',
      1 => 1373739675,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '320285568a51b73ab47-75754827',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/interface/view/tpl/head.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<body>
<div id="tudo">
	<header class="topo">
		<div class="title_box">
			<div class="logo"><a href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
"></a></div>
			<h1>K13WEB SYSTEM</h1>
			
			<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13PageAjuda.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
			
		</div>
				
	</header>
	
	
	<section id="form_login_section">
		<header class="k13-acrilico">
			<div class="titulo_login">Identificação do Usuário</div>
			<button type="button" title="Cancelar login" id="close_login">X</button>
			<br style="clear: both;">
		</header>
		<article>
			<form action="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
login/" method="post" name="form_login" id="form_login">
				<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/msg_system.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
				<label class="linha_campo" id="msg_invalid_login">Login ou Senha invalidos!</label>
			
				<div rel="qtd" class="linha_campo">
					<label rel="login" class="notnull" for="login">Usuário:</label>
					<div class="tooltip">
						<span class="wrapper">
						<div class="text">Login ou email de usuário para entrar no sistema</div>
						<div class="required">* Preenchimento Obrigatório</div>
						</span>
					</div>
					<input type="text" rel="login" id="login" name="login" form="form_login" title="Login de usuário para entrar no sistema" class="tstring notnull" value="" required="true" autocomplete="off" autofocus="autofocus">
				</div>
				
				<div rel="qtd" class="linha_campo">
					<label rel="senha" class="notnull" for="senha">Senha:</label>
					<div class="tooltip">
						<span class="wrapper">
						<div class="text">Senha de usuário para entrar no sistema</div>
						<div class="required">* Preenchimento Obrigatório</div>
						</span>
					</div>

					<input type="password" rel="senha" id="senha" name="senha" form="form_login" required="true" title="Senha de usuário para entrar no sistema" class="tpassword notnull" value="" autocomplete="off">
				</div>
				
				<div class="tool_box">
					<button form="form_login" aria-disabled="false" role="button" class="bt_submit bt_submit_detalhe tool k13-acrilico" title="Entrar no Sistema" value="Entrar" type="submit"><span class="cad-button-icon-login"></span><span class="cad-button-text">Entrar</span></button>
					<a hre="#" class="mini_link" id="recuperar_senha" title="Clique aqui caso você tenha esquecido ou perdido a senha">Esqueci a senha</a>
					<div class="clear"></div>
				</div>
			</form>
			
		</article>
	</section>

<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/interface/view/tpl/rodape.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>