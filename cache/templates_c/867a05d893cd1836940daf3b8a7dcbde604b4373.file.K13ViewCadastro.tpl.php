<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 17:04:00
         compiled from "../framework/modules/view/K13ViewCadastro.tpl" */ ?>
<?php /*%%SmartyHeaderCode:115505568c6302449d6-47204481%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '867a05d893cd1836940daf3b8a7dcbde604b4373' => 
    array (
      0 => '../framework/modules/view/K13ViewCadastro.tpl',
      1 => 1382280896,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '115505568c6302449d6-47204481',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php echo $_smarty_tpl->getVariable('doctype')->value;?>

<html <?php echo $_smarty_tpl->getVariable('html_params')->value;?>
>
<head>
	<title><?php echo $_smarty_tpl->getVariable('titulo')->value;?>
</title>
	
<?php echo $_smarty_tpl->getVariable('metatags')->value;?>

	<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/header_libs.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<?php echo $_smarty_tpl->getVariable('css')->value;?>

<?php echo $_smarty_tpl->getVariable('js')->value;?>

<?php echo $_smarty_tpl->getVariable('htmlHead')->value;?>

</head>
<body>
<script>
	//Cria a mensagem "Carregando" enquanto carrega a pagina - a mensagem sera apagada pelo script especifico ao carregar o evento
	var $box_modal_inicializing = create_modal_box('Carregando');
	setTimeout(function(){
		try{
			//Verifica se ha algum modal informando que esta carregando
			if($box_modal_inicializing){
				destroy_modal_box($box_modal_inicializing);
			}
		}
		catch(e){
			destroy_modal_box($box_modal_inicializing);
		}
	},20000);
</script>

<?php echo $_smarty_tpl->getVariable('system_errors')->value;?>

	<div class="page_box" id="page-cadastro">
		<?php if ($_smarty_tpl->getVariable('erro_permissao')->value==false&&$_smarty_tpl->getVariable('show_title_box')->value==true){?>
		<div class="title_box">
			<?php if ($_smarty_tpl->getVariable('entity_icone')->value!=''){?><span id="page_ico" class="entity_ico" style="background-image:url('<?php echo $_smarty_tpl->getVariable('entity_icone')->value;?>
')"></span>
			<?php }else{ ?><span id="page_ico" class="entity_ico"></span>
			<?php }?>
			<h1><?php echo $_smarty_tpl->getVariable('cad_form_titulo')->value;?>
</h1>
			<span class="title_status"><?php echo $_smarty_tpl->getVariable('cad_status')->value;?>
</span>
			<?php if ($_smarty_tpl->getVariable('show_breadcrumb')->value==true){?><breadcrumb><?php echo $_smarty_tpl->getVariable('breadcrumb')->value;?>
</breadcrumb><?php }?>
			<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/K13PageAjuda.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
		</div>
		<?php }?>
			<div class="form_wrapper">
				<?php if (count($_smarty_tpl->getVariable('cad_ToolBar')->value)>0){?>
				<menu class="tool_box" id="tool_box" <?php if ($_smarty_tpl->getVariable('show_tool_box')->value==false){?>style="display: none;"<?php }?>>
					<?php  $_smarty_tpl->tpl_vars['toolbar'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('cad_ToolBar')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['toolbar']->key => $_smarty_tpl->tpl_vars['toolbar']->value){
?>
						<?php echo $_smarty_tpl->tpl_vars['toolbar']->value;?>

					<?php }} ?>
				</menu>
				<?php }?>

				<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/msg_system.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
				
				<?php if (count($_smarty_tpl->getVariable('form_errorMessage')->value)>0&&false){?>
				<script>
				$(function() {
					var $form_det = K13FWCadastro.formMestre.get_form();
					var erros = [];
					<?php  $_smarty_tpl->tpl_vars['errmsg'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('form_errorMessage')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['errmsg']->key => $_smarty_tpl->tpl_vars['errmsg']->value){
?>
						erros.push('<?php echo $_smarty_tpl->tpl_vars['errmsg']->value;?>
');
					<?php }} ?>
					msgbox_show_errors(erros, $form_det);
				});
				</script>
				<?php }?>


				<?php if ($_smarty_tpl->getVariable('erro_permissao')->value!=true){?>
				
				<?php if ($_smarty_tpl->getVariable('page_description')->value!=''){?>
				<!-- Descricao da pagina -->
				<div class="page-description ui-widget" rel="<?php echo $_smarty_tpl->getVariable('tab')->value['id'];?>
">
					<div class="ui-state-highlight ui-corner-all"> 
						<p><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
						<?php echo $_smarty_tpl->getVariable('page_description')->value;?>
</p>
					</div>
				</div>
				<?php }?>

				<div class="form_box">
					<?php if (count($_smarty_tpl->getVariable('tabs')->value)>0){?>

					<div id="tabs">
						<!-- Abas -->
						<ul class="tab-list">
							<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('tabs')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value){
?>
							<li rel="tab_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" data-tab-id="<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" data-disable_form_action_bar="<?php if ($_smarty_tpl->tpl_vars['tab']->value['disable_form_action_bar']){?>1<?php }else{ ?>0<?php }?>"><a href="#tab_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['tab']->value['label'];?>
</a></li>
							<?php }} ?>

							<!-- Navegaçao nas abas -->
							<div id="form-nav-tabs">
								<a href="#" id="form-tab-prev" title="Aba Anterior (Ctrl + Esquerda)">voltar</a>
								<a href="#" id="form-tab-next" title="Próxima Aba (Ctrl + Direita)">avançar</a>
							</div>
						</ul>
						
						<!-- Conteudo das abas -->
						<?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('tabs')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value){
?>
						<div id="tab_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" rel="tab_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
">
							<?php if ($_smarty_tpl->tpl_vars['tab']->value['description']!=''){?>
							<!-- Descricao da aba -->
							<div class="tab-description ui-widget" rel="<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
">
								<div style="margin-bottom: 5px;padding: 0 .7em;" class="ui-state-highlight ui-corner-all"> 
									<p><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
									<?php echo $_smarty_tpl->tpl_vars['tab']->value['description'];?>
</p>
								</div>
							</div>
							<?php }?>

							<?php if ($_smarty_tpl->tpl_vars['tab']->value['url']==''){?>
								<?php if ($_smarty_tpl->tpl_vars['tab']->value['default']==true){?><form action="<?php echo $_smarty_tpl->getVariable('form_file_action')->value;?>
" method="post" class="formulario" id="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" name="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" enctype="multipart/form-data" accept-charset="utf-8">
								<?php }else{ ?><div class="form-tab-panel"><?php }?>
									<?php echo $_smarty_tpl->tpl_vars['tab']->value['html'];?>

								<?php if ($_smarty_tpl->tpl_vars['tab']->value['default']==true){?></form>
								<?php }else{ ?></div><?php }?>
							<?php }else{ ?>
								<iframe name="frame_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" id="frame_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
" class="form_tab_subframe" src="<?php echo $_smarty_tpl->tpl_vars['tab']->value['url'];?>
" border="0" width="100%" onload="set_config_tab_iframe('#frame_<?php echo $_smarty_tpl->tpl_vars['tab']->value['id'];?>
');"></iframe>
							<?php }?>
						</div>
						<?php }} ?>
					</div><!-- fim tabs -->

					<input type="hidden" name="action" value="<?php echo $_smarty_tpl->getVariable('cad_action')->value;?>
" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" />
					<input type="hidden" name="after_process" value="<?php echo $_smarty_tpl->getVariable('statusAfterProcess')->value;?>
" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" />

					<?php if ($_smarty_tpl->getVariable('show_action_box')->value==true){?>
					<div class="tool_box form_action_bar" <?php if ($_smarty_tpl->getVariable('show_action_box')->value==false){?>style="display: none;"<?php }?>>
						
						<button type="submit" value="Salvar" id="bt_submit_action" title="Salvar Registro" class="cad-button-salvar" role="button" aria-disabled="false" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" <?php if ($_smarty_tpl->getVariable('show_bt_salvar')->value==false){?>style="display: none;"<?php }?>><span class="cad-button-icon-salvar"></span><span class="cad-button-text">Salvar</span></button>
						
						<a href="<?php echo $_smarty_tpl->getVariable('url_consulta')->value;?>
" id="bt_cancel_action" class="cancelar" title="Cancelar Alterações" <?php if ($_smarty_tpl->getVariable('show_bt_cancelar')->value==false){?>style="display: none;"<?php }?>>Cancelar</a>
						<div class="clear"></div>
					</div>
					<?php }?>

					<?php }else{ ?>
					<form action="<?php echo $_smarty_tpl->getVariable('form_file_action')->value;?>
" method="post" class="formulario" id="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" name="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" enctype="multipart/form-data" accept-charset="utf-8">
						<?php echo $_smarty_tpl->getVariable('cad_form')->value;?>

						<input type="hidden" name="action" value="<?php echo $_smarty_tpl->getVariable('cad_action')->value;?>
" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" />
						<input type="hidden" name="after_process" value="<?php echo $_smarty_tpl->getVariable('statusAfterProcess')->value;?>
" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" />

						<?php if ($_smarty_tpl->getVariable('show_action_box')->value==true){?>
						<div class="tool_box form_action_bar" <?php if ($_smarty_tpl->getVariable('show_action_box')->value==false){?>style="display: none;"<?php }?>>
							<button type="submit" value="Salvar" id="bt_submit_action" title="Salvar Registro" class="cad-button-salvar" role="button" aria-disabled="false" form="<?php echo $_smarty_tpl->getVariable('form_id')->value;?>
" <?php if ($_smarty_tpl->getVariable('show_bt_salvar')->value==false){?>style="display: none;"<?php }?>><span class="cad-button-icon-salvar"></span><span class="cad-button-text">Salvar</span></button>
							
							<a href="<?php echo $_smarty_tpl->getVariable('url_consulta')->value;?>
" id="bt_cancel_action" class="cancelar" title="Cancelar Alterações" <?php if ($_smarty_tpl->getVariable('show_bt_cancelar')->value==false){?>style="display: none;"<?php }?>>Cancelar</a>
							<div class="clear"></div>
						</div>
						<?php }?>

					</form>
					<?php }?>
				</div><!-- Fim form_box -->
				<?php }?>

			</div><!-- Fim form_wrapper -->
	</div><!-- Fim page_box -->

<?php echo $_smarty_tpl->getVariable('htmlBody')->value;?>

</body>
</html>
