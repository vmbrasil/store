<?php /* Smarty version Smarty-3.0.6, created on 2015-05-29 17:45:02
         compiled from "../framework/modules/view/K13ViewFileExplorer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:270485568cfcece9126-10504130%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd91590ec9e7ad961b21555c26bf05acd03270a71' => 
    array (
      0 => '../framework/modules/view/K13ViewFileExplorer.tpl',
      1 => 1353011921,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '270485568cfcece9126-10504130',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php echo $_smarty_tpl->getVariable('doctype')->value;?>

<html <?php echo $_smarty_tpl->getVariable('html_params')->value;?>
>
<head>
	<title><?php echo $_smarty_tpl->getVariable('titulo')->value;?>
</title>
<?php echo $_smarty_tpl->getVariable('metatags')->value;?>


	<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/header_libs.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/jstree/jquery.tree.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('k13UriDir')->value;?>
terceiros/plugins/fileuploader.js"></script>

<?php echo $_smarty_tpl->getVariable('css')->value;?>

<?php echo $_smarty_tpl->getVariable('js')->value;?>

<?php echo $_smarty_tpl->getVariable('htmlHead')->value;?>


</head>
<body>
<?php echo $_smarty_tpl->getVariable('system_errors')->value;?>

	<input type="hidden" name="token" id="token" value="<?php echo $_smarty_tpl->getVariable('token')->value;?>
" style="display:none">
	
	<div class="page_box">
		<div id="container">
			<div id="menu">
				<a id="upload" class="button" title="Enviar novo Arquivo">Novo Arquivo</a>
				<a id="create" class="button" title="Criar nova pasta">Nova Pasta</a>
				<a id="delete" class="button" title="Deletar arquivo/pasta">Deletar</a>
				<a id="move" class="button" title="Mover arquivo">Mover</a>
				<a id="rename" class="button" title="Renomear arquivo/pasta">Renomear</a>
				<a id="refresh" class="button" title="Atualizar listagem de arquivos">Atualizar</a>
				
				<form class="form_search">
					<input type="text" name="file_search" id="file_search" required="required" title="Digite o nome do arquivo para busca" placeholder="Busca de arquivo">
					<button id="bt_search" title="Buscar Arquivo">OK</button>
				</form>
			</div>
		
			<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/msg_system.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

			<div class="block">
				<div id="column_left"></div>
				<div id="column_right"></div>
				<div class="clear"></div>
			</div>
			
			<div id="selection_box">
				<h3>Arquivos Selecionados:</h3>
				<button id="bt_close_selection_box">X</button>
				<form>
					<?php echo $_smarty_tpl->getVariable('input_selected_itens')->value;?>

					<button id="bt_confirm_selection_box" type="button" title="Clique para finalizar a seleção de arquivos e fechar a janela">Concluir</button>
				</form>
			</div><!-- Fim selection_box -->
			
		</div><!-- Fim container -->

	</div><!-- Fim page_box -->

<?php echo $_smarty_tpl->getVariable('htmlBody')->value;?>

</body>
</html>
