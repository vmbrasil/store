<?php /* Smarty version Smarty-3.0.6, created on 2015-05-26 13:31:56
         compiled from "../../framework_k13_v1.0/modules/interface/view/tpl/head.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2963155649ffc3b4e07-73338940%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '428bbeb31befa82dd069a7c0cc8362f93c57dee6' => 
    array (
      0 => '../../framework_k13_v1.0/modules/interface/view/tpl/head.tpl',
      1 => 1374073497,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2963155649ffc3b4e07-73338940',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title><?php echo (($tmp = @$_smarty_tpl->getVariable('titulo')->value)===null||$tmp==='' ? '' : $tmp);?>
</title>
	<link rel="shortcut icon" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/images/favicon.ico" />
	
	<!-- Framework header_libs -->
<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('k13fwFullDir')->value).("modules/view/header_libs.tpl"), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
	
	<!--css do sistema-->
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo (($tmp = @$_smarty_tpl->getVariable('k13SysSkinTemplate')->value)===null||$tmp==='' ? 'default' : $tmp);?>
/css/system.css" />
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo (($tmp = @$_smarty_tpl->getVariable('k13SysSkinTemplate')->value)===null||$tmp==='' ? 'default' : $tmp);?>
/css/estilo.css" />
	<!-- <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo (($tmp = @$_smarty_tpl->getVariable('k13SysSkinTemplate')->value)===null||$tmp==='' ? 'default' : $tmp);?>
/css/menu_sistema.css" /> -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_smarty_tpl->getVariable('HOST')->value;?>
view/skins/<?php echo (($tmp = @$_smarty_tpl->getVariable('k13SysSkinTemplate')->value)===null||$tmp==='' ? 'default' : $tmp);?>
/css/frameworkStyle.css" />


<?php $_template = new Smarty_Internal_Template("modules/system/view/system_script.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

<script type="text/javascript">
	function config_iframe(){
		$('#frame_central').each(function(){
			//Redimenciona conteudo
			$(this).height($(this).contents().height()+100);
			//Remove bg
			$(this).contents().find('body').css('background','transparent');
			//alert($(this).attr('src'));
		});
	}
</script>

	<!-- Plugins dinamicos -->
	<?php echo (($tmp = @$_smarty_tpl->getVariable('metatags')->value)===null||$tmp==='' ? '' : $tmp);?>

	<?php echo (($tmp = @$_smarty_tpl->getVariable('css')->value)===null||$tmp==='' ? '' : $tmp);?>

	<?php echo (($tmp = @$_smarty_tpl->getVariable('js')->value)===null||$tmp==='' ? '' : $tmp);?>

	<?php echo (($tmp = @$_smarty_tpl->getVariable('htmlHead')->value)===null||$tmp==='' ? '' : $tmp);?>

	
	<!-- <style type="text/css"></style> -->

</head>
